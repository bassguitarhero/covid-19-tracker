import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Grid} from '@material-ui/core';

import { Provider } from 'react-redux';

import store from './store';

import Compare from './components/Compare';
import Detail from './components/Detail';
import About from './components/About';

import NavBar from './components/NavBar';
import Footer from './components/Footer';

class App extends Component {
	constructor() {
		super();
		this.state = {
			displayCompare: true,
			displayDetail: false,
			displayAbout: false, 
			tabDisplayValue: 0
		}
	}

	setDisplayCompare = () => {
		this.setState({
			displayCompare: true,
			displayDetail: false,
			displayAbout: false, 
			tabDisplayValue: 0
		});
	}

	setDisplayDetail = () => {
		this.setState({
			displayCompare: false,
			displayDetail: true,
			displayAbout: false, 
			tabDisplayValue: 1
		});
	}

	setDisplayAbout = () => {
		this.setState({
			displayCompare: false,
			displayDetail: false,
			displayAbout: true, 
			tabDisplayValue: 2
		});
	}

  render() {
  	const { displayCompare, displayDetail, displayAbout, tabDisplayValue} = this.state;

    return (
      <Provider store={store}>
        <div className="App">
        	<NavBar displayCompare={this.setDisplayCompare} displayDetail={this.setDisplayDetail} displayAbout={this.setDisplayAbout} />
        	<Grid container style={styles.dataContainer}>
				  	<Grid md={1} item />
				    <Grid md={10} item xs>
		        	{displayCompare && 
		        		<Compare />
		        	}
		        	{displayDetail && 
		        		<Detail />
		        	}
		        	{displayAbout && 
		        		<About />
		        	}
				    </Grid>
					</Grid>
					<Footer tabDisplayValue={tabDisplayValue} displayCompare={this.setDisplayCompare} displayDetail={this.setDisplayDetail} displayAbout={this.setDisplayAbout}/>
        </div>
      </Provider>
    );
  } 
}

const styles = {
	dataContainer: {
		marginBottom: 100,
		paddingLeft: 10,
		paddingRight: 10
	},
	stickToBottom: {
    width: '100%',
    position: 'fixed',
    bottom: 0,
  }
}
// Comment this out in order to export for the web
export default App;

// Comment this out in order to view locally
// ReactDOM.render(<App />, document.getElementById('app'));










