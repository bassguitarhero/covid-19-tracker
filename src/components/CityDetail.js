import React, { Component } from 'react';
import SF from './cities/SF';
import NYC from './cities/NYC';
import CHI from './cities/CHI';

class CityDetail extends Component {
	constructor() {
		super();
		this.state = {

		}
	}

	render() {
		const { location } = this.props;

		return (
			<div>
				{(location === 'San Francisco') &&
					<SF location={location} />
				}
				{(location === 'New York City') &&
					<NYC location={location} />
				}
				{(location === 'Chicago') &&
					<CHI location={location} />
				}
			</div>
		);
	}
}

export default CityDetail;