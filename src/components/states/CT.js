import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';

import SettingsModal from '../SettingsModal';
import ModalBackdrop from '../ModalBackdrop';
import SettingsModalIcon from '../SettingsModalIcon';

import { getCaseDataCTCounty, clearCaseDataCTCounty } from '../../actions/counties';
import { getColorsListCountiesCT, getCountiesCTListCT, clearImportCountiesCT } from '../../actions/importData';

class Connecticut extends Component {
	constructor(props) {
		super(props);
		this.state = {
			colorsList: [],
			countiesConnecticut: [],
			selectedCountiesList: [],

			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showCounties: false, 
			showChart: false,
			showTotalCasesChart: true,
			showPercentCasesChart: false,
			showDailyCasesChart: false, 
			showTotalDeathsChart: false, 
			showPercentDeathsChart: false,
			showDailyDeathsChart: false,

			//
			showTotalChart: true, 
			showPercentChart: false,
			showDailyChart: false,
			showCasesChart: true,
			showDeathsChart: false,
			showHospitalizationsChart: false,

			// Line
			totalCasesChartLine: {},
			percentCasesChartLine: {},
			dailyCasesChartLine: {},
			totalDeathsChartLine: {},
			percentDeathsChartLine: {},
			dailyDeathsChartLine: {},
			hospitalBedsChartLine: {},
			sevenDayAverageChartLine: {},

			// Bar
			totalCasesChartBar: {},
			percentCasesChartBar: {},
			dailyCasesChartBar: {},
			totalDeathsChartBar: {},
			percentDeathsChartBar: {},
			dailyDeathsChartBar: {},
			hospitalBedsChartBar: {},
			sevenDayAverageChartBar: {},

			todaysDate: '',
			datesArray: [],
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			doNotDisplay: false,
			population: 0,
			showTotalPercentDailyControls: true,
			showTotalChart: true,
			showPercentChart: false,
			showDailyChart: false,
			showCasesChart: true,
			showDeathsChart: false,
			showHospitalChart: false,
			showICUChart: false,
			showCaseData: true,
			showHospitalData: false,
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: true,
			displayDailyAverageButtons: true,
			showSevenDayAverage: true,
			showDailyCount: false,
			triggerChart: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom'
	}

	handleShowSevenDayAverage = () => {
		this.setState({showSevenDayAverage: true, showDailyCount: false});
	}

	handleShowDailyCount = () => {
		this.setState({showSevenDayAverage: false, showDailyCount: true});
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	setCasesChart = () => {
		this.setState({
			showCasesChart: true,
			showDeathsChart: false,
			showHospitalizationsChart: false
		});
	}

	setDeathsChart = () => {
		this.setState({
			showCasesChart: false,
			showDeathsChart: true,
			showHospitalizationsChart: false
		});
	}

	setHospitalizationsChart = () => {
		this.setState({
			showCasesChart: false,
			showDeathsChart: false,
			showHospitalizationsChart: true
		});
	}

	setTotalChart = () => {
		this.setState({
			showTotalChart: true,
			showPercentChart: false,
			showDailyChart: false
		});
	}

	setPercentChart = () => {
		this.setState({
			showTotalChart: false,
			showPercentChart: true,
			showDailyChart: false
		});
	}

	setDailyChart = () => {
		this.setState({
			showTotalChart: false,
			showPercentChart: false,
			showDailyChart: true
		});
	}

	formatDate = (date) => {
		const fields = date.split('-');
		return fields[1]+'/'+fields[2];
	}

	setChartData = () => {
		const { selectedCountiesList, datesArray } = this.state;
		// console.log('Selected Counties: ', selectedCountiesList);

		var dataTotalCases = [];
		var dataPercentCases = [];
		var dataDailyCases = [];
		var dataTotalDeaths = [];
		var dataPercentDeaths = [];
		var dataDailyDeaths = [];
		var dataSevenDayAverage = [];
		var dataHospitalizations = [];

		var datasetsTotalCases_line = [];
		var datasetsPercentCases_line = [];
		var datasetsDailyCases_line = [];
		var datasetsTotalDeaths_line = [];
		var datasetsPercentDeaths_line = [];
		var datasetsDailyDeaths_line = [];
		var datasetsSevenDayAverage_line = [];
		var datasetsHospitalizations_line = [];

		var datasetsTotalCases_bar = [];
		var datasetsPercentCases_bar = [];
		var datasetsDailyCases_bar = [];
		var datasetsTotalDeaths_bar = [];
		var datasetsPercentDeaths_bar = [];
		var datasetsDailyDeaths_bar = [];
		var datasetsSevenDayAverage_bar = [];
		var datasetsHospitalizations_bar = [];

		var labelDates = [];
		// for (var i = 0; i < datesArray.length; i++) {
		// 	labelDates.push(this.formatDate(datesArray[i]));
		// }
		// console.log('Label Dates: ', labelDates);

		for (var i = 0; i < selectedCountiesList.length; i++) {
			for (var j = 0; j < selectedCountiesList[i].data.length; j++) {
				labelDates.push(this.formatDate(selectedCountiesList[i].data[j].date));
				dataTotalCases.push(selectedCountiesList[i].data[j].total_cases);
				dataPercentCases.push(selectedCountiesList[i].data[j].percent_cases);
				dataDailyCases.push(selectedCountiesList[i].data[j].daily_cases);
				dataTotalDeaths.push(selectedCountiesList[i].data[j].total_deaths);
				dataPercentDeaths.push(selectedCountiesList[i].data[j].percent_deaths);
				dataDailyDeaths.push(selectedCountiesList[i].data[j].daily_deaths);
				dataSevenDayAverage.push(selectedCountiesList[i].data[j].sevenDayAverage);
				dataHospitalizations.push(selectedCountiesList[i].data[j].hospitalizations);
			}

			datasetsTotalCases_line.push({
				label: selectedCountiesList[i].name,
				data: dataTotalCases,
				fill: false,
				borderColor: selectedCountiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentCases_line.push({
				label: selectedCountiesList[i].name,
				data: dataPercentCases,
				fill: false,
				borderColor: selectedCountiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyCases_line.push({
				label: selectedCountiesList[i].name,
				data: dataDailyCases,
				fill: false,
				borderColor: selectedCountiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsTotalDeaths_line.push({
				label: selectedCountiesList[i].name,
				data: dataTotalDeaths,
				fill: false,
				borderColor: selectedCountiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentDeaths_line.push({
				label: selectedCountiesList[i].name,
				data: dataPercentDeaths,
				fill: false,
				borderColor: selectedCountiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyDeaths_line.push({
				label: selectedCountiesList[i].name,
				data: dataDailyDeaths,
				fill: false,
				borderColor: selectedCountiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverage_line.push({
				label: selectedCountiesList[i].name,
				data: dataSevenDayAverage,
				fill: false,
				borderColor: selectedCountiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsHospitalizations_line.push({
				label: selectedCountiesList[i].name,
				data: dataHospitalizations,
				fill: false,
				borderColor: selectedCountiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});

			datasetsTotalCases_bar.push({
				label: selectedCountiesList[i].name,
				data: dataTotalCases,
				backgroundColor: selectedCountiesList[i].color
			});
			datasetsPercentCases_bar.push({
				label: selectedCountiesList[i].name,
				data: dataPercentCases,
				backgroundColor: selectedCountiesList[i].color
			});
			datasetsDailyCases_bar.push({
				label: selectedCountiesList[i].name,
				data: dataDailyCases,
				backgroundColor: selectedCountiesList[i].color
			});
			datasetsTotalDeaths_bar.push({
				label: selectedCountiesList[i].name,
				data: dataTotalDeaths,
				backgroundColor: selectedCountiesList[i].color
			});
			datasetsPercentDeaths_bar.push({
				label: selectedCountiesList[i].name,
				data: dataPercentDeaths,
				backgroundColor: selectedCountiesList[i].color
			});
			datasetsDailyDeaths_bar.push({
				label: selectedCountiesList[i].name,
				data: dataDailyDeaths,
				backgroundColor: selectedCountiesList[i].color
			});
			datasetsSevenDayAverage_bar.push({
				label: selectedCountiesList[i].name,
				data: dataSevenDayAverage,
				backgroundColor: selectedCountiesList[i].color
			});
			datasetsHospitalizations_bar.push({
				label: selectedCountiesList[i].name,
				data: dataHospitalizations,
				backgroundColor: selectedCountiesList[i].color
			});

			dataTotalCases = [];
			dataPercentCases = [];
			dataDailyCases = [];
			dataTotalDeaths = [];
			dataPercentDeaths = [];
			dataDailyDeaths = [];
			dataSevenDayAverage = [];
			dataHospitalizations = [];
		}

		this.setState({
			// Line
			totalCasesChartLine: {
				labels: labelDates,
				datasets: datasetsTotalCases_line
			},
			percentCasesChartLine: {
				labels: labelDates,
				datasets: datasetsPercentCases_line
			},
			dailyCasesChartLine: {
				labels: labelDates,
				datasets: datasetsDailyCases_line
			},
			totalDeathsChartLine: {
				labels: labelDates,
				datasets: datasetsTotalDeaths_line
			},
			percentDeathsChartLine: {
				labels: labelDates,
				datasets: datasetsPercentDeaths_line
			},
			dailyDeathsChartLine: {
				labels: labelDates,
				datasets: datasetsDailyDeaths_line
			},
			hospitalBedsChartLine: {
				labels: labelDates,
				datasets: datasetsHospitalizations_line
			},
			sevenDayAverageChartLine: {
				labels: labelDates,
				datasets: datasetsSevenDayAverage_line
			},

			// Bar
			totalCasesChartBar: {
				labels: labelDates,
				datasets: datasetsTotalCases_bar
			},
			percentCasesChartBar: {
				labels: labelDates,
				datasets: datasetsPercentCases_bar
			},
			dailyCasesChartBar: {
				labels: labelDates,
				datasets: datasetsDailyCases_bar
			},
			totalDeathsChartBar: {
				labels: labelDates,
				datasets: datasetsTotalDeaths_bar
			},
			percentDeathsChartBar: {
				labels: labelDates,
				datasets: datasetsPercentDeaths_bar
			},
			dailyDeathsChartBar: {
				labels: labelDates,
				datasets: datasetsDailyDeaths_bar
			},
			hospitalBedsChartBar: {
				labels: labelDates,
				datasets: datasetsHospitalizations_bar
			},
			sevenDayAverageChartBar: {
				labels: labelDates,
				datasets: datasetsSevenDayAverage_bar
			},

			showChart: true
		})
	}

	handleSelectCountyCT = (county, population) => {
		const { selectedCountiesList, countiesConnecticut } = this.state;
		if (this.selectedCounties.has(county)) {
			this.selectedCounties.delete(county);
			let newSelectedCounties = selectedCountiesList.filter(e => e.name !== county);
			for (var i = 0; i < countiesConnecticut.length; i++) {
				if (countiesConnecticut[i].name == county) {
					countiesConnecticut[i].active = false;
				}
			}
			this.setState({
				selectedCountiesList: newSelectedCounties,
				countiesConnecticut: countiesConnecticut
			});
		} else {
			this.selectedCounties.add(county);
			for (var i = 0; i < countiesConnecticut.length; i++) {
				if (countiesConnecticut[i].name == county) {
					countiesConnecticut[i].active = true;
				}
			}
			this.setState({
				countiesConnecticut: countiesConnecticut,
				population: population
			});
			this.props.getCaseDataCTCounty(county);
		}
	}

	getDatesList = () => {
		var listDate = [];
		var listDateFormatted = [];
		var startDate = '2020-03-01';
		var endDate = this.state.todaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		this.setState({
			datesArray: listDate
		});
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();
		var newDate = date.setDate(date.getDate() -1);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		this.setState({
			todaysDate: today
		});
	}

	setDisplayTotalCases = () => {
		this.setState({
			showTotalCasesChart: true,
			showPercentCasesChart: false,
			showDailyCasesChart: false,
			showTotalDeathsChart: false,
			showPercentDeathsChart: false,
			showDailyDeathsChart: false
		});
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.selectedCounties = new Set();
		this.getTodaysDate();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
		this.props.getColorsListCountiesCT();
		this.props.getCountiesCTListCT();
	}

	componentDidUpdate(lastProps, lastState) {
		// console.log('This: ', this.state.todaysDate);
		// console.log('Last: ', lastState.todaysDate);
		if (this.state.triggerChart === true && lastState.triggerChart === false) {
			this.setChartData();
			this.setState({
				triggerChart:  false
			});
		}

		if (this.props.importColorsListCountiesCT !== null && lastProps.importColorsListCountiesCT === null) {
			const colorsList = [];
			this.props.importColorsListCountiesCT.map(datum => {
				colorsList.push(datum);
			});
			this.setState({colorsList: colorsList});
		}

		if (this.props.loadingCaseDataCountyCT === false && (lastProps.loadingCaseDataCountyCT === true || lastProps.loadingCaseDataCountyCT === undefined)) {
			const { caseDataCountyCT } = this.props;
			const { colorsList, datesArray, countiesConnecticut, selectedCountiesList, population } = this.state;
			// console.log('Case Data County CT: ', caseDataCountyCT);
			// console.log('Dates Array: ', datesArray);
			var data = [];
			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, daily_cases: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, hospitalizations: 0, sevenDayAverage: 0});
			}
			// console.log('Data:  ', data);
			var previous_total_cases = 0;
			var previous_percent_cases = 0;
			var previous_total_deaths = 0;
			var previous_percent_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				caseDataCountyCT.map(datum => {
					if (data[i].date == datum.dateupdated.split('T')[0]) {
						data[i].total_cases = datum.totalcases;
						data[i].percent_cases = (datum.totalcases / population * 100).toFixed(5);
						data[i].daily_cases = datum.totalcases - previous_total_cases;
						previous_total_cases = datum.totalcases;
						previous_percent_cases = (datum.totalcases / population * 100).toFixed(5);
						data[i].total_deaths = datum.totaldeaths;
						data[i].percent_deaths = (datum.totaldeaths / population * 100).toFixed(5);
						data[i].daily_deaths = datum.totaldeaths - previous_total_deaths;
						previous_total_deaths = datum.totaldeaths;
						previous_percent_deaths = (datum.totaldeaths / population * 100).toFixed(5);
						data[i].hospitalizations = datum.hospitalization;
					} else {
						data[i].total_cases = previous_total_cases;
						data[i].percent_cases = previous_percent_cases;
						data[i].total_deaths = previous_total_deaths;
						data[i].percent_deaths = previous_percent_deaths;
					}
				});
			}
			// console.log('Data: ', data);
			var sevenDayCount = 0;
			for (var i = 0; i < data.length; i++) {
				sevenDayCount += parseFloat(data[i].daily_cases);
				if (i < 7) {
					data[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(data[i - 7].daily_cases);
					data[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
				}
			}
			if (data[data.length - 1].total_cases == 0) {
				data[data.length - 1].total_cases = data[data.length - 2].total_cases;
			}
			if (data[data.length - 1].percent_cases == 0) {
				data[data.length - 1].percent_cases = data[data.length - 2].percent_cases;
			}
			var randomNumber = (Math.random() * 56).toFixed();
			const countyData = { name: caseDataCountyCT[0].county, population: population, color: colorsList[randomNumber], data: data }
			selectedCountiesList.push(countyData);
			this.setState({
				selectedCountiesList: selectedCountiesList,
				population: 0,
				triggerChart: true
			});
			this.props.clearCaseDataCTCounty();
		}

		if (this.props.importCountiesCTListCT !== null && (lastProps.importCountiesCTListCT === null || lastProps.importCountiesCTListCT === undefined)) {
			const countiesConnecticut = [];
			this.props.importCountiesCTListCT.map(datum => {
				countiesConnecticut.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			// console.log('countiesConnecticut: ', countiesConnecticut);
			this.setState({
				countiesConnecticut: countiesConnecticut
			});
		}

		if (this.state.todaysDate != '' && lastState.todaysDate == '') {
			this.getDatesList();
		}
	}

	componentWillUnmount() {
		this.props.clearImportCountiesCT();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { countiesConnecticut, showChart, showAxisX } = this.state;
		const { showTotalCasesChart, showPercentCasesChart, showDailyCasesChart, showTotalDeathsChart, showPercentDeathsChart, showDailyDeathsChart } = this.state;
		const { chartHeight, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons } = this.state;
		const { showTotalPercentDailyControls, showTotalChart, showPercentChart, showDailyChart } = this.state;
		const { showCasesChart, showDeathsChart, showHospitalizationsChart } = this.state;
		const { displayDailyAverageButtons, showSevenDayAverage, showDailyCount } = this.state;

		return (
			<div style={{flex: 1}}>
				{this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} showSevenDayAverage={showSevenDayAverage} showDailyCount={showDailyCount} displaySevenDayAverage={this.handleShowSevenDayAverage} displayDailyCount={this.handleShowDailyCount} />
        }
        {showChart ? 
        	<div>
        		{!showHospitalizationsChart && 
        			<Grid container>
        				<Grid item xs={4} style={styles.chartTypeButtonContainer}>
        					<Button style={showTotalChart ? styles.chartTypeButtonSelected : styles.chartTypeButton}  onClick={this.setTotalChart}>Total</Button>
        				</Grid>
        				<Grid item xs={4} style={styles.chartTypeButtonContainer}>
        					<Button style={showPercentChart ? styles.chartTypeButtonSelected : styles.chartTypeButton}  onClick={this.setPercentChart}>Percent</Button>
        				</Grid>
        				<Grid item xs={4} style={styles.chartTypeButtonContainer}>
        					<Button style={showDailyChart ? styles.chartTypeButtonSelected : styles.chartTypeButton}  onClick={this.setDailyChart}>Daily</Button>
        				</Grid>
        			</Grid>
        		}
        		{showCasesChart && 
        			<div>
        				{showTotalChart && 
        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        						{showLineGraph && 
        							<Line 
					          		data={this.state.totalCasesChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Cases by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
        						}
        						{showBarGraph && 
        							<div>

        							</div>
        						}
        					</div>
        				}
        				{showPercentChart && 
        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        						{showLineGraph && 
        							<Line 
					          		data={this.state.percentCasesChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Percent by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
        						}
        						{showBarGraph && 
        							<div>

        							</div>
        						}
        					</div>
        				}
        				{showDailyChart && 
        					<div>
        						{showSevenDayAverage && 
        							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        								{showLineGraph && 
		        							<Line 
							          		data={this.state.sevenDayAverageChartLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: '7 Day Average Cases by County',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                          } else {
					                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                          }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:false, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
		        						}
		        						{showBarGraph && 
		        							<div>

		        							</div>
		        						}
        							</div>
        						}
        						{showDailyCount && 
        							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        								{showLineGraph && 
		        							<Line 
							          		data={this.state.dailyCasesChartLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Daily Cases by County',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                          } else {
					                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                          }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:false, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
		        						}
		        						{showBarGraph && 
		        							<div>

		        							</div>
		        						}
        							</div>
        						}
        					</div>
        				}
        			</div>
        		}
        		{showDeathsChart && 
        			<div>
        				{showTotalChart && 
        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        						{showLineGraph && 
        							<Line 
					          		data={this.state.totalDeathsChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Deaths by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
        						}
        						{showBarGraph && 
        							<div>

        							</div>
        						}
        					</div>
        				}
        				{showPercentChart && 
        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        						{showLineGraph && 
        							<Line 
					          		data={this.state.percentDeathsChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Percent Deaths by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
        						}
        						{showBarGraph && 
        							<div>

        							</div>
        						}
        					</div>
        				}
        				{showDailyChart && 
        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        						{showLineGraph && 
        							<Line 
					          		data={this.state.dailyDeathsChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Daily Deaths by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
        						}
        						{showBarGraph && 
        							<div>

        							</div>
        						}
        					</div>
        				}
        			</div>
        		}
        		{showHospitalizationsChart && 
        			<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        				{showLineGraph && 
        					<Line 
			          		data={this.state.hospitalBedsChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Hospitalizations by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
        				}
        				{showBarGraph && 
        					<Bar 
			          		data={this.state.hospitalBedsChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Hospitalizations by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
        				}
        			</div>
        		}
        		<Grid container>
        			<Grid item xs={4} style={styles.chartTypeButtonContainer}>
        				<Button style={showCasesChart ? styles.chartTypeButtonSelected : styles.chartTypeButton}  onClick={this.setCasesChart}>Cases</Button>
        			</Grid>
        			<Grid item xs={4} style={styles.chartTypeButtonContainer}>
        				<Button style={showDeathsChart ? styles.chartTypeButtonSelected : styles.chartTypeButton}  onClick={this.setDeathsChart}>Deaths</Button>
        			</Grid>
        			<Grid item xs={4} style={styles.chartTypeButtonContainer}>
        				<Button style={showHospitalizationsChart ? styles.chartTypeButtonSelected : styles.chartTypeButton}  onClick={this.setHospitalizationsChart}>Hospital</Button>
        			</Grid>
        		</Grid>
        	</div>  :
        	<div style={styles.selectCounty}>
						<span style={styles.selectCountyText}>Select one or more counties...</span>
					</div>
        }
				{countiesConnecticut.length > 0 && 
					<div style={styles.locationSelectButtonsContainer}>
						{countiesConnecticut.map((county, index) => (
							<div style={styles.locationSelectButtonContainer} key={index}>
								<Button style={county.active ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleSelectCountyCT.bind(this, county.name, county.population)}>{county.name}</Button>
							</div>
						))}
					</div>
				}
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		importColorsListCountiesCT: state.importData.importColorsListCountiesCT, 
		importCountiesCTListCT: state.importData.importCountiesCTListCT, 

		caseDataCountyCT: state.counties.caseDataCountyCT,
		loadingCaseDataCountyCT: state.counties.loadingCaseDataCountyCT,
	}
}

export default connect(mapStateToProps, { getCaseDataCTCounty, clearCaseDataCTCounty, getColorsListCountiesCT, getCountiesCTListCT, clearImportCountiesCT })(Connecticut);

const styles = {
	selectCounty: {
		flex: 1, 
		padding: 20,
		textAlign: 'center',
		// alignSelf: 'center',
		// justifyContent: 'center',
		// alignItems: 'center'
	},
	selectCountyText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	chartTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		padding: 10
	},
	chartTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	chartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	locationSelectButton: {
		backgroundColor: '#FFF',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	locationSelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	locationSelectButtonsContainer: {
		display: 'flex',
		flex: 1, 
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		marginTop: 20
	},
	locationSelectButtonContainer: {
		margin: 5
	}
}










