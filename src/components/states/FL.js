import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getColorsListCountiesFL, getCountiesFLListFL, clearImportCountiesFL } from '../../actions/importData';
import { getFloridaData, clearFloridaData } from '../../actions/counties';

class Florida extends Component {
	constructor(props) {
		super(props);
		this.state = {
			chartColors: [],
			countiesFlorida: []
		}
	}

	componentDidMount() {
		this.props.getColorsListCountiesFL();
		this.props.getCountiesFLListFL();
		this.props.getFloridaData();
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.importColorsListCountiesFL !== null && lastProps.importColorsListCountiesFL === null) {
			const chartColors = [];
			this.props.importColorsListCountiesFL.map(datum => {
				chartColors.push(datum);
			});
			this.setState({
				chartColors: chartColors
			});
			// console.log('Chart Colors: ', chartColors);
		}

		if (this.props.importCountiesFLListFL !== null && lastProps.importCountiesFLListFL === null) {
			const countiesFlorida = [];
			this.props.importCountiesFLListFL.map(datum => {
				countiesFlorida.push({
					county: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesFlorida: countiesFlorida
			});
			// console.log('Counties Florida: ', countiesFlorida);
		}

		if (this.props.loadingDataFlorida === false && lastProps.loadingDataFlorida === true) {
			console.log('Florida Data: ', this.props.dataFlorida);
		}
	}

	componentWillUnmount() {
		this.props.clearFloridaData();
		this.props.clearImportCountiesFL();
	}

	render() {
		return (
			<div>
				Florida
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		importColorsListCountiesFL: state.importData.importColorsListCountiesFL, 
		importCountiesFLListFL: state.importData.importCountiesFLListFL, 

		dataFlorida: state.counties.dataFlorida,
		loadingDataFlorida: state.counties.loadingDataFlorida,
	}
}

export default connect(mapStateToProps, { getColorsListCountiesFL, getCountiesFLListFL, clearImportCountiesFL, getFloridaData, clearFloridaData })(Florida);

const styles = {

}










