import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

import SettingsModal from '../SettingsModal';
import ModalBackdrop from '../ModalBackdrop';
import SettingsModalIcon from '../SettingsModalIcon';

import { getCaseDataNyCounty, clearCaseDataNyCounty } from '../../actions/counties';
import { getColorsListCountiesNY, getCountiesNYListNY, clearImportCountiesNY } from '../../actions/importData';

class NY extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedCountiesList: [],
			chartColors: [],
			countiesNewYork: [],
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showCounties: false, 
			showChart: false,
			showTotalCasesChart: true,
			showPercentCasesChart: false,
			showDailyCasesChart: false, 
			showTotalTestsChart: false, 
			showPercentTestsChart: false,
			showDailyTestsChart: false,

			// Line
			totalCasesChartLine: {},
			percentCasesChartLine: {},
			dailyCasesChartLine: {},
			totalTestsChartLine: {},
			percentTestsChartLine: {},
			dailyTestsChartLine: {},
			sevenDayAverageChartLine: {},

			// Bar
			totalCasesChartBar: {},
			percentCasesChartBar: {},
			dailyCasesChartBar: {},
			totalTestsChartBar: {},
			percentTestsChartBar: {},
			dailyTestsChartBar: {},
			sevenDayAverageChartBar: {},

			todaysDate: '',
			datesArray: [],
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			doNotDisplay: false,
			population: 0,
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: true,
			displayDailyAverageButtons: true,
			showSevenDayAverage: true,
			showDailyCount: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom'
	}

	handleShowSevenDayAverage = () => {
		this.setState({showSevenDayAverage: true, showDailyCount: false});
	}

	handleShowDailyCount = () => {
		this.setState({showSevenDayAverage: false, showDailyCount: true});
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	setChartData = () => {
		const { datesArray, selectedCountiesList } = this.state;
		if (selectedCountiesList.length > 0) {
			const labelDates = [];
			var total_cases = [];
			var percent_cases = [];
			var daily_cases = [];
			var total_tests = [];
			var percent_tests = [];
			var daily_tests = [];
			var seven_day_average = [];

			// Line
			const dataset_total_cases_line = [];
			const dataset_percent_cases_line = [];
			const dataset_daily_cases_line = [];
			const dataset_total_tests_line = [];
			const dataset_percent_tests_line = [];
			const dataset_daily_tests_line = [];
			const dataset_seven_day_average_line = [];

			// Bar
			const dataset_total_cases_bar = [];
			const dataset_percent_cases_bar = [];
			const dataset_daily_cases_bar = [];
			const dataset_total_tests_bar = [];
			const dataset_percent_tests_bar = [];
			const dataset_daily_tests_bar = [];
			const dataset_seven_day_average_bar = [];

			for (var i = 0; i < datesArray.length; i++) {
				var fields = datesArray[i].split('-');
				labelDates.push(fields[1]+'/'+fields[2]);
			}
			for (let county of this.selectedCounties) {
				for (var i = 0; i < selectedCountiesList.length; i++) {
					if (selectedCountiesList[i].name == county) {
						for (var j = 0; j < selectedCountiesList[i].data.length; j++) {
							total_cases.push(selectedCountiesList[i].data[j].total_cases);
							percent_cases.push(selectedCountiesList[i].data[j].percent_cases);
							daily_cases.push(selectedCountiesList[i].data[j].daily_cases);
							total_tests.push(selectedCountiesList[i].data[j].total_tests);
							percent_tests.push(selectedCountiesList[i].data[j].percent_tests);
							daily_tests.push(selectedCountiesList[i].data[j].daily_tests);
							seven_day_average.push(selectedCountiesList[i].data[j].sevenDayAverage);
						}

						// Line
						dataset_total_cases_line.push({
							label: selectedCountiesList[i].name,
							data: total_cases,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_percent_cases_line.push({
							label: selectedCountiesList[i].name,
							data: percent_cases,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_daily_cases_line.push({
							label: selectedCountiesList[i].name,
							data: daily_cases,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_total_tests_line.push({
							label: selectedCountiesList[i].name,
							data: total_tests,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_percent_tests_line.push({
							label: selectedCountiesList[i].name,
							data: percent_tests,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_daily_tests_line.push({
							label: selectedCountiesList[i].name,
							data: daily_tests,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_seven_day_average_line.push({
							label: selectedCountiesList[i].name,
							data: seven_day_average,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});

						// Bar
						dataset_total_cases_bar.push({
							label: selectedCountiesList[i].name,
							data: total_cases,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_percent_cases_bar.push({
							label: selectedCountiesList[i].name,
							data: percent_cases,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_daily_cases_bar.push({
							label: selectedCountiesList[i].name,
							data: daily_cases,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_total_tests_bar.push({
							label: selectedCountiesList[i].name,
							data: total_tests,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_percent_tests_bar.push({
							label: selectedCountiesList[i].name,
							data: percent_tests,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_daily_tests_bar.push({
							label: selectedCountiesList[i].name,
							data: daily_tests,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_seven_day_average_bar.push({
							label: selectedCountiesList[i].name,
							data: seven_day_average,
							backgroundColor: selectedCountiesList[i].color
						});

						total_cases = [];
						percent_cases = [];
						daily_cases = [];
						total_tests = [];
						percent_tests = [];
						daily_tests = [];
						seven_day_average = [];
					}
				}
			}
			this.setState({
				// Line
				totalCasesChartLine: {
					labels: labelDates,
					datasets: dataset_total_cases_line
				},
				percentCasesChartLine: {
					labels: labelDates,
					datasets: dataset_percent_cases_line
				},
				dailyCasesChartLine: {
					labels: labelDates,
					datasets: dataset_daily_cases_line
				},
				totalTestsChartLine: {
					labels: labelDates,
					datasets: dataset_total_tests_line
				},
				percentTestsChartLine: {
					labels: labelDates,
					datasets: dataset_percent_tests_line
				},
				dailyTestsChartLine: {
					labels: labelDates,
					datasets: dataset_daily_tests_line
				},
				sevenDayAverageChartLine: {
					labels: labelDates,
					datasets: dataset_seven_day_average_line
				},

				// Bar
				totalCasesChartBar: {
					labels: labelDates,
					datasets: dataset_total_cases_bar
				},
				percentCasesChartBar: {
					labels: labelDates,
					datasets: dataset_percent_cases_bar
				},
				dailyCasesChartBar: {
					labels: labelDates,
					datasets: dataset_daily_cases_bar
				},
				totalTestsChartBar: {
					labels: labelDates,
					datasets: dataset_total_tests_bar
				},
				percentTestsChartBar: {
					labels: labelDates,
					datasets: dataset_percent_tests_bar
				},
				dailyTestsChartBar: {
					labels: labelDates,
					datasets: dataset_daily_tests_bar
				},
				sevenDayAverageChartBar: {
					labels: labelDates,
					datasets: dataset_seven_day_average_bar
				},

				showChart: true
			})
		} else {
			this.setState({showChart: false});
		}
	}

	handleSelectCounty = (county, population) => {
		const { countiesNewYork } = this.state;
		const { selectedCountiesList } = this.state;
		let { showChart } = this.state;
		if (this.selectedCounties.has(county)) {
			this.selectedCounties.delete(county);
			let newSelectedCountiesList = selectedCountiesList.filter(e => e.name !== county);
			for (var i = 0; i < countiesNewYork.length; i++) {
				if (countiesNewYork[i].county == county) {
					countiesNewYork[i].active = false;
				}
			}
			this.setState({
				countiesNewYork: countiesNewYork,
				selectedCountiesList: newSelectedCountiesList,
				showChart: showChart
			});
		} else {
			this.selectedCounties.add(county);
			for (var i = 0; i < countiesNewYork.length; i++) {
				if (countiesNewYork[i].county == county) {
					countiesNewYork[i].active = true;
				}
			}
			this.props.getCaseDataNyCounty(county);
			this.setState({countiesNewYork: countiesNewYork, population: population});
		}
	}

	getDatesList = () => {
		var listDate = [];
		var listDateFormatted = [];
		var startDate = '2020-03-01';
		var endDate = this.state.todaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		this.setState({
			datesArray: listDate
		});
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();
		var newDate = date.setDate(date.getDate() -1);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		this.setState({
			todaysDate: today
		});
	}

	setDisplayTotalCases = () => {
		this.setState({
			showTotalCasesChart: true,
			showPercentCasesChart: false,
			showDailyCasesChart: false,
			showTotalTestsChart: false,
			showPercentTestsChart: false,
			showDailyTestsChart: false
		});
	}

	setDisplayPercentCases = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: true,
			showDailyCasesChart: false,
			showTotalTestsChart: false,
			showPercentTestsChart: false,
			showDailyTestsChart: false
		});
	}

	setDisplayDailyCases = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: true,
			showTotalTestsChart: false,
			showPercentTestsChart: false,
			showDailyTestsChart: false
		});
	}

	setDisplayTotalTests = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: false,
			showTotalTestsChart: true,
			showPercentTestsChart: false,
			showDailyTestsChart: false
		});
	}

	setDisplayPercentTests = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: false,
			showTotalTestsChart: false,
			showPercentTestsChart: true,
			showDailyTestsChart: false
		});
	}

	setDisplayDailyTests = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: false,
			showTotalTestsChart: false,
			showPercentTestsChart: false,
			showDailyTestsChart: true
		});
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.selectedCounties = new Set();
		this.getTodaysDate();
		this.updateWindowDimensions();
		this.props.getColorsListCountiesNY();
		this.props.getCountiesNYListNY();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps, lastState) {
		// console.log('This Colors List: ', this.props.importColorsList);
		// console.log('Last Colors List: ', lastProps.importColorsList);
		if (this.props.importColorsListCountiesNY !== null && lastProps.importColorsListCountiesNY === null) {
			const chartColors = [];
			// console.log('Colors: ', this.props.importColorsList);
			this.props.importColorsListCountiesNY.map(datum => {
				chartColors.push(datum);
			})
			// console.log('Chart Colors: ', chartColors);
			this.setState({
				chartColors: chartColors
			});
		}

		if (this.props.importCountiesNYListNY !== null && lastProps.importCountiesNYListNY === null) {
			// console.log('Counties NY: ', this.props.importCountiesNYList);
			const countiesNewYork = [];
			this.props.importCountiesNYListNY.map(datum => {
				countiesNewYork.push({
					county: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesNewYork: countiesNewYork
			});
			// console.log('Counties NY 2: ', countiesNewYork);
			// this.props.getColorsList();
		}

		if (this.props.loadingCaseDataCountyNy === false && lastProps.loadingCaseDataCountyNy === true) {
			const { caseDataCountyNy } = this.props;
			// console.log('Case Data County NY: ', caseDataCountyNy);
			const { selectedCountiesList, datesArray, chartColors, population } = this.state;
			// console.log('Chart Colors: ', chartColors);
			var randomNumber = (Math.random() * 56).toFixed();
			const countyData = {name: caseDataCountyNy[0].county, color: chartColors[randomNumber], data: []};
			for (var i = 0; i < datesArray.length; i++) {
				countyData.data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, daily_cases: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, sevenDayAverage: 0});
			}
			for (var i = 0; i < countyData.data.length; i++) {
				caseDataCountyNy.map(datum => {
					if (countyData.data[i].date == datum.test_date.split('T')[0]) {
						countyData.data[i].total_cases = datum.cumulative_number_of_positives;
						countyData.data[i].percent_cases = (datum.cumulative_number_of_positives / population * 100).toFixed(2);
						countyData.data[i].daily_cases = datum.new_positives;
						countyData.data[i].total_tests = datum.cumulative_number_of_tests;
						countyData.data[i].percent_tests = (datum.cumulative_number_of_tests / population * 100).toFixed(2);
						countyData.data[i].daily_tests = datum.total_number_of_tests;
					}
				});
			}
			var sevenDayCount = 0;
			for (var i = 0; i < countyData.data.length; i++) {
				sevenDayCount += parseFloat(countyData.data[i].daily_cases);
				if (i < 7) {
					countyData.data[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(countyData.data[i - 7].daily_cases);
					countyData.data[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
				}
			}
			if (countyData.data[countyData.data.length - 1].total_cases == 0) {
				countyData.data[countyData.data.length - 1].total_cases = countyData.data[countyData.data.length - 2].total_cases;
			}
			if (countyData.data[countyData.data.length - 1].percent_cases == 0) {
				countyData.data[countyData.data.length - 1].percent_cases = countyData.data[countyData.data.length - 2].percent_cases;
			}
			if (countyData.data[countyData.data.length - 1].total_tests == 0) {
				countyData.data[countyData.data.length - 1].total_tests = countyData.data[countyData.data.length - 2].total_tests;
			}
			if (countyData.data[countyData.data.length - 1].percent_tests == 0) {
				countyData.data[countyData.data.length - 1].percent_tests = countyData.data[countyData.data.length - 2].percent_tests;
			}
			selectedCountiesList.push(countyData);
			this.setState({selectedCountiesList: selectedCountiesList, population: 0});
			this.props.clearCaseDataNyCounty();
			this.setChartData();
		}
		if (this.state.todaysDate !== '' && lastState.todaysDate === '') {
			this.getDatesList();
		}
		if (this.state.selectedCountiesList !== lastState.selectedCountiesList) {
			this.setChartData();
		}
	}

	componentWillUnmount() {
		this.props.clearImportCountiesNY();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { countiesNewYork, showChart, showAxisX } = this.state;
		const { showTotalCasesChart, showPercentCasesChart, showDailyCasesChart, showTotalTestsChart, showPercentTestsChart, showDailyTestsChart } = this.state;
		const { chartHeight, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons } = this.state;
		const { displayDailyAverageButtons, showSevenDayAverage, showDailyCount } = this.state;

		return (
			<div>
				{this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} showSevenDayAverage={showSevenDayAverage} showDailyCount={showDailyCount} displaySevenDayAverage={this.handleShowSevenDayAverage} displayDailyCount={this.handleShowDailyCount} />
        }
				{showChart ? 
					<div>
						<div>
							<Grid container>
								<Grid item xs={4} style={styles.chartTypeButtonContainer}>
									<Button style={showTotalCasesChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayTotalCases}>Total Cases</Button>
								</Grid>
								<Grid item xs={4} style={styles.chartTypeButtonContainer}>
									<Button style={showPercentCasesChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayPercentCases}>Percent Cases</Button>
								</Grid>
								<Grid item xs={4} style={styles.chartTypeButtonContainer}>
									<Button style={showDailyCasesChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayDailyCases}>Daily Cases</Button>
								</Grid>
								<Grid item xs={4} style={styles.chartTypeButtonContainer}>
									<Button style={showTotalTestsChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayTotalTests}>Total Tests</Button>
								</Grid>
								<Grid item xs={4} style={styles.chartTypeButtonContainer}>
									<Button style={showPercentTestsChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayPercentTests}>Percent Tests</Button>
								</Grid>
								<Grid item xs={4} style={styles.chartTypeButtonContainer}>
									<Button style={showDailyTestsChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayDailyTests}>Daily Tests</Button>
								</Grid>
							</Grid>
						</div>
						{showTotalCasesChart && 
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								{showLineGraph && 
									<Line 
			          		data={this.state.totalCasesChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Total Cases by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
			          }
			          {showBarGraph && 
			          	<Bar 
			          		data={this.state.totalCasesChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Total Cases by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
			          }
		          </div>
						}
						{showPercentCasesChart && 
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								{showLineGraph && 
									<Line 
			          		data={this.state.percentCasesChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Percent Cases by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
			          }
			          {showBarGraph && 
			          	<Bar 
			          		data={this.state.percentCasesChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Percent Cases by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
			          }
		          </div>
						}
						{showDailyCasesChart  && 
							<div>
								{showSevenDayAverage && 
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										{showLineGraph && 
											<Line 
					          		data={this.state.sevenDayAverageChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Seven Day Average per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
										}
										{showBarGraph && 
											<Bar 
					          		data={this.state.sevenDayAverageChartBar}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Seven Day Average per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
			                		maintainAspectRatio: false
					              }}
					          	/>
					          }
									</div>
								}
								{showDailyCount && 
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										{showLineGraph && 
											<Line 
					          		data={this.state.dailyCasesChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Daily Cases per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
										}
										{showLineGraph && 
											<Bar 
					          		data={this.state.dailyCasesChartBar}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Daily Cases per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
			                		maintainAspectRatio: false
					              }}
					          	/>
					          }
									</div>
								}
							</div>
						}
						{showTotalTestsChart && 
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								{showLineGraph && 
									<Line 
			          		data={this.state.totalTestsChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Total Tests by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
			          }
			          {showBarGraph && 
			          	<Bar 
			          		data={this.state.totalTestsChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Total Tests by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
			          }
		          </div>
						}
						{showPercentTestsChart && 
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								{showLineGraph && 
									<Line 
			          		data={this.state.percentTestsChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Percent Tests by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
			          }
			          {showBarGraph && 
			          	<Bar 
			          		data={this.state.percentTestsChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Percent Tests by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
			          }
		          </div>
						}
						{showDailyTestsChart && 
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								{showLineGraph && 
									<Line 
			          		data={this.state.dailyTestsChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Daily Tests per County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
								}
								{showBarGraph && 
									<Bar 
			          		data={this.state.dailyTestsChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Daily Tests per County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
			          }
							</div>
						}
					</div> :
					<div style={styles.selectCounty}>
						<span style={styles.selectCountyText}>Select one or more counties...</span>
					</div>
				}
				<div style={styles.countySelectButtonsContainer}>
					{countiesNewYork.map((county, index) => (
						<div style={styles.countySelectButtonContainer} key={index}>
							<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCounty.bind(this, county.county, county.population)}>{county.county}</Button>
						</div>
					))}
				</div>
				<div style={styles.sourceURLContainer}>
					<a style={styles.sourceURL} target="_blank" href="https://health.data.ny.gov/resource/xdss-u53e"><span style={styles.sourceURLText}>Source: health.data.ny.gov</span></a>
				</div>
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		importColorsListCountiesNY: state.importData.importColorsListCountiesNY,
		importCountiesNYListNY: state.importData.importCountiesNYListNY,

		caseDataCountyNy: state.counties.caseDataCountyNy,
		loadingCaseDataCountyNy: state.counties.loadingCaseDataCountyNy
	}
}

export default connect(mapStateToProps,  { getCaseDataNyCounty, clearCaseDataNyCounty, getColorsListCountiesNY, getCountiesNYListNY, clearImportCountiesNY })(NY);

const styles = {
	chartTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		padding: 10
	},
	chartTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	chartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	selectCounty: {
		padding: 20,
		textAlign: 'center'
	},
	selectCountyText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	countySelectButtonsContainer: {
		display: 'flex',
		flex: 1, 
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		marginTop: 20
	},
	countySelectButtonContainer: {
		margin: 5
	},
	countySelectButton: {
		backgroundColor: '#FFF',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	countySelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
}










