import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

import SettingsModal from '../SettingsModal';
import ModalBackdrop from '../ModalBackdrop';
import SettingsModalIcon from '../SettingsModalIcon';

import { getCaseDataCaCounty, clearCaseDataCaCounty, getHospitalDataCaCounty, clearHospitalDataCaCounty } from '../../actions/counties';
import { getColorsListCountiesCA, getCountiesCAListCA, clearImportCountiesCA } from '../../actions/importData';

class CA extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedCountiesList: [],
			chartColors: [],
			countiesCalifornia: [],
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showCounties: false, 
			showChart: false,
			showTotalCasesChart: true,
			showPercentCasesChart: false,
			showDailyCasesChart: false, 
			showTotalDeathsChart: false, 
			showPercentDeathsChart: false,
			showDailyDeathsChart: false,

			// Line
			totalCasesChartLine: {},
			percentCasesChartLine: {},
			dailyCasesChartLine: {},
			totalDeathsChartLine: {},
			percentDeathsChartLine: {},
			dailyDeathsChartLine: {},
			hospitalBedsChartLine: {},
			icuBedsChartLine: {},
			sevenDayAverageChartLine: {},

			// Bar
			totalCasesChartBar: {},
			percentCasesChartBar: {},
			dailyCasesChartBar: {},
			totalDeathsChartBar: {},
			percentDeathsChartBar: {},
			dailyDeathsChartBar: {},
			hospitalBedsChartBar: {},
			icuBedsChartBar: {},
			sevenDayAverageChartBar: {},

			todaysDate: '',
			datesArray: [],
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			doNotDisplay: false,
			population: 0,
			showTotalPercentDailyControls: true,
			showTotalChart: true,
			showPercentChart: false,
			showDailyChart: false,
			showCasesChart: true,
			showDeathsChart: false,
			showHospitalChart: false,
			showICUChart: false,
			showCaseData: true,
			showHospitalData: false,
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: true,
			displayDailyAverageButtons: true,
			showSevenDayAverage: true,
			showDailyCount: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom'
	}

	handleShowSevenDayAverage = () => {
		this.setState({showSevenDayAverage: true, showDailyCount: false});
	}

	handleShowDailyCount = () => {
		this.setState({showSevenDayAverage: false, showDailyCount: true});
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	setChartData = () => {
		const { datesArray, selectedCountiesList, showCaseData, showHospitalData } = this.state;
		if (showCaseData) {
			if (selectedCountiesList.length > 0) {
				const labelDates = [];
				var total_cases = [];
				var percent_cases = [];
				var daily_cases = [];
				var total_deaths = [];
				var percent_deaths = [];
				var daily_deaths = [];
				var seven_day_average = [];

				// Line
				const dataset_total_cases_line = [];
				const dataset_percent_cases_line = [];
				const dataset_daily_cases_line = [];
				const dataset_total_deaths_line = [];
				const dataset_percent_deaths_line = [];
				const dataset_daily_deaths_line = [];
				const dataset_seven_day_average_line = [];

				// Bar
				const dataset_total_cases_bar = [];
				const dataset_percent_cases_bar = [];
				const dataset_daily_cases_bar = [];
				const dataset_total_deaths_bar = [];
				const dataset_percent_deaths_bar = [];
				const dataset_daily_deaths_bar = [];
				const dataset_seven_day_average_bar = [];

				for (var i = 0; i < datesArray.length; i++) {
					var fields = datesArray[i].split('-');
					labelDates.push(fields[1]+'/'+fields[2]);
				}
				for (let county of this.selectedCounties) {
					for (var i = 0; i < selectedCountiesList.length; i++) {
						if (selectedCountiesList[i].name == county) {
							for (var j = 0; j < selectedCountiesList[i].data.length; j++) {
								total_cases.push(selectedCountiesList[i].data[j].total_cases);
								percent_cases.push(selectedCountiesList[i].data[j].percent_cases);
								daily_cases.push(selectedCountiesList[i].data[j].daily_cases);
								total_deaths.push(selectedCountiesList[i].data[j].total_deaths);
								percent_deaths.push(selectedCountiesList[i].data[j].percent_deaths);
								daily_deaths.push(selectedCountiesList[i].data[j].daily_deaths);
								seven_day_average.push(selectedCountiesList[i].data[j].sevenDayAverage);
							}
							// Line
							dataset_total_cases_line.push({
								label: selectedCountiesList[i].name,
								data: total_cases,
								fill: false,
								borderColor: selectedCountiesList[i].color,
								pointRadius: 1,
								lineWidth: 3
							});
							dataset_percent_cases_line.push({
								label: selectedCountiesList[i].name,
								data: percent_cases,
								fill: false,
								borderColor: selectedCountiesList[i].color,
								pointRadius: 1,
								lineWidth: 3
							})
							dataset_daily_cases_line.push({
								label: selectedCountiesList[i].name,
								data: daily_cases,
								fill: false,
								borderColor: selectedCountiesList[i].color,
								pointRadius: 1,
								lineWidth: 3
							});
							dataset_total_deaths_line.push({
								label: selectedCountiesList[i].name,
								data: total_deaths,
								fill: false,
								borderColor: selectedCountiesList[i].color,
								pointRadius: 1,
								lineWidth: 3
							});
							dataset_percent_deaths_line.push({
								label: selectedCountiesList[i].name,
								data: percent_deaths,
								fill: false,
								borderColor: selectedCountiesList[i].color,
								pointRadius: 1,
								lineWidth: 3
							})
							dataset_daily_deaths_line.push({
								label: selectedCountiesList[i].name,
								data: daily_deaths,
								fill: false,
								borderColor: selectedCountiesList[i].color,
								pointRadius: 1,
								lineWidth: 3
							});
							dataset_seven_day_average_line.push({
								label: selectedCountiesList[i].name,
								data: seven_day_average,
								fill: false,
								borderColor: selectedCountiesList[i].color,
								pointRadius: 1,
								lineWidth: 3
							});

							// Bar
							dataset_total_cases_bar.push({
								label: selectedCountiesList[i].name,
								data: total_cases,
								backgroundColor: selectedCountiesList[i].color
							});
							dataset_percent_cases_bar.push({
								label: selectedCountiesList[i].name,
								data: percent_cases,
								backgroundColor: selectedCountiesList[i].color
							})
							dataset_daily_cases_bar.push({
								label: selectedCountiesList[i].name,
								data: daily_cases,
								backgroundColor: selectedCountiesList[i].color
							});
							dataset_total_deaths_bar.push({
								label: selectedCountiesList[i].name,
								data: total_deaths,
								backgroundColor: selectedCountiesList[i].color
							});
							dataset_percent_deaths_bar.push({
								label: selectedCountiesList[i].name,
								data: percent_deaths,
								backgroundColor: selectedCountiesList[i].color
							})
							dataset_daily_deaths_bar.push({
								label: selectedCountiesList[i].name,
								data: daily_deaths,
								backgroundColor: selectedCountiesList[i].color
							});
							dataset_seven_day_average_bar.push({
								label: selectedCountiesList[i].name,
								data: seven_day_average,
								backgroundColor: selectedCountiesList[i].color
							});

							total_cases = [];
							percent_cases = [];
							daily_cases = [];
							total_deaths = [];
							percent_deaths = [];
							daily_deaths = [];
							seven_day_average = [];
						}
					}
				}
				this.setState({
					// Line
					totalCasesChartLine: {
						labels: labelDates,
						datasets: dataset_total_cases_line
					},
					percentCasesChartLine: {
						labels: labelDates,
						datasets: dataset_percent_cases_line
					},
					dailyCasesChartLine: {
						labels: labelDates,
						datasets: dataset_daily_cases_line
					},
					totalDeathsChartLine: {
						labels: labelDates,
						datasets: dataset_total_deaths_line
					},
					percentDeathsChartLine: {
						labels: labelDates,
						datasets: dataset_percent_deaths_line
					},
					dailyDeathsChartLine: {
						labels: labelDates,
						datasets: dataset_daily_deaths_line
					},
					sevenDayAverageChartLine: {
						labels: labelDates,
						datasets: dataset_seven_day_average_line
					},

					// Bar
					totalCasesChartBar: {
						labels: labelDates,
						datasets: dataset_total_cases_bar
					},
					percentCasesChartBar: {
						labels: labelDates,
						datasets: dataset_percent_cases_bar
					},
					dailyCasesChartBar: {
						labels: labelDates,
						datasets: dataset_daily_cases_bar
					},
					totalDeathsChartBar: {
						labels: labelDates,
						datasets: dataset_total_deaths_bar
					},
					percentDeathsChartBar: {
						labels: labelDates,
						datasets: dataset_percent_deaths_bar
					},
					dailyDeathsChartBar: {
						labels: labelDates,
						datasets: dataset_daily_deaths_bar
					},
					sevenDayAverageChartBar: {
						labels: labelDates,
						datasets: dataset_seven_day_average_bar
					},

					showChart: true
				})
			} else {
				this.setState({showChart: false});
			}
		}
		if (showHospitalData) {
			console.log('Hospital Data');
			console.log('Selected Counties List: ', selectedCountiesList);
			if (selectedCountiesList.length > 0) {
				const labelDates = [];
				var hospital_beds = [];
				var icu_beds = [];

				// Line
				const dataset_hospital_beds_line = [];
				const dataset_icu_beds_line = [];

				// Bar
				const dataset_hospital_beds_bar = [];
				const dataset_icu_beds_bar = [];

				for (var i = 0; i < datesArray.length; i++) {
					var fields = datesArray[i].split('-');
					labelDates.push(fields[1]+'/'+fields[2]);
				}
				for (let county of this.selectedCounties) {
					for (var i = 0; i < selectedCountiesList.length; i++) {
						if (selectedCountiesList[i].name == county) {
							for (var j = 0; j < selectedCountiesList[i].data.length; j++) {
								hospital_beds.push(selectedCountiesList[i].data[j].hospital_beds_in_use);
								icu_beds.push(selectedCountiesList[i].data[j].ICU_beds_in_use);
							}
							// Line
							dataset_hospital_beds_line.push({
								label: selectedCountiesList[i].name,
								data: hospital_beds,
								fill: false,
								borderColor: selectedCountiesList[i].color,
								pointRadius: 1,
								lineWidth: 3
							});
							dataset_icu_beds_line.push({
								label: selectedCountiesList[i].name,
								data: icu_beds,
								fill: false,
								borderColor: selectedCountiesList[i].color,
								pointRadius: 1,
								lineWidth: 3
							});

							// Bar
							dataset_hospital_beds_bar.push({
								label: selectedCountiesList[i].name,
								data: hospital_beds,
								backgroundColor: selectedCountiesList[i].color
							});
							dataset_icu_beds_bar.push({
								label: selectedCountiesList[i].name,
								data: icu_beds,
								backgroundColor: selectedCountiesList[i].color
							});

							hospital_beds = [];
							icu_beds = [];
						}
					}
				}
				this.setState({
					// Line
					hospitalBedsChartLine: {
						labels: labelDates,
						datasets: dataset_hospital_beds_line
					},
					icuBedsChartLine: {
						labels: labelDates,
						datasets: dataset_icu_beds_line
					},

					// Bar
					hospitalBedsChartBar: {
						labels: labelDates,
						datasets: dataset_hospital_beds_bar
					},
					icuBedsChartBar: {
						labels: labelDates,
						datasets: dataset_icu_beds_bar
					},

					showChart: true
				})
			}
		}	
	}

	handleSelectCounty = (county, population) => {
		const { countiesCalifornia } = this.state;
		const { selectedCountiesList, showCaseData, showHospitalData } = this.state;
		let { showChart } = this.state;
		if (this.selectedCounties.has(county)) {
			this.selectedCounties.delete(county);
			let newSelectedCountiesList = selectedCountiesList.filter(e => e.name !== county);
			for (var i = 0; i < countiesCalifornia.length; i++) {
				if (countiesCalifornia[i].county == county) {
					countiesCalifornia[i].active = false;
				}
			}
			this.setState({
				countiesCalifornia: countiesCalifornia,
				selectedCountiesList: newSelectedCountiesList,
				showChart: showChart
			});
		} else {
			this.selectedCounties.add(county);
			for (var i = 0; i < countiesCalifornia.length; i++) {
				if (countiesCalifornia[i].county == county) {
					countiesCalifornia[i].active = true;
				}
			}
			if (showCaseData) {
				this.props.getCaseDataCaCounty(county);
			}
			if (showHospitalData) {
				this.props.getHospitalDataCaCounty(county);
			}
			this.setState({
				countiesCalifornia: countiesCalifornia, 
				population: population
			});
		}
	}

	getDatesList = () => {
		var listDate = [];
		var listDateFormatted = [];
		var startDate = '2020-03-01';
		var endDate = this.state.todaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		this.setState({
			datesArray: listDate
		});
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();
		var newDate = date.setDate(date.getDate() -1);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		this.setState({
			todaysDate: today
		});
	}

	setDisplayTotalCases = () => {
		this.setState({
			showTotalCasesChart: true,
			showPercentCasesChart: false,
			showDailyCasesChart: false,
			showTotalDeathsChart: false,
			showPercentDeathsChart: false,
			showDailyDeathsChart: false
		});
	}

	setDisplayPercentCases = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: true,
			showDailyCasesChart: false,
			showTotalDeathsChart: false,
			showPercentDeathsChart: false,
			showDailyDeathsChart: false
		});
	}

	setDisplayDailyCases = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: true,
			showTotalDeathsChart: false,
			showPercentDeathsChart: false,
			showDailyDeathsChart: false
		});
	}

	setDisplayTotalDeaths = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: false,
			showTotalDeathsChart: true,
			showPercentDeathsChart: false,
			showDailyDeathsChart: false
		});
	}

	setDisplayPercentDeaths = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: false,
			showTotalDeathsChart: false,
			showPercentDeathsChart: true,
			showDailyDeathsChart: false
		});
	}

	setDisplayDailyDeaths = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: false,
			showTotalDeathsChart: false,
			showPercentDeathsChart: false,
			showDailyDeathsChart: true
		});
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	setDisplayTotal = () => {
		this.setState({
			showTotalChart: true,
			showPercentChart: false, 
			showDailyChart: false
		})
	}

	setDisplayPercent = () => {
		this.setState({
			showTotalChart: false,
			showPercentChart: true, 
			showDailyChart: false
		})
	}

	setDisplayDaily = () => {
		this.setState({
			showTotalChart: false,
			showPercentChart: false, 
			showDailyChart: true
		})
	}

	setDisplayCases = () => {
		if (!this.state.showCaseData) {
			this.handleShowCaseData();
		}
		this.setState({
			showCasesChart: true,
			showDeathsChart: false
		})
	}

	setDisplayDeaths = () => {
		if (!this.state.showCaseData) {
			this.handleShowCaseData();
		}
		this.setState({
			showCasesChart: false,
			showDeathsChart: true
		})
	}

	setDisplayHospitalBeds = () => {
		if (!this.state.showHospitalData) {
			this.handleShowHospitalData();
		}
		this.setState({
			showHospitalChart: true,
			showICUChart: false
		})
	}

	setDisplayICUBeds = () => {
		if (!this.state.showHospitalData) {
			this.handleShowHospitalData();
		}
		this.setState({
			showHospitalChart: false,
			showICUChart: true
		})
	}

	handleShowCaseData = () => {
		const { countiesCalifornia } = this.state;
		for (var i = 0; i < countiesCalifornia.length; i++) {
			countiesCalifornia[i].active = false;
		}
		this.setState({
			selectedCountiesList: [],
			countiesCalifornia: countiesCalifornia,
			showChart: false,
			showCaseData: true,
			showHospitalData: false,
			showTotalChart: true,
			showPercentChart: false, 
			showDailyChart: false,
			showCasesChart: true,
			showDeathsChart: false,
			showHospitalChart: false,
			showICUChart: false,
			showTotalPercentDailyControls: true
		})
		this.selectedCounties.clear();
	}

	handleShowHospitalData = () => {
		const { countiesCalifornia } = this.state;
		for (var i = 0; i < countiesCalifornia.length; i++) {
			countiesCalifornia[i].active = false;
		}
		this.setState({
			selectedCountiesList: [],
			countiesCalifornia: countiesCalifornia,
			showChart: false,
			showCaseData: false,
			showHospitalData: true,
			showTotalChart: false,
			showPercentChart: false, 
			showDailyChart: false,
			showCasesChart: false,
			showDeathsChart: false,
			showTotalPercentDailyControls: false
		})
		this.selectedCounties.clear();
	}

	componentDidMount() {
		this.selectedCounties = new Set();
		this.getTodaysDate();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
		this.props.getColorsListCountiesCA(); 
		this.props.getCountiesCAListCA();
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.importColorsListCountiesCA !== null && lastProps.importColorsListCountiesCA === null) {
			const chartColors = [];
			this.props.importColorsListCountiesCA.map(datum => {
				chartColors.push(datum);
			});
			this.setState({
				chartColors: chartColors
			});
		}

		if (this.props.importCountiesCAListCA !== null && lastProps.importCountiesCAListCA === null) {
			const countiesCalifornia = [];
			this.props.importCountiesCAListCA.map(datum => {
				countiesCalifornia.push({
					county: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesCalifornia: countiesCalifornia
			});
		}

		if (this.props.loadingHospitalDataCountyCa === false && lastProps.loadingHospitalDataCountyCa === true) {
			const { hospitalDataCountyCa } = this.props;
			// console.log('Hospital Data: ', hospitalDataCountyCa);
			const { selectedCountiesList, datesArray, chartColors, population } = this.state;
			var randomNumber = (Math.random() * 56).toFixed();
			const countyData = {name: hospitalDataCountyCa[0].county, color: chartColors[randomNumber], data: []};
			for (var i = 0; i < datesArray.length; i++) {
				countyData.data.push({date: datesArray[i], available_hospital_beds: 0, hospital_beds_in_use: 0, available_ICU_beds: 0, ICU_beds_in_use: 0});
			}
			for (var i = 0; i < countyData.data.length; i++) {
				hospitalDataCountyCa.map(datum => {
					if (countyData.data[i].date == datum.todays_date.split('T')[0]) {
						countyData.data[i].hospital_beds_in_use = parseFloat(datum.hospitalized_covid_confirmed_patients) + parseFloat(datum.hospitalized_suspected_covid_patients);
						countyData.data[i].ICU_beds_in_use = parseFloat(datum.icu_covid_confirmed_patients) + parseFloat(datum.icu_suspected_covid_patients);
					}
				})
			}
			if (countyData.data[countyData.data.length - 1].hospital_beds_in_use == 0) {
				countyData.data[countyData.data.length - 1].hospital_beds_in_use = countyData.data[countyData.data.length - 2].hospital_beds_in_use;
			}
			if (countyData.data[countyData.data.length - 1].ICU_beds_in_use == 0) {
				countyData.data[countyData.data.length - 1].ICU_beds_in_use = countyData.data[countyData.data.length - 2].ICU_beds_in_use;
			}
			selectedCountiesList.push(countyData);
			this.setState({selectedCountiesList: selectedCountiesList, population: 0});
			this.props.clearHospitalDataCaCounty();
			this.setChartData();
		}
		if (this.props.loadingCaseDataCountyCa === false && lastProps.loadingCaseDataCountyCa === true) {
			const { caseDataCountyCa } = this.props;
			// console.log('Case Data County Ca: ', caseDataCountyCa);
			const { selectedCountiesList, datesArray, chartColors, population } = this.state;
			var randomNumber = (Math.random() * 56).toFixed();
			const countyData = {name: caseDataCountyCa[0].county, color: chartColors[randomNumber], data: []};
			for (var i = 0; i < datesArray.length; i++) {
				countyData.data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, daily_cases: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverage: 0});
			}
			for (var i = 0; i < countyData.data.length; i++) {
				caseDataCountyCa.map(datum => {
					if (countyData.data[i].date == datum.date.split('T')[0]) {
						countyData.data[i].total_cases = datum.totalcountconfirmed;
						countyData.data[i].percent_cases = (datum.totalcountconfirmed / population * 100).toFixed(2);
						countyData.data[i].daily_cases = datum.newcountconfirmed;
						countyData.data[i].total_deaths = datum.totalcountdeaths;
						countyData.data[i].percent_deaths = (datum.totalcountdeaths / population * 100).toFixed(2);
						countyData.data[i].daily_deaths = datum.newcountdeaths;
					}
				});
			}
			var sevenDayCount = 0;
			for (var i = 0; i < countyData.data.length; i++) {
				sevenDayCount += parseFloat(countyData.data[i].daily_cases);
				if (i < 7) {
					countyData.data[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(countyData.data[i - 7].daily_cases);
					countyData.data[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
				}
			}
			if (countyData.data[countyData.data.length - 1].total_cases == 0) {
				countyData.data[countyData.data.length - 1].total_cases = countyData.data[countyData.data.length - 2].total_cases;
			}
			if (countyData.data[countyData.data.length - 1].percent_cases == 0) {
				countyData.data[countyData.data.length - 1].percent_cases = countyData.data[countyData.data.length - 2].percent_cases;
			}
			if (countyData.data[countyData.data.length - 1].total_deaths == 0) {
				countyData.data[countyData.data.length - 1].total_deaths = countyData.data[countyData.data.length - 2].total_deaths;
			}
			if (countyData.data[countyData.data.length - 1].percent_deaths == 0) {
				countyData.data[countyData.data.length - 1].percent_deaths = countyData.data[countyData.data.length - 2].percent_deaths;
			}
			selectedCountiesList.push(countyData);
			this.setState({selectedCountiesList: selectedCountiesList, population: 0});
			this.props.clearCaseDataCaCounty();
			this.setChartData();
		}
		if (this.state.todaysDate !== '' && lastState.todaysDate === '') {
			this.getDatesList();
		}
		if (this.state.selectedCountiesList !== lastState.selectedCountiesList) {
			this.setChartData();
		}
	}

	componentWillUnmount() {
		this.props.clearImportCountiesCA();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { countiesCalifornia, showChart, showAxisX } = this.state;
		const { showTotalCasesChart, showPercentCasesChart, showDailyCasesChart, showTotalDeathsChart, showPercentDeathsChart, showDailyDeathsChart } = this.state;
		const { chartHeight, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons } = this.state;
		const { showTotalPercentDailyControls, showTotalChart, showPercentChart, showDailyChart } = this.state;
		const { showCasesChart, showDeathsChart, showHospitalChart, showICUChart, showCaseData, showHospitalData } = this.state;
		const { displayDailyAverageButtons, showSevenDayAverage, showDailyCount } = this.state;

		return (
			<div>
				{this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} showSevenDayAverage={showSevenDayAverage} showDailyCount={showDailyCount} displaySevenDayAverage={this.handleShowSevenDayAverage} displayDailyCount={this.handleShowDailyCount} />
        }
				{showChart ? 
					<div>
						{showTotalPercentDailyControls && 
							<div>
								<Grid container>
									<Grid item xs={4} style={styles.chartTypeButtonContainer}>
										<Button style={showTotalChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayTotal}>Total</Button>
									</Grid>
									<Grid item xs={4} style={styles.chartTypeButtonContainer}>
										<Button style={showPercentChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayPercent}>Percent</Button>
									</Grid>
									<Grid item xs={4} style={styles.chartTypeButtonContainer}>
										<Button style={showDailyChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayDaily}>Daily</Button>
									</Grid>
								</Grid>
							</div>
						}
						{showTotalChart && 
							<div>
								{showCasesChart && 
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										{showLineGraph && 
											<Line 
					          		data={this.state.totalCasesChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Cases by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
					          }
					          {showBarGraph && 
					          	<Bar 
					          		data={this.state.totalCasesChartBar}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Cases by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
			                		maintainAspectRatio: false
					              }}
					          	/>
					          }
				          </div>
								}
								{showDeathsChart && 
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										{showLineGraph && 
											<Line 
					          		data={this.state.totalDeathsChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Deaths by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
					          }
					          {showBarGraph && 
					          	<Bar 
					          		data={this.state.totalDeathsChartBar}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Deaths by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
			                		maintainAspectRatio: false
					              }}
					          	/>
					          }
				          </div>
								}
							</div>
						}
						{showPercentChart && 
							<div>
								{showCasesChart && 
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										{showLineGraph && 
											<Line 
					          		data={this.state.percentCasesChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Percent Cases by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
					          }
					          {showBarGraph && 
					          	<Bar 
					          		data={this.state.percentCasesChartBar}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Percent Cases by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
			                		maintainAspectRatio: false
					              }}
					          	/>
					          }
				          </div>
								}
								{showDeathsChart && 
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										{showLineGraph && 
											<Line 
					          		data={this.state.percentDeathsChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Percent Deaths by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
					          }
					          {showBarGraph && 
					          	<Bar 
					          		data={this.state.percentDeathsChartBar}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Percent Deaths by County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
			                		maintainAspectRatio: false
					              }}
					          	/>
					          }
				          </div>
								}
							</div>
						}
						{showDailyChart && 
							<div>
								{showCasesChart && 
									<div>
										{showSevenDayAverage && 
											<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
												{showLineGraph && 
													<Line 
							          		data={this.state.sevenDayAverageChartLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Seven Day Average per County',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                          } else {
					                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                          }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:false, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
												}
												{showBarGraph && 
													<Bar 
							          		data={this.state.sevenDayAverageChartBar}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Seven Day Average per County',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                          } else {
					                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                          }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:false, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
					                		maintainAspectRatio: false
							              }}
							          	/>
							          }
											</div>
										}
										{showDailyCount && 
											<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
												{showLineGraph && 
													<Line 
							          		data={this.state.dailyCasesChartLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Daily Cases per County',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                          } else {
					                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                          }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:false, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
												}
												{showBarGraph && 
													<Bar 
							          		data={this.state.dailyCasesChartBar}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Daily Cases per County',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                          } else {
					                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                          }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:false, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
					                		maintainAspectRatio: false
							              }}
							          	/>
							          }
											</div>
										}
									</div>
								}
								{showDeathsChart && 
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										{showLineGraph && 
											<Line 
					          		data={this.state.dailyDeathsChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Daily Deaths per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
										}
										{showBarGraph && 
											<Bar 
					          		data={this.state.dailyDeathsChartBar}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Daily Deaths per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
			                		maintainAspectRatio: false
					              }}
					          	/>
					          }
									</div>
								}
							</div>
						}
						{showHospitalChart && 
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								{showLineGraph && 
									<Line 
			          		data={this.state.hospitalBedsChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Hospital Beds in Use per County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
								}
								{showBarGraph && 
									<Bar 
			          		data={this.state.hospitalBedsChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Hospital Beds in Use per County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
			          }
							</div>
						}
						{showICUChart && 
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								{showLineGraph && 
									<Line 
			          		data={this.state.icuBedsChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'ICU Beds in Use per County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
								}
								{showBarGraph && 
									<Bar 
			          		data={this.state.icuBedsChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'ICU Beds in Use per County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
			          }
							</div>
						}
					</div> : 
					<div style={styles.selectCounty}>
						<span style={styles.selectCountyText}>Select one or more counties...</span>
					</div>
				}
				<div>
					<Grid container>
						<Grid item xs={6} style={styles.chartTypeButtonContainer}>
							<Button style={showCasesChart ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setDisplayCases}>Cases</Button>
						</Grid>
						<Grid item xs={6} style={styles.chartTypeButtonContainer}>
							<Button style={showDeathsChart ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setDisplayDeaths}>Deaths</Button>
						</Grid>
						<Grid item xs={6} style={styles.chartTypeButtonContainer}>
							<Button style={showHospitalChart ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setDisplayHospitalBeds}>Hospital Beds</Button>
						</Grid>
						<Grid item xs={6} style={styles.chartTypeButtonContainer}>
							<Button style={showICUChart ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setDisplayICUBeds}>ICU Beds</Button>
						</Grid>
					</Grid>
				</div>
				<div style={styles.countySelectButtonsContainer}>
					{countiesCalifornia.map((county, index) => (
						<div style={styles.countySelectButtonContainer} key={index}>
							<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCounty.bind(this, county.county, county.population)}>{county.county}</Button>
						</div>
					))}
				</div>
				{showCaseData && 
					<div style={styles.sourceURLContainer}>
						<a style={styles.sourceURL} target="_blank" href="https://data.ca.gov/dataset/covid-19-cases"><span style={styles.sourceURLText}>Source: data.ca.gov</span></a>
					</div>
				}
				{showHospitalData && 
					<div style={styles.sourceURLContainer}>
						<a style={styles.sourceURL} target="_blank" href="https://data.ca.gov/dataset/covid-19-hospital-data"><span style={styles.sourceURLText}>Source: data.ca.gov</span></a>
					</div>
				}
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		importColorsListCountiesCA: state.importData.importColorsListCountiesCA,
		importCountiesCAListCA: state.importData.importCountiesCAListCA,

		caseDataCountyCa: state.counties.caseDataCountyCa,
		loadingCaseDataCountyCa: state.counties.loadingCaseDataCountyCa,

		hospitalDataCountyCa: state.counties.hospitalDataCountyCa,
		loadingHospitalDataCountyCa: state.counties.loadingHospitalDataCountyCa
	}
}

export default connect(mapStateToProps, { getCaseDataCaCounty, clearCaseDataCaCounty, getHospitalDataCaCounty, clearHospitalDataCaCounty, getColorsListCountiesCA, getCountiesCAListCA, clearImportCountiesCA })(CA);

const styles = {
	subChartTypeButton: {
		backgroundColor: '#32a852',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	subChartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	chartTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		padding: 10
	},
	chartTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	chartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	selectCounty: {
		padding: 20,
		textAlign: 'center'
	},
	selectCountyText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	countySelectButtonsContainer: {
		display: 'flex',
		flex: 1, 
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		marginTop: 20
	},
	countySelectButtonContainer: {
		margin: 5
	},
	countySelectButton: {
		backgroundColor: '#FFF',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	countySelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
}










