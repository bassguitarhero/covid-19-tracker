import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

class SettingsModalIcon extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0
		}
	}

	openSettingsModal = () => {
		this.props.openSettingsModal();
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { showAxisX } = this.state;
		return (
			<div style={styles.settingsModalPlaceholder}>
				<div style={showAxisX ? styles.settingsModalPosition : styles.settingsModalPositionMobile}>
	        <div style={styles.settingsModalCircle}>  
	          <span style={styles.settingsModalCirclePlus}>
	            <Button style={styles.settingsModalCirclePlus} onClick={this.openSettingsModal}>
	            	<Icon path={mdiCogOutline}
					        title="Chart Settings"
					        size={2}
					        horizontal
					        vertical
					        />
	            </Button>
	          </span>
	        </div>
	      </div>
	    </div>
		);
	}
}

export default SettingsModalIcon;

const styles = {
	settingsModalPlaceholder: {
		position: 'relative'
	},
	settingsModalPosition: {
		position: 'fixed',
		bottom: 55,
		right: 100
	},
	settingsModalPositionMobile: {
		position: 'fixed',
		bottom: 55,
		right: 20
	},
	settingsModalCircle: {
		height: 80,
    width: 80,
    // -moz-border-radius: 50%;
    borderRadius: 40,
    backgroundColor: '#666',
    color: 'black',
    display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	settingsModalCirclePlus: {
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	}
}