import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Button, Grid, Select, MenuItem } from '@material-ui/core';
import { connect } from 'react-redux';

import { resetHeaderCompare, resetHeaderDetail, resetHeaderAbout } from '../actions/layout';

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCompare: true,
      showDetail: false,
      showAbout: false,
      itemName: 'Compare',
      navigation: [
        {
          name: "Compare",
          id: "Compare"
        },
        {
          name: "Detail",
          id: "Detail"
        },
        {
          name: "About",
          id: "About"
        }
      ],
      selectedItem: '',
      showCompare: false,
      windowWidth: '',
      windowHeight: ''
    }
  }

  handleItemChange = (e) => {
    this.setState({
      itemName: e.target.value
    });
  }

  setDisplayCompare = () => {
    this.props.displayCompare();
    this.setState({
      showCompare: true,
      showDetail: false,
      showAbout: false,
    });
  }

  setDisplayDetail = () => {
    this.props.displayDetail();
    this.setState({
      showCompare: false,
      showDetail: true,
      showAbout: false,
    });
  }

  setDisplayAbout = () => {
    this.props.displayAbout();
    this.setState({
      showCompare: false,
      showDetail: false,
      showAbout: true,
    });
  }

  componentDidUpdate(lastProps, lastState) {
    if (this.state.itemName !== lastState.itemName) {
      if (this.state.itemName == "Compare") {
        this.setDisplayCompare();
      }
      if (this.state.itemName == "Detail") {
        this.setDisplayDetail();
      }
      if (this.state.itemName == "About") {
        this.setDisplayAbout();
      }
    }
    if (this.props.updateHeaderCompare === true && lastProps.updateHeaderCompare === false) {
      // console.log('Countries');
      this.setState({
        showCompare: true,
        showDetail: false,
        showAbout: false,
        itemName: 'Compare'
      });
      this.props.resetHeaderCompare();
    }
    if (this.props.updateHeaderDetail === true && lastProps.updateHeaderDetail === false) {
      // console.log('Countries');
      this.setState({
        showCompare: false,
        showDetail: true,
        showAbout: false,
        itemName: 'Detail'
      });
      this.props.resetHeaderDetail();
    }
    if (this.props.updateHeaderAbout === true && lastProps.updateHeaderAbout === false) {
      // console.log('Countries');
      this.setState({
        showCompare: false,
        showDetail: false,
        showAbout: true,
        itemName: 'About'
      });
      this.props.resetHeaderAbout();
    }
  }

  updateWindowDimensions = () => {
    if (window.innerWidth/window.innerHeight <= .75) {
      this.setState({showCompare: false});
    } else {
      this.setState({showCompare: true});
    }
    this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateWindowDimensions);
    this.updateWindowDimensions();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  render() {
    const { itemName, navigation, showCompare } = this.state;

    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <Grid container style={styles.dataContainer}>
              <Grid md={1} item />
              <Grid md={10} item container spacing={1} xs>
                <Grid item xs={6} style={styles.titleText}>
                  <Typography variant="title" color="inherit">
                    <span style={{fontWeight: 'bold'}}>COVID-19</span> 
                    {showCompare && <span style={styles.itemNameText}> Tracker</span>}
                    {!showCompare && <span style={styles.itemNameText}>: </span>}
                  </Typography>
                </Grid>
                <Grid item xs={6} style={styles.menuWrapper}>
                  {showCompare && <span style={styles.itemNameText}>View: </span>}
                  <Select
                    value={itemName} 
                    onChange={this.handleItemChange.bind(this)}
                    inputProps={{
                      name: `Compare`,
                      id: "Compare"
                    }} 
                    style={styles.selectItemName} 
                  >
                    {navigation.map((item, index) => (
                      <MenuItem value={item.name} key={index}>{item.name}</MenuItem>
                    ))}
                  </Select>
                </Grid>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    updateHeaderCities: state.layout.updateHeaderCities,
    updateHeaderCounties: state.layout.updateHeaderCounties, 
    updateHeaderStates: state.layout.updateHeaderStates,
    updateHeaderCountries: state.layout.updateHeaderCountries,
    updateHeaderCompare: state.layout.updateHeaderCompare,
    updateHeaderDetail: state.layout.updateHeaderDetail,
    updateHeaderAbout: state.layout.updateHeaderAbout
  }
}

export default connect(mapStateToProps, { resetHeaderCompare, resetHeaderDetail, resetHeaderAbout })(NavBar);

const styles = {
  appWrapper: {
    display: 'flex',
    flex: 1
  },
  titleWrapper: {
    flex: 1
  },
  buttonsWrapper: {
    marginBottom: 10
  }, 
  buttonContainer: {
    flex: 1
  },
  button: {
    backgroundColor: '#FFF',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 25px',
  },
  buttonSelected: {
    backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0px 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
  },
  menuWrapper: {
    textAlign: 'right'
  },
  selectItemName: {
    color: 'white',
    paddingRight: 10,
    paddingLeft: 10,
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center'
  },
  titleText: {
    justifyContent: 'center',
    paddingTop: 10
  }
}









