import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

import SettingsModal from '../SettingsModal';
import ModalBackdrop from '../ModalBackdrop';
import SettingsModalIcon from '../SettingsModalIcon';

import { getHongKongData, clearHongKongData } from '../../actions/cities';

class HK extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showChart: false,
			chartDailyConfirmed: {},
			chartTotalConfirmed: {},
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			todaysDate: '',
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: false,
			displayDailyAverageButtons: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom',
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	formatDate = (s) => {
		var date = s.split('/');
		return date[1]+'/'+date[0];
	}

	setChartDataHongKong = () => {
		const { dataHK } = this.props;
		const labelDatesSet = new Set();
		dataHK.map((datum, index) => {
			labelDatesSet.add(datum["Report date"])
		});
		const labelDatesArray = Array.from(labelDatesSet);
		const hkData = [];
		var hkDailyTotal = 0;
		var hkTotal = 0;
		var date = ''
		for (var i = 0; i < labelDatesArray.length; i++) {
			dataHK.map((datum, index) => {
				if (datum["Report date"] == labelDatesArray[i]) {
					date = datum["Report date"];
					hkDailyTotal += 1;
					hkTotal += 1;
				}
			});
			hkData.push({date: date, daily_total: hkDailyTotal, cumulative_total: hkTotal});
			hkDailyTotal = 0;
		}
		var labelDates = [];
		var dailyCount = [];
		var totalCount = [];
		for (var i = 0; i < hkData.length; i++) {
			labelDates.push(this.formatDate(hkData[i].date));
			dailyCount.push(hkData[i].daily_total);
			totalCount.push(hkData[i].cumulative_total);
		}
		this.setState({
			chartDailyConfirmed: {
				labels: labelDates,
				datasets: [
					{
						label: 'Daily Cases',
						data: dailyCount,
            backgroundColor: '#EBCCD1' // red
					}
				]
			},
			chartTotalConfirmed: {
				labels: labelDates,
				datasets: [
					{
						data: totalCount,
						label: 'Total Cases',
						fill: false,
						borderColor: '#EBCCD1',
						pointRadius: 1,
						lineWidth: 3
					}
				]
			}
		})
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.props.getHongKongData();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps) {
		if (this.props.loadingDataHK === false && (lastProps.loadingDataHK === true || lastProps.loadingDataHK === undefined)) {
			this.setChartDataHongKong();
		}
	}

	componentWillUnmount() {
		this.props.clearHongKongData();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { loadingDataHK } = this.props;
		const { showChart, showAxisX, chartHeight, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons, displayDailyAverageButtons } = this.state;
		return (
			<div>
				 {this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} />
        }
        {this.state.showSettingsModal && <ModalBackdrop />}
				{loadingDataHK === false ? 
					<Grid container>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Line
									data={this.state.chartTotalConfirmed}
									options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Total Cases in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
		                tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[0].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
		                scales: {
		                	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
									     yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
		                },
	                  maintainAspectRatio: false
			            }}
			        	/>
		        	</div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Bar
									data={this.state.chartDailyConfirmed}
									options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Daily Confirmed in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
		                tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[0].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
		                scales: {
									    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
									     yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
									  },
	                  maintainAspectRatio: false
			            }}
			        	/>
		        	</div>
						</Grid>
						<Grid item xs={12}>
							<div style={styles.sourceURLContainer}>
								<a style={styles.sourceURL} target="_blank" href="https://api.data.gov.hk/v2/filter?q=%7B%22resource%22%3A%22http%3A%2F%2Fwww.chp.gov.hk%2Ffiles%2Fmisc%2Fenhanced_sur_covid_19_eng.csv%22%2C%22section%22%3A1%2C%22format%22%3A%22json%22%7D"><span style={styles.sourceURLText}>Source: Api.data.gov.hk</span></a>
							</div>
						</Grid>
					</Grid> :
					<div style={styles.loadingData}>
						<span style={styles.loadingDataText}>Loading Hong Kong Data</span> <Dots />
					</div>
				}
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dataHK: state.cities.dataHK,
		loadingDataHK: state.cities.loadingDataHK
	}
}

export default connect(mapStateToProps, { getHongKongData, clearHongKongData })(HK);

const styles = {
	chart: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		flex: 1,
		minHeight: 300
	}, 
	chartWide: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		flex: 1,
		minHeight: 400
	}, 
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
	loadingData: {
		padding: 20,
		textAlign: 'center'
	},
	loadingDataText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	settingsModalPlaceholder: {
		position: 'relative'
	},
	settingsModalPosition: {
		position: 'fixed',
		bottom: 50,
		right: 100
	},
	settingsModalPositionMobile: {
		position: 'fixed',
		bottom: 50,
		right: 20
	},
	settingsModalCircle: {
		height: 100,
    width: 100,
    // -moz-border-radius: 50%;
    borderRadius: 50,
    backgroundColor: '#666',
    color: 'black',
    display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	settingsModalCirclePlus: {
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	}
}









