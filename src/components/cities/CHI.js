import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';

import SettingsModal from '../SettingsModal';
import ModalBackdrop from '../ModalBackdrop';
import SettingsModalIcon from '../SettingsModalIcon';

import { getChicagoData, clearChicagoData } from '../../actions/cities';

class Chicago extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			population: 2706000,
			colorsList: [],

			// Charts
			// Line
			chartTotalCasesLine: {},
			chartPercentCasesLine: {},
			chartDailyCasesLine: {},
			chartSevenDayAverageCasesLine: {},
			chartTotalDeathsLine: {},
			chartPercentDeathsLine: {},
			chartDailyDeathsLine: {},
			chartSevenDayAverageDeathsLine: {},

			// Bar
			chartTotalCasesBar: {},
			chartPercentCasesBar: {},
			chartDailyCasesBar: {},
			chartSevenDayAverageCasesBar: {},
			chartTotalDeathsBar: {},
			chartPercentDeathsBar: {},
			chartDailyDeathsBar: {},
			chartSevenDayAverageDeathsBar: {},

			// Pie
			chartGenderCases: {},
			chartAgeCases: {},
			chartEthnicityCases: {},

			chartGenderDeaths: {},
			chartAgeDeaths: {},
			chartEthnicityDeaths: {},

			// Controls
			showTotal: true,
			showPercent: false,
			showDaily: false,
			showCases: true,
			showDeaths: false,

			// Chart Controls
			showChart: false,
			triggerChartDisplay: false, 
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '2020-03-01',
			endDate: '',
			todaysDate: '',
			datesArray: [],
			doNotDisplay: false,
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: true,
			displayDailyAverageButtons: true,
			showDailyCount: false,
			showSevenDayAverage: true
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom'
	}

	handleShowSevenDayAverage = () => {
		this.setState({showSevenDayAverage: true, showDailyCount: false});
	}

	handleShowDailyCount = () => {
		this.setState({showSevenDayAverage: false, showDailyCount: true});
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	setDisplayChart = () => {
		const { colorsList, data, datesArray } = this.state;
		console.log('Data: ', data);

		const labelDates = [];
		for (var i = 0; i < datesArray.length; i++) {
			var fields = datesArray[i].split('-');
			var date = fields[1]+'/'+fields[2];
			labelDates.push(date);
		}

		const color = '#f7022b';

		//Line
		var datasetsTotalCasesLine = [];
		var datasetsPercentCasesLine = [];
		var	datasetsDailyCasesLine = [];
		var datasetsSevenDayAverageCasesLine = [];
		var	datasetsTotalDeathsLine = [];
		var datasetsPercentDeathsLine = [];
		var datasetsDailyDeathsLine = [];
		var datasetsSevenDayAverageDeathsLine = [];

		// Bar
		var datasetsTotalCasesBar = [];
		var datasetsPercentCasesBar = [];
		var	datasetsDailyCasesBar = [];
		var datasetsSevenDayAverageCasesBar = [];
		var	datasetsTotalDeathsBar = [];
		var datasetsPercentDeathsBar = [];
		var datasetsDailyDeathsBar = [];
		var datasetsSevenDayAverageDeathsBar = [];

		// Data
		var dataTotalCases = [];
		var dataPercentCases = [];
		var dataDailyCases = [];
		var dataSevenDayAverageCases = [];
		var dataTotalDeaths = [];
		var dataPercentDeaths = [];
		var dataDailyDeaths = [];
		var dataSevenDayAverageDeaths = [];

		for (var i = 0; i < data.length; i++) {
			dataTotalCases.push(data[i].total_cases);
			dataPercentCases.push(data[i].percent_cases);
			dataDailyCases.push(data[i].daily_cases);
			dataSevenDayAverageCases.push(data[i].sevenDayAverageCases);
			dataTotalDeaths.push(data[i].total_deaths);
			dataPercentDeaths.push(data[i].percent_deaths);
			dataDailyDeaths.push(data[i].daily_deaths);
			dataSevenDayAverageDeaths.push(data[i].sevenDayAverageDeaths);
		}

		// Line
		datasetsTotalCasesLine.push({
			label: 'Chicago',
			data: dataTotalCases,
			fill: false,
			borderColor: color,
			pointRadius: 1,
			lineWidth: 3
		});
		datasetsPercentCasesLine.push({
			label: 'Chicago',
			data: dataPercentCases,
			fill: false,
			borderColor: color,
			pointRadius: 1,
			lineWidth: 3
		});
		datasetsDailyCasesLine.push({
			label: 'Chicago',
			data: dataDailyCases,
			fill: false,
			borderColor: color,
			pointRadius: 1,
			lineWidth: 3
		});
		datasetsSevenDayAverageCasesLine.push({
			label: 'Chicago',
			data: dataSevenDayAverageCases,
			fill: false,
			borderColor: color,
			pointRadius: 1,
			lineWidth: 3
		});
		datasetsTotalDeathsLine.push({
			label: 'Chicago',
			data: dataTotalDeaths,
			fill: false,
			borderColor: color,
			pointRadius: 1,
			lineWidth: 3
		});
		datasetsPercentDeathsLine.push({
			label: 'Chicago',
			data: dataPercentDeaths,
			fill: false,
			borderColor: color,
			pointRadius: 1,
			lineWidth: 3
		});
		datasetsDailyDeathsLine.push({
			label: 'Chicago',
			data: dataDailyDeaths,
			fill: false,
			borderColor: color,
			pointRadius: 1,
			lineWidth: 3
		});
		datasetsSevenDayAverageDeathsLine.push({
			label: 'Chicago',
			data: dataSevenDayAverageDeaths,
			fill: false,
			borderColor: color,
			pointRadius: 1,
			lineWidth: 3
		});

		// Bar
		datasetsTotalCasesBar.push({
			label: 'Chicago',
			data: dataTotalCases,
			backgroundColor: color
		});
		datasetsPercentCasesBar.push({
			label: 'Chicago',
			data: dataPercentCases,
			backgroundColor: color
		});
		datasetsDailyCasesBar.push({
			label: 'Chicago',
			data: dataDailyCases,
			backgroundColor: color
		});
		datasetsSevenDayAverageCasesBar.push({
			label: 'Chicago',
			data: dataSevenDayAverageCases,
			backgroundColor: color
		});
		datasetsTotalDeathsBar.push({
			label: 'Chicago',
			data: dataTotalDeaths,
			backgroundColor: color
		});
		datasetsPercentDeathsBar.push({
			label: 'Chicago',
			data: dataPercentDeaths,
			backgroundColor: color
		});
		datasetsDailyDeathsBar.push({
			label: 'Chicago',
			data: dataDailyDeaths,
			backgroundColor: color
		});
		datasetsSevenDayAverageDeathsBar.push({
			label: 'Chicago',
			data: dataSevenDayAverageDeaths,
			backgroundColor: color
		});

		var cases_male = 0;
		var cases_female = 0;
		var cases_unknown_gender = 0;

		var cases_age_0_17 = 0;
		var cases_age_18_29 = 0;
		var cases_age_30_39 = 0;
		var cases_age_40_49 = 0;
		var cases_age_50_59 = 0;
		var cases_age_60_69 = 0;
		var cases_age_70_79 = 0;
		var cases_age_80_ = 0;
		var cases_age_unknown = 0;

		var cases_asian_non_latinx = 0;
		var cases_black_non_latinx = 0;
		var cases_latinx = 0;
		var cases_other_non_latinx = 0;
		var cases_white_non_latinx = 0;
		var cases_unknown_race_eth = 0;

		var deaths_male = 0;
		var deaths_female = 0;
		var deaths_unknown_gender = 0;

		var deaths_0_17_yrs = 0;
		var deaths_18_29_yrs = 0;
		var deaths_30_39_yrs = 0;
		var deaths_40_49_yrs = 0;
		var deaths_50_59_yrs = 0;
		var deaths_60_69_yrs = 0;
		var deaths_70_79_yrs = 0;
		var deaths_80_yrs = 0;
		var deaths_unknown_age = 0;

		var deaths_asian_non_latinx = 0;
		var deaths_black_non_latinx = 0;
		var deaths_latinx = 0;
		var deaths_other_non_latinx = 0;
		var deaths_white_non_latinx = 0;
		var deaths_unknown_race_eth = 0;

		var cases_gender_total = 0;
		var cases_age_total = 0;
		var cases_ethnicity_total = 0;

		var deaths_gender_total  = 0;
		var deaths_age_total = 0;
		var deaths_ethnicity_total = 0;

		// Fill out other charts
		for (var i = 0; i < data.length; i++) {
			// Gender
			cases_gender_total += parseInt(data[i].cases_male);
			cases_male += parseInt(data[i].cases_male);

			cases_gender_total += parseInt(data[i].cases_female);
			cases_female += parseInt(data[i].cases_female);

			cases_gender_total += parseInt(data[i].cases_unknown_gender);
			cases_unknown_gender += parseInt(data[i].cases_unknown_gender);

			// Ages
			cases_age_total += parseInt(data[i].cases_age_0_17);
			cases_age_0_17 += parseInt(data[i].cases_age_0_17);

			cases_age_total += parseInt(data[i].cases_age_18_29);
			cases_age_18_29 += parseInt(data[i].cases_age_18_29);

			cases_age_total += parseInt(data[i].cases_age_30_39);
			cases_age_30_39 += parseInt(data[i].cases_age_30_39);

			cases_age_total += parseInt(data[i].cases_age_40_49);
			cases_age_40_49 += parseInt(data[i].cases_age_40_49);

			cases_age_total += parseInt(data[i].cases_age_50_59);
			cases_age_50_59 += parseInt(data[i].cases_age_50_59);

			cases_age_total += parseInt(data[i].cases_age_60_69);
			cases_age_60_69 += parseInt(data[i].cases_age_60_69);

			cases_age_total += parseInt(data[i].cases_age_70_79);
			cases_age_70_79 += parseInt(data[i].cases_age_70_79);

			cases_age_total += parseInt(data[i].cases_age_80_);
			cases_age_80_ += parseInt(data[i].cases_age_80_);

			cases_age_total += parseInt(data[i].cases_age_unknown);
			cases_age_unknown += parseInt(data[i].cases_age_unknown);

			// Ethnicity
			cases_ethnicity_total += parseInt(data[i].cases_asian_non_latinx);
			cases_asian_non_latinx += parseInt(data[i].cases_asian_non_latinx);

			cases_ethnicity_total += parseInt(data[i].cases_black_non_latinx);
			cases_black_non_latinx += parseInt(data[i].cases_black_non_latinx);

			cases_ethnicity_total += parseInt(data[i].cases_latinx);
			cases_latinx += parseInt(data[i].cases_latinx);

			cases_ethnicity_total += parseInt(data[i].cases_other_non_latinx);
			cases_other_non_latinx += parseInt(data[i].cases_other_non_latinx);

			cases_ethnicity_total += parseInt(data[i].cases_white_non_latinx);
			cases_white_non_latinx += parseInt(data[i].cases_white_non_latinx);

			cases_ethnicity_total += parseInt(data[i].cases_unknown_race_eth);
			cases_unknown_race_eth += parseInt(data[i].cases_unknown_race_eth);

			// Gender
			deaths_gender_total += parseInt(data[i].deaths_male);
			deaths_male += parseInt(data[i].deaths_male);

			deaths_gender_total += parseInt(data[i].deaths_female);
			deaths_female += parseInt(data[i].deaths_female);

			deaths_gender_total += parseInt(data[i].deaths_unknown_gender);
			deaths_unknown_gender += parseInt(data[i].deaths_unknown_gender);

			// Age
			deaths_age_total += parseInt(data[i].deaths_0_17_yrs);
			deaths_0_17_yrs += parseInt(data[i].deaths_0_17_yrs);

			deaths_age_total += parseInt(data[i].deaths_18_29_yrs);
			deaths_18_29_yrs += parseInt(data[i].deaths_18_29_yrs);

			deaths_age_total += parseInt(data[i].deaths_30_39_yrs);
			deaths_30_39_yrs += parseInt(data[i].deaths_30_39_yrs);

			deaths_age_total += parseInt(data[i].deaths_40_49_yrs);
			deaths_40_49_yrs += parseInt(data[i].deaths_40_49_yrs);

			deaths_age_total += parseInt(data[i].deaths_50_59_yrs);
			deaths_50_59_yrs += parseInt(data[i].deaths_50_59_yrs);

			deaths_age_total += parseInt(data[i].deaths_60_69_yrs);
			deaths_60_69_yrs += parseInt(data[i].deaths_60_69_yrs);

			deaths_age_total += parseInt(data[i].deaths_70_79_yrs);
			deaths_70_79_yrs += parseInt(data[i].deaths_70_79_yrs);

			deaths_age_total += parseInt(data[i].deaths_80_yrs);
			deaths_80_yrs += parseInt(data[i].deaths_80_yrs);

			deaths_age_total += parseInt(data[i].deaths_unknown_age);
			deaths_unknown_age += parseInt(data[i].deaths_unknown_age);

			// Ethnicity
			deaths_ethnicity_total += parseInt(data[i].deaths_asian_non_latinx);
			deaths_asian_non_latinx += parseInt(data[i].deaths_asian_non_latinx);

			deaths_ethnicity_total += parseInt(data[i].deaths_black_non_latinx);
			deaths_black_non_latinx += parseInt(data[i].deaths_black_non_latinx);

			deaths_ethnicity_total += parseInt(data[i].deaths_latinx);
			deaths_latinx += parseInt(data[i].deaths_latinx);

			deaths_ethnicity_total += parseInt(data[i].deaths_other_non_latinx);
			deaths_other_non_latinx += parseInt(data[i].deaths_other_non_latinx);

			deaths_ethnicity_total += parseInt(data[i].deaths_white_non_latinx);
			deaths_white_non_latinx += parseInt(data[i].deaths_white_non_latinx);

			deaths_ethnicity_total += parseInt(data[i].deaths_unknown_race_eth);	
			deaths_unknown_race_eth += parseInt(data[i].deaths_unknown_race_eth);	
		}

		var dataGenderCases = [];
		var labelsGenderCases = [];
		var dataAgeCases = [];
		var labelsAgeCases = [];
		var dataEthnicityCases = [];
		var labelsEthnicityCases = [];

		var dataGenderDeaths = [];
		var labelsGenderDeaths = [];
		var dataAgeDeaths = [];
		var labelsAgeDeaths = [];
		var dataEthnicityDeaths = [];
		var labelsEthnicityDeaths = [];

		dataGenderCases.push((cases_male / cases_gender_total).toFixed());
		dataGenderCases.push((cases_female / cases_gender_total).toFixed());
		dataGenderCases.push((cases_unknown_gender / cases_gender_total).toFixed());
		labelsGenderCases.push('Male');
		labelsGenderCases.push('Female');
		labelsGenderCases.push('Unknown');

		dataAgeCases.push((cases_age_0_17 / cases_age_total).toFixed());
		dataAgeCases.push((cases_age_18_29 / cases_age_total).toFixed());
		dataAgeCases.push((cases_age_30_39 / cases_age_total).toFixed());
		dataAgeCases.push((cases_age_40_49 / cases_age_total).toFixed());
		dataAgeCases.push((cases_age_50_59 / cases_age_total).toFixed());
		dataAgeCases.push((cases_age_60_69 / cases_age_total).toFixed());
		dataAgeCases.push((cases_age_70_79 / cases_age_total).toFixed());
		dataAgeCases.push((cases_age_80_ / cases_age_total).toFixed());
		dataAgeCases.push((cases_age_unknown / cases_age_total).toFixed());
		labelsAgeCases.push('0 - 17');
		labelsAgeCases.push('18 - 29');
		labelsAgeCases.push('30 - 39');
		labelsAgeCases.push('40 - 49');
		labelsAgeCases.push('50 - 59');
		labelsAgeCases.push('60 - 69');
		labelsAgeCases.push('70 - 79');
		labelsAgeCases.push('80+');
		labelsAgeCases.push('Unknown');

		dataEthnicityCases.push((cases_asian_non_latinx / cases_ethnicity_total).toFixed());
		dataEthnicityCases.push((cases_black_non_latinx / cases_ethnicity_total).toFixed());
		dataEthnicityCases.push((cases_latinx / cases_ethnicity_total).toFixed());
		dataEthnicityCases.push((cases_other_non_latinx / cases_ethnicity_total).toFixed());
		dataEthnicityCases.push((cases_white_non_latinx / cases_ethnicity_total).toFixed());
		dataEthnicityCases.push((cases_unknown_race_eth / cases_ethnicity_total).toFixed());
		labelsEthnicityCases.push('Asian');
		labelsEthnicityCases.push('Black');
		labelsEthnicityCases.push('LatinX');
		labelsEthnicityCases.push('Other');
		labelsEthnicityCases.push('White');
		labelsEthnicityCases.push('Unknown');

		dataGenderDeaths.push((deaths_male / deaths_gender_total).toFixed());
		dataGenderDeaths.push((deaths_female / deaths_gender_total).toFixed());
		dataGenderDeaths.push((deaths_unknown_gender / deaths_gender_total).toFixed());
		labelsGenderDeaths.push('Male');
		labelsGenderDeaths.push('Female');
		labelsGenderDeaths.push('Unknown');

		dataAgeDeaths.push((deaths_0_17_yrs / deaths_age_total).toFixed());
		dataAgeDeaths.push((deaths_18_29_yrs / deaths_age_total).toFixed());
		dataAgeDeaths.push((deaths_30_39_yrs / deaths_age_total).toFixed());
		dataAgeDeaths.push((deaths_40_49_yrs / deaths_age_total).toFixed());
		dataAgeDeaths.push((deaths_50_59_yrs / deaths_age_total).toFixed());
		dataAgeDeaths.push((deaths_60_69_yrs / deaths_age_total).toFixed());
		dataAgeDeaths.push((deaths_70_79_yrs / deaths_age_total).toFixed());
		dataAgeDeaths.push((deaths_80_yrs / deaths_age_total).toFixed());
		dataAgeDeaths.push((deaths_unknown_age / deaths_age_total).toFixed());
		labelsAgeDeaths.push('0 - 17');
		labelsAgeDeaths.push('18 - 29');
		labelsAgeDeaths.push('30 - 39');
		labelsAgeDeaths.push('40 - 49');
		labelsAgeDeaths.push('50 - 59');
		labelsAgeDeaths.push('60 - 69');
		labelsAgeDeaths.push('70 - 79');
		labelsAgeDeaths.push('80+');
		labelsAgeDeaths.push('Unknown');

		dataEthnicityDeaths.push((deaths_asian_non_latinx / deaths_ethnicity_total).toFixed());
		dataEthnicityDeaths.push((deaths_black_non_latinx / deaths_ethnicity_total).toFixed());
		dataEthnicityDeaths.push((deaths_latinx / deaths_ethnicity_total).toFixed());
		dataEthnicityDeaths.push((deaths_other_non_latinx / deaths_ethnicity_total).toFixed());
		dataEthnicityDeaths.push((deaths_white_non_latinx / deaths_ethnicity_total).toFixed());
		dataEthnicityDeaths.push((deaths_unknown_race_eth / deaths_ethnicity_total).toFixed());
		labelsEthnicityDeaths.push('Asian');
		labelsEthnicityDeaths.push('Black');
		labelsEthnicityDeaths.push('LatinX');
		labelsEthnicityDeaths.push('Other');
		labelsEthnicityDeaths.push('White');
		labelsEthnicityDeaths.push('Unknown');

		console.log('cases_male', cases_male);
		console.log('cases_female', cases_female);
		console.log('cases_unknown_gender', cases_unknown_gender);

		console.log('cases_age_0_17', cases_age_0_17);
		console.log('cases_age_18_29', cases_age_18_29);
		console.log('cases_age_30_39', cases_age_30_39);
		console.log('cases_age_40_49', cases_age_40_49);
		console.log('cases_age_50_59', cases_age_50_59);
		console.log('cases_age_60_69', cases_age_60_69);
		console.log('cases_age_70_79', cases_age_70_79);
		console.log('cases_age_80_', cases_age_80_);
		console.log('cases_age_unknown', cases_age_unknown);

		console.log('cases_gender_total',cases_gender_total );
		console.log('cases_age_total', cases_age_total);

		// var cases_ethnicity_total = 0;

		// var deaths_gender_total  = 0;
		// var deaths_age_total = 0;
		// var deaths_ethnicity_total = 0;

		// var cases_asian_non_latinx = 0;
		// var cases_black_non_latinx = 0;
		// var cases_latinx = 0;
		// var cases_other_non_latinx = 0;
		// var cases_white_non_latinx = 0;
		// var cases_unknown_race_eth = 0;

		// var deaths_male = 0;
		// var deaths_female = 0;
		// var deaths_unknown_gender = 0;

		// var deaths_0_17_yrs = 0;
		// var deaths_18_29_yrs = 0;
		// var deaths_30_39_yrs = 0;
		// var deaths_40_49_yrs = 0;
		// var deaths_50_59_yrs = 0;
		// var deaths_60_69_yrs = 0;
		// var deaths_70_79_yrs = 0;
		// var deaths_80_yrs = 0;
		// var deaths_unknown_age = 0;

		// var deaths_asian_non_latinx = 0;
		// var deaths_black_non_latinx = 0;
		// var deaths_latinx = 0;
		// var deaths_other_non_latinx = 0;
		// var deaths_white_non_latinx = 0;
		// var deaths_unknown_race_eth = 0;

		// var cases_gender_total = 0;
		// var cases_age_total = 0;
		// var cases_ethnicity_total = 0;

		// var deaths_gender_total  = 0;
		// var deaths_age_total = 0;
		// var deaths_ethnicity_total = 0;

		console.log('dataGenderCases', dataGenderCases);
		console.log('dataGenderCases', dataGenderCases);
		console.log('dataEthnicityCases', dataEthnicityCases);

		console.log('dataGenderDeaths', dataGenderDeaths);
		console.log('dataAgeDeaths', dataAgeDeaths);
		console.log('dataEthnicityDeaths', dataEthnicityDeaths);

		this.setState({
			// Line
			chartTotalCasesLine: {
				labels: labelDates,
				datasets: datasetsTotalCasesLine
			},
			chartPercentCasesLine: {
				labels: labelDates,
				datasets: datasetsPercentCasesLine
			},
			chartDailyCasesLine: {
				labels: labelDates,
				datasets: datasetsDailyCasesLine
			},
			chartSevenDayAverageCasesLine: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageCasesLine
			},
			chartTotalDeathsLine: {
				labels: labelDates,
				datasets: datasetsTotalDeathsLine
			},
			chartPercentDeathsLine: {
				labels: labelDates,
				datasets: datasetsPercentDeathsLine
			},
			chartDailyDeathsLine: {
				labels: labelDates,
				datasets: datasetsDailyDeathsLine
			},
			chartSevenDayAverageDeathsLine: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageDeathsLine
			},

			// Bar
			chartTotalCasesBar: {
				labels: labelDates,
				datasets: datasetsTotalCasesBar
			},
			chartPercentCasesBar: {
				labels: labelDates,
				datasets: datasetsPercentCasesBar
			},
			chartDailyCasesBar: {
				labels: labelDates,
				datasets: datasetsDailyCasesBar
			},
			chartSevenDayAverageCasesBar: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageCasesBar
			},
			chartTotalDeathsBar: {
				labels: labelDates,
				datasets: datasetsTotalDeathsBar
			},
			chartPercentDeathsBar: {
				labels: labelDates,
				datasets: datasetsPercentDeathsBar
			},
			chartDailyDeathsBar: {
				labels: labelDates,
				datasets: datasetsDailyDeathsBar
			},
			chartSevenDayAverageDeathsBar: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageDeathsBar
			},

			chartGenderCases: {
				labels: labelsGenderCases,
        datasets: [
          {
            label: 'Cases by Gender',
            data: dataGenderCases,
           	backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1']
          }
        ]
			},
			chartAgeCases: {
				labels: labelsAgeCases,
        datasets: [
          {
            label: 'Cases by Age',
            data: dataAgeCases,
           	backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1', '#d1b986', '#9195cf', '#c969b1', '#666', '#eb4034', '#9b34eb']
          }
        ]
			},
			chartEthnicityCases: {
				labels: labelsEthnicityCases,
        datasets: [
          {
            label: 'Cases by Ethnicity',
            data: dataEthnicityCases,
           	backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1', '#d1b986', '#9195cf', '#c969b1']
          }
        ]
			},

			chartGenderDeaths: {
				labels: labelsGenderDeaths,
        datasets: [
          {
            label: 'Deaths by Gender',
            data: dataGenderDeaths,
           	backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1']
          }
        ]
			},
			chartAgeDeaths: {
				labels: labelsAgeDeaths,
        datasets: [
          {
            label: 'Deaths by Age',
            data: dataAgeDeaths,
           	backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1', '#d1b986', '#9195cf', '#c969b1', '#666', '#eb4034', '#9b34eb']
          }
        ]
			},
			chartEthnicityDeaths: {
				labels: labelsEthnicityDeaths,
        datasets: [
          {
            label: 'Deaths by Ethnicity',
            data: dataEthnicityDeaths,
           	backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1', '#d1b986', '#9195cf', '#c969b1']
          }
        ]
			},

			showChart: true
		});
	}

	handleDisplayTotal = () => {
		this.setState({
			showTotal: true,
			showPercent:  false,
			showDaily: false
		});
	}

	handleDisplayPercent = () => {
		this.setState({
			showTotal: false,
			showPercent:  true,
			showDaily: false
		});
	}

	handleDisplayDaily = () => {
		this.setState({
			showTotal: false,
			showPercent:  false,
			showDaily: true
		});
	}

	handleDisplayCases = () => {
		this.setState({
			showCases: true,
			showDeaths: false,
		});
	}

	handleDisplayDeaths = () => {
		this.setState({
			showCases: false,
			showDeaths: true,
		});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	getDatesList = () => {
		const { startDate, endDate } = this.state;
		var listDate = [];
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		this.setState({
			datesArray: listDate
		});
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();

		// today
		// var dd = String(date.getDate()).padStart(2, '0');
		// var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
		// var yyyy = date.getFullYear();
		// var today = yyyy + '-' + mm + '-' + dd;

		// yesterday
		var newDate = date.setDate(date.getDate() -1);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		this.setState({
			todaysDate: today,
			endDate: today
		});
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.props.getChicagoData();
		this.getTodaysDate();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.state.todaysDate !== '' && lastState.todaysDate === '') {
			this.getDatesList();
		}

		if (this.state.triggerChartDisplay === true && lastState.triggerChartDisplay === false) {
			this.setState({
				triggerChartDisplay: false
			});
			this.setDisplayChart();
		}

		if (this.state.importColorsListCitiesCHI !== null && lastState.importColorsListCitiesCHI === null) {
			const colorsList = [];
			this.state.importColorsListCitiesCHI.map(datum => {
				colorsList.push(datum);
			});
			this.setState({
				colorsList: colorsList
			});
		}

		if (this.props.loadingDataChicago === false && (lastProps.loadingDataChicago === true || lastProps.loadingDataChicago === undefined)) {
			const { dataChicago } = this.props;
			// console.log('Data Chicago: ',  dataChicago);
			const { colorsList, datesArray, population } = this.state;
			const data = [];
			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, daily_cases: 0, sevenDayAverageCases: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, cases_age_0_17: 0, cases_age_18_29: 0, cases_age_30_39: 0, cases_age_40_49: 0, cases_age_50_59: 0,  cases_age_60_69: 0, cases_age_70_79: 0, cases_age_80_: 0, cases_age_unknown: 0, cases_asian_non_latinx: 0, cases_black_non_latinx: 0, cases_female: 0, cases_latinx: 0, cases_male: 0, cases_other_non_latinx: 0, cases_unknown_gender: 0, cases_unknown_race_eth: 0, cases_white_non_latinx: 0, deaths_0_17_yrs: 0, deaths_18_29_yrs: 0, deaths_30_39_yrs: 0, deaths_40_49_yrs: 0, deaths_50_59_yrs: 0, deaths_60_69_yrs: 0, deaths_70_79_yrs: 0, deaths_80_yrs: 0, deaths_asian_non_latinx: 0, deaths_black_non_latinx: 0, deaths_female: 0, deaths_latinx: 0, deaths_male: 0, deaths_other_non_latinx: 0, deaths_total: 0, deaths_unknown_age: 0, deaths_unknown_gender: 0, deaths_unknown_race_eth: 0, deaths_white_non_latinx: 0})
			}

			var total_cases = 0;
			var total_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				dataChicago.map(datum => {
					if (datum.lab_report_date !== undefined) {
						if (data[i].date == datum.lab_report_date.split('T')[0]) {
							data[i].daily_cases = parseInt(datum.cases_total);
							data[i].daily_deaths = parseInt(datum.deaths_total);
							total_cases += parseInt(datum.cases_total);
							total_deaths += parseInt(datum.deaths_total);
							data[i].total_cases = total_cases;
							data[i].total_deaths = total_deaths;
							data[i].percent_cases = (total_cases / population * 100).toFixed(5);
							data[i].percent_deaths = (total_deaths / population * 100).toFixed(5);
						}
					}
				})
			}

			if (data[data.length - 1].total_cases == 0) {
				data[data.length - 1].total_cases = data[data.length -2].total_cases;
			}
			if (data[data.length - 1].percent_cases == 0) {
				data[data.length - 1].percent_cases = data[data.length -2].percent_cases;
			}
			if (data[data.length -1].total_deaths == 0) {
				data[data.length -1].total_deaths = data[data.length -2].total_deaths;
			}
			if (data[data.length -1].percent_deaths == 0) {
				data[data.length -1].percent_deaths = data[data.length -2].percent_deaths;
			}

			var seven_day_count = 0;
			var seven_day_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += data[i].daily_cases;
				seven_day_deaths += data[i].daily_deaths;
				if (i < 7) {
					data[i].sevenDayAverageCases = (seven_day_count / (i + 1)).toFixed(5);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / (i + 1)).toFixed(5);
				}
				else {
					seven_day_count -= data[i - 7].daily_cases;
					seven_day_deaths -= data[i - 7].daily_deaths;
					data[i].sevenDayAverageCases = (seven_day_count / 7).toFixed(5);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / 7).toFixed(5);
				}
			}

			var cases_male = 0;
			var cases_female = 0;
			var cases_unknown_gender = 0;

			var cases_age_0_17 = 0;
			var cases_age_18_29 = 0;
			var cases_age_30_39 = 0;
			var cases_age_40_49 = 0;
			var cases_age_50_59 = 0;
			var cases_age_60_69 = 0;
			var cases_age_70_79 = 0;
			var cases_age_80_ = 0;
			var cases_age_unknown = 0;

			var cases_asian_non_latinx = 0;
			var cases_black_non_latinx = 0;
			var cases_latinx = 0;
			var cases_other_non_latinx = 0;
			var cases_white_non_latinx = 0;
			var cases_unknown_race_eth = 0;

			var deaths_male = 0;
			var deaths_female = 0;
			var deaths_unknown_gender = 0;

			var deaths_0_17_yrs = 0;
			var deaths_18_29_yrs = 0;
			var deaths_30_39_yrs = 0;
			var deaths_40_49_yrs = 0;
			var deaths_50_59_yrs = 0;
			var deaths_60_69_yrs = 0;
			var deaths_70_79_yrs = 0;
			var deaths_80_yrs = 0;
			var deaths_unknown_age = 0;

			var deaths_asian_non_latinx = 0;
			var deaths_black_non_latinx = 0;
			var deaths_latinx = 0;
			var deaths_other_non_latinx = 0;
			var deaths_white_non_latinx = 0;
			var deaths_unknown_race_eth = 0;

			for (var i =0; i < data.length; i++) {
				dataChicago.map(datum => {
					if (datum.lab_report_date !== undefined) {
						if (data[i].date == datum.lab_report_date.split('T')[0]) {
							cases_male += parseInt(datum.cases_male);
							cases_female += parseInt(datum.cases_female);
							cases_unknown_gender += parseInt(datum.cases_unknown_gender);

							cases_age_0_17 += parseInt(datum.cases_age_0_17);
							cases_age_18_29 += parseInt(datum.cases_age_18_29);
							cases_age_30_39 += parseInt(datum.cases_age_30_39);
							cases_age_40_49 += parseInt(datum.cases_age_40_49);
							cases_age_50_59 += parseInt(datum.cases_age_50_59);
							cases_age_60_69 += parseInt(datum.cases_age_60_69);
							cases_age_70_79 += parseInt(datum.cases_age_70_79);
							cases_age_80_ += parseInt(datum.cases_age_80_);
							cases_age_unknown += parseInt(datum.cases_age_unknown);

							cases_asian_non_latinx += parseInt(datum.cases_asian_non_latinx);
							cases_black_non_latinx += parseInt(datum.cases_black_non_latinx);
							cases_latinx += parseInt(datum.cases_latinx);
							cases_other_non_latinx += parseInt(datum.cases_other_non_latinx);
							cases_white_non_latinx += parseInt(datum.cases_white_non_latinx);
							cases_unknown_race_eth += parseInt(datum.cases_unknown_race_eth);

							deaths_male += parseInt(datum.deaths_male);
							deaths_female += parseInt(datum.deaths_female);
							deaths_unknown_gender += parseInt(datum.deaths_unknown_gender);

							deaths_0_17_yrs += parseInt(datum.deaths_0_17_yrs);
							deaths_18_29_yrs += parseInt(datum.deaths_18_29_yrs);
							deaths_30_39_yrs += parseInt(datum.deaths_30_39_yrs);
							deaths_40_49_yrs += parseInt(datum.deaths_40_49_yrs);
							deaths_50_59_yrs += parseInt(datum.deaths_50_59_yrs);
							deaths_60_69_yrs += parseInt(datum.deaths_60_69_yrs);
							deaths_70_79_yrs += parseInt(datum.deaths_70_79_yrs);
							deaths_80_yrs += parseInt(datum.deaths_80_yrs);
							deaths_unknown_age += parseInt(datum.deaths_unknown_age);

							deaths_asian_non_latinx += parseInt(datum.deaths_asian_non_latinx);
							deaths_black_non_latinx += parseInt(datum.deaths_black_non_latinx);
							deaths_latinx += parseInt(datum.deaths_latinx);
							deaths_other_non_latinx += parseInt(datum.deaths_other_non_latinx);
							deaths_white_non_latinx += parseInt(datum.deaths_white_non_latinx);
							deaths_unknown_race_eth += parseInt(datum.deaths_unknown_race_eth);	

							data[i].cases_male += cases_male;
							data[i].cases_female += cases_female;
							data[i].cases_unknown_gender += cases_unknown_gender;

							data[i].cases_age_0_17 += cases_age_0_17;
							data[i].cases_age_18_29 += cases_age_18_29;
							data[i].cases_age_30_39 += cases_age_30_39;
							data[i].cases_age_40_49 += cases_age_40_49;
							data[i].cases_age_50_59 += cases_age_50_59;
							data[i].cases_age_60_69 += cases_age_60_69;
							data[i].cases_age_70_79 += cases_age_70_79;
							data[i].cases_age_80_ += cases_age_80_;
							data[i].cases_age_unknown += cases_age_unknown;

							data[i].cases_asian_non_latinx += cases_asian_non_latinx;
							data[i].cases_black_non_latinx += cases_black_non_latinx;
							data[i].cases_latinx += cases_latinx;
							data[i].cases_other_non_latinx += cases_other_non_latinx;
							data[i].cases_white_non_latinx += cases_white_non_latinx;
							data[i].cases_unknown_race_eth += cases_unknown_race_eth;

							data[i].deaths_male += deaths_male;
							data[i].deaths_female += deaths_female;
							data[i].deaths_unknown_gender += deaths_unknown_gender;

							data[i].deaths_0_17_yrs += deaths_0_17_yrs;
							data[i].deaths_18_29_yrs += deaths_18_29_yrs;
							data[i].deaths_30_39_yrs += deaths_30_39_yrs;
							data[i].deaths_40_49_yrs += deaths_40_49_yrs;
							data[i].deaths_50_59_yrs += deaths_50_59_yrs;
							data[i].deaths_60_69_yrs += deaths_60_69_yrs;
							data[i].deaths_70_79_yrs += deaths_70_79_yrs;
							data[i].deaths_80_yrs += deaths_80_yrs;
							data[i].deaths_unknown_age += deaths_unknown_age;

							data[i].deaths_asian_non_latinx += deaths_asian_non_latinx;
							data[i].deaths_black_non_latinx += deaths_black_non_latinx;
							data[i].deaths_latinx += deaths_latinx;
							data[i].deaths_other_non_latinx += deaths_other_non_latinx;
							data[i].deaths_white_non_latinx += deaths_white_non_latinx;
							data[i].deaths_unknown_race_eth += deaths_unknown_race_eth;							
						}
					}
				});
			}

			this.setState({
				showChart: true,
				data: data,
				triggerChartDisplay: true
			});
		}
	}

	componentWillUnmount() {
		this.props.clearChicagoData();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { showChart } = this.state;
		const { showTotal, showPercent, showDaily, showCases, showDeaths, showSevenDayAverage, showDailyCount } = this.state;
		const { showAxisX, chartHeight, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons, displayDailyAverageButtons } = this.state;
		const { doNotDisplay } = this.state;

		return (
			<div>
				{this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} showSevenDayAverage={showSevenDayAverage} showDailyCount={showDailyCount} displaySevenDayAverage={this.handleShowSevenDayAverage} displayDailyCount={this.handleShowDailyCount} />
        }
        {this.state.showSettingsModal && <ModalBackdrop />}
        {showChart ? 
        	<div>
        		<Grid container>
							<Grid item xs={4} style={styles.chartTypeButtonContainer}>
								<Button style={showTotal ? styles.chartTotalPercentDailyButtonSelected : styles.chartTotalPercentDailyButton} onClick={this.handleDisplayTotal}>Total</Button>
							</Grid>
							<Grid item xs={4} style={styles.chartTypeButtonContainer}>
								<Button style={showPercent ? styles.chartTotalPercentDailyButtonSelected : styles.chartTotalPercentDailyButton} onClick={this.handleDisplayPercent}>Percent</Button>
							</Grid>
							<Grid item xs={4} style={styles.chartTypeButtonContainer}>
								<Button style={showDaily ? styles.chartTotalPercentDailyButtonSelected : styles.chartTotalPercentDailyButton} onClick={this.handleDisplayDaily}>Daily</Button>
							</Grid>
						</Grid>
        		{showCases && 
        			<div>
        				{showTotal && 
        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        						{showLineGraph && 
        							<Line 
					          		data={this.state.chartTotalCasesLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Cases',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                        } else {
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                        }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
				                        callback: function(value, index, values) {
				                          if(parseInt(value) >= 1000){
				                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                          } else {
				                            return value;
				                          }
				                        }
				                       }, 
				                       gridLines: {
				                       	display:false, 
				                       	drawBorder: false
				                       } 
				                     }
				                   ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
        						}
        						{showBarGraph && 
        							<Bar
												data={this.state.chartTotalCasesBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: 'Total Cases',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                        } else {
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                        }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
				                        callback: function(value, index, values) {
				                          if(parseInt(value) >= 1000){
				                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                          } else {
				                            return value;
				                          }
				                        }
				                       }, 
				                       stacked: true, 
				                       gridLines: {
				                       	display:false, 
				                       	drawBorder: false
				                       } 
				                     }
				                   ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
        						}
        					</div>
        				}
        				{showPercent && 
        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        						{showLineGraph && 
        							<Line 
					          		data={this.state.chartPercentCasesLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Percent Cases',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                        } else {
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                        }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
				                        callback: function(value, index, values) {
				                          if(parseInt(value) >= 1000){
				                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                          } else {
				                            return value;
				                          }
				                        }
				                       }, 
				                       gridLines: {
				                       	display:false, 
				                       	drawBorder: false
				                       } 
				                     }
				                   ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
        						}
        						{showBarGraph && 
        							<Bar
												data={this.state.chartPercentCasesBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: 'Percent Cases',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                        } else {
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                        }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
				                        callback: function(value, index, values) {
				                          if(parseInt(value) >= 1000){
				                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                          } else {
				                            return value;
				                          }
				                        }
				                       }, 
				                       stacked: true, 
				                       gridLines: {
				                       	display:false, 
				                       	drawBorder: false
				                       } 
				                     }
				                   ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
        						}
        					</div>
        				}
        				{showDaily && 
        					<div>
        						{showDailyCount && 
        							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        								{showLineGraph && 
        									<Line 
							          		data={this.state.chartDailyCasesLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Daily Cases',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                        } else {
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                        }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
						                        callback: function(value, index, values) {
						                          if(parseInt(value) >= 1000){
						                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                          } else {
						                            return value;
						                          }
						                        }
						                       }, 
						                       gridLines: {
						                       	display:false, 
						                       	drawBorder: false
						                       } 
						                     }
						                   ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
        								}
        								{showBarGraph && 
        									<Bar
														data={this.state.chartDailyCasesBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Daily Cases',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                        } else {
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                        }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
						                        callback: function(value, index, values) {
						                          if(parseInt(value) >= 1000){
						                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                          } else {
						                            return value;
						                          }
						                        }
						                       }, 
						                       stacked: true, 
						                       gridLines: {
						                       	display:false, 
						                       	drawBorder: false
						                       } 
						                     }
						                   ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
        								}
        							</div>
        						}
        						{showSevenDayAverage && 
        							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        								{showLineGraph && 
        									<Line 
							          		data={this.state.chartSevenDayAverageCasesLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: '7 Day Average Cases',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                        } else {
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                        }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
						                        callback: function(value, index, values) {
						                          if(parseInt(value) >= 1000){
						                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                          } else {
						                            return value;
						                          }
						                        }
						                       }, 
						                       gridLines: {
						                       	display:false, 
						                       	drawBorder: false
						                       } 
						                     }
						                   ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
        								}
        								{showBarGraph && 
        									<Bar
														data={this.state.chartSevenDayAverageCasesBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: '7 Day Average Cases',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                        } else {
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                        }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
						                        callback: function(value, index, values) {
						                          if(parseInt(value) >= 1000){
						                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                          } else {
						                            return value;
						                          }
						                        }
						                       }, 
						                       stacked: true, 
						                       gridLines: {
						                       	display:false, 
						                       	drawBorder: false
						                       } 
						                     }
						                   ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
        								}
        							</div>
        						}
        					</div>
        				}
        			</div>
        		}
        		{showDeaths && 
        			<div>
        				{showTotal && 
        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        						{showLineGraph && 
        							<Line 
					          		data={this.state.chartTotalDeathsLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Deaths',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                        } else {
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                        }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
				                        callback: function(value, index, values) {
				                          if(parseInt(value) >= 1000){
				                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                          } else {
				                            return value;
				                          }
				                        }
				                       }, 
				                       gridLines: {
				                       	display:false, 
				                       	drawBorder: false
				                       } 
				                     }
				                   ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
        						}
        						{showBarGraph && 
        							<Bar
												data={this.state.chartTotalDeathsBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: 'Total Cases',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                        } else {
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                        }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
				                        callback: function(value, index, values) {
				                          if(parseInt(value) >= 1000){
				                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                          } else {
				                            return value;
				                          }
				                        }
				                       }, 
				                       stacked: true, 
				                       gridLines: {
				                       	display:false, 
				                       	drawBorder: false
				                       } 
				                     }
				                   ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
        						}
        					</div>
        				}
        				{showPercent && 
        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        						{showLineGraph && 
        							<Line 
					          		data={this.state.chartPercentDeathsLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Percent Deaths',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                        } else {
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                        }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
				                        callback: function(value, index, values) {
				                          if(parseInt(value) >= 1000){
				                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                          } else {
				                            return value;
				                          }
				                        }
				                       }, 
				                       gridLines: {
				                       	display:false, 
				                       	drawBorder: false
				                       } 
				                     }
				                   ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
        						}
        						{showBarGraph && 
        							<Bar
												data={this.state.chartPercentDeathsBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: 'Total Cases',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                        } else {
				                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                        }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
				                        callback: function(value, index, values) {
				                          if(parseInt(value) >= 1000){
				                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                          } else {
				                            return value;
				                          }
				                        }
				                       }, 
				                       stacked: true, 
				                       gridLines: {
				                       	display:false, 
				                       	drawBorder: false
				                       } 
				                     }
				                   ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
        						}
        					</div>
        				}
        				{showDaily && 
        					<div>
        						{showDailyCount && 
        							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        								{showLineGraph && 
        									<Line 
							          		data={this.state.chartDailyDeathsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Daily Deaths',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                        } else {
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                        }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
						                        callback: function(value, index, values) {
						                          if(parseInt(value) >= 1000){
						                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                          } else {
						                            return value;
						                          }
						                        }
						                       }, 
						                       gridLines: {
						                       	display:false, 
						                       	drawBorder: false
						                       } 
						                     }
						                   ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
        								}
        								{showBarGraph && 
        									<Bar
														data={this.state.chartSevenDayAverageDeathsLine}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Daily Deaths',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                        } else {
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                        }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
						                        callback: function(value, index, values) {
						                          if(parseInt(value) >= 1000){
						                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                          } else {
						                            return value;
						                          }
						                        }
						                       }, 
						                       stacked: true, 
						                       gridLines: {
						                       	display:false, 
						                       	drawBorder: false
						                       } 
						                     }
						                   ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
        								}
        							</div>
        						}
        						{showSevenDayAverage && 
        							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
        								{showLineGraph && 
        									<Line 
							          		data={this.state.chartSevenDayAverageDeathsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: '7 Day Average Deaths',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                        } else {
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                        }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
						                        callback: function(value, index, values) {
						                          if(parseInt(value) >= 1000){
						                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                          } else {
						                            return value;
						                          }
						                        }
						                       }, 
						                       gridLines: {
						                       	display:false, 
						                       	drawBorder: false
						                       } 
						                     }
						                   ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
        								}
        								{showBarGraph && 
        									<Bar
														data={this.state.chartSevenDayAverageDeathsBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: '7 Day Average Deaths',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                        } else {
						                          return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                        }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
						                        callback: function(value, index, values) {
						                          if(parseInt(value) >= 1000){
						                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                          } else {
						                            return value;
						                          }
						                        }
						                       }, 
						                       stacked: true, 
						                       gridLines: {
						                       	display:false, 
						                       	drawBorder: false
						                       } 
						                     }
						                   ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
        								}
        							</div>
        						}
        					</div>
        				}
        			</div>
        		}
        		<Grid container>
        			<Grid item xs={6} style={styles.chartTypeButtonContainer}>
        				<Button style={showCases ? styles.chartCasesDeathsTestsButtonSelected : styles.chartCasesDeathsTestsButton} onClick={this.handleDisplayCases}>Cases</Button>
        			</Grid>
        			<Grid item xs={6} style={styles.chartTypeButtonContainer}>
        				<Button style={showDeaths ? styles.chartCasesDeathsTestsButtonSelected : styles.chartCasesDeathsTestsButton} onClick={this.handleDisplayDeaths}>Deaths</Button>
        			</Grid>
        		</Grid>
        		{doNotDisplay && 
        			<div>
		        		<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
			          	<Pie 
			          		data={this.state.chartGenderCases}
			          		options={{
			          			title: {
		                    display: this.props.displayTitle,
		                    text: 'Cases by Gender',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  maintainAspectRatio: false
			          		}}
			          	/>
			          </div>
			          <div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
			          	<Pie 
			          		data={this.state.chartAgeCases}
			          		options={{
			          			title: {
		                    display: this.props.displayTitle,
		                    text: 'Cases by Age',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  maintainAspectRatio: false
			          		}}
			          	/>
			          </div>
			          <div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
			          	<Pie 
			          		data={this.state.chartEthnicityCases}
			          		options={{
			          			title: {
		                    display: this.props.displayTitle,
		                    text: 'Cases by Ethnicity',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  maintainAspectRatio: false
			          		}}
			          	/>
			          </div>
			          <div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
			          	<Pie 
			          		data={this.state.chartGenderDeaths}
			          		options={{
			          			title: {
		                    display: this.props.displayTitle,
		                    text: 'Deaths by Gender',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  maintainAspectRatio: false
			          		}}
			          	/>
			          </div>
			          <div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
			          	<Pie 
			          		data={this.state.chartAgeDeaths}
			          		options={{
			          			title: {
		                    display: this.props.displayTitle,
		                    text: 'Deaths by Age',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  maintainAspectRatio: false
			          		}}
			          	/>
			          </div>
			          <div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
			          	<Pie 
			          		data={this.state.chartEthnicityDeaths}
			          		options={{
			          			title: {
		                    display: this.props.displayTitle,
		                    text: 'Deaths by Ethnicity',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  maintainAspectRatio: false
			          		}}
			          	/>
			          </div>
		          </div>
		        }
        	</div> :
        	<div>
        		Loading Data...
        	</div>
        }
        <div style={styles.sourceURLContainer}>
					<a style={styles.sourceURL} target="_blank" href="https://data.cityofchicago.org/resource/naz8-j4nc.json"><span style={styles.sourceURLText}>Source: data.cityofchicago.org</span></a>
				</div>
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		importColorsListCitiesCHI: state.importData.importColorsListCitiesCHI, 

		dataChicago: state.cities.dataChicago,
		loadingDataChicago: state.cities.loadingDataChicago
	}
}

export default connect(mapStateToProps, { getChicagoData, clearChicagoData })(Chicago);

const styles = {
	chartTotalPercentDailyButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartTotalPercentDailyButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartTypeButtonsContainer: {
		display: 'flex',
		flexDirection: 'row',
		flex: 1,
		justifyContent: 'center'
	},
	chartTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		padding: 10
	},
	chartTypeButton: {
		backgroundColor: '#32a852',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartCasesDeathsTestsButton: {
		backgroundColor: '#32a852',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartCasesDeathsTestsButtonSelected:  {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	controlTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	controlTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	locationSelectButton: {
		backgroundColor: '#FFF',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	locationSelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	locationSelectButtonsContainer: {
		display: 'flex',
		flex: 1, 
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		marginTop: 20
	},
	locationSelectButtonContainer: {
		margin: 5
	},
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	}
}