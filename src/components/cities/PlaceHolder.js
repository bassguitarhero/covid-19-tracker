import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getPlaceHolderDataSet } from '../../actions/cities';

class PlaceHolder extends Component {
	componentDidMount() {
		this.props.getPlaceHolderDataSet();
	}

	componentDidUpdate(lastProps) {
		if (this.props.loadingDataPlaceHolder === false && lastProps.loadingDataPlaceHolder) {
			console.log('Place Holder: ', this.props.dataPlaceHolder);
		}
	}

	render() {
		return (
			<div>
				Place Holder
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dataPlaceHolder: state.cities.dataPlaceHolder,
		loadingDataPlaceHolder: state.cities.loadingDataPlaceHolder
	}
}

export default connect(mapStateToProps, { getPlaceHolderDataSet })(PlaceHolder);

const styles = {

}