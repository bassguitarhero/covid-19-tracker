import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

import SettingsModal from '../SettingsModal';
import ModalBackdrop from '../ModalBackdrop';
import SettingsModalIcon from '../SettingsModalIcon';

import { getNYCCaseData, clearNYCCaseData } from '../../actions/cities';

class NYC extends Component {
	constructor(props) {
		super(props);
		this.state = {
			nycTotalCasesChart: {},
			nycPopulationPercentCasesChart: {},
			nycTotalTestsChart: {},
			nycNewPositivesChart: {},
			nycDailyTestsChart: {},
			nycTotalPercentTestsChart: {}, 
			nycPopulation: 8399000,
			nycBoroughTotalChart: {},
			nycBoroughDailyChart: {},
			nycBoroughBarChart: {},
			boroughNewYork: '#D6E9C6',
			boroughQueens: '#FAEBCC',
			boroughKings: '#EBCCD1',
			boroughBronx: '#d1b986',
			boroughRichmond: '#9195cf',
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			todaysDate: '',
			datesArray: [],
			showLineGraph: true,
			showBarGraph: false, 
			displayLineBarButtons: false,
			displayDailyAverageButtons: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom',
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	getDatesList = () => {
		var listDate = [];
		var listDateFormatted = [];
		var startDate = '2020-03-02';
		var endDate = this.state.todaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		this.setState({
			datesArray: listDate
		});
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();
		var newDate = date.setDate(date.getDate() -1);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		this.setState({
			todaysDate: today
		});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	formatDate = (s) => {
		var fields = s.split('T');
		var date = fields[0].split('-');
		return date[1]+'/'+date[2];
	}

	setChartNYCCaseData = () => {
		const { dataNYCCases } = this.props;
		const { nycPopulation, datesArray } = this.state;
		const setLabelDates = new Set();
		var totalNewYork = [];
		var dailyNewYork = [];
		var totalQueens = [];
		var dailyQueens = [];
		var totalKings = [];
		var dailyKings = [];
		var totalBronx = [];
		var dailyBronx = [];
		var totalRichmond = [];
		var dailyRichmond = [];
		var boroughNewYorkData = [];
		var boroughQueensData = [];
		var boroughKingsData = [];
		var boroughBronxData = [];
		var boroughRichmondData = [];
		const nyConfirmedData = [];
		for (var i = 0; i < datesArray.length; i++) {
			nyConfirmedData.push({date: datesArray[i], confirmed_count: 0, confirmed_percent: 0, test_count: 0, positive_count: 0, daily_tests: 0, test_percent: 0});
			boroughNewYorkData.push({date: datesArray[i], daily_count: 0, total_count: 0});
			boroughQueensData.push({date: datesArray[i], daily_count: 0, total_count: 0});
			boroughKingsData.push({date: datesArray[i], daily_count: 0, total_count: 0});
			boroughBronxData.push({date: datesArray[i], daily_count: 0, total_count: 0});
			boroughRichmondData.push({date: datesArray[i], daily_count: 0, total_count: 0});
		}
		// console.log('NYC Data: ', dataNYCCases);
		for (var i = 0; i < nyConfirmedData.length; i++) {
			dataNYCCases.map(datum => {
				if (datum.test_date.split('T')[0] == nyConfirmedData[i].date) {
					if (datum.county == "New York" || datum.county == "Queens" || datum.county == "Kings" || datum.county == "Bronx" || datum.county == "Richmond") {
						nyConfirmedData[i].confirmed_count += parseInt(datum.cumulative_number_of_positives);
						nyConfirmedData[i].confirmed_percent += ((parseInt(datum.cumulative_number_of_positives) / nycPopulation) * 100);
						nyConfirmedData[i].test_count += parseInt(datum.cumulative_number_of_tests);
						nyConfirmedData[i].positive_count += parseInt(datum.new_positives);
						nyConfirmedData[i].daily_tests += parseInt(datum.total_number_of_tests);
						nyConfirmedData[i].test_percent += ((parseInt(datum.cumulative_number_of_tests) / nycPopulation) * 100);
					}
					if (datum.county == "New York") {
						boroughNewYorkData[i].daily_count += parseInt(datum.new_positives);
						boroughNewYorkData[i].total_count += parseInt(datum.cumulative_number_of_positives);
					}
					if (datum.county == "Queens") {
						boroughQueensData[i].daily_count += parseInt(datum.new_positives);
						boroughQueensData[i].total_count += parseInt(datum.cumulative_number_of_positives);
					}
					if (datum.county == "Kings") {
						boroughKingsData[i].daily_count += parseInt(datum.new_positives);
						boroughKingsData[i].total_count += parseInt(datum.cumulative_number_of_positives);
					}
					if (datum.county == "Bronx") {
						boroughBronxData[i].daily_count += parseInt(datum.new_positives);
						boroughBronxData[i].total_count += parseInt(datum.cumulative_number_of_positives);
					}
					if (datum.county == "Richmond") {
						boroughRichmondData[i].daily_count += parseInt(datum.new_positives);
						boroughRichmondData[i].total_count += parseInt(datum.cumulative_number_of_positives);
					}
				}
			});
		}
		if (nyConfirmedData[nyConfirmedData.length - 1].confirmed_count == 0) {
			nyConfirmedData[nyConfirmedData.length - 1].confirmed_count = nyConfirmedData[nyConfirmedData.length - 2].confirmed_count;
		}
		if (nyConfirmedData[nyConfirmedData.length - 1].confirmed_percent == 0) {
			nyConfirmedData[nyConfirmedData.length - 1].confirmed_percent = nyConfirmedData[nyConfirmedData.length - 2].confirmed_percent;
		}
		if (nyConfirmedData[nyConfirmedData.length - 1].positive_count == 0) {
			nyConfirmedData[nyConfirmedData.length - 1].positive_count = nyConfirmedData[nyConfirmedData.length - 2].positive_count;
		}
		if (nyConfirmedData[nyConfirmedData.length - 1].test_count == 0) {
			nyConfirmedData[nyConfirmedData.length - 1].test_count = nyConfirmedData[nyConfirmedData.length - 2].test_count;
		}
		if (nyConfirmedData[nyConfirmedData.length - 1].test_percent == 0) {
			nyConfirmedData[nyConfirmedData.length - 1].test_percent = nyConfirmedData[nyConfirmedData.length - 2].test_percent;
		}
		// console.log('NY Confirmed Data: ', nyConfirmedData);
		const label_dates = [];
		const nyc_total_count = [];
		const nyc_total_percent = [];
		const nyc_total_tests = [];
		const nyc_new_positives = [];
		const nyc_daily_tests = [];
		const nyc_test_percent = [];
		var nycTotalCount = 0;
		for (var i = 0; i < nyConfirmedData.length; i++) {
			label_dates.push(this.formatDate(nyConfirmedData[i].date));
			nyc_total_count.push(nyConfirmedData[i].confirmed_count);
			nyc_total_percent.push(nyConfirmedData[i].confirmed_percent.toFixed(2));
			nyc_total_tests.push(nyConfirmedData[i].test_count);
			nyc_new_positives.push(nyConfirmedData[i].positive_count);
			nyc_daily_tests.push(nyConfirmedData[i].daily_tests);
			nyc_test_percent.push(nyConfirmedData[i].test_percent.toFixed(2));
			nycTotalCount = nyConfirmedData[i].confirmed_count;
		}
		const boroughNewYorkDailyCases = [];
		const boroughNewYorkTotalCases = [];
		var boroughNewYorkTotal = 0;
		for (var i = 0; i < boroughNewYorkData.length; i++) {
			boroughNewYorkDailyCases.push(boroughNewYorkData[i].daily_count);
			boroughNewYorkTotalCases.push(boroughNewYorkData[i].total_count);
			boroughNewYorkTotal += parseInt(boroughNewYorkData[i].daily_count);
		}
		const boroughQueensDailyCases = [];
		const boroughQueensTotalCases = [];
		var boroughQueensTotal = 0;
		for (var i = 0; i < boroughQueensData.length; i++) {
			boroughQueensDailyCases.push(boroughQueensData[i].daily_count);
			boroughQueensTotalCases.push(boroughQueensData[i].total_count);
			boroughQueensTotal += parseInt(boroughQueensData[i].daily_count);
		}
		const boroughKingsDailyCases = [];
		const boroughKingsTotalCases = [];
		var boroughKingsTotal = 0;
		for (var i = 0; i < boroughKingsData.length; i++) {
			boroughKingsDailyCases.push(boroughKingsData[i].daily_count);
			boroughKingsTotalCases.push(boroughKingsData[i].total_count);
			boroughKingsTotal += parseInt(boroughKingsData[i].daily_count);
		}
		const boroughBronxDailyCases = [];
		const boroughBronxTotalCases = [];
		var boroughBronxTotal = 0;
		for (var i = 0; i < boroughBronxData.length; i++) {
			boroughBronxDailyCases.push(boroughBronxData[i].daily_count);
			boroughBronxTotalCases.push(boroughBronxData[i].total_count);
			boroughBronxTotal += parseInt(boroughBronxData[i].daily_count);
		}
		const boroughRichmondDailyCases = [];
		const boroughRichmondTotalCases = [];
		var boroughRichmondTotal = 0;
		for (var i = 0; i < boroughRichmondData.length; i++) {
			boroughRichmondDailyCases.push(boroughRichmondData[i].daily_count);
			boroughRichmondTotalCases.push(boroughRichmondData[i].total_count);
			boroughRichmondTotal += parseInt(boroughRichmondData[i].daily_count);
		}
		// if (nyc_total_count[nyc_total_count.length - 1] == 0) {
		// 	nyc_total_count[nyc_total_count.length - 1] = nyc_total_count[nyc_total_count.length - 2]
		// }
		// if (nyc_total_percent[nyc_total_percent.length - 1] == 0) {
		// 	nyc_total_percent[nyc_total_percent.length - 1] = nyc_total_percent[nyc_total_percent.length - 2]
		// }
		// if (nyc_total_tests[nyc_total_tests.length - 1] == 0) {
		// 	nyc_total_tests[nyc_total_tests.length - 1] = nyc_total_tests[nyc_total_tests.length - 2]
		// }
		// if (nyc_test_percent[nyc_test_percent.length - 1] == 0) {
		// 	nyc_test_percent[nyc_test_percent.length - 1] = nyc_test_percent[nyc_test_percent.length - 2]
		// }
		if (boroughNewYorkTotalCases[boroughNewYorkTotalCases.length - 1] == 0) {
			boroughNewYorkTotalCases[boroughNewYorkTotalCases.length - 1] = boroughNewYorkTotalCases[boroughNewYorkTotalCases.length - 2]
		}
		if (boroughKingsTotalCases[boroughKingsTotalCases.length - 1] == 0) {
			boroughKingsTotalCases[boroughKingsTotalCases.length - 1] = boroughKingsTotalCases[boroughKingsTotalCases.length - 2]
		}
		if (boroughQueensTotalCases[boroughQueensTotalCases.length - 1] == 0) {
			boroughQueensTotalCases[boroughQueensTotalCases.length - 1] = boroughQueensTotalCases[boroughQueensTotalCases.length - 2]
		}
		if (boroughBronxTotalCases[boroughBronxTotalCases.length - 1] == 0) {
			boroughBronxTotalCases[boroughBronxTotalCases.length - 1] = boroughBronxTotalCases[boroughBronxTotalCases.length - 2]
		}
		if (boroughRichmondTotalCases[boroughRichmondTotalCases.length - 1] == 0) {
			boroughRichmondTotalCases[boroughRichmondTotalCases.length - 1] = boroughRichmondTotalCases[boroughRichmondTotalCases.length - 2]
		}
		// console.log('Total NYC Cases: ', nycTotalCount)
		this.setState({
			nycTotalCasesChart: {
				labels: label_dates,
      	datasets: [
	      	{
	      		label: 'NYC Cases',
            data: nyc_total_count,
            fill: false,
            borderColor: '#EBCCD1', // red,
						pointRadius: 1,
						lineWidth: 3
	      	},
      	]
			},
			nycPopulationPercentCasesChart: {
				labels: label_dates,
				datasets: [
					{
	      		label: 'NYC Cases',
            data: nyc_total_percent,
            fill: false,
            borderColor: '#EBCCD1', // red
            pointRadius: 1,
						lineWidth: 3
	      	},
				]
			},
			nycTotalTestsChart: {
				labels: label_dates,
				datasets: [
					{
						label: 'NYC Cases',
            data: nyc_total_tests,
            fill: false,
            borderColor: '#EBCCD1', // red
            pointRadius: 1,
						lineWidth: 3
					}
				]
			},
			nycTotalPercentTestsChart: {
				labels: label_dates,
				datasets: [
					{
						label: 'NYC Cases',
            data: nyc_test_percent,
            fill: false,
            borderColor: '#EBCCD1', // red
            pointRadius: 1,
						lineWidth: 3
					}
				]
			},
			nycNewPositivesChart: {
				labels: label_dates,
				datasets: [
	      	{
	      		label: 'Daily Tests',
            data: nyc_daily_tests,
            backgroundColor: '#D6E9C6' // green
	      	},
	      	{
	      		label: 'New Positives',
            data: nyc_new_positives,
            backgroundColor: '#EBCCD1' // red
	      	}
				]
			},
			nycBoroughTotalChart: {
				labels: label_dates,
				datasets: [
					{
	      		label: 'New York',
            data: boroughNewYorkTotalCases,
            fill: false,
            borderColor: this.state.boroughNewYork, // green
            pointRadius: 1,
						lineWidth: 3
	      	},
	      	{
	      		label: 'Queens',
            data: boroughQueensTotalCases,
            fill: false,
            borderColor: this.state.boroughQueens, // green
            pointRadius: 1,
						lineWidth: 3
	      	},
	      	{
	      		label: 'Kings',
            data: boroughKingsTotalCases,
            fill: false,
            borderColor: this.state.boroughKings, // green
            pointRadius: 1,
						lineWidth: 3
	      	},
	      	{
	      		label: 'Bronx',
            data: boroughBronxTotalCases,
            fill: false,
            borderColor: this.state.boroughBronx, // green
            pointRadius: 1,
						lineWidth: 3
	      	},
	      	{
	      		label: 'Richmond',
            data: boroughRichmondTotalCases,
            fill: false,
            borderColor: this.state.boroughRichmond, // green
            pointRadius: 1,
						lineWidth: 3
	      	}
				]
			},
			nycBoroughDailyChart: {
				labels: label_dates,
				datasets: [
					{
	      		label: 'New York',
            data: boroughNewYorkDailyCases,
            backgroundColor: this.state.boroughNewYork // green
	      	},
	      	{
	      		label: 'Queens',
            data: boroughQueensDailyCases,
            backgroundColor: this.state.boroughQueens // green
	      	},
	      	{
	      		label: 'Kings',
            data: boroughKingsDailyCases,
            backgroundColor: this.state.boroughKings // green
	      	},
	      	{
	      		label: 'Bronx',
            data: boroughBronxDailyCases,
            backgroundColor: this.state.boroughBronx // green
	      	},
	      	{
	      		label: 'Richmond',
            data: boroughRichmondDailyCases,
            backgroundColor: this.state.boroughRichmond // green
	      	},
				]
			},
			nycBoroughBarChart: {
				labels: ['New York', 'Queens', 'Kings', 'Bronx', 'Richmond'],
				datasets: [
					{
						label: "NYC Boroughs",
						data: [
							(parseInt(boroughNewYorkTotal)/nycTotalCount*100).toFixed(), 
							(parseInt(boroughQueensTotal)/nycTotalCount*100).toFixed(), 
							(parseInt(boroughKingsTotal)/nycTotalCount*100).toFixed(), 
							(parseInt(boroughBronxTotal)/nycTotalCount*100).toFixed(), 
							(parseInt(boroughRichmondTotal)/nycTotalCount*100).toFixed()
						],
						backgroundColor: [
							this.state.boroughNewYork, this.state.boroughQueens, this.state.boroughKings, this.state.boroughBronx, this.state.boroughRichmond
						]
					}
				]
			}
		});
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.props.getNYCCaseData();
		this.getTodaysDate();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
		// console.log('This Loading Data NYC Cases 1: ', this.props.loadingDataNYCCases);
	}

	componentDidUpdate(lastProps, lastState) {
		// console.log('Last Loading Data NYC Cases: ', lastProps.loadingDataNYCCases);
		// console.log('This Loading Data NYC Cases 2: ', this.props.loadingDataNYCCases);
		if (this.props.loadingDataNYCCases === false && (lastProps.loadingDataNYCCases === true || lastProps.loadingDataNYCCases === undefined)) {
			this.setChartNYCCaseData();
		}
		if (this.state.todaysDate !== '' && lastState.todaysDate === '') {
			this.getDatesList();
		}
	}

	componentWillUnmount() {
		this.props.clearNYCCaseData();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { loadingDataNYCCases } = this.props;
		const { showAxisX, chartHeight, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons, displayDailyAverageButtons } = this.state;
		return (
			<div>
				 {this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} />
        }
        {this.state.showSettingsModal && <ModalBackdrop />}
				{loadingDataNYCCases === false ? 
					<Grid container>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Line 
			        		data={this.state.nycTotalCasesChart}
			        		options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Total Cases in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
		                tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
		                scales: {
		                	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
									    yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
		                },
	                  maintainAspectRatio: false
			            }}
			        	/>
			        </div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Line
									data={this.state.nycPopulationPercentCasesChart}
									options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Percent of Population Cases in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
		                scales: {
		                	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
									    yAxes: [{ ticks: {mirror: true}, gridLines: {display:false, drawBorder: false} }]
		                },
	                  maintainAspectRatio: false
			            }}
			        	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Line
									data={this.state.nycTotalTestsChart}
									options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Total Tests in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
		                tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
		                scales: {
		                	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
									    yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
		                },
	                  maintainAspectRatio: false
			            }}
			        	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Line
									data={this.state.nycTotalPercentTestsChart}
									options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Tests as Percent of Population in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
		                scales: {
		                	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
									    yAxes: [{ ticks: {mirror: true}, gridLines: {display:false, drawBorder: false} }]
		                },
	                  maintainAspectRatio: false
			            }}
			        	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Bar
									data={this.state.nycNewPositivesChart}
									options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Daily Tests/New Positives in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
		                tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
	                  scales: {
									    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
									    yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         stacked: true, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
									  },
	                  maintainAspectRatio: false
			            }}
			        	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Bar
									data={this.state.nycBoroughDailyChart}
									options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Daily Cases by Borough in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
		                tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
		                scales: {
									    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
									    yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         stacked: true, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
									  },
	                  maintainAspectRatio: false
			            }}
			        	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Line
									data={this.state.nycBoroughTotalChart}
									options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Total Cases by Borough in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
		                tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
		                scales: {
		                	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
									    yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
		                },
	                  maintainAspectRatio: false
			            }}
			        	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Pie
									data={this.state.nycBoroughBarChart}
									options={{
		                title: {
		                  display: this.props.displayTitle,
		                  text: 'Percent of Cases by Borough in ' + this.props.location,
		                  fontSize: 25
		                },
		                legend: {
		                  display: this.props.displayLegend,
		                  position: this.props.legendPosition
		                },
	                  maintainAspectRatio: false
			            }}
			        	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={styles.sourceURLContainer}>
								<a style={styles.sourceURL} target="_blank" href="https://health.data.ny.gov/Health/New-York-State-Statewide-COVID-19-Testing/xdss-u53e"><span style={styles.sourceURLText}>Source: Health.Data.NY.gov</span></a>
							</div>
						</Grid>
					</Grid> :
					<div style={styles.loadingData}>
						<span style={styles.loadingDataText}>Loading NYC Cases</span> <Dots />
					</div>
				}
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dataNYCCases: state.cities.dataNYCCases,
		loadingDataNYCCases: state.cities.loadingDataNYCCases
	}
}

export default connect(mapStateToProps, { getNYCCaseData, clearNYCCaseData })(NYC);

const styles = {
	chart: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		flex: 1,
		minHeight: 300
	}, 
	chartWide: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		flex: 1,
		minHeight: 400
	}, 
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
	loadingData: {
		padding: 20,
		textAlign: 'center'
	},
	loadingDataText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	settingsModalPlaceholder: {
		position: 'relative'
	},
	settingsModalPosition: {
		position: 'fixed',
		bottom: 50,
		right: 100
	},
	settingsModalPositionMobile: {
		position: 'fixed',
		bottom: 50,
		right: 20
	},
	settingsModalCircle: {
		height: 100,
    width: 100,
    // -moz-border-radius: 50%;
    borderRadius: 50,
    backgroundColor: '#666',
    color: 'black',
    display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	settingsModalCirclePlus: {
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	}
}









