import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

import SettingsModal from '../SettingsModal';
import ModalBackdrop from '../ModalBackdrop';
import SettingsModalIcon from '../SettingsModalIcon';

import { getDataDateAndTransmission, getDataAgeGroupAndGender, getDataHospitalizations, getDataTesting, getDataRaceAndEthnicity, clearAllChartData, getSFZipCodeData } from '../../actions/data';

class Chart extends Component {
	constructor(props) {
		super(props);
		this.state = {
			_isMounted: false,
			dailyConfirmedCountData: {},
			totalConfirmedCountData: {},
			totalTransmissionCategoryData: {},
			dailyHospitalizationData: {},
			dailyTestingData: {},
			totalAgeGroupAndGender: {},
			chartRaceData: {},
			pieChartMenWomen: {}, 
			totalPieChart: {},
			chartZipCodesConfirmedCount: {},
			chartZipCodesEstimatedCount: {},
			showChartDataTransmission: false,
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			yesterdaysDate: '',
			doNotDisplay: false,
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: false,
			displayDailyAverageButtons: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom',
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	formatDate = (s) => {
		// var fields = s.split('T');
		var date = s.split('-');
		return date[1]+'/'+date[2];
	}

	formatDateT = (s) => {
		var fields = s.split('T');
		var date = fields[0].split('-');
		return date[1]+'/'+date[2];
	}

	getDatesList = () => {
		var listDate = [];
		var startDate ='2020-03-05';
		var endDate = this.state.yesterdaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		this.setState({
			startDate: startDate,
			endDate: endDate,
			datesArray: listDate
		});
	}

	// fix later
	getYesterdaysDate = () => {
		var date = new Date();
		var newDate = date.setDate(date.getDate() -1);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var yesterday = yyyy + '-' + mm + '-' + dd;
		this.setState({
			yesterdaysDate: yesterday
		});
	}

	setChartZipCode = () => {
		const { dataZipCode } = this.props;
		var chartDataCountConfirmed = [];
		var chartDataCountPer10k = [];
		var chartDataCountConfirmedLabels = [];
		var chartDataCountPer10kLabels = [];
		var confirmedCount = [];
		var countPer10k = [];
		const zipCodeData = [];
		var count = 0;
		var rate = 0;
		dataZipCode.map(datum => {
			if (parseInt(datum.count) > 0) {
				count = datum.count;
			} else {
				count = 0;
			}
			if (parseInt(datum.rate) > 0) {
				rate = datum.rate;
			} else {
				rate = 0;
			}
			zipCodeData.push({zipCode: datum.zip_code, confirmedCount: count, rate: rate});
		});
		zipCodeData.sort((a,b) => (a.zipCode > b.zipCode) ? 1 : ((b.zipCode > a.zipCode) ? -1 : 0)); 	
		var zipCodes = [];
		const zipCodeConfirmedCount = [];
		const zipCodeRate = [];
		for (var i = 0; i < zipCodeData.length; i++) {
			zipCodes.push(zipCodeData[i].zipCode);
			zipCodeConfirmedCount.push(zipCodeData[i].confirmedCount);
			zipCodeRate.push(zipCodeData[i].rate);
		}
		this.setState({
			chartZipCodesConfirmedCount: {
				labels: zipCodes,
				datasets: [
					{
						label: 'Count per Zip Code',
						data: zipCodeConfirmedCount,
						backgroundColor: "#D6E9C6"
					}
				]
			},
			chartZipCodesEstimatedCount: {
				labels: zipCodes,
				datasets: [
					{
						label: 'Estimated Count per 10k',
						data: zipCodeRate,
						backgroundColor: "#1d7578"
					}
				]
			}
		})
	}

	setChartDataRaceAndEthnicity = () => {
		const { dataRaceAndEthnicity } = this.props;
		const raceSet = new Set();
		var total_cases = 0;
		dataRaceAndEthnicity.map((datum, index) => {
			raceSet.add(datum.race_ethnicity);
			total_cases += parseInt(datum.new_confirmed_cases);
		});
		var arrayRace = Array.from(raceSet);
		const raceChartData = [];
		for (var i = 0; i < arrayRace.length; i++) {
			raceChartData.push({race: arrayRace[i], count: 0, percent: 0});
		}
		for (var i = 0; i < raceChartData.length; i++) {
			dataRaceAndEthnicity.map(datum => {
				if (datum.race_ethnicity == raceChartData[i].race) {
					raceChartData[i].count += parseInt(datum.new_confirmed_cases);
				}
			})
		}
		for (var i = 0; i < raceChartData.length; i++) {
			raceChartData[i].percent = ((parseInt(raceChartData[i].count) / total_cases) * 100).toFixed(1);
		}
		var race_labels = [];
		var race_count = [];
		var race_percent = [];
		for (var i = 0; i < raceChartData.length; i++) {
			race_labels.push(raceChartData[i].race);
			race_count.push(raceChartData[i].count);
			race_percent.push(raceChartData[i].percent);
		}
		this.setState({
			chartRaceData: {
				labels: race_labels,
        datasets: [
          {
            label: 'Race Breakdown',
            data: race_percent,
           	backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1', '#d1b986', '#9195cf', '#c969b1', '#666', '#eb4034', '#9b34eb']
          }
        ]
			}
		});
	}

	setChartDataTesting = () => {
		const { dataTesting } = this.props;
		const { datesArray } = this.state;
		const tests = [];
		for (var i = 0; i < datesArray.length; i++) {
			tests.push({date: datesArray[i], tests: 0, positive_count: 0});
		}
		for (var i = 0; i < tests.length; i++) {
			dataTesting.map(datum => {
				if (datum.specimen_collection_date.split('T')[0] == tests[i].date) {
					tests[i].tests = datum.tests;
					tests[i].positive_count = datum.pos;
				}
			})
		}
		const label_dates = [];
		const tests_count = [];
		const positive_count = [];
		var reverseDataTesting = dataTesting.reverse();
		for (var i = 0; i < tests.length; i++) {
			label_dates.push(this.formatDate(tests[i].date));
			tests_count.push(tests[i].tests);
			positive_count.push(tests[i].positive_count);
		}
		this.setState({
			dailyTestingData: {
				labels: label_dates,
        datasets: [
          {
            label: 'Tests',
            data: tests_count,
            backgroundColor: '#D6E9C6' // green
          },
          {
          	label: 'Positives',
          	data: positive_count,
          	backgroundColor: '#FAEBCC' // yellow
          }
        ]
			}
		});
	}

	setChartDataHospitalizations = () => {
		const { dataHospitalizations } = this.props;
		const { datesArray } = this.state;
		const hospitalizationDates = new Set();
		const label_dates = [];
		dataHospitalizations.map((datum, index) => {
			hospitalizationDates.add(datum.reportdate);
		});
		const arrayDates = Array.from(hospitalizationDates);
		const dailyHospitalizationData = [];
		for (var i = 0; i < arrayDates.length; i++) {
			dailyHospitalizationData.push({date: arrayDates[i], hospitalization_count: 0, icu_count: 0});
		}
		dataHospitalizations.map((datum, index) => { 
			for (var i = 0; i < dailyHospitalizationData.length; i++) {
				if (datum.reportdate == dailyHospitalizationData[i].date) {
					if (datum.dphcategory == "Med/Surg" && datum.covidstatus == "COVID+") {
						dailyHospitalizationData[i].hospitalization_count = datum.patientcount
					}
					if (datum.dphcategory == "ICU" && datum.covidstatus == "COVID+") {
						dailyHospitalizationData[i].icu_count = datum.patientcount
					}
				}
			}
		});

		var hospitalization_rates = [];
		var icu_rates = [];
		for (var i = 0; i < dailyHospitalizationData.length; i++) {
			label_dates.push(this.formatDateT(dailyHospitalizationData[i].date));
			hospitalization_rates.push(dailyHospitalizationData[i].hospitalization_count);
			icu_rates.push(dailyHospitalizationData[i].icu_count);
		}

		this.setState({
      dailyHospitalizationData: {
        labels: label_dates,
        datasets: [
          {
            label: 'Hospital',
            data: hospitalization_rates,
            backgroundColor: '#D6E9C6' // green
          },
          {
          	label: 'ICU',
          	data: icu_rates,
          	backgroundColor: '#FAEBCC' // yellow
          }
        ]
      }
     });
	}

	setChartDataAgeGroupAndGender = () => {
		const { dataAgeGroupAndGender } = this.props;
		const ageRanges = new Set();
		dataAgeGroupAndGender.map((datum, index) => {
			ageRanges.add(datum.age_group);
		});
		const coedAgeGroupAndGender = [];
		const maleAgeGroup = [];
		const femaleAgeGroup = [];
		const arrayAgeRanges = ["under 18", "18-30", "31-40", "41-50", "51-60", "61-70", "71-80", "81+"];
		for (var i = 0; i < arrayAgeRanges.length; i++) {
			coedAgeGroupAndGender.push({age_group: arrayAgeRanges[i], count: 0, percent: 0});
			maleAgeGroup.push({age_group: arrayAgeRanges[i], count: 0});
			femaleAgeGroup.push({age_group: arrayAgeRanges[i], count: 0});
		}
		var total_cases_age_group = 0;
		dataAgeGroupAndGender.map((datum, index) => {
			for (var i = 0; i < coedAgeGroupAndGender.length; i++) {
				if (datum.age_group == coedAgeGroupAndGender[i].age_group) {
					coedAgeGroupAndGender[i].count += parseInt(datum.new_confirmed_cases);
					total_cases_age_group += parseInt(datum.new_confirmed_cases);
				}
			}
			for (var i = 0; i < maleAgeGroup.length; i++) {
				if (datum.age_group == maleAgeGroup[i].age_group && datum.gender == "Male") {
					maleAgeGroup[i].count += parseInt(datum.new_confirmed_cases);
				}
			}
			for (var i = 0; i < femaleAgeGroup.length; i++) {
				if (datum.age_group == femaleAgeGroup[i].age_group && datum.gender == "Female") {
					femaleAgeGroup[i].count += parseInt(datum.new_confirmed_cases);
				}
			}
		});
		for (var i = 0; i < coedAgeGroupAndGender.length; i++) {
			coedAgeGroupAndGender[i].percent = (parseInt(coedAgeGroupAndGender[i].count) / parseInt(total_cases_age_group) * 100).toFixed();
		}
		var total_coed_count = [];
		for (var i = 0; i < coedAgeGroupAndGender.length; i++) {
			total_coed_count.push(coedAgeGroupAndGender[i].percent);
		}
		var total_men = 0;
		var total_male_count = [];
		for (var i = 0; i < maleAgeGroup.length; i++) {
			total_male_count.push(maleAgeGroup[i].count);
			total_men += maleAgeGroup[i].count;
		}
		var total_women = 0;
		var total_female_count = [];
		for (var i = 0; i < femaleAgeGroup.length; i++) {
			total_female_count.push(femaleAgeGroup[i].count);
			total_women += femaleAgeGroup[i].count;
		}
		this.setState({
      totalAgeGroupAndGender: {
        labels: arrayAgeRanges,
        datasets: [
          {
          	label: 'Male',
          	data: total_male_count,
          	backgroundColor: '#FAEBCC' // yellow
          },
          {
          	label: 'Female',
          	data: total_female_count,
          	backgroundColor: '#EBCCD1' // red
          }
        ]
      },
      totalPieChart: {
      	labels: arrayAgeRanges,
      	datasets: [
      		{
      			label: 'Errybody',
            data: total_coed_count,
            backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1', '#d1b986', '#9195cf', '#c969b1', '#666']
      		}
      	]
      },
      pieChartMenWomen: {
      	labels: ['Men', 'Women'],
      	datasets: [
      		{
      			label: 'Men vs Women',
            data: [((total_men/total_cases_age_group)*100).toFixed(), ((total_women/total_cases_age_group)*100).toFixed()],
            backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1', '#d1b986', '#9195cf', '#c969b1', '#666']
      		}
      	]
      }
    });
	}

	setChartDataDateAndTransmission = () => {
		const { dataDateAndTransmission } = this.props;
		// console.log('Data: ', dataDateAndTransmission);
		const { datesArray } = this.state;
		const setDates = new Set();
		const label_dates = [];
		const community_count = [];
		const from_contact_count = [];
		const unknown_count = [];
		const total_count = [];
		const chart_death_count = [];
		// dataDateAndTransmission.reverse().map((datum, index) => {
		// 	setDates.add(datum.date);
		// });
		const arrayDates = Array.from(setDates);
		const dailyConfirmedCountData = [];
		for (var i = 0; i < datesArray.length; i++) {
			dailyConfirmedCountData.push({date: datesArray[i], unknown_count: 0, from_contact_count: 0, community_count: 0, total_count: 0, death_count: 0});
		}
		var total_death_count = 0;
		for (var i = 0; i < dailyConfirmedCountData.length; i++) {
			dataDateAndTransmission.map((datum, index) => { 
			// console.log('Date: ', dailyConfirmedCountData[i].date);
				if (datum.specimen_collection_date.split('T')[0] == dailyConfirmedCountData[i].date) {
					// console.log('Date: ', dailyConfirmedCountData[i].date);
					if (datum.transmission_category == 'Unknown') {
						dailyConfirmedCountData[i].unknown_count += parseInt(datum.case_count);
					}
					if (datum.transmission_category == 'From Contact') {
						dailyConfirmedCountData[i].from_contact_count += parseInt(datum.case_count);
					}
					if (datum.transmission_category == 'Community') {
						dailyConfirmedCountData[i].community_count += parseInt(datum.case_count);
					}
					if (datum.case_disposition == 'Death') {
						total_death_count += parseInt(datum.case_count);
					}
					dailyConfirmedCountData[i].death_count = parseInt(total_death_count);
				}
			});
		}
		var total_all_cases = 0;
		var total_community_cases = 0;
		var total_from_contact_cases = 0;
		var total_unknown_cases = 0;
		for (var i = 0; i < dailyConfirmedCountData.length; i++) {
			label_dates.push(this.formatDate(dailyConfirmedCountData[i].date));
			community_count.push(dailyConfirmedCountData[i].community_count);
			total_community_cases += parseInt(dailyConfirmedCountData[i].community_count);
			from_contact_count.push(dailyConfirmedCountData[i].from_contact_count);
			total_from_contact_cases += parseInt(dailyConfirmedCountData[i].from_contact_count);
			unknown_count.push(dailyConfirmedCountData[i].unknown_count);
			total_unknown_cases += parseInt(dailyConfirmedCountData[i].unknown_count);
			total_all_cases += parseInt(dailyConfirmedCountData[i].community_count) + parseInt(dailyConfirmedCountData[i].from_contact_count) + parseInt(dailyConfirmedCountData[i].unknown_count);
			total_count.push(total_all_cases);
			chart_death_count.push(dailyConfirmedCountData[i].death_count);
		}
		// console.log('Last Death Count: ', chart_death_count[chart_death_count.length -1])
		// 10
		if (chart_death_count[chart_death_count.length -10] == 0) {
			chart_death_count[chart_death_count.length -10] = total_death_count;
		}
		// 9
		if (chart_death_count[chart_death_count.length -9] == 0) {
			chart_death_count[chart_death_count.length -9] = total_death_count;
		}
		// 8
		if (chart_death_count[chart_death_count.length -8] == 0) {
			chart_death_count[chart_death_count.length -8] = total_death_count;
		}
		// 7
		if (chart_death_count[chart_death_count.length -7] == 0) {
			chart_death_count[chart_death_count.length -7] = total_death_count;
		}
		// 6
		if (chart_death_count[chart_death_count.length -6] == 0) {
			chart_death_count[chart_death_count.length -6] = total_death_count;
		}
		// 5
		if (chart_death_count[chart_death_count.length -5] == 0) {
			chart_death_count[chart_death_count.length -5] = total_death_count;
		}
		// 4
		if (chart_death_count[chart_death_count.length -4] == 0) {
			chart_death_count[chart_death_count.length -4] = total_death_count;
		}
		// 3
		if (chart_death_count[chart_death_count.length -3] == 0) {
			chart_death_count[chart_death_count.length -3] = total_death_count;
		}
		// 2
		if (chart_death_count[chart_death_count.length -2] == 0) {
			chart_death_count[chart_death_count.length -2] = total_death_count;
		}
		// 1
		if (chart_death_count[chart_death_count.length -1] == 0) {
			chart_death_count[chart_death_count.length -1] = total_death_count;
		}
		var total_cases_by_transmission_category = [];
		total_cases_by_transmission_category.push((total_community_cases/total_all_cases*100).toFixed());
		total_cases_by_transmission_category.push((total_from_contact_cases/total_all_cases*100).toFixed());
		total_cases_by_transmission_category.push((total_unknown_cases/total_all_cases*100).toFixed());
		// console.log('Daily Confirmed Count Data: ', dailyConfirmedCountData);
		// console.log('Chart Death Count: ', chart_death_count);
    this.setState({
      dailyConfirmedCountData: {
        labels: label_dates,
        datasets: [
          {
            label: 'Community',
            data: community_count,
            backgroundColor: '#D6E9C6', // green,
          },
          {
          	label: 'From Contact',
          	data: from_contact_count,
          	backgroundColor: '#FAEBCC' // yellow
          },
          {
          	label: 'Unknown',
          	data: unknown_count,
          	backgroundColor: '#EBCCD1' // red
          }
        ]
      },
      totalConfirmedCountData: {
      	labels: label_dates,
      	datasets: [
	      	{
	      		label: 'Total Cases',
            data: total_count,
            fill: false,
            borderColor: '#EBCCD1',
            pointRadius: 1,
						lineWidth: 3
	      	},
	      	{
	      		label: 'Total Deaths',
	      		data: chart_death_count,
	      		fill: false,
	      		borderColor: '#FAEBCC',
	      		pointRadius: 1,
						lineWidth: 3
	      	}
      	]
      },
      totalTransmissionCategoryData: {
      	labels: ['Community', 'From Contact', 'Unknown'],
      	datasets: [
      		{
      			label: 'Total Cases',
            data: total_cases_by_transmission_category,
            backgroundColor: ['#D6E9C6', '#FAEBCC', '#EBCCD1']
      		}
      	]
      }
    });
  }

  updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false});
		} else {
			this.setState({showAxisX: true});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.getYesterdaysDate();
		this.props.getDataDateAndTransmission();
		this.props.getDataAgeGroupAndGender();
		this.props.getDataHospitalizations();
		this.props.getDataTesting();
		this.props.getDataRaceAndEthnicity();
		// this.props.getSFZipCodeData();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.loadingDataDateAndTransmission === false && lastProps.loadingDataDateAndTransmission === true) {
			this.setChartDataDateAndTransmission();
		}
		if (this.props.loadingDataAgeGroupAndGender === false && lastProps.loadingDataAgeGroupAndGender === true) {
			this.setChartDataAgeGroupAndGender();
		}
		if (this.props.loadingDataHospitalizations === false && lastProps.loadingDataHospitalizations === true) {
			this.setChartDataHospitalizations();
		}
		if (this.props.loadingDataTesting === false && lastProps.loadingDataTesting === true) {
			this.setChartDataTesting();
		}
		if (this.props.loadingDataRaceAndEthnicity === false && lastProps.loadingDataRaceAndEthnicity === true) {
			this.setChartDataRaceAndEthnicity();
		}
		if (this.props.loadingDataZipCode === false && lastProps.loadingDataZipCode === true) {
			this.setChartZipCode();
		}
		if (this.state.yesterdaysDate !== '' && lastState.yesterdaysDate == '') {
			this.getDatesList();
		}
	}

	componentWillUnmount() {
		this.props.clearAllChartData();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { loadingDataDateAndTransmission, loadingDataAgeGroupAndGender, loadingDataHospitalizations, loadingDataTesting, loadingDataRaceAndEthnicity, loadingDataZipCode } = this.props;
		const { showChartDataTransmission, showAxisX, chartHeight, startDate, endDate, doNotDisplay, showLineGraph, showBarGraph, displayLineBarButtons, displayDailyAverageButtons } = this.state;
		return (
			<div>
        {this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} />
        }
        {this.state.showSettingsModal && <ModalBackdrop />}
				{loadingDataDateAndTransmission === false ? 
					<Grid container>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Line 
		          		data={this.state.totalConfirmedCountData}
		          		options={{
	                  title: {
	                    display: this.props.displayTitle,
	                    text: 'Total Confirmed cases in ' + this.props.location,
	                    fontSize: 25
	                  },
	                  legend: {
	                    display: this.props.displayLegend,
	                    position: this.props.legendPosition
	                  },
	                  tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
	                  scales: {
	                  	xAxes: [{ display: showAxisX, gridLines: {display:false, drawBorder: false} }],
									    yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
	                  },
	                  maintainAspectRatio: false
		              }}
		          	/>
		          </div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Bar 
		              data={this.state.dailyConfirmedCountData} 
		              options={{
	                  title: {
	                    display: this.props.displayTitle,
	                    text: 'Daily Confirmed cases in ' + this.props.location,
	                    fontSize: 25
	                  },
	                  legend: {
	                    display: this.props.displayLegend,
	                    position: this.props.legendPosition
	                  },
	                  tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
	                  scales: {
									    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false, drawBorder: false} }],
									    yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         stacked: true, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
									  },
                  	maintainAspectRatio: false
		              }}
		          	/>
		          </div>
						</Grid>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		          	<Pie 
		          		data={this.state.totalTransmissionCategoryData}
		          		options={{
		          			title: {
	                    display: this.props.displayTitle,
	                    text: 'Cases in ' + this.props.location + ' by Transmission %',
	                    fontSize: 25
	                  },
	                  legend: {
	                    display: this.props.displayLegend,
	                    position: this.props.legendPosition
	                  },
	                  maintainAspectRatio: false
		          		}}
		          	/>
		          </div>
						</Grid>
						<Grid item xs={12}>
							<div style={styles.sourceURLContainer}>
								<a style={styles.sourceURL} target="_blank" href="https://data.sfgov.org/COVID-19/COVID-19-Cases-Summarized-by-Date-Transmission-and/tvq9-ec9w"><span style={styles.sourceURLText}>Source: Data.SFGov.org</span></a>
							</div>
						</Grid>
					</Grid> : 
					<div style={styles.loadingData}>
						<span style={styles.loadingDataText}>Loading Data</span> <Dots />
					</div>
				}
				{loadingDataHospitalizations === false ? 
					<Grid container>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Bar 
		              data={this.state.dailyHospitalizationData} 
		              options={{
	                  title: {
	                    display: this.props.displayTitle,
	                    text: 'Hospitalization Rates in ' + this.props.location,
	                    fontSize: 25
	                  },
	                  legend: {
	                    display: this.props.displayLegend,
	                    position: this.props.legendPosition
	                  },
	                  tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
	                  scales: {
									    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false, drawBorder: false} }],
									    yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         stacked: true, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
									  },
                  	maintainAspectRatio: false
		              }}
		          	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={styles.sourceURLContainer}>
								<a style={styles.sourceURL} target="_blank" href="https://data.sfgov.org/COVID-19/COVID-19-Hospitalizations/nxjg-bhem"><span style={styles.sourceURLText}>Source: Data.SFGov.org</span></a>
							</div>
						</Grid>
					</Grid> :
					<div style={styles.loadingData}>
						<span style={styles.loadingDataText}>Loading Hospitalization Data</span> <Dots />
					</div>
				}
				{loadingDataTesting === false ? 
					<Grid container>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Bar 
		              data={this.state.dailyTestingData} 
		              options={{
	                  title: {
	                    display: this.props.displayTitle,
	                    text: 'Testing Rates in ' + this.props.location,
	                    fontSize: 25
	                  },
	                  legend: {
	                    display: this.props.displayLegend,
	                    position: this.props.legendPosition
	                  },
	                  tooltips: {
						          callbacks: {
				                label: function(tooltipItem, data) {
			                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                          } else {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                          }
				                }
						          } // end callbacks:
						        }, //end tooltips 
	                  scales: {
									    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false, drawBorder: false} }],
									    yAxes: [
									    	{ ticks: {
									    		mirror: true, 
									    		beginAtZero: true,
                          callback: function(value, index, values) {
                            if(parseInt(value) >= 1000){
                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return value;
                            }
                          }
                         }, 
                         stacked: true, 
                         gridLines: {
                         	display:false, 
                         	drawBorder: false
                         } 
                       }
                     ]
									  },
	                  maintainAspectRatio: false
		              }}
		          	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={styles.sourceURLContainer}>
								<a style={styles.sourceURL} target="_blank" href="https://data.sfgov.org/COVID-19/COVID-19-Tests/nfpa-mg4g"><span style={styles.sourceURLText}>Source: Data.SFGov.org</span></a>
							</div>
						</Grid>
					</Grid> :
					<div style={styles.loadingData}>
						<span style={styles.loadingDataText}>Loading Testing Data</span> <Dots />
					</div>
				}
				{loadingDataAgeGroupAndGender === false ? 
					<Grid container>
						{doNotDisplay && 
							<Grid item xs={12}>
								<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
									<Bar 
			              data={this.state.totalAgeGroupAndGender} 
			              options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Age Groups in ' + this.props.location,
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
										    xAxes: [{ stacked: showAxisX, stacked: true, gridLines: {display:false, drawBorder: false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
										  },
		                  maintainAspectRatio: false
			              }}
			          	/>
								</div>
							</Grid>
						}
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Pie 
		          		data={this.state.totalPieChart}
		          		options={{
		          			title: {
	                    display: this.props.displayTitle,
	                    text: 'Age Groups by % in ' + this.props.location,
	                    fontSize: 25
	                  },
	                  legend: {
	                    display: this.props.displayLegend,
	                    position: this.props.legendPosition
	                  },
	                  maintainAspectRatio: false
		          		}}
		          	/>
							</div>
						</Grid>
						{doNotDisplay &&
							<Grid item xs={12}>
								<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
									<Pie 
			          		data={this.state.pieChartMenWomen}
			          		options={{
			          			title: {
		                    display: this.props.displayTitle,
		                    text: 'Men & Women by % in ' + this.props.location,
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  maintainAspectRatio: false
			          		}}
			          	/>
								</div>
							</Grid>
						}
						<Grid item xs={12}>
							<div style={styles.sourceURLContainer}>
								<a style={styles.sourceURL} target="_blank" href="https://data.sfgov.org/COVID-19/COVID-19-Cases-Summarized-by-Age-Group-and-Gender/sunc-2t3k"><span style={styles.sourceURLText}>Source: Data.SFGov.org</span></a>
							</div>
						</Grid>
					</Grid> :
					<div style={styles.loadingData}>
						<span style={styles.loadingDataText}>Loading Age Group and Gender Data</span> <Dots />
					</div>
				}
				{loadingDataRaceAndEthnicity === false ? 
					<Grid container>
						<Grid item xs={12}>
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								<Pie 
		          		data={this.state.chartRaceData}
		          		options={{
		          			title: {
	                    display: this.props.displayTitle,
	                    text: '% Cases by Race/Ethnicity in ' + this.props.location,
	                    fontSize: 25
	                  },
	                  legend: {
	                    display: this.props.displayLegend,
	                    position: this.props.legendPosition
	                  },
	                  maintainAspectRatio: false
		          		}}
		          	/>
							</div>
						</Grid>
						<Grid item xs={12}>
							<div style={styles.sourceURLContainer}>
								<a style={styles.sourceURL} target="_blank" href="https://data.sfgov.org/COVID-19/COVID-19-Cases-Summarized-by-Race-and-Ethnicity/vqqm-nsqg"><span style={styles.sourceURLText}>Source: Data.SFGov.org</span></a>
							</div>
						</Grid>
					</Grid> :
					<div style={styles.loadingData}>
						<span style={styles.loadingDataText}>Loading Race and Ethnicity</span> <Dots />
					</div>
				}
				{doNotDisplay && 
					<div>
						{loadingDataZipCode === false ? 
							<Grid container>
								<Grid item xs={12}>
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										<Bar 
				          		data={this.state.chartZipCodesConfirmedCount}
				          		options={{
			                  title: {
			                    display: this.props.displayTitle,
			                    text: 'Confirmed Count per Zip Code',
			                    fontSize: 25
			                  },
			                  legend: {
			                    display: this.props.displayLegend,
			                    position: this.props.legendPosition
			                  },
			                  tooltips: {
								          callbacks: {
						                label: function(tooltipItem, data) {
					                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                          } else {
		                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                          }
						                }
								          } // end callbacks:
								        }, //end tooltips 
			                  scales: {
			                  	xAxes: [{ gridLines: {display:false, drawBorder: false} }],
											    yAxes: [
											    	{ ticks: {
											    		mirror: true, 
											    		beginAtZero: true,
		                          callback: function(value, index, values) {
		                            if(parseInt(value) >= 1000){
		                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return value;
		                            }
		                          }
		                         }, 
		                         gridLines: {
		                         	display:false, 
		                         	drawBorder: false
		                         } 
		                       }
		                     ]
			                  },
			                  maintainAspectRatio: false
				          		}}
				          	/>
									</div>
								</Grid>
								<Grid item xs={12}>
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										<Bar 
				          		data={this.state.chartZipCodesEstimatedCount}
				          		options={{
			                  title: {
			                    display: this.props.displayTitle,
			                    text: 'Estimated per 10k per Zip Code',
			                    fontSize: 25
			                  },
			                  legend: {
			                    display: this.props.displayLegend,
			                    position: this.props.legendPosition
			                  },
			                  tooltips: {
								          callbacks: {
						                label: function(tooltipItem, data) {
					                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                          } else {
		                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                          }
						                }
								          } // end callbacks:
								        }, //end tooltips 
			                  scales: {
			                  	xAxes: [{ gridLines: {display:false, drawBorder: false} }],
											    yAxes: [
											    	{ ticks: {
											    		mirror: true, 
											    		beginAtZero: true,
		                          callback: function(value, index, values) {
		                            if(parseInt(value) >= 1000){
		                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return value;
		                            }
		                          }
		                         }, 
		                         gridLines: {
		                         	display:false, 
		                         	drawBorder: false
		                         } 
		                       }
		                     ]
			                  },
			                  maintainAspectRatio: false
				          		}}
				          	/>
									</div>
								</Grid>
								<Grid item xs={12}>
									<div style={styles.sourceURLContainer}>
										<a style={styles.sourceURL} target="_blank" href="https://data.sfgov.org/COVID-19/Confirmed-COVID-19-Cases-by-ZIP-Code/favi-qct6"><span style={styles.sourceURLText}>Source: Data.SFGov.org</span></a>
									</div>
								</Grid>
							</Grid> :
							<div style={styles.loadingData}>
								<span style={styles.loadingDataText}>Loading Data Zip Codes</span> <Dots />
							</div>
						}
					</div>
				}
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dataDateAndTransmission: state.data.dataDateAndTransmission,
		loadingDataDateAndTransmission: state.data.loadingDataDateAndTransmission,

		dataAgeGroupAndGender: state.data.dataAgeGroupAndGender,
		loadingDataAgeGroupAndGender: state.data.loadingDataAgeGroupAndGender,

		dataHospitalizations: state.data.dataHospitalizations,
		loadingDataHospitalizations: state.data.loadingDataHospitalizations,

		dataTesting: state.data.dataTesting,
		loadingDataTesting: state.data.loadingDataTesting,

		dataRaceAndEthnicity: state.data.dataRaceAndEthnicity,
		loadingDataRaceAndEthnicity: state.data.loadingDataRaceAndEthnicity,

		dataZipCode: state.data.dataZipCode,
		loadingDataZipCode: state.data.loadingDataZipCode
	}
}

export default connect(mapStateToProps, { getDataDateAndTransmission, getDataAgeGroupAndGender, getDataHospitalizations, getDataTesting, getDataRaceAndEthnicity, clearAllChartData, getSFZipCodeData })(Chart);

const styles = {
	chart: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		flex: 1,
		minHeight: 300
	}, 
	chartWide: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		flex: 1,
		minHeight: 400
	}, 
	twoCharts: {
		display: 'flex',
		flexDirection: 'row'
	},
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
	loadingData: {
		padding: 20,
		textAlign: 'center'
	},
	loadingDataText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	settingsModalPlaceholder: {
		position: 'relative'
	},
	settingsModalPosition: {
		position: 'fixed',
		bottom: 50,
		right: 100
	},
	settingsModalPositionMobile: {
		position: 'fixed',
		bottom: 50,
		right: 20
	},
	settingsModalCircle: {
		height: 100,
    width: 100,
    // -moz-border-radius: 50%;
    borderRadius: 50,
    backgroundColor: '#666',
    color: 'black',
    display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	settingsModalCirclePlus: {
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	}
}








