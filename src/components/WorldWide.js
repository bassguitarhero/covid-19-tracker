import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

import SettingsModal from './SettingsModal';
import ModalBackdrop from './ModalBackdrop';
import SettingsModalIcon from './SettingsModalIcon';

import { getWorldWideEuropaData, clearWorldWideEuropaData } from '../actions/europa';
import { getColorsListCountries, clearImportCountries } from '../actions/importData';

class WorldWide extends Component {
	constructor(props) {
		super(props);
		this.state = {
			todaysDate: '',
			datesArray: [],
			countriesList: [],
			displayWorldCountries: false,
			/// Line
			chartTotalCasesLine: {},
			chartTotalDeathsLine: {},
			chartPercentCasesLine: {},
			chartPercentDeathsLine: {}, 
			chartDailyCasesLine: {},
			chartDailyDeathsLine: {},
			chartSevenDayAverageLine: {},

			// Bar
			chartTotalCasesBar: {},
			chartTotalDeathsBar: {},
			chartPercentCasesBar: {},
			chartPercentDeathsBar: {}, 
			chartDailyCasesBar: {},
			chartDailyDeathsBar: {},
			chartSevenDayAverageBar: {},

			datasetsTotalCases: [],
			datasetsTotalDeaths: [],
			datasetsPercentCases: [],
			datasetsPercentDeaths: [],
			datasetsDailyCases: [],
			datasetsDailyDeaths: [],
			showChart: false, 
			showTotalCasesChart: true,
			showTotalDeathsChart: false,
			showPercentCasesChart: false,
			showPercentDeathsChart: false, 
			showDailyCasesChart: false,
			showDailyDeathsChart: false,
			chartColors: [
				"#D6E9C6", "#FAEBCC", "#EBCCD1", "#d1b986", "#9195cf", "#c969b1", "#666", "#32a852",
				"#5ab337", "#4ea832", "#3cb1b5", "#5db0b3", "#5fb38d", "#7532a8", "#1cad6c", "#a88132",
				"#1d7578", "#467173", "#333", "#92a63c", "#92b305", "#b007b3", "#ad111e", "#9e474e",
				"#9e654a", "#a68451", "#e3b674", "#de8c14", "#7a14e0",  "#9a53e0", "#341652", "#7c7980",
				"#46414d", "#080cd1", "#0ab2cc", "#4baebd", "#0aa63e", "#137032", "#396e4b", "#648a45",
				"#386b0f", "#11694e", "#3d5704", "#727a5e", "#857a61", "#806a33", "#c29117", "#b8ab8d",
				"#bd919b", "#824d58", "#87142b", "#c2116f", "#4f59e8","#878bc7", "#60bbdb", "#5dd487"
			],
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showChartTypeTotal: true,
			showChartTypePercent: false,
			showChartTypeDaily: false,
			showChartTypeCases: true,
			showChartTypeDeaths: false,
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			showLineGraph: true,
			showBarGraph: false, 
			displayLineBarButtons: true,
			displayDailyAverageButtons: true,
			showSevenDayAverage: true,
			showDailyCount: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom'
	}

	handleShowSevenDayAverage = () => {
		this.setState({showSevenDayAverage: true, showDailyCount: false});
	}

	handleShowDailyCount = () => {
		this.setState({showSevenDayAverage: false, showDailyCount: true});
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	setChartTypeTotal = () => {
		this.setState({
			showChartTypeTotal: false,
			showChartTypePercent: false,
			showChartTypeDaily: true
		});
	}

	setChartTypePercent = () => {
		this.setState({
			showChartTypeTotal: false,
			showChartTypePercent: false,
			showChartTypeDaily: true
		});
	}

	setChartTypeDaily = () => {
		this.setState({
			showChartTypeTotal: false,
			showChartTypePercent: false,
			showChartTypeDaily: true
		});
	}

	setChartTypeCases = () => {
		this.setState({
			showChartTypeCases: true,
			showChartTypeDeaths: false
		});
	}

	setChartTypeDeaths = () => {
		this.setState({
			showChartTypeCases: false,
			showChartTypeDeaths: true
		});	
	}

	setDisplayChartTypeTotalCases = () => {
		this.setState({
			showTotalCasesChart: true,
			showTotalDeathsChart: false,
			showPercentCasesChart: false,
			showPercentDeathsChart: false, 
			showDailyCasesChart: false,
			showDailyDeathsChart: false,
		});
	}

	setDisplayChartTypeTotalDeaths = () => {
		this.setState({
			showTotalCasesChart: false,
			showTotalDeathsChart: true,
			showPercentCasesChart: false,
			showPercentDeathsChart: false, 
			showDailyCasesChart: false,
			showDailyDeathsChart: false,
		});
	}

	setDisplayChartTypeDailyCases = () => {
		this.setState({
			showTotalCasesChart: false,
			showTotalDeathsChart: false,
			showPercentCasesChart: false,
			showPercentDeathsChart: false, 
			showDailyCasesChart: true,
			showDailyDeathsChart: false,
		});
	}

	setDisplayChartTypeDailyDeaths = () => {
		this.setState({
			showTotalCasesChart: false,
			showTotalDeathsChart: false,
			showPercentCasesChart: false,
			showPercentDeathsChart: false, 
			showDailyCasesChart: false,
			showDailyDeathsChart: true,
		});
	}

	formatDate = (s) => {
		var fields = s.split('-');
		return fields[2]+'/'+fields[1]+'/'+fields[0];
	}

	getDatesList = () => {
		var listDate = [];
		var listDateFormatted = [];
		var startDate = '2020-01-23';
		var endDate = this.state.todaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		for (var i = 0; i < listDate.length; i++) {
			listDateFormatted.push(this.formatDate(listDate[i]));
		}
		this.setState({
			datesArray: listDateFormatted
		});
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();
		// var dd = String(date.getDate()).padStart(2, '0');
		// var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
		// var yyyy = date.getFullYear();
		// var today = yyyy + '-' + mm + '-' + dd;
		var newDate = date.setDate(date.getDate() -1);
		// console.log('New Date: ', newDate);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		// console.log('Yesterday: ', today);
		this.setState({
			todaysDate: today
		});
	}

	setChartData = () => {
		const { countriesList, chartColors, datesArray } = this.state;
		//Line
		var datasetsTotalCasesLine = [];
		var	datasetsTotalDeathsLine = [];
		var datasetsPercentCasesLine = [];
		var datasetsPercentDeathsLine = [];
		var	datasetsDailyCasesLine = [];
		var datasetsDailyDeathsLine = [];
		var datasetsSevenDayAverageLine = [];

		// Bar
		var datasetsTotalCasesBar = [];
		var	datasetsTotalDeathsBar = [];
		var datasetsPercentCasesBar = [];
		var datasetsPercentDeathsBar = [];
		var	datasetsDailyCasesBar = [];
		var datasetsDailyDeathsBar = [];
		var datasetsSevenDayAverageBar = [];

		var dataTotalCases = [];
		var dataTotalDeaths = [];
		var dataPercentCases = [];
		var dataPercentDeaths = [];
		var dataDailyCases = [];
		var dataDailyDeaths = [];
		var dataSevenDayAverage = [];

		var countryName = '';
		var countryColor = '';
		const labelDates = [];
		for (var i = 0; i < datesArray.length; i++) {
			var fields = datesArray[i].split('/');
			var date = fields[1]+'/'+fields[0];
			labelDates.push(date);
		}
		for (let countryID of this.selectedCountries) {
			for (var i = 0; i < countriesList.length; i++) {
				if (countryID === countriesList[i].id) {
					countryName = countriesList[i].name;
					countryColor = countriesList[i].color;
					for (var j = 0; j < countriesList[i].data.length; j++) {
						dataTotalCases.push(countriesList[i].data[j].total_cases);
						dataTotalDeaths.push(countriesList[i].data[j].total_deaths);
						dataPercentCases.push(countriesList[i].data[j].percent_cases);
						dataPercentDeaths.push(countriesList[i].data[j].percent_deaths);
						dataDailyCases.push(countriesList[i].data[j].daily_cases);
						dataDailyDeaths.push(countriesList[i].data[j].daily_deaths);
						dataSevenDayAverage.push(countriesList[i].data[j].sevenDayAverage);
					}
				}
			}
			// Line
			datasetsTotalCasesLine.push({
				label: countryName,
				data: dataTotalCases,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsTotalDeathsLine.push({
				label: countryName,
				data: dataTotalDeaths,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentCasesLine.push({
				label: countryName,
				data: dataPercentCases,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentDeathsLine.push({
				label: countryName,
				data: dataPercentDeaths,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyCasesLine.push({
				label: countryName,
				data: dataDailyCases,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyDeathsLine.push({
				label: countryName,
				data: dataDailyDeaths,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageLine.push({
				label: countryName,
				data: dataSevenDayAverage,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});

			// Bar
			datasetsTotalCasesBar.push({
				label: countryName,
				data: dataTotalCases,
				backgroundColor: countryColor
			});
			datasetsTotalDeathsBar.push({
				label: countryName,
				data: dataTotalDeaths,
				backgroundColor: countryColor
			});
			datasetsPercentCasesBar.push({
				label: countryName,
				data: dataPercentCases,
				backgroundColor: countryColor
			});
			datasetsPercentDeathsBar.push({
				label: countryName,
				data: dataPercentDeaths,
				backgroundColor: countryColor
			});
			datasetsDailyCasesBar.push({
				label: countryName,
				data: dataDailyCases,
				backgroundColor: countryColor
			});
			datasetsDailyDeathsBar.push({
				label: countryName,
				data: dataDailyDeaths,
				backgroundColor: countryColor
			});
			datasetsSevenDayAverageBar.push({
				label: countryName,
				data: dataSevenDayAverage,
				backgroundColor: countryColor
			});

			dataTotalCases = [];
			dataTotalDeaths = [];
			dataDailyCases = [];
			dataDailyDeaths = [];
			dataPercentCases = [];
			dataPercentDeaths = [];
			dataSevenDayAverage = [];
		}
		this.setState({
			// Line
			chartTotalCasesLine: {
				labels: labelDates,
				datasets: datasetsTotalCasesLine
			},
			chartTotalDeathsLine: {
				labels: labelDates,
				datasets: datasetsTotalDeathsLine
			},
			chartPercentCasesLine: {
				labels: labelDates,
				datasets: datasetsPercentCasesLine
			},
			chartPercentDeathsLine: {
				labels: labelDates,
				datasets: datasetsPercentDeathsLine
			},
			chartDailyCasesLine: {
				labels: labelDates,
				datasets: datasetsDailyCasesLine
			},
			chartDailyDeathsLine: {
				labels: labelDates,
				datasets: datasetsDailyDeathsLine
			},
			chartSevenDayAverageLine: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageLine
			},

			// Bar
			chartTotalCasesBar: {
				labels: labelDates,
				datasets: datasetsTotalCasesBar
			},
			chartTotalDeathsBar: {
				labels: labelDates,
				datasets: datasetsTotalDeathsBar
			},
			chartPercentCasesBar: {
				labels: labelDates,
				datasets: datasetsPercentCasesBar
			},
			chartPercentDeathsBar: {
				labels: labelDates,
				datasets: datasetsPercentDeathsBar
			},
			chartDailyCasesBar: {
				labels: labelDates,
				datasets: datasetsDailyCasesBar
			},
			chartDailyDeathsBar: {
				labels: labelDates,
				datasets: datasetsDailyDeathsBar
			},
			chartSevenDayAverageBar: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageBar
			},

			showChart: true
		});
	}

	handleSelectCountry = (countryID) => {
		const { countriesList } = this.state;
		if (this.selectedCountries.has(countryID)) {
			this.selectedCountries.delete(countryID);
			for (var i = 0; i < countriesList.length; i++) {
				if (countryID == countriesList[i].id) {
					countriesList[i].active = false;
				}
			}
		} else {
			this.selectedCountries.add(countryID);
			for (var i = 0; i < countriesList.length; i++) {
				if (countryID == countriesList[i].id) {
					countriesList[i].active = true;
				}
			}
		}
		this.setState({
			countriesList: countriesList
		});
		this.setChartData();
	}

	setDisplayWorldWide = () => {
		const { dataWorldWideEuropa } = this.props;
		const { countriesList, datesArray, chartColors } = this.state;
		dataWorldWideEuropa.map(datum => {
			this.worldCountries.add(datum.countriesAndTerritories);
		});
		// console.log('Data: ', dataWorldWideEuropa);
		for (let country of this.worldCountries) {
			countriesList.push({id: '', name: country, population: 0, continent: '', color: chartColors[(Math.random() * 56).toFixed()], active: false, data: []});
		}
		for (var i = 0; i < countriesList.length; i++) {
			dataWorldWideEuropa.map(datum => {
				if (countriesList[i].name == datum.countriesAndTerritories) {
					countriesList[i].name = String(datum.countriesAndTerritories).replace(/_/g,' ');
					countriesList[i].id = datum.countryterritoryCode;
					countriesList[i].population = parseInt(datum.popData2019);
					countriesList[i].continent = datum.continentExp;
				}
			});
			for (var j = 0; j < datesArray.length; j++) {
				countriesList[i].data.push({date: datesArray[j], daily_cases: 0, daily_deaths: 0, total_cases: 0, total_deaths: 0, percent_cases: 0, percent_deaths: 0, sevenDayAverage: 0});
			}
		}
		var totalCases = 0;
		var totalDeaths = 0;
		var sevenDayCount = 0;
		for (var i = 0; i < countriesList.length; i++) {
			for (var j = 0; j < countriesList[i].data.length; j++) {
				dataWorldWideEuropa.map(datum => {
					if (countriesList[i].id == datum.countryterritoryCode && countriesList[i].data[j].date == datum.dateRep) {
						totalCases += Math.abs(datum.cases);
						totalDeaths += Math.abs(datum.deaths);
						countriesList[i].data[j].daily_cases = Math.abs(datum.cases);
						countriesList[i].data[j].daily_deaths = Math.abs(datum.deaths);
						countriesList[i].data[j].total_cases = Math.abs(totalCases);
						countriesList[i].data[j].total_deaths = Math.abs(totalDeaths);
						countriesList[i].data[j].percent_cases = ((Math.abs(totalCases)/parseInt(countriesList[i].population))*100).toFixed(10);
						countriesList[i].data[j].percent_deaths = ((Math.abs(totalDeaths)/parseInt(countriesList[i].population))*100).toFixed(10);
					}
				})
			}
			for (var j = 0; j < countriesList[i].data.length; j++) {
				sevenDayCount += countriesList[i].data[j].daily_cases;
				if (j < 7) {
					countriesList[i].data[j].sevenDayAverage = (sevenDayCount / (j + 1)).toFixed(2);
				}
				if (j >= 7) {
					sevenDayCount -= countriesList[i].data[j - 7].daily_cases;
					countriesList[i].data[j].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
				}
			}
			totalCases = 0;
			totalDeaths = 0;
			sevenDayCount = 0;
		}
		this.setState({
			displayWorldCountries: true
		});
		for (var i = 0; i < countriesList.length; i++) {
			// Check 2nd to Last
			if (countriesList[i].data[countriesList[i].data.length -2].total_cases == 0) {
				countriesList[i].data[countriesList[i].data.length -2].total_cases = countriesList[i].data[countriesList[i].data.length -3].total_cases;
			}
			if (countriesList[i].data[countriesList[i].data.length -2].total_deaths == 0) {
				countriesList[i].data[countriesList[i].data.length -2].total_deaths = countriesList[i].data[countriesList[i].data.length -3].total_deaths;
			}
			if (countriesList[i].data[countriesList[i].data.length -2].percent_cases == 0) {
				countriesList[i].data[countriesList[i].data.length -2].percent_cases = countriesList[i].data[countriesList[i].data.length -3].percent_cases;
			}
			if (countriesList[i].data[countriesList[i].data.length -2].percent_deaths == 0) {
				countriesList[i].data[countriesList[i].data.length -2].percent_deaths = countriesList[i].data[countriesList[i].data.length -3].percent_deaths;
			}
			// Check Last
			if (countriesList[i].data[countriesList[i].data.length -1].total_cases == 0) {
				countriesList[i].data[countriesList[i].data.length -1].total_cases = countriesList[i].data[countriesList[i].data.length -2].total_cases;
			}
			if (countriesList[i].data[countriesList[i].data.length -1].total_deaths == 0) {
				countriesList[i].data[countriesList[i].data.length -1].total_deaths = countriesList[i].data[countriesList[i].data.length -2].total_deaths;
			}
			if (countriesList[i].data[countriesList[i].data.length -1].percent_cases == 0) {
				countriesList[i].data[countriesList[i].data.length -1].percent_cases = countriesList[i].data[countriesList[i].data.length -2].percent_cases;
			}
			if (countriesList[i].data[countriesList[i].data.length -1].percent_deaths == 0) {
				countriesList[i].data[countriesList[i].data.length -1].percent_deaths = countriesList[i].data[countriesList[i].data.length -2].percent_deaths;
			}
		}
	}

	setChartTypeTotal = () => {
		this.setState({
			showChartTypeTotal: true,
			showChartTypePercent: false,
			showChartTypeDaily: false
		});
	}

	setChartTypePercent = () => {
		this.setState({
			showChartTypeTotal: false,
			showChartTypePercent: true,
			showChartTypeDaily: false
		});
	}

	setChartTypeDaily = () => {
		this.setState({
			showChartTypeTotal: false,
			showChartTypePercent: false,
			showChartTypeDaily: true
		});
	}

	setChartTypeCases = () => {
		this.setState({
			showChartTypeCases: true,
			showChartTypeDeaths: false
		});
	}

	setChartTypeDeaths = () => {
		this.setState({
			showChartTypeCases: false,
			showChartTypeDeaths: true
		});
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.getTodaysDate();
		this.props.getWorldWideEuropaData();
		this.worldCountries = new Set();
		this.selectedCountries = new Set();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.loadingDataWorldWideEuropa === false && lastProps.loadingDataWorldWideEuropa === true) {
			this.setDisplayWorldWide();
		}
		
		if (this.props.importColorsListCountries !== null && lastProps.importColorsListCountries === null) {
			const chartColors = [];
			this.props.importColorsListCountries.map(datum => {
				chartColors.push(datum);
			});
			this.setState({
				chartColors: chartColors
			});
		}

		if (this.state.todaysDate !== '' && lastState.todaysDate === '') {
			this.getDatesList();
		}
	}

	componentWillUnmount() {
		this.props.clearWorldWideEuropaData();
		this.props.clearImportCountries();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { displayWorldCountries, countriesList, showChart } = this.state;
		const { showTotalCasesChart, showTotalDeathsChart, showDailyCasesChart, showDailyDeathsChart, showAxisX } = this.state;
		const { showChartTypeCases, showChartTypeDeaths, showChartTypeTotal, showChartTypePercent, showChartTypeDaily, chartTypeButtonSelected, chartTypeButton, chartHeight, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons } = this.state;
		const { loadingDataWorldWideEuropa } = this.props;
		const { displayDailyAverageButtons, showSevenDayAverage, showDailyCount } = this.state;

		return(
			<div>
				 {this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} showSevenDayAverage={showSevenDayAverage} showDailyCount={showDailyCount} displaySevenDayAverage={this.handleShowSevenDayAverage} displayDailyCount={this.handleShowDailyCount} />
        }
        {this.state.showSettingsModal && <ModalBackdrop />}
				{loadingDataWorldWideEuropa === false ?
					<div>
						{displayWorldCountries ? 
							<div>
								{showChart ? 
									<div>
										<Grid container>
											<Grid item xs={4} style={styles.chartTypeButtonContainer}>
												<Button style={showChartTypeTotal ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setChartTypeTotal}>Total</Button>
											</Grid>
											<Grid item xs={4} style={styles.chartTypeButtonContainer}>
												<Button style={showChartTypePercent ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setChartTypePercent}>Percent</Button>
											</Grid>
											<Grid item xs={4} style={styles.chartTypeButtonContainer}>
												<Button style={showChartTypeDaily ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setChartTypeDaily}>Daily</Button>
											</Grid>
											<Grid item xs={6} style={styles.chartTypeButtonContainer}>
												<Button style={showChartTypeCases ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setChartTypeCases}>Cases</Button>
											</Grid>
											<Grid item xs={6} style={styles.chartTypeButtonContainer}>
												<Button style={showChartTypeDeaths ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setChartTypeDeaths}>Deaths</Button>
											</Grid>
										</Grid>
										{showChartTypeCases && 
											<div>
												{showChartTypeTotal && 
													<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
														{showLineGraph && 
															<Line 
									          		data={this.state.chartTotalCasesLine}
									          		options={{
								                  title: {
								                    display: this.props.displayTitle,
								                    text: 'Total Cases by Country',
								                    fontSize: 25
								                  },
								                  legend: {
								                    display: this.props.displayLegend,
								                    position: this.props.legendPosition
								                  },
								                  tooltips: {
													          callbacks: {
											                label: function(tooltipItem, data) {
										                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                            }
											                }
													          } // end callbacks:
													        }, //end tooltips 
								                  scales: {
								                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
																    		beginAtZero: true,
							                          callback: function(value, index, values) {
							                            if(parseInt(value) >= 1000){
							                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							                            } else {
							                              return value;
							                            }
							                          }
							                         }, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
								                  },
								                  maintainAspectRatio: false
									              }}
									          	/>
									          }
									          {showBarGraph && 
									          	<Bar
																data={this.state.chartTotalCasesBar}
																options={{
									                title: {
									                  display: this.props.displayTitle,
									                  text: 'Total Cases by Country',
									                  fontSize: 25
									                },
									                legend: {
									                  display: this.props.displayLegend,
									                  position: this.props.legendPosition
									                },
									                tooltips: {
													          callbacks: {
											                label: function(tooltipItem, data) {
										                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                            }
											                }
													          } // end callbacks:
													        }, //end tooltips 
									                scales: {
																    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
																    		beginAtZero: true,
							                          callback: function(value, index, values) {
							                            if(parseInt(value) >= 1000){
							                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							                            } else {
							                              return value;
							                            }
							                          }
							                         }, 
							                         stacked: true, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
																  },
																  maintainAspectRatio: false
										            }}
										        	/>
									          }
								          </div>
												}
												{showChartTypePercent && 
													<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
														{showLineGraph && 
															<Line 
									          		data={this.state.chartPercentCasesLine}
									          		options={{
								                  title: {
								                    display: this.props.displayTitle,
								                    text: 'Percent Cases by Country',
								                    fontSize: 25
								                  },
								                  legend: {
								                    display: this.props.displayLegend,
								                    position: this.props.legendPosition
								                  },
								                  scales: {
								                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
							                         }, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
								                  },
								                  maintainAspectRatio: false
									              }}
									          	/>
									          }
									          {showBarGraph && 
									          	<Bar
																data={this.state.chartPercentCasesBar}
																options={{
									                title: {
									                  display: this.props.displayTitle,
									                  text: 'Percent Cases by Country',
									                  fontSize: 25
									                },
									                legend: {
									                  display: this.props.displayLegend,
									                  position: this.props.legendPosition
									                },
									                tooltips: {
													          callbacks: {
											                label: function(tooltipItem, data) {
										                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                            }
											                }
													          } // end callbacks:
													        }, //end tooltips 
									                scales: {
																    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
																    		beginAtZero: true,
							                          callback: function(value, index, values) {
							                            if(parseInt(value) >= 1000){
							                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							                            } else {
							                              return value;
							                            }
							                          }
							                         }, 
							                         stacked: true, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
																  },
																  maintainAspectRatio: false
										            }}
										        	/>
									          }
								          </div>
												}
												{showChartTypeDaily && 
													<div>
														{showSevenDayAverage && 
															<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
																{showLineGraph && 
																	<Line 
											          		data={this.state.chartSevenDayAverageLine}
											          		options={{
										                  title: {
										                    display: this.props.displayTitle,
										                    text: 'Seven Day Average Confirmed Cases',
										                    fontSize: 25
										                  },
										                  legend: {
										                    display: this.props.displayLegend,
										                    position: this.props.legendPosition
										                  },
										                  tooltips: {
															          callbacks: {
													                label: function(tooltipItem, data) {
												                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
												                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
								                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								                            } else {
								                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                            }
													                }
															          } // end callbacks:
															        }, //end tooltips 
										                  scales: {
										                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
																		    yAxes: [
																		    	{ ticks: {
																		    		mirror: true, 
																		    		beginAtZero: true,
									                          callback: function(value, index, values) {
									                            if(parseInt(value) >= 1000){
									                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									                            } else {
									                              return value;
									                            }
									                          }
									                         }, 
									                         gridLines: {
									                         	display:false, 
									                         	drawBorder: false
									                         } 
									                       }
									                     ]
										                  },
										                  maintainAspectRatio: false
											              }}
											          	/>
																}
																{showBarGraph && 
																	<Bar
																		data={this.state.chartSevenDayAverageBar}
																		options={{
											                title: {
											                  display: this.props.displayTitle,
											                  text: 'Seven Day Average Confirmed Cases',
											                  fontSize: 25
											                },
											                legend: {
											                  display: this.props.displayLegend,
											                  position: this.props.legendPosition
											                },
											                tooltips: {
															          callbacks: {
													                label: function(tooltipItem, data) {
												                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
												                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
								                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								                            } else {
								                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                            }
													                }
															          } // end callbacks:
															        }, //end tooltips 
											                scales: {
																		    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
																		    yAxes: [
																		    	{ ticks: {
																		    		mirror: true, 
																		    		beginAtZero: true,
									                          callback: function(value, index, values) {
									                            if(parseInt(value) >= 1000){
									                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									                            } else {
									                              return value;
									                            }
									                          }
									                         }, 
									                         stacked: true, 
									                         gridLines: {
									                         	display:false, 
									                         	drawBorder: false
									                         } 
									                       }
									                     ]
																		  },
																		  maintainAspectRatio: false
												            }}
												        	/>
												        }
															</div>
														}
														{showDailyCount && 
															<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
																{showLineGraph && 
																	<Line 
											          		data={this.state.chartDailyCasesLine}
											          		options={{
										                  title: {
										                    display: this.props.displayTitle,
										                    text: 'Daily Confirmed Cases',
										                    fontSize: 25
										                  },
										                  legend: {
										                    display: this.props.displayLegend,
										                    position: this.props.legendPosition
										                  },
										                  tooltips: {
															          callbacks: {
													                label: function(tooltipItem, data) {
												                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
												                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
								                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								                            } else {
								                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                            }
													                }
															          } // end callbacks:
															        }, //end tooltips 
										                  scales: {
										                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
																		    yAxes: [
																		    	{ ticks: {
																		    		mirror: true, 
																		    		beginAtZero: true,
									                          callback: function(value, index, values) {
									                            if(parseInt(value) >= 1000){
									                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									                            } else {
									                              return value;
									                            }
									                          }
									                         }, 
									                         gridLines: {
									                         	display:false, 
									                         	drawBorder: false
									                         } 
									                       }
									                     ]
										                  },
										                  maintainAspectRatio: false
											              }}
											          	/>
																}
																{showBarGraph && 
																	<Bar
																		data={this.state.chartDailyCasesBar}
																		options={{
											                title: {
											                  display: this.props.displayTitle,
											                  text: 'Daily Confirmed Cases',
											                  fontSize: 25
											                },
											                legend: {
											                  display: this.props.displayLegend,
											                  position: this.props.legendPosition
											                },
											                tooltips: {
															          callbacks: {
													                label: function(tooltipItem, data) {
												                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
												                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
								                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
								                            } else {
								                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                            }
													                }
															          } // end callbacks:
															        }, //end tooltips 
											                scales: {
																		    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
																		    yAxes: [
																		    	{ ticks: {
																		    		mirror: true, 
																		    		beginAtZero: true,
									                          callback: function(value, index, values) {
									                            if(parseInt(value) >= 1000){
									                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
									                            } else {
									                              return value;
									                            }
									                          }
									                         }, 
									                         stacked: true, 
									                         gridLines: {
									                         	display:false, 
									                         	drawBorder: false
									                         } 
									                       }
									                     ]
																		  },
																		  maintainAspectRatio: false
												            }}
												        	/>
												        }
															</div>
														}
													</div>
												}
											</div>
										}
										{showChartTypeDeaths && 
											<div>
												{showChartTypeTotal && 
													<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
														{showLineGraph && 
															<Line 
									          		data={this.state.chartTotalDeathsLine}
									          		options={{
								                  title: {
								                    display: this.props.displayTitle,
								                    text: 'Total Deaths by Country',
								                    fontSize: 25
								                  },
								                  legend: {
								                    display: this.props.displayLegend,
								                    position: this.props.legendPosition
								                  },
								                  tooltips: {
													          callbacks: {
											                label: function(tooltipItem, data) {
										                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                            }
											                }
													          } // end callbacks:
													        }, //end tooltips 
								                  scales: {
								                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
																    		beginAtZero: true,
							                          callback: function(value, index, values) {
							                            if(parseInt(value) >= 1000){
							                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							                            } else {
							                              return value;
							                            }
							                          }
							                         }, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
								                  },
								                  maintainAspectRatio: false
									              }}
									          	/>
									          }
									          {showBarGraph && 
									          	<Bar
																data={this.state.chartTotalDeathsBar}
																options={{
									                title: {
									                  display: this.props.displayTitle,
									                  text: 'Total Deaths by Country',
									                  fontSize: 25
									                },
									                legend: {
									                  display: this.props.displayLegend,
									                  position: this.props.legendPosition
									                },
									                tooltips: {
													          callbacks: {
											                label: function(tooltipItem, data) {
										                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                            }
											                }
													          } // end callbacks:
													        }, //end tooltips 
									                scales: {
																    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
																    		beginAtZero: true,
							                          callback: function(value, index, values) {
							                            if(parseInt(value) >= 1000){
							                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							                            } else {
							                              return value;
							                            }
							                          }
							                         }, 
							                         stacked: true, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
																  },
																  maintainAspectRatio: false
										            }}
										        	/>
									          }
								          </div>
												}
												{showChartTypePercent && 
													<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
														{showLineGraph && 
															<Line 
									          		data={this.state.chartPercentDeathsLine}
									          		options={{
								                  title: {
								                    display: this.props.displayTitle,
								                    text: 'Percent Deaths by Country',
								                    fontSize: 25
								                  },
								                  legend: {
								                    display: this.props.displayLegend,
								                    position: this.props.legendPosition
								                  },
								                  scales: {
								                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
																    		beginAtZero: true,
							                          callback: function(value, index, values) {
							                            if(parseInt(value) >= 1000){
							                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							                            } else {
							                              return value;
							                            }
							                          }
							                         }, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
								                  },
								                  maintainAspectRatio: false
									              }}
									          	/>
									          }
									          {showBarGraph && 
									          	<Bar
																data={this.state.chartPercentDeathsBar}
																options={{
									                title: {
									                  display: this.props.displayTitle,
									                  text: 'Percent Deaths by Country',
									                  fontSize: 25
									                },
									                legend: {
									                  display: this.props.displayLegend,
									                  position: this.props.legendPosition
									                },
									                tooltips: {
													          callbacks: {
											                label: function(tooltipItem, data) {
										                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                            }
											                }
													          } // end callbacks:
													        }, //end tooltips 
									                scales: {
																    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
																    		beginAtZero: true,
							                          callback: function(value, index, values) {
							                            if(parseInt(value) >= 1000){
							                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							                            } else {
							                              return value;
							                            }
							                          }
							                         }, 
							                         stacked: true, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
																  },
																  maintainAspectRatio: false
										            }}
										        	/>
									          }
								          </div>
												}
												{showChartTypeDaily && 
													<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
														{showLineGraph && 
															<Line 
									          		data={this.state.chartDailyDeathsLine}
									          		options={{
								                  title: {
								                    display: this.props.displayTitle,
								                    text: 'Daily Confirmed Deaths',
								                    fontSize: 25
								                  },
								                  legend: {
								                    display: this.props.displayLegend,
								                    position: this.props.legendPosition
								                  },
								                  tooltips: {
													          callbacks: {
											                label: function(tooltipItem, data) {
										                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                            }
											                }
													          } // end callbacks:
													        }, //end tooltips 
								                  scales: {
								                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
																    		beginAtZero: true,
							                          callback: function(value, index, values) {
							                            if(parseInt(value) >= 1000){
							                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							                            } else {
							                              return value;
							                            }
							                          }
							                         }, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
								                  },
								                  maintainAspectRatio: false
									              }}
									          	/>
														}
														{showBarGraph && 
															<Bar
																data={this.state.chartDailyDeathsBar}
																options={{
									                title: {
									                  display: this.props.displayTitle,
									                  text: 'Daily Confirmed Deaths',
									                  fontSize: 25
									                },
									                legend: {
									                  display: this.props.displayLegend,
									                  position: this.props.legendPosition
									                },
									                tooltips: {
													          callbacks: {
											                label: function(tooltipItem, data) {
										                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
										                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                            }
											                }
													          } // end callbacks:
													        }, //end tooltips 
									                scales: {
																    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
																    yAxes: [
																    	{ ticks: {
																    		mirror: true, 
																    		beginAtZero: true,
							                          callback: function(value, index, values) {
							                            if(parseInt(value) >= 1000){
							                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
							                            } else {
							                              return value;
							                            }
							                          }
							                         }, 
							                         stacked: true, 
							                         gridLines: {
							                         	display:false, 
							                         	drawBorder: false
							                         } 
							                       }
							                     ]
																  },
																  maintainAspectRatio: false
										            }}
										        	/>
										        }
													</div>
												}
											</div>
										}
									</div> :
									<div style={styles.loadingData}>
										<span style={styles.loadingDataText}>Select one or more countries...</span>
									</div>
								}
								<div style={styles.countryButtonsContainer}>
									{countriesList.map((country, index) => (
										<div style={styles.countryButtonContainer} key={index}>
											<Button style={country.active ? styles.countrySelectButtonSelected : styles.countrySelectButton} onClick={this.handleSelectCountry.bind(this, country.id)}>
												{country.name}
											</Button>
										</div>
									))}
								</div>
							</div> :
							<div style={styles.loadingData}>
								<span style={styles.loadingDataText}>Processing World Country Data</span> <Dots />
							</div>
						}
					</div> :
					<div style={styles.loadingData}>
						<span style={styles.loadingDataText}>Loading World Countries</span> <Dots />
					</div>
				}
				<div style={styles.sourceURLContainer}>
					<a style={styles.sourceURL} target="_blank" href="https://opendata.ecdc.europa.eu/covid19/casedistribution/json/"><span style={styles.sourceURLText}>Source: OpenData.ECDC.Europa.EU</span></a>
				</div>
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		importColorsListCountries: state.importData.importColorsListCountries, 

		dataWorldWideEuropa: state.europa.dataWorldWideEuropa,
		loadingDataWorldWideEuropa: state.europa.loadingDataWorldWideEuropa
	}
}

export default connect(mapStateToProps, { getWorldWideEuropaData, clearWorldWideEuropaData, getColorsListCountries, clearImportCountries })(WorldWide);

const styles = {
	countryComparisonChart: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		minHeight: 300
	},
	countryComparisonChartWide: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		minHeight: 400
	},
	selectCountries: {
		padding: 20,
		textAlign: 'center'
	},
	selectCountriesText: {
		fontSize: 20,
		fontWeight: 'bold'
	},
	countryButtonsContainer: {
		display: 'flex',
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center'
	},
	countryButtonContainer: {
		margin: 5
	},
	countrySelectButton: {
		backgroundColor: '#FFF',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	countrySelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	chartTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		padding: 10
	},
	chartTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	chartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	subChartTypeButton: {
		backgroundColor: '#32a852',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	subChartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	loadingData: {
		padding: 20,
		textAlign: 'center'
	},
	loadingDataText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
	settingsModalPlaceholder: {
		position: 'relative'
	},
	settingsModalPosition: {
		position: 'fixed',
		bottom: 50,
		right: 100
	},
	settingsModalPositionMobile: {
		position: 'fixed',
		bottom: 50,
		right: 20
	},
	settingsModalCircle: {
		height: 100,
    width: 100,
    // -moz-border-radius: 50%;
    borderRadius: 50,
    backgroundColor: '#666',
    color: 'black',
    display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	settingsModalCirclePlus: {
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	}
}










