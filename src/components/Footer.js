import React, { Component } from 'react';
import { Paper, Tabs, Tab, BottomNavigation } from '@material-ui/core';
import { connect } from 'react-redux';

import { changeHeaderCities, changeHeaderCounties, changeHeaderStates, changeHeaderCountries, changeHeaderCompare, changeHeaderDetail, changeHeaderAbout } from '../actions/layout';

class Footer extends Component {
	setDisplayCities = () => {
    this.props.displayCities();
    this.props.changeHeaderCities();
  }

  setDisplayCounties = () => {
    this.props.displayCounties();
    this.props.changeHeaderCounties();
  }

  setDisplayStates = () => {
    this.props.displayStates();
    this.props.changeHeaderStates();
  }

  setDisplayCountries = () => {
    this.props.displayCountries();
    this.props.changeHeaderCountries();
  }

  setDisplayCompare = () => {
  	this.props.displayCompare();
  	this.props.changeHeaderCompare();
  }

  setDisplayDetail = () => {
  	this.props.displayDetail();
  	this.props.changeHeaderDetail();
  }

  setDisplayAbout = () => {
  	this.props.displayAbout();
  	this.props.changeHeaderAbout();
  }

	render() {
		return (
			<div style={styles.stickToBottom}>
				<Paper>
				  <Tabs
				  	value={this.props.tabDisplayValue}
				    indicatorColor="primary"
				    textColor="primary"
				    centered
				  >
				    <Tab onClick={this.setDisplayCompare} label="COMPARE" />
				    <Tab onClick={this.setDisplayDetail} label="DETAIL" />
				    <Tab onClick={this.setDisplayAbout} label="ABOUT" />
				  </Tabs>
				</Paper>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {

	}
}

export default connect(mapStateToProps, { changeHeaderCities, changeHeaderCounties, changeHeaderStates, changeHeaderCountries, changeHeaderCompare, changeHeaderDetail, changeHeaderAbout })(Footer);

const styles = {
	stickToBottom: {
    width: '100%',
    position: 'fixed',
    bottom: 0,
  },
}