import React, { Component } from 'react';

class About extends Component {
	render() {
		return (
			<div style={styles.wrapper}>	
				<div style={styles.titleWrapper}>
					<span style={styles.title}>About This Project</span>
				</div>
				<div style={styles.textWrapper}>
					<span style={styles.text}>This project was built by <a href="https://jasonboyce.com/" target="_blank"><span style={styles.textURL}>Jason Boyce</span></a>, off of publicly-available APIs for COVID-19 Data.</span>
				</div>
				<div style={styles.textWrapper}>
					<span style={styles.text}>Not all cities, and counties have testing, hospitalization or ICU information. These charts are left at 0. Countries do not have testing, hospitalization, or ICU information as well, so their charts are left at 0. Not all counties have information on deaths, either.</span>
				</div>
				<div style={styles.textWrapper}>
					<span style={styles.text}>The source code for this project is available <a href="https://bitbucket.org/bassguitarhero/covid-19-tracker/src/master/" target="_blank"><span style={styles.textURL}>here</span></a>.</span>
				</div>
				<div style={styles.textWrapper}>
					<span style={styles.text}>For questions, <a href="https://jasonboyce.com/contact/" target="_blank"><span style={styles.textURL}>please contact me</span></a>.</span>
				</div>
			</div>
		);
	}
}

export default About;

const styles = {
	wrapper: {
		padding: 10
	},
	titleWrapper: {
		textAlign: 'center'
	},
	title: {
		fontSize: 24,
		fontWeight: 'bold',
		textAlign: 'center'
	},
	textWrapper: {
		padding: 10
	},
	text: {
		
	},
	textURL: {
		fontWeight: 'bold'
	}
}