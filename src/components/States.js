import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

import SettingsModal from './SettingsModal';
import ModalBackdrop from './ModalBackdrop';
import SettingsModalIcon from './SettingsModalIcon';

import { getCovidTrackerSingleStateDaily, resetSingleStateDaily, finishLoadingChartDataState } from '../actions/covidtracker';
import { getColorsListStates, getStatesListStates, clearImportStates } from '../actions/importData';


class States extends Component {
	constructor(props) {
		super(props);
		this.state = {
			states: [],
			chartDataStates: [], 
			singleStateSelected: '', 
			singleStateSelectedAbbreviation: '',
			singleStateSelectedColor: '', 
			singleStateSelectedPopulation: 0,
			showChart: false, 
			showPopulationChart: true,
			showPercentChart: false,
			showDailyChart: false,
			showCasesChart: true, 
			showDeathsChart: false,
			showTestsChart: false,
			showChartControls: false,
			showHospitalizedChart: false,
			showICUChart: false,

			// Line
			chartSingleStateDailyLine: {},
			multiStateChartLine: {}, 
			finalChartStatesLine: {},
			percentMultiStateChartLine: {},
			deathChartLine: {},
			deathPercentChartLine: {},
			testChartLine: {},
			testPercentChartLine: {},
			chartDailyCasesLine: {},
			chartDailyDeathsLine: {},
			chartHospitalizedLine: {},
			chartICULine: {},
			chartSevenDayAverageLine: {},
			chartTestPositivityPercentLine: {},
			chartTestPositivitySevenDayAverageLine: {},

			// Bar
			chartSingleStateDailyBar: {},
			multiStateChartBar: {}, 
			finalChartStatesBar: {},
			percentMultiStateChartBar: {},
			deathChartBar: {},
			deathPercentChartBar: {},
			testChartBar: {},
			testPercentChartBar: {},
			chartDailyCasesBar: {},
			chartDailyDeathsBar: {},
			chartHospitalizedBar: {},
			chartICUBar: {},
			chartSevenDayAverageBar: {},
			chartTestPositivityPercentBar: {},
			chartTestPositivitySevenDayAverageBar: {},

			datesArray: [],
			todaysDate: '',
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: true,
			displayDailyAverageButtons: true,
			showSevenDayAverage: true,
			showDailyCount: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom'
	}

	handleShowSevenDayAverage = () => {
		this.setState({showSevenDayAverage: true, showDailyCount: false});
	}

	handleShowDailyCount = () => {
		this.setState({showSevenDayAverage: false, showDailyCount: true});
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	formatDate = (s) => {
		var fields = s.split('-');
		return fields[0]+fields[1]+fields[2];
	}

	getDatesList = () => {
		var listDate = [];
		var listDateFormatted = [];
		var startDate = '2020-03-05';
		var endDate = this.state.todaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		for (var i = 0; i < listDate.length; i++) {
			listDateFormatted.push(this.formatDate(listDate[i]));
		}
		this.setState({
			datesArray: listDateFormatted
		});
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();

		// today
		// var dd = String(date.getDate()).padStart(2, '0');
		// var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
		// var yyyy = date.getFullYear();
		// var today = yyyy + '-' + mm + '-' + dd;

		// yesterday
		var newDate = date.setDate(date.getDate() -1);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		// console.log('Yesterday: ', today);
		this.setState({
			todaysDate: today
		});
	}

	setChartDataStates = () => {
		const { chartDataStates, datesArray } = this.state;
		if (chartDataStates.length === 0) {
			this.setState({showChart: false});
			return;
		}
		var label_dates = [];
		var label_dates_set = new Set();
		// Line
		var chart_datasets_line = [];
		var percent_chart_datasets_line = [];
		var total_death_datasets_line = [];
		var percent_death_datasets_line = [];
		var total_test_datasets_line = [];
		var percent_test_datasets_line = [];
		var daily_case_datasets_line = []; 
		var daily_death_datasets_line = []; 
		var daily_test_datasets_line = []; 
		var hospitalizedCurrently_datasets_line = [];
		var inIcuCurrently_datasets_line = [];
		var sevenDayAverage_datasets_line = [];
		var positivityRate_datasets_line = [];
		var positivitySevenDayAverage_datasets_line = [];

		// Bar
		var chart_datasets_bar = [];
		var percent_chart_datasets_bar = [];
		var total_death_datasets_bar = [];
		var percent_death_datasets_bar = [];
		var total_test_datasets_bar = [];
		var percent_test_datasets_bar = [];
		var daily_case_datasets_bar = []; 
		var daily_death_datasets_bar = []; 
		var daily_test_datasets_bar = []; 
		var hospitalizedCurrently_datasets_bar = [];
		var inIcuCurrently_datasets_bar = [];
		var sevenDayAverage_datasets_bar = [];
		var positivityRate_datasets_bar = [];
		var positivitySevenDayAverage_datasets_bar = [];

		for (var i = 0; i < datesArray.length; i++) {
			label_dates_set.add(datesArray[i]);
		}
		var label_dates_array = Array.from(label_dates_set);
		for (var i = 0; i < label_dates_array.length; i++) {
			label_dates.push(String(label_dates_array[i]).slice(4,6) + '/' + String(label_dates_array[i]).slice(6,8))
		}
		var randomColor;
		// for (var i = 0; i < datesArray.length; i++) {
		// 	state_data_array.push({date: label_dates_array[i], count: 0, percent: 0, death_count: 0, death_percent: 0, test_count: 0, test_percent: 0, daily_case_count: 0, daily_death_count: 0, daily_test_count: 0, daily_hospital_count: 0, daily_icu_count: 0, sevenDayAverage: 0});
		// }
		chartDataStates.map(state => {
			var state_data_array = [];
			for (var i = 0; i < datesArray.length; i++) {
				state_data_array.push({date: label_dates_array[i], count: 0, percent: 0, death_count: 0, death_percent: 0, test_count: 0, test_percent: 0, daily_case_count: 0, daily_death_count: 0, daily_test_count: 0, daily_hospital_count: 0, daily_icu_count: 0, sevenDayAverage: 0, positivityRate: 0, positivitySevenDayAverage: 0});
				for (var j = 0; j < state.data.length; j++) {
					if (state.data[j].date == label_dates_array[i]) {
						state_data_array[i].count += state.data[j].positive;
						state_data_array[i].percent += (Math.abs(state.data[j].positive) / state.population * 100).toFixed(3);
						state_data_array[i].death_count += state.data[j].death;
						state_data_array[i].death_percent += (Math.abs(state.data[j].death) / state.population * 100).toFixed(3);
						state_data_array[i].test_count += state.data[j].posNeg;
						state_data_array[i].test_percent += (Math.abs(state.data[j].posNeg) / state.population * 100).toFixed(2);
						state_data_array[i].daily_case_count = Math.abs(state.data[j].positiveIncrease);
						state_data_array[i].daily_death_count = Math.abs(state.data[j].deathIncrease);
						state_data_array[i].daily_test_count = Math.abs(state.data[j].totalTestResultsIncrease);
						state_data_array[i].daily_hospital_count = Math.abs(state.data[j].hospitalizedCurrently);
						state_data_array[i].daily_icu_count += Math.abs(state.data[j].inIcuCurrently);
						if (state.data[j].positiveIncrease !== 0) {
							state_data_array[i].positivityRate = (parseInt(state.data[j].positiveIncrease) / parseInt(state.data[j].totalTestResultsIncrease) * 100).toFixed(5);
						} else {
							state_data_array[i].positivityRate = 0;
						}
					}
				}
			}
			var seven_day_count = 0;
			var positivity_rate_seven_day_count = 0;
			for (var i = 0; i < state_data_array.length; i++) {
				seven_day_count += state_data_array[i].daily_case_count;
				positivity_rate_seven_day_count += parseFloat(state_data_array[i].positivityRate);
				if (i < 7) {
					state_data_array[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(2);
					state_data_array[i].positivitySevenDayAverage = (parseFloat(positivity_rate_seven_day_count) / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					seven_day_count -= state_data_array[i - 7].daily_case_count;
					positivity_rate_seven_day_count -= parseFloat(state_data_array[i - 7].positivityRate);
					state_data_array[i].sevenDayAverage = (seven_day_count / 7).toFixed(2);
					state_data_array[i].positivitySevenDayAverage = (parseFloat(positivity_rate_seven_day_count) / 7).toFixed(2);
				}
			}
			// console.log('State_ Data Array: ', state_data_array);
			// Check 2nd to last values for zeroes
			if (state_data_array[state_data_array.length -2].count == 0) {
				state_data_array[state_data_array.length -2].count = state_data_array[state_data_array.length -3].count
			}
			if (state_data_array[state_data_array.length -2].percent == 0) {
				state_data_array[state_data_array.length -2].percent = state_data_array[state_data_array.length -3].percent
			}
			if (state_data_array[state_data_array.length -2].death_count == 0) {
				state_data_array[state_data_array.length -2].death_count = state_data_array[state_data_array.length -3].death_count
			}
			if (state_data_array[state_data_array.length -2].death_percent == 0) {
				state_data_array[state_data_array.length -2].death_percent = state_data_array[state_data_array.length -3].death_percent
			}
			if (state_data_array[state_data_array.length -2].test_count == 0) {
				state_data_array[state_data_array.length -2].test_count = state_data_array[state_data_array.length -3].test_count
			}
			if (state_data_array[state_data_array.length -2].test_percent == 0) {
				state_data_array[state_data_array.length -2].test_percent = state_data_array[state_data_array.length -3].test_percent
			}
			if (state_data_array[state_data_array.length -2].sevenDayAverage == 0) {
				state_data_array[state_data_array.length -2].sevenDayAverage = state_data_array[state_data_array.length -3].sevenDayAverage
			}
			if (state_data_array[state_data_array.length -2].daily_hospital_count == 0) {
				state_data_array[state_data_array.length -2].daily_hospital_count = state_data_array[state_data_array.length -3].daily_hospital_count
			}
			if (state_data_array[state_data_array.length -2].daily_icu_count == 0) {
				state_data_array[state_data_array.length -2].daily_icu_count = state_data_array[state_data_array.length -3].daily_icu_count
			}
			if (state_data_array[state_data_array.length -2].positivityRate == 0) {
				state_data_array[state_data_array.length -2].positivityRate = state_data_array[state_data_array.length -3].positivityRate
			}
			if (state_data_array[state_data_array.length -2].positivitySevenDayAverage == 0) {
				state_data_array[state_data_array.length -2].positivitySevenDayAverage = state_data_array[state_data_array.length -3].positivitySevenDayAverage
			}
			// Check last values for zeroes
			if (state_data_array[state_data_array.length -1].count == 0) {
				state_data_array[state_data_array.length -1].count = state_data_array[state_data_array.length -2].count
			}
			if (state_data_array[state_data_array.length -1].percent == 0) {
				state_data_array[state_data_array.length -1].percent = state_data_array[state_data_array.length -2].percent
			}
			if (state_data_array[state_data_array.length -1].death_count == 0) {
				state_data_array[state_data_array.length -1].death_count = state_data_array[state_data_array.length -2].death_count
			}
			if (state_data_array[state_data_array.length -1].death_percent == 0) {
				state_data_array[state_data_array.length -1].death_percent = state_data_array[state_data_array.length -2].death_percent
			}
			if (state_data_array[state_data_array.length -1].test_count == 0) {
				state_data_array[state_data_array.length -1].test_count = state_data_array[state_data_array.length -2].test_count
			}
			if (state_data_array[state_data_array.length -1].test_percent == 0) {
				state_data_array[state_data_array.length -1].test_percent = state_data_array[state_data_array.length -2].test_percent
			}
			if (state_data_array[state_data_array.length -1].sevenDayAverage == 0) {
				state_data_array[state_data_array.length -1].sevenDayAverage = state_data_array[state_data_array.length -2].sevenDayAverage
			}
			if (state_data_array[state_data_array.length -1].daily_hospital_count == 0) {
				state_data_array[state_data_array.length -1].daily_hospital_count = state_data_array[state_data_array.length -2].daily_hospital_count
			}
			if (state_data_array[state_data_array.length -1].daily_icu_count == 0) {
				state_data_array[state_data_array.length -1].daily_icu_count = state_data_array[state_data_array.length -2].daily_icu_count
			}
			if (state_data_array[state_data_array.length -1].positivityRate == 0) {
				state_data_array[state_data_array.length -1].positivityRate = state_data_array[state_data_array.length -2].positivityRate
			}
			if (state_data_array[state_data_array.length -1].positivitySevenDayAverage == 0) {
				state_data_array[state_data_array.length -1].positivitySevenDayAverage = state_data_array[state_data_array.length -2].positivitySevenDayAverage
			}
			var data = [];
			var percent_data = [];
			var death_data = [];
			var death_percent_data = [];
			var test_data = [];
			var test_percent_data = [];
			var daily_case_data = []; 
			var daily_death_data = []; 
			var daily_test_data = []; 
			var hospitalizedCurrently_data = [];
			var inIcuCurrently_data = [];
			var sevenDayAverage_data = [];
			var positivityRate_data = [];
			var positivitySevenDayAverage_data = [];
			for (var i = 0; i < state_data_array.length; i++) {
				data.push(state_data_array[i].count);
				percent_data.push(state_data_array[i].percent);
				death_data.push(state_data_array[i].death_count);
				death_percent_data.push(state_data_array[i].death_percent);
				test_data.push(state_data_array[i].test_count);
				test_percent_data.push(state_data_array[i].test_percent);
				daily_case_data.push(state_data_array[i].daily_case_count);
				daily_death_data.push(state_data_array[i].daily_death_count);
				daily_test_data.push(state_data_array[i].daily_test_count);
				hospitalizedCurrently_data.push(state_data_array[i].daily_hospital_count);
				inIcuCurrently_data.push(state_data_array[i].daily_icu_count);
				sevenDayAverage_data.push(state_data_array[i].sevenDayAverage);
				positivityRate_data.push(state_data_array[i].positivityRate);
				positivitySevenDayAverage_data.push(state_data_array[i].positivitySevenDayAverage);
			}
			// console.log('State Data Array:  ', state_data_array);
			// console.log('Seven Day Average: ', sevenDayAverage_data);
			randomColor = Math.floor(Math.random()*16777215).toString(16);

			// Line
			chart_datasets_line.push({
				label: state.name,
				data: data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			percent_chart_datasets_line.push({
				label: state.name,
				data: percent_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			total_death_datasets_line.push({
				label: state.name,
				data: death_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			percent_death_datasets_line.push({
				label: state.name,
				data: death_percent_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			total_test_datasets_line.push({
				label: state.name,
				data: test_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			percent_test_datasets_line.push({
				label: state.name,
				data: test_percent_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			daily_case_datasets_line.push({
				label: state.name,
				data: daily_case_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			daily_death_datasets_line.push({
				label: state.name,
				data: daily_death_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			daily_test_datasets_line.push({
				label: state.name,
				data: daily_test_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			hospitalizedCurrently_datasets_line.push({
				label: state.name,
				data: hospitalizedCurrently_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			inIcuCurrently_datasets_line.push({
				label: state.name,
				data: inIcuCurrently_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			sevenDayAverage_datasets_line.push({
				label: state.name,
				data: sevenDayAverage_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			positivityRate_datasets_line.push({
				label: state.name,
				data: positivityRate_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});
			positivitySevenDayAverage_datasets_line.push({
				label: state.name,
				data: positivitySevenDayAverage_data,
				fill: false,
				borderColor: state.color,
				pointRadius: 1,
				lineWidth: 3
			});

			// Bar
			chart_datasets_bar.push({
				label: state.name,
				data: data,
				backgroundColor: state.color
			});
			percent_chart_datasets_bar.push({
				label: state.name,
				data: percent_data,
				backgroundColor: state.color
			});
			total_death_datasets_bar.push({
				label: state.name,
				data: death_data,
				backgroundColor: state.color
			});
			percent_death_datasets_bar.push({
				label: state.name,
				data: death_percent_data,
				backgroundColor: state.color
			});
			total_test_datasets_bar.push({
				label: state.name,
				data: test_data,
				backgroundColor: state.color
			});
			percent_test_datasets_bar.push({
				label: state.name,
				data: test_percent_data,
				backgroundColor: state.color
			});
			daily_case_datasets_bar.push({
				label: state.name,
				data: daily_case_data,
				backgroundColor: state.color
			});
			daily_death_datasets_bar.push({
				label: state.name,
				data: daily_death_data,
				backgroundColor: state.color
			});
			daily_test_datasets_bar.push({
				label: state.name,
				data: daily_test_data,
				backgroundColor: state.color
			});
			hospitalizedCurrently_datasets_bar.push({
				label: state.name,
				data: hospitalizedCurrently_data,
				backgroundColor: state.color
			});
			inIcuCurrently_datasets_bar.push({
				label: state.name,
				data: inIcuCurrently_data,
				backgroundColor: state.color
			});
			sevenDayAverage_datasets_bar.push({
				label: state.name,
				data: sevenDayAverage_data,
				backgroundColor: state.color
			});
			positivityRate_datasets_bar.push({
				label: state.name,
				data: positivityRate_data,
				backgroundColor: state.color
			});
			positivitySevenDayAverage_datasets_bar.push({
				label: state.name,
				data: positivitySevenDayAverage_data,
				backgroundColor: state.color
			});
		});

		this.setState({
			// Line
			multiStateChartLine: {
				labels: label_dates,
				datasets: chart_datasets_line
			},
			percentMultiStateChartLine: {
				labels: label_dates,
				datasets: percent_chart_datasets_line
			},
			deathChartLine: {
				labels: label_dates,
				datasets: total_death_datasets_line
			},
			deathPercentChartLine: {
				labels: label_dates,
				datasets: percent_death_datasets_line
			},
			testChartLine: {
				labels: label_dates,
				datasets: total_test_datasets_line
			},
			testPercentChartLine: {
				labels: label_dates,
				datasets: percent_test_datasets_line
			},
			chartDailyCasesLine: {
				labels: label_dates,
				datasets: daily_case_datasets_line
			},
			chartDailyDeathsLine: {
				labels: label_dates,
				datasets: daily_death_datasets_line
			},
			chartDailyTestsLine: {
				labels: label_dates,
				datasets: daily_test_datasets_line
			},
			chartHospitalizedLine: {
				labels: label_dates,
				datasets: hospitalizedCurrently_datasets_line
			},
			chartICULine: {
				labels: label_dates,
				datasets: inIcuCurrently_datasets_line
			},
			chartSevenDayAverageLine: {
				labels: label_dates,
				datasets: sevenDayAverage_datasets_line
			},
			chartTestPositivityPercentLine: {
				labels: label_dates,
				datasets: positivityRate_datasets_line
			},
			chartTestPositivitySevenDayAverageLine: {
				labels: label_dates,
				datasets: positivitySevenDayAverage_datasets_line
			},

			// Bar
			multiStateChartBar: {
				labels: label_dates,
				datasets: chart_datasets_bar
			},
			percentMultiStateChartBar: {
				labels: label_dates,
				datasets: percent_chart_datasets_bar
			},
			deathChartBar: {
				labels: label_dates,
				datasets: total_death_datasets_bar
			},
			deathPercentChartBar: {
				labels: label_dates,
				datasets: percent_death_datasets_bar
			},
			testChartBar: {
				labels: label_dates,
				datasets: total_test_datasets_bar
			},
			testPercentChartBar: {
				labels: label_dates,
				datasets: percent_test_datasets_bar
			},
			chartDailyCasesBar: {
				labels: label_dates,
				datasets: daily_case_datasets_bar
			},
			chartDailyDeathsBar: {
				labels: label_dates,
				datasets: daily_death_datasets_bar
			},
			chartDailyTestsBar: {
				labels: label_dates,
				datasets: daily_test_datasets_bar
			},
			chartHospitalizedBar: {
				labels: label_dates,
				datasets: hospitalizedCurrently_datasets_bar
			},
			chartICUBar: {
				labels: label_dates,
				datasets: inIcuCurrently_datasets_bar
			},
			chartSevenDayAverageBar: {
				labels: label_dates,
				datasets: sevenDayAverage_datasets_bar
			},
			chartTestPositivityPercentBar: {
				labels: label_dates,
				datasets: positivityRate_datasets_bar
			},
			chartTestPositivitySevenDayAverageBar: {
				labels: label_dates,
				datasets: positivitySevenDayAverage_datasets_bar
			},

			showChart: true
		})
	}

	handleCheckStatesSelected = (name, abbreviation, color, population) => {
		const { states } = this.state;
		if (this.statesSelected.has(abbreviation)) {
			let { chartDataStates } = this.state;
			this.statesSelected.delete(abbreviation);
			let newChartDataStates = chartDataStates.filter(e => e.name !== name);
			for (var i = 0; i < states.length; i++) {
				if (states[i].abbreviation == abbreviation) {
					states[i].active = false;
				}
			}
			this.setState({chartDataStates: newChartDataStates, states: states});
		} else {
			this.statesSelected.add(abbreviation);
			this.props.getCovidTrackerSingleStateDaily(abbreviation.toLowerCase());
			for (var i = 0; i < states.length; i++) {
				if (states[i].abbreviation == abbreviation) {
					states[i].active = true;
				}
			}
			this.setState({singleStateSelected: name, singleStateSelectedAbbreviation: abbreviation, singleStateSelectedColor: color, singleStateSelectedPopulation: population, states: states, hasHospital: false, hasICU: false});
		}
	}

	setDisplayChartTypeCases = () => {
		this.setState({
			showChartControls: true,
			showCasesChart: true, 
			showDeathsChart: false,
			showTestsChart: false,
			showHospitalizedChart: false,
			showICUChart: false
		});
	}

	setDisplayChartTypeDeaths = () => {
		this.setState({
			showChartControls: true,
			showCasesChart: false, 
			showDeathsChart: true,
			showTestsChart: false,
			showHospitalizedChart: false,
			showICUChart: false
		});
	}

	setDisplayChartTypeTests = () => {
		this.setState({
			showChartControls: true,
			showCasesChart: false, 
			showDeathsChart: false,
			showTestsChart: true,
			showHospitalizedChart: false,
			showICUChart: false
		});
	}

	setDisplayChartTypeHospitalizations = () => {
		this.setState({
			showChartControls: false,
			showCasesChart: false, 
			showDeathsChart: false,
			showTestsChart: false,
			showHospitalizedChart: true,
			showICUChart: false
		});
	}

	setDisplayChartTypeICU = () => {
		this.setState({
			showChartControls: false,
			showCasesChart: false, 
			showDeathsChart: false,
			showTestsChart: false,
			showHospitalizedChart: false,
			showICUChart: true
		});
	}

	setDisplayPopulation = () => {
		this.setState({
			showPopulationChart: true,
			showPercentChart: false,
			showDailyChart: false
		});
	}

	setDisplayPercent = () => {
		this.setState({
			showPopulationChart: false,
			showPercentChart: true,
			showDailyChart: false
		});
	}

	setDisplayDaily = () => {
		this.setState({
			showPopulationChart: false,
			showPercentChart: false,
			showDailyChart: true
		});
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.statesSelected = new Set();
		this.getTodaysDate();
		this.updateWindowDimensions();
		this.props.getColorsListStates();
		this.props.getStatesListStates();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.importColorsListStates !== null && lastProps.importColorsListStates === null) {
			const colorsList = [];
			this.props.importColorsListStates.map(datum  => {
				colorsList.push(datum);
			});
			this.setState({colorsList: colorsList});
		}

		if (this.props.importStatesListStates !== null && lastProps.importStatesListStates === null) {
			const states = [];
			this.props.importStatesListStates.map(datum => {
				states.push({
					name: datum.name,
					population: datum.population, 
					abbreviation: datum.abbreviation,
					active: false,
				});
			});
			// console.log('States: ', states);
			this.setState({
				states: states
			});
		}

		if (this.props.loadingDataSingleStateDaily === false && lastProps.loadingDataSingleStateDaily == true) {
			const { dataSingleStateDaily } = this.props;
			let { chartDataStates } = this.state;
			let reverseDataSingleStateDaily = dataSingleStateDaily.reverse()
			chartDataStates.push({name: this.state.singleStateSelected, data: reverseDataSingleStateDaily, color: this.state.colorsList[(Math.random() * 56).toFixed()], population: this.state.singleStateSelectedPopulation});
			this.setState({chartDataStates: chartDataStates});
			this.props.finishLoadingChartDataState();
			this.setChartDataStates();
		}
		if (this.state.chartDataStates !== lastState.chartDataStates) {
			this.setChartDataStates();
		}
		if (this.state.showChart === true && lastState.showChart === false) {
			this.setState({
				showChartControls: true
			});
		}
		if (this.state.todaysDate !== '' && lastState.todaysDate === '') {
			this.getDatesList();
		}
	}

	componentWillUnmount() {
		this.props.resetSingleStateDaily();
		window.removeEventListener('resize', this.updateWindowDimensions);
		this.props.clearImportStates();
	}

	render() {
		const { states, chartSingleStateDaily, singleStateSelected, showChart, showPopulationChart, showPercentChart, showDailyChart, showChartControls } = this.state;
		const { showCasesChart, showDeathsChart, showTestsChart, showHospitalizedChart, showICUChart, showAxisX, chartHeight, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons } = this.state;
		const { loadingDataSingleStateDaily } = this.props;
		const { displayDailyAverageButtons, showSevenDayAverage, showDailyCount } = this.state;

		return (
			<div>
				 {this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} showSevenDayAverage={showSevenDayAverage} showDailyCount={showDailyCount} displaySevenDayAverage={this.handleShowSevenDayAverage} displayDailyCount={this.handleShowDailyCount} />
        }
        {this.state.showSettingsModal && <ModalBackdrop />}
				<Grid container spacing={1} style={styles.statesHeader}>
				{showChartControls && 
					<Grid item container xs={12}>
						<Grid item xs={4} style={styles.chartTypeButtonContainer}>
							<Button style={showPopulationChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayPopulation}>Total</Button>
						</Grid>
						<Grid item xs={4} style={styles.chartTypeButtonContainer}>
							<Button style={showPercentChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayPercent}>Percent</Button>
						</Grid>
						<Grid item xs={4} style={styles.chartTypeButtonContainer}>
							<Button style={showDailyChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayDaily}>Daily</Button>
						</Grid>
					</Grid>
				}
				</Grid>
				{showChart ? 
					<Grid container>
						{showCasesChart && 
							<Grid item xs={12}>
								<div>
									{showPopulationChart && 
										<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
											{showLineGraph && 
												<Line 
						          		data={this.state.multiStateChartLine}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Total Cases per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
								                	var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
							        }
						         	{showBarGraph && 
							         	<Bar 
						          		data={this.state.multiStateChartBar}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Total Cases per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         stacked: true, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						         	}
							      </div>
					        }
					        {showPercentChart && 
					        	<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
					        		{showLineGraph && 
												<Line 
						          		data={this.state.percentMultiStateChartLine}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Cases by Percent of Population per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  scales: {
					                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
													    yAxes: [{ ticks: {mirror: true}, gridLines: {display:false, drawBorder: false} }]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						          {showBarGraph && 
						          	<Bar 
						          		data={this.state.percentMultiStateChartBar}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Cases by Percent of Population per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         stacked: true, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						        </div>
					        }
					        {showDailyChart && 
					        	<div>
					        		{showSevenDayAverage && 
							        	<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
							        		{showLineGraph && 
							        			<Line 
								          		data={this.state.chartSevenDayAverageLine}
								          		options={{
							                  title: {
							                    display: this.props.displayTitle,
							                    text: 'Seven Day Average per State',
							                    fontSize: 25
							                  },
							                  legend: {
							                    display: this.props.displayLegend,
							                    position: this.props.legendPosition
							                  },
							                  tooltips: {
												          callbacks: {
										                label: function(tooltipItem, data) {
									                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                            }
										                }
												          } // end callbacks:
												        }, //end tooltips 
							                  scales: {
							                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
															    yAxes: [
															    	{ ticks: {
															    		mirror: true, 
															    		beginAtZero: true,
						                          callback: function(value, index, values) {
						                            if(parseInt(value) >= 1000){
						                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return value;
						                            }
						                          }
						                         }, 
						                         gridLines: {
						                         	display:false, 
						                         	drawBorder: false
						                         } 
						                       }
						                     ]
							                  },
					                  		maintainAspectRatio: false
								              }}
								          	/>
							        		}
							        		{showBarGraph && 
														<Bar 
								          		data={this.state.chartSevenDayAverageBar}
								          		options={{
							                  title: {
							                    display: this.props.displayTitle,
							                    text: 'Seven Day Average per State',
							                    fontSize: 25
							                  },
							                  legend: {
							                    display: this.props.displayLegend,
							                    position: this.props.legendPosition
							                  },
							                  tooltips: {
												          callbacks: {
										                label: function(tooltipItem, data) {
									                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                            }
										                }
												          } // end callbacks:
												        }, //end tooltips 
							                  scales: {
							                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
															    yAxes: [
															    	{ ticks: {
															    		mirror: true, 
															    		beginAtZero: true,
						                          callback: function(value, index, values) {
						                            if(parseInt(value) >= 1000){
						                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return value;
						                            }
						                          }
						                         }, 
						                         stacked: true, 
						                         gridLines: {
						                         	display:false, 
						                         	drawBorder: false
						                         } 
						                       }
						                     ]
							                  },
					                  		maintainAspectRatio: false
								              }}
								          	/>
								          }
								        </div>
								      }
								      {showDailyCount && 
								      	<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
							        		{showLineGraph && 
							        			<Line 
								          		data={this.state.chartDailyCasesLine}
								          		options={{
							                  title: {
							                    display: this.props.displayTitle,
							                    text: 'Daily Count per State',
							                    fontSize: 25
							                  },
							                  legend: {
							                    display: this.props.displayLegend,
							                    position: this.props.legendPosition
							                  },
							                  tooltips: {
												          callbacks: {
										                label: function(tooltipItem, data) {
									                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                            }
										                }
												          } // end callbacks:
												        }, //end tooltips 
							                  scales: {
							                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
															    yAxes: [
															    	{ ticks: {
															    		mirror: true, 
															    		beginAtZero: true,
						                          callback: function(value, index, values) {
						                            if(parseInt(value) >= 1000){
						                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return value;
						                            }
						                          }
						                         }, 
						                         gridLines: {
						                         	display:false, 
						                         	drawBorder: false
						                         } 
						                       }
						                     ]
							                  },
					                  		maintainAspectRatio: false
								              }}
								          	/>
							        		}
							        		{showBarGraph && 
														<Bar 
								          		data={this.state.chartDailyCasesBar}
								          		options={{
							                  title: {
							                    display: this.props.displayTitle,
							                    text: 'Daily Count per State',
							                    fontSize: 25
							                  },
							                  legend: {
							                    display: this.props.displayLegend,
							                    position: this.props.legendPosition
							                  },
							                  tooltips: {
												          callbacks: {
										                label: function(tooltipItem, data) {
									                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
									                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
					                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                            }
										                }
												          } // end callbacks:
												        }, //end tooltips 
							                  scales: {
							                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
															    yAxes: [
															    	{ ticks: {
															    		mirror: true, 
															    		beginAtZero: true,
						                          callback: function(value, index, values) {
						                            if(parseInt(value) >= 1000){
						                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
						                            } else {
						                              return value;
						                            }
						                          }
						                         }, 
						                         stacked: true, 
						                         gridLines: {
						                         	display:false, 
						                         	drawBorder: false
						                         } 
						                       }
						                     ]
							                  },
					                  		maintainAspectRatio: false
								              }}
								          	/>
								          }
								        </div>
								      }
								    </div>
					        }
				        </div>
							</Grid>
						}
						{showDeathsChart && 
							<Grid item xs={12}>
								<div>
									{showPopulationChart && 
										<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
											{showLineGraph && 
												<Line 
						          		data={this.state.deathChartLine}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Total Deaths per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						          {showBarGraph && 
						          	<Bar 
						          		data={this.state.deathChartBar}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Total Deaths per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         stacked: true, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						        </div>
					        }
					        {showPercentChart && 
					        	<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
					        		{showLineGraph && 
												<Line 
						          		data={this.state.deathPercentChartLine}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Deaths by Percent of Population per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  scales: {
					                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
													    yAxes: [{ ticks: {mirror: true}, gridLines: {display:false, drawBorder: false} }]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						          {showBarGraph && 
						          	<Bar 
						          		data={this.state.deathPercentChartBar}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Deaths by Percent of Population per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         stacked: true, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						        </div>
					        }
					        {showDailyChart && 
					        	<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
					        		{showLineGraph && 
					        			<Line 
						          		data={this.state.chartDailyDeathsLine}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Daily Deaths per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
					        		}
					        		{showBarGraph && 
							        	<Bar 
						          		data={this.state.chartDailyDeathsBar}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Daily Deaths per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         stacked: true, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						        </div>
					        }
				        </div>
							</Grid>
						}
						{showTestsChart && 
							<Grid item xs={12}>
								<div>
									{showPopulationChart && 
										<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
											{showLineGraph && 
												<Line 
						          		data={this.state.testChartLine}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Total Tests per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						          {showBarGraph && 
						          	<Bar 
						          		data={this.state.testChartBar}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                   	text: 'Total Tests per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         stacked: true, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }	
						        </div>
					        }
					        {showPercentChart && 
					        	<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
					        		{showLineGraph  && 
												<Line 
						          		data={this.state.chartTestPositivityPercentLine}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Positivity Percent per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  scales: {
					                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
													    yAxes: [{ ticks: {mirror: true}, gridLines: {display:false, drawBorder: false} }]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						         	{showBarGraph && 
						         		<Bar 
						          		data={this.state.chartTestPositivityPercentBar}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                   	text: 'Positivity Percent per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         stacked: true, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						         	}		
						        </div>
					        }
					        {showDailyChart && 
					        	<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
					        		{showLineGraph && 
					        			<Line 
						          		data={this.state.chartTestPositivitySevenDayAverageLine}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Seven Day Average Positive Tests Percent per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  scales: {
					                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
					        		}
					        		{showBarGraph && 
							        	<Bar 
						          		data={this.state.chartTestPositivitySevenDayAverageBar}
						          		options={{
					                  title: {
					                    display: this.props.displayTitle,
					                    text: 'Seven Day Average Positive Tests Percent per State',
					                    fontSize: 25
					                  },
					                  legend: {
					                    display: this.props.displayLegend,
					                    position: this.props.legendPosition
					                  },
					                  tooltips: {
										          callbacks: {
								                label: function(tooltipItem, data) {
							                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
							                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                            }
								                }
										          } // end callbacks:
										        }, //end tooltips 
					                  scales: {
					                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
													    yAxes: [
													    	{ ticks: {
													    		mirror: true, 
													    		beginAtZero: true,
				                          callback: function(value, index, values) {
				                            if(parseInt(value) >= 1000){
				                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return value;
				                            }
				                          }
				                         }, 
				                         stacked: true, 
				                         gridLines: {
				                         	display:false, 
				                         	drawBorder: false
				                         } 
				                       }
				                     ]
					                  },
			                  		maintainAspectRatio: false
						              }}
						          	/>
						          }
						        </div>
					        }
				        </div>
							</Grid>
						}
						{showHospitalizedChart && 
							<Grid item xs={12}>
								<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
									{showLineGraph && 
										<Line 
				          		data={this.state.chartHospitalizedLine}
				          		options={{
			                  title: {
			                    display: this.props.displayTitle,
			                    text: 'Hospitalizations per State',
			                    fontSize: 25
			                  },
			                  legend: {
			                    display: this.props.displayLegend,
			                    position: this.props.legendPosition
			                  },
			                  tooltips: {
								          callbacks: {
						                label: function(tooltipItem, data) {
					                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                            }
						                }
								          } // end callbacks:
								        }, //end tooltips 
			                  scales: {
			                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
											    yAxes: [
											    	{ ticks: {
											    		mirror: true, 
											    		beginAtZero: true,
		                          callback: function(value, index, values) {
		                            if(parseInt(value) >= 1000){
		                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return value;
		                            }
		                          }
		                         }, 
		                         gridLines: {
		                         	display:false, 
		                         	drawBorder: false
		                         } 
		                       }
		                     ]
			                  },
	                  		maintainAspectRatio: false
				              }}
				          	/>
									}
									{showBarGraph && 
										<Bar 
				          		data={this.state.chartHospitalizedBar}
				          		options={{
			                  title: {
			                    display: this.props.displayTitle,
			                    text: 'Hospitalizations per State',
			                    fontSize: 25
			                  },
			                  legend: {
			                    display: this.props.displayLegend,
			                    position: this.props.legendPosition
			                  },
			                  tooltips: {
								          callbacks: {
						                label: function(tooltipItem, data) {
					                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                            }
						                }
								          } // end callbacks:
								        }, //end tooltips 
			                  scales: {
			                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
											    yAxes: [
											    	{ ticks: {
											    		mirror: true, 
											    		beginAtZero: true,
		                          callback: function(value, index, values) {
		                            if(parseInt(value) >= 1000){
		                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return value;
		                            }
		                          }
		                         }, 
		                         stacked: true, 
		                         gridLines: {
		                         	display:false, 
		                         	drawBorder: false
		                         } 
		                       }
		                     ]
			                  },
	                  		maintainAspectRatio: false
				              }}
				          	/>
				          }
								</div>
							</Grid>
						}
						{showICUChart && 
							<Grid item xs={12}>
								<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
									{showLineGraph && 
										<Line 
				          		data={this.state.chartICULine}
				          		options={{
			                  title: {
			                    display: this.props.displayTitle,
			                    text: 'ICU Hospitalizations per State',
			                    fontSize: 25
			                  },
			                  legend: {
			                    display: this.props.displayLegend,
			                    position: this.props.legendPosition
			                  },
			                  tooltips: {
								          callbacks: {
						                label: function(tooltipItem, data) {
					                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                               return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                            }
						                }
								          } // end callbacks:
								        }, //end tooltips 
			                  scales: {
			                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
											    yAxes: [
											    	{ ticks: {
											    		mirror: true, 
											    		beginAtZero: true,
		                          callback: function(value, index, values) {
		                            if(parseInt(value) >= 1000){
		                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return value;
		                            }
		                          }
		                         }, 
		                         gridLines: {
		                         	display:false, 
		                         	drawBorder: false
		                         } 
		                       }
		                     ]
			                  },
	                  		maintainAspectRatio: false
				              }}
				          	/>
									}
									{showBarGraph && 
										<Bar 
				          		data={this.state.chartICUBar}
				          		options={{
			                  title: {
			                    display: this.props.displayTitle,
			                    text: 'ICU Hospitalizations per State',
			                    fontSize: 25
			                  },
			                  legend: {
			                    display: this.props.displayLegend,
			                    position: this.props.legendPosition
			                  },
			                  tooltips: {
								          callbacks: {
						                label: function(tooltipItem, data) {
					                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
					                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                            }
						                }
								          } // end callbacks:
								        }, //end tooltips 
			                  scales: {
			                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
											    yAxes: [
											    	{ ticks: {
											    		mirror: true, 
											    		beginAtZero: true,
		                          callback: function(value, index, values) {
		                            if(parseInt(value) >= 1000){
		                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return value;
		                            }
		                          }
		                         }, 
		                         stacked: true, 
		                         gridLines: {
		                         	display:false, 
		                         	drawBorder: false
		                         } 
		                       }
		                     ]
			                  },
	                  		maintainAspectRatio: false
				              }}
				          	/>
				          }
								</div>
							</Grid>
						}
						<Grid item xs={12} container style={styles.chartTypeButtonsContainer}>
							<Grid item xs={4} style={styles.chartTypeButtonContainer}>
								<Button style={showCasesChart ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setDisplayChartTypeCases}>Cases</Button>
							</Grid>
							<Grid item xs={4} style={styles.chartTypeButtonContainer}>
								<Button style={showDeathsChart ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setDisplayChartTypeDeaths}>Deaths</Button>
							</Grid>
							<Grid item xs={4} style={styles.chartTypeButtonContainer}>
								<Button style={showTestsChart ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setDisplayChartTypeTests}>Tests</Button>
							</Grid>
							<Grid item xs={6} style={styles.chartTypeButtonContainer}>
								<Button style={showHospitalizedChart ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setDisplayChartTypeHospitalizations}>Hospitalizations</Button>
							</Grid>
							<Grid item xs={6} style={styles.chartTypeButtonContainer}>
								<Button style={showICUChart ? styles.subChartTypeButtonSelected : styles.subChartTypeButton} onClick={this.setDisplayChartTypeICU}>ICU</Button>
							</Grid>
						</Grid>
					</Grid> :
					<div style={styles.selectStates}>
						<span style={styles.selectStateText}>Select one or more states...</span>
					</div>
				}
				{this.props.importStatesList !== null && 
					<div style={styles.stateSelectButtonsContainer}>
						{states.map((state, index) => (
							<div style={styles.stateSelectButtonContainer} key={index}>
								<Button style={state.active ? styles.stateSelectButtonSelected : styles.stateSelectButton} onClick={this.handleCheckStatesSelected.bind(this, state.name, state.abbreviation, state.backgroundColor, state.population)}>{state.abbreviation}</Button>
							</div>
						))}
					</div>
				}
				<div style={styles.sourceURLContainer}>
					<a style={styles.sourceURL} target="_blank" href="https://covidtracking.com/"><span style={styles.sourceURLText}>Source: CovidTracking.com</span></a>
				</div>
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		importColorsListStates: state.importData.importColorsListStates,
		importStatesListStates: state.importData.importStatesListStates,

		dataSingleStateDaily: state.covidtracker.dataSingleStateDaily,
		loadingDataSingleStateDaily: state.covidtracker.loadingDataSingleStateDaily
	}
}

export default connect(mapStateToProps, { getCovidTrackerSingleStateDaily, resetSingleStateDaily, finishLoadingChartDataState, getColorsListStates, getStatesListStates, clearImportStates })(States);

const styles = {
	chartButtonsContainer: {
		flex: 1
	},
	chartTypeButtonsContainer: {
		display: 'flex',
		flexDirection: 'row',
		flex: 1,
		justifyContent: 'center'
	},
	chartTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		padding: 10
	},
	chartTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	subChartTypeButton: {
		backgroundColor: '#32a852',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	subChartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	stateComparisonChart: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		minHeight: 300
	},
	stateComparisonChartWide: {
		marginLeft: 20,
		marginRight: 20,
		marginTop: 10,
		marginBottom: 10,
		minHeight: 400
	},
	selectStates: {
		padding: 20,
		textAlign: 'center'
	},
	selectStateText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	statesHeader: {
		display: 'flex',
		flex: 1,
		padding: 20,
		flexDirection: 'row',
		textAlign: 'center'
	},
	statesTitle: {
		flex: 1,
		justifyContent: 'center'
	},
	statesTitleText: {
		fontSize: 24,
		fontWeight: 'bold'
	},
	stateSelectButtonsContainer: {
		display: 'flex',
		flex: 1, 
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		marginTop: 20
	},
	stateSelectButtonContainer: {
		margin: 5
	},
	stateSelectButton: {
		backgroundColor: '#FFF',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	stateSelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
	settingsModalPlaceholder: {
		position: 'relative'
	},
	settingsModalPosition: {
		position: 'fixed',
		bottom: 50,
		right: 100
	},
	settingsModalPositionMobile: {
		position: 'fixed',
		bottom: 50,
		right: 20
	},
	settingsModalCircle: {
		height: 100,
    width: 100,
    // -moz-border-radius: 50%;
    borderRadius: 50,
    backgroundColor: '#666',
    color: 'black',
    display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	settingsModalCirclePlus: {
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	}
}



















