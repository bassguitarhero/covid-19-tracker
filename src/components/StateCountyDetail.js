import React, { Component } from 'react';
import CA from './states/CA';
import NY from './states/NY';
import FL from './states/FL';
import CT from './states/CT';

class StateCountyDetail extends Component {
	render() {
		const { location } = this.props;

		return (
			<div>
				{(location === 'California') &&
					<CA location={location} />
				}
				{(location === 'New York') &&
					<NY location={location} />
				}
				{(location === 'Florida') &&  
					<FL location={location} />
				}
				{(location === 'Connecticut') &&  
					<CT location={location} />
				}
			</div>
		);
	}
}

export default StateCountyDetail;










