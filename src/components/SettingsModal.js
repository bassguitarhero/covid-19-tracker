import React, { Component } from 'react';
import { Button, Grid, Select, MenuItem } from '@material-ui/core';

class SettingsModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			defaultChartHeight: {
				id: 0,
				name: '',
				value: 0
			},
			chartHeightName: '',
			newChartHeight: 0, 
			newStartDate: '',
			newEndDate: '',
			chartSizes: [
				{
					id: 1,
					name: "Tiny",
					value: 300
				},
				{
					id: 2,
					name: "Small",
					value: 400
				},
				{
					id: 3,
					name: "Medium",
					value: 500
				},
				{
					id: 4,
					name: "Large",
					value: 600
				},
				{
					id: 5,
					name: "Extra Large",
					value: 700
				}
			],
			portraitMode: false,
			windowWidth: 0,
			windowHeight: 0,
			showLineGraph: true,
			showBarGraph: false,
			showAltPercentGraph: true,
			showPercentGraph: false,
			startMonthsList: [
				{
					id: 3,
					name: '03',
					value: '03',
					active: true
				},
				{
					id: 4,
					name: '04',
					value: '04',
					active: true
				},
				{
					id: 5,
					name: '05',
					value: '05',
					active: true
				},
				{
					id: 6,
					name: '06',
					value: '06',
					active: true
				},
				{
					id: 7,
					name: '07',
					value: '07',
					active: true
				},
				{
					id: 8,
					name: '08',
					value: '08',
					active: true
				},
				{
					id: 9,
					name: '09',
					value: '09',
					active: true
				},
			],
			endMonthsList: [
				{
					id: 3,
					name: '03',
					value: '03',
					active: true
				},
				{
					id: 4,
					name: '04',
					value: '04',
					active: true
				},
				{
					id: 5,
					name: '05',
					value: '05',
					active: true
				},
				{
					id: 6,
					name: '06',
					value: '06',
					active: true
				},
				{
					id: 7,
					name: '07',
					value: '07',
					active: true
				},
				{
					id: 8,
					name: '08',
					value: '08',
					active: true
				},
				{
					id: 9,
					name: '09',
					value: '09',
					active: true
				},
			],
			days30: [
				{
					id: 1,
					name: '01',
					value: '01',
					active: true
				},
				{
					id: 2,
					name: '02',
					value: '02',
					active: true
				},
				{
					id: 3,
					name: '03',
					value: '03',
					active: true
				},
				{
					id: 4,
					name: '04',
					value: '04',
					active: true
				},
				{
					id: 5,
					name: '05',
					value: '05',
					active: true
				},
				{
					id: 6,
					name: '06',
					value: '06',
					active: true
				},
				{
					id: 7,
					name: '07',
					value: '07',
					active: true
				},
				{
					id: 8,
					name: '08',
					value: '08',
					active: true
				},
				{
					id: 9,
					name: '09',
					value: '09',
					active: true
				},
				{
					id: 10,
					name: '10',
					value: '10',
					active: true
				},
				{
					id: 11,
					name: '11',
					value: '11',
					active: true
				},
				{
					id: 12,
					name: '12',
					value: '12',
					active: true
				},
				{
					id: 13,
					name: '13',
					value: '13',
					active: true
				},
				{
					id: 14,
					name: '14',
					value: '14',
					active: true
				},
				{
					id: 15,
					name: '15',
					value: '15',
					active: true
				},
				{
					id: 16,
					name: '16',
					value: '16',
					active: true
				},
				{
					id: 17,
					name: '17',
					value: '17',
					active: true
				},
				{
					id: 18,
					name: '18',
					value: '18',
					active: true
				},
				{
					id: 19,
					name: '19',
					value: '19',
					active: true
				},
				{
					id: 20,
					name: '20',
					value: '20',
					active: true
				},
				{
					id: 21,
					name: '21',
					value: '21',
					active: true
				},
				{
					id: 22,
					name: '22',
					value: '22',
					active: true
				},
				{
					id: 23,
					name: '23',
					value: '23',
					active: true
				},
				{
					id: 24,
					name: '24',
					value: '24',
					active: true
				},
				{
					id: 25,
					name: '25',
					value: '25',
					active: true
				},
				{
					id: 26,
					name: '26',
					value: '26',
					active: true
				},
				{
					id: 27,
					name: '27',
					value: '27',
					active: true
				},
				{
					id: 28,
					name: '28',
					value: '28',
					active: true
				},
				{
					id: 29,
					name: '29',
					value: '29',
					active: true
				},
				{
					id: 30,
					name: '30',
					value: '30',
					active: true
				},
			],
			days31: [
				{
					id: 1,
					name: '01',
					value: '01',
					active: true
				},
				{
					id: 2,
					name: '02',
					value: '02',
					active: true
				},
				{
					id: 3,
					name: '03',
					value: '03',
					active: true
				},
				{
					id: 4,
					name: '04',
					value: '04',
					active: true
				},
				{
					id: 5,
					name: '05',
					value: '05',
					active: true
				},
				{
					id: 6,
					name: '06',
					value: '06',
					active: true
				},
				{
					id: 7,
					name: '07',
					value: '07',
					active: true
				},
				{
					id: 8,
					name: '08',
					value: '08',
					active: true
				},
				{
					id: 9,
					name: '09',
					value: '09',
					active: true
				},
				{
					id: 10,
					name: '10',
					value: '10',
					active: true
				},
				{
					id: 11,
					name: '11',
					value: '11',
					active: true
				},
				{
					id: 12,
					name: '12',
					value: '12',
					active: true
				},
				{
					id: 13,
					name: '13',
					value: '13',
					active: true
				},
				{
					id: 14,
					name: '14',
					value: '14',
					active: true
				},
				{
					id: 15,
					name: '15',
					value: '15',
					active: true
				},
				{
					id: 16,
					name: '16',
					value: '16',
					active: true
				},
				{
					id: 17,
					name: '17',
					value: '17',
					active: true
				},
				{
					id: 18,
					name: '18',
					value: '18',
					active: true
				},
				{
					id: 19,
					name: '19',
					value: '19',
					active: true
				},
				{
					id: 20,
					name: '20',
					value: '20',
					active: true
				},
				{
					id: 21,
					name: '21',
					value: '21',
					active: true
				},
				{
					id: 22,
					name: '22',
					value: '22',
					active: true
				},
				{
					id: 23,
					name: '23',
					value: '23',
					active: true
				},
				{
					id: 24,
					name: '24',
					value: '24',
					active: true
				},
				{
					id: 25,
					name: '25',
					value: '25',
					active: true
				},
				{
					id: 26,
					name: '26',
					value: '26',
					active: true
				},
				{
					id: 27,
					name: '27',
					value: '27',
					active: true
				},
				{
					id: 28,
					name: '28',
					value: '28',
					active: true
				},
				{
					id: 29,
					name: '29',
					value: '29',
					active: true
				},
				{
					id: 30,
					name: '30',
					value: '30',
					active: true
				},
				{
					id: 31,
					name: '31',
					value: '31',
					active: true
				},
			],
			yearsList: [
				{
					id: 1,
					name: '2020',
					value: '2020'
				},
			],
			startMonth: '',
			startDay: '',
			startYear: '',
			endMonth: '', 
			endDay: '',
			endYear: '',
			defaultStartMonth: {
				id: 0,
				name: '',
				value: ''
			},
			defaultStartDay: {
				id: 0,
				name: '',
				value: ''
			},
			defaultStartYear: {
				id: 0,
				name: '',
				value: ''
			},
			defaultEndMonth: {
				id: 0,
				name: '',
				value: '',
				active: true
			},
			defaultEndDay: {
				id: 0,
				name: '',
				value: ''
			},
			defaultEndYear: {
				id: 0,
				name: '',
				value: ''
			},
			newStartMonth: '',
			newStartDay: '',
			newStartYear: '',
			newEndMonth: '',
			newEndDay: '',
			newEndYear: '',
			showStartDaysList30: false,
			showStartDaysList31: true,
			showEndDaysList30: false,
			showEndDaysList31: true
		}
	}

	handleCloseModal = () => {
		this.props.closeModal();
	}

	handleChartSizeUpdate = (size) => {
		this.props.changeChartSizeSettings(size);
	}

	handleShowAltPercent = () => {
		console.log('Hi');
		this.props.showAltPercent();
	}

	handleShowPercent = () => {
		console.log('Hello');
		this.props.showPercent();
	}

	handleChartChange = (e) => {
		console.log('What is e 1: ', e);
		const { chartSizes } = this.state;
		for (let size of chartSizes) {
			if (size.name == e.target.value) {
				this.setState({
					newChartHeight: size.value,
					chartHeightName: size.name
				});
				this.handleChartSizeUpdate(size.value);
			}
		}
	}

	handleStartMonthChange = (e) => {
		const { newStartDate, newEndDate, endMonthsList } = this.state;
		const newStartDateFields = newStartDate.split('-');
		var editNewStartDate = newStartDateFields[0]+'-'+e.target.value+'-'+newStartDateFields[2];
		const newEndMonthsList = [];
		if (e.target.value == '03' || e.target.value == '05' || e.target.value == '07' || e.target.value == '08') {
			this.setState({
				showStartDaysList31: true,
				showStartDaysList30: false,
				newStartMonth: e.target.value,
				newStartDate: editNewStartDate
			});
		}
		if (e.target.value == '04' || e.target.value == '06' || e.target.value == '09') {
			this.setState({
				showStartDaysList30: true,
				showStartDaysList31: false,
				newStartMonth: e.target.value,
				newStartDate: editNewStartDate
			});
		}
		for (var i = 0; i < endMonthsList.length; i++) {
			if (e.target.value <= endMonthsList[i].value) {
				newEndMonthsList.push({
					id: endMonthsList[i].id,
					name: endMonthsList[i].name,
					value: endMonthsList[i].value,
					active: true
				});
			}
		}
		// console.log('newEndMonthsList: ', newEndMonthsList);
		if (newEndMonthsList[0].value > this.state.newEndMonth) {
			this.setState({endMonthsList: newEndMonthsList, newEndMonth: newEndMonthsList[0].value});
		} else {
			this.setState({endMonthsList: newEndMonthsList});
		}
		this.props.editChartDate(editNewStartDate, newEndDate);
	}

	handleStartDayChange = (e) => {
		console.log('What is e 2: ', e);
		const { showStartDaysList30, showStartDaysList31, days30, days31, newStartDate, newEndDate } = this.state;
		const newStartDateFields = newStartDate.split('-');
		const editNewStartDate = newStartDateFields[0]+'-'+newStartDateFields[1]+'-'+e.target.value;
		if (showStartDaysList30) {
			for (var i = 0; i < days30.length; i++) {
				if (days30[i].value == e.target.value) {
					this.setState({
						newStartDay: days30[i].value,
						newStartDate: editNewStartDate
					});
				}
			}
		}
		if (showStartDaysList31) {
			for (var i = 0; i < days31.length; i++) {
				if (days31[i].value == e.target.value) {
					this.setState({
						newStartDay: days31[i].value,
						newStartDate: editNewStartDate
					});
				}
			}
		}
		this.props.editChartDate(editNewStartDate, newEndDate);
	}

	handleStartYearChange = (e) => {
		console.log('What is e3: ', e);
	}

	handleEndMonthChange = (e) => {
		console.log('What is e 4: ', e);
		const { newStartDate, newEndDate } = this.state;
		const newEndDateFields = newEndDate.split('-');
		var editNewEndDate = newEndDateFields[0]+'-'+e.target.value+'-'+newEndDateFields[2];
		if (e.target.value == '03' || e.target.value == '05' || e.target.value == '07' || e.target.value == '08') {
			this.setState({
				showEndDaysList31: true,
				showEndDaysList30: false,
				newEndMonth: e.target.value,
				newEndDate: editNewEndDate
			});
		}
		if (e.target.value == '04' || e.target.value == '06') {
			this.setState({
				showEndDaysList30: true,
				showEndDaysList31: false,
				newEndMonth: e.target.value,
				newEndDate: editNewEndDate
			});
		}
		this.props.editChartDate(newStartDate, editNewEndDate);
	}

	handleEndDayChange = (e) => {
		console.log('What is e 5: ', e);
		const { showStartDaysList30, showStartDaysList31, days30, days31, newStartDate, newEndDate } = this.state;
		const newEndDateFields = newEndDate.split('-');
		const editNewEndDate = newEndDateFields[0]+'-'+newEndDateFields[1]+'-'+e.target.value;
		if (showStartDaysList30) {
			for (var i = 0; i < days30.length; i++) {
				if (days30[i].value == e.target.value) {
					this.setState({
						newEndDay: days30[i].value,
						newEndDate: editNewEndDate
					});
				}
			}
		}
		if (showStartDaysList31) {
			for (var i = 0; i < days31.length; i++) {
				if (days31[i].value == e.target.value) {
					this.setState({
						newEndDay: days31[i].value,
						newEndDate: editNewEndDate
					});
				}
			}
		}
		this.props.editChartDate(newStartDate, editNewEndDate);
	}

	handleEndYearChange = (e) => {
		console.log('What is e 6: ', e);
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({portraitMode: true});
		} else {
			this.setState({portraitMode: false});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	handleShowLineGraph = () => {
		this.props.displayLineGraph();
	}

	handleShowBarGraph = () => {
		this.props.displayBarGraph();
	}

	handleShowSevenDayAverage = () => {
		this.props.displaySevenDayAverage();
	}

	handleShowDailyCount = () => {
		this.props.displayDailyCount();
	}

	componentDidMount() {
		const { chartSizes, defaultChartHeight } = this.state;
		const { chartHeight, startDate, endDate } = this.props;
		for (var i = 0; i < chartSizes.length; i++) {
			if (chartSizes[i].value == chartHeight) {
				defaultChartHeight.id = chartSizes[i].id;
				defaultChartHeight.name = chartSizes[i].name;
				defaultChartHeight.value = chartSizes[i].value;
			}
		}

		const startDateFields = startDate.split('-');
		const endDateFields = endDate.split('-');

		const { defaultStartMonth, defaultStartDay, defaultStartYear } = this.state;
		const { defaultEndMonth, defaultEndDay, defaultEndYear } = this.state;

		defaultStartMonth.id = parseInt(startDateFields[1], 10);
		defaultStartMonth.name = startDateFields[1];
		defaultStartMonth.value = startDateFields[1];
		defaultStartDay.id = parseInt(startDateFields[2], 10);
		defaultStartDay.name = startDateFields[2];
		defaultStartDay.value = startDateFields[2];
		defaultStartYear.id = parseInt(startDateFields[0], 10);
		defaultStartYear.name = startDateFields[0];
		defaultStartYear.value = startDateFields[0];
		defaultEndMonth.id = parseInt(endDateFields[1], 10);
		defaultEndMonth.name = endDateFields[1];
		defaultEndMonth.value = endDateFields[1];
		defaultEndMonth.active = true;
		defaultEndDay.id = parseInt(endDateFields[2], 10);
		defaultEndDay.name = endDateFields[2];
		defaultEndDay.value = endDateFields[2];
		defaultEndYear.id = parseInt(endDateFields[0], 10);
		defaultEndYear.name = endDateFields[0];
		defaultEndYear.value = endDateFields[0];

		this.setState({
			defaultChartHeight: defaultChartHeight,
			chartHeightName: defaultChartHeight.name,
			newStartDate: startDate,
			newEndDate: endDate,
			startMonth: startDateFields[1],
			startDay: startDateFields[2],
			startYear: startDateFields[0],
			endMonth: endDateFields[1],
			endDay: endDateFields[2],
			endYear: endDateFields[0],
			newStartMonth: startDateFields[1],
			newStartDay: startDateFields[2],
			newStartYear: startDateFields[0],
			newEndMonth: endDateFields[1],
			newEndDay: endDateFields[2],
			newEndYear: endDateFields[0],
			defaultStartMonth: defaultStartMonth, 
			defaultStartDay: defaultStartDay,
			defaultStartYear: defaultStartYear,
			defaultEndMonth: defaultEndMonth,
			defaultEndDay: defaultEndDay,
			defaultEndYear: defaultEndYear
		});

		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastState) {

	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { chartSizes, defaultChartHeight, chartHeightName, portraitMode } = this.state;
		const { chartHeight, startDate, endDate } = this.props;
		const { showLineGraph, showBarGraph, displayLineBarButtons } = this.props;
		const { displayDailyAverageButtons, showSevenDayAverage, showDailyCount } = this.props;
		const { displayDateChangeControls } = this.props;
		const { startMonth, startDay, startYear, endMonth, endDay, endYear } = this.state;
		const { startMonthsList, endMonthsList, days30, days31, yearsList, showStartDaysList30, showStartDaysList31, showEndDaysList30, showEndDaysList31 } = this.state;
		const { defaultStartMonth, defaultStartDay, defaultStartYear, defaultEndMonth, defaultEndDay, defaultEndYear } = this.state;
		const { newStartMonth, newStartDay, newStartYear, newEndMonth, newEndDay, newEndYear } = this.state;
		const { displayPercentAltPercentButtons, showAltPercentGraph, showPercentGraph } = this.props;

		return (
			<div style={portraitMode ? styles.modalPortrait : styles.modal}>
				<Grid container style={styles.modalHeader}>
					<Grid item xs={11}>
						<span style={styles.modalTitle}>Chart Settings</span>
					</Grid>
					<Grid item xs={1}>
						<Button style={styles.closeButton} onClick={this.handleCloseModal}>X</Button>
					</Grid>
				</Grid>
				<Grid container>
					<Grid md={1} item />
					<Grid item md={8} xs={12} style={{padding: 10}}>
						<span style={{fontSize: 20, padding: 10}}>Chart Height:</span>
						<Select
							value={chartHeightName} 
							onChange={this.handleChartChange.bind(this)}
							inputProps={{
                name: defaultChartHeight.name,
                id: defaultChartHeight.id
              }} 
              style={styles.selectChartName} 
						>
							{chartSizes.map((size, index) => (
								<MenuItem value={size.name} key={index}>{size.name}</MenuItem>
							))}
						</Select>
					</Grid>
				</Grid>
				{displayDateChangeControls && 
					<Grid container style={{padding: 10}}>
						<Grid md={1} item />
						<Grid container item md={10} xs={12} style={{padding: 10}}>
							<Grid item xs={12}><span style={{fontSize: 20, padding: 10}}>Date Range:</span></Grid>
							<Grid container item xs={12}>
								<Grid container item sm={5} xs={12} style={{padding: 5}}>
									<Grid item sm={3} xs={4}>
										<Select
											value={newStartMonth} 
											onChange={this.handleStartMonthChange.bind(this)}
											inputProps={{
												name: defaultStartMonth.name,
                				id: defaultStartMonth.id
				              }} 
				              style={styles.selectDateRange} 
										>
											{startMonthsList.map((month, index) => (
												<MenuItem value={month.name} key={index}>{month.name}</MenuItem>
											))}
										</Select>
									</Grid>
									<Grid item sm={3} xs={4}>
										{showStartDaysList30 && 
											<Select
												value={newStartDay} 
												onChange={this.handleStartDayChange.bind(this)}
												inputProps={{
													name: defaultStartDay.name,
	                				id: defaultStartDay.id
					              }} 
					              style={styles.selectDateRange} 
											>
												{days30.map((day, index) => (
													<MenuItem value={day.name} key={index}>{day.name}</MenuItem>
												))}
											</Select>
										}
										{showStartDaysList31 && 
											<Select
												value={newStartDay} 
												onChange={this.handleStartDayChange.bind(this)}
												inputProps={{
													name: defaultStartDay.name,
	                				id: defaultStartDay.id
					              }} 
					              style={styles.selectDateRange} 
											>
												{days31.map((day, index) => (
													<MenuItem value={day.name} key={index}>{day.name}</MenuItem>
												))}
											</Select>
										}
									</Grid>
									<Grid item sm={3} xs={4}>
										<Select
											value={newStartYear} 
											onChange={this.handleStartYearChange.bind(this)}
											inputProps={{
												name: defaultStartYear.name,
                				id: defaultStartYear.id
				              }} 
				              style={styles.selectDateRange} 
										>
											{yearsList.map((year, index) => (
												<MenuItem value={year.name} key={index}>{year.name}</MenuItem>
											))}
										</Select>
									</Grid>
								</Grid>
								<Grid item sm={1} xs={12} style={{justifyContent: 'center', alignSelf: 'center', alignItems: 'center', textAlign: 'center'}}>
									<span style={{fontSize: 20, padding: 10}}>to</span>
								</Grid>
								<Grid container item sm={5} xs={12} style={{padding: 5}}>
									<Grid item sm={3} xs={4}>
										<Select
											value={newEndMonth} 
											onChange={this.handleEndMonthChange.bind(this)}
											inputProps={{
												name: defaultEndMonth.name,
                				id: defaultEndMonth.id,
                				active: true
				              }} 
				              style={styles.selectDateRange} 
										>
											{endMonthsList.map((month, index) => (
														<MenuItem value={month.name} key={index}>{month.name}</MenuItem>
											))}
										</Select>
									</Grid>
									<Grid item sm={3} xs={4}>
										{showEndDaysList30 && 
											<Select
												value={newEndDay} 
												onChange={this.handleEndDayChange.bind(this)}
												inputProps={{
													name: defaultEndDay.name,
	                				id: defaultEndDay.id
					              }} 
					              style={styles.selectDateRange} 
											>
												{days30.map((day, index) => (
													<MenuItem value={day.name} key={index}>{day.name}</MenuItem>
												))}
											</Select>
										}
										{showEndDaysList31 && 
											<Select
												value={newEndDay} 
												onChange={this.handleEndDayChange.bind(this)}
												inputProps={{
													name: defaultEndDay.name,
	                				id: defaultEndDay.id
					              }} 
					              style={styles.selectDateRange} 
											>
												{days31.map((day, index) => (
													<MenuItem value={day.name} key={index}>{day.name}</MenuItem>
												))}
											</Select>
										}
									</Grid>
									<Grid item sm={3} xs={4}>
										<Select
											value={newEndYear} 
											onChange={this.handleEndYearChange.bind(this)}
											inputProps={{
												name: defaultEndYear.name,
                				id: defaultEndYear.id
				              }} 
				              style={styles.selectDateRange} 
										>
											{yearsList.map((year, index) => (
												<MenuItem value={year.name} key={index}>{year.name}</MenuItem>
											))}
										</Select>
									</Grid>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
				}
				{displayLineBarButtons && 
					<Grid container style={{padding: 10}}>
						<Grid md={1} item />
						<Grid item md={4} xs={6} style={{padding: 10}}>
							<Button 
								style={showLineGraph ? styles.chartTypeSelectButtonSelected : styles.chartTypeSelectButton }
								onClick={this.handleShowLineGraph}
								>
									Line Graph
								</Button>
						</Grid>
						<Grid item md={4} xs={6} style={{padding: 10}}>
							<Button 
								style={showBarGraph ? styles.chartTypeSelectButtonSelected : styles.chartTypeSelectButton }
								onClick={this.handleShowBarGraph}
							>
								Bar Graph
							</Button>
						</Grid>
					</Grid>
				}
				{displayDailyAverageButtons && 
					<Grid container style={{padding: 10}}>
						<Grid md={1} item />
						<Grid item md={4} xs={6} style={{padding: 10}}>
							<Button 
								style={showSevenDayAverage ? styles.chartTypeSelectButtonSelected : styles.chartTypeSelectButton }
								onClick={this.handleShowSevenDayAverage}
								>
									7 Day Average
								</Button>
						</Grid>
						<Grid item md={4} xs={6} style={{padding: 10}}>
							<Button 
								style={showDailyCount ? styles.chartTypeSelectButtonSelected : styles.chartTypeSelectButton }
								onClick={this.handleShowDailyCount}
							>
								Daily Count
							</Button>
						</Grid>
					</Grid>
				}
				{displayPercentAltPercentButtons && 
					<div>
						<Grid container style={{padding: 10}}>
							<Grid md={1} item />
							<Grid item md={4} xs={6} style={{padding: 10}}>
								<Button 
									style={showAltPercentGraph ? styles.chartTypeSelectButtonSelected : styles.chartTypeSelectButton }
									onClick={this.handleShowAltPercent}
									>
										Alt Percent
									</Button>
							</Grid>
							<Grid item md={4} xs={6} style={{padding: 10}}>
								<Button 
									style={showPercentGraph ? styles.chartTypeSelectButtonSelected : styles.chartTypeSelectButton }
									onClick={this.handleShowPercent}
								>
									Percent
								</Button>
							</Grid>
						</Grid>
						<Grid container style={{padding: 10}}>
							<Grid md={1} item />
							<Grid item md={10} xs={10} style={{padding: 10}}>
								<span style={{fontSize: 16}}>"Alt Percent" is Per Million for Cases and Tests, and Per 100,000 for Deaths</span>
							</Grid>
						</Grid>
					</div>
				}
			</div>
		);
	}
}

export default SettingsModal;

const styles = {
	modalPortrait: {
		width: '90%',
		backgroundColor: '#FFF',
		boxShadow: '0 2px 8px rgba(0, 0, 0, .3)',
		borderRadius: 25,
		position: 'fixed',
		top: '4vh',
		left: '5%',
		maxHeight: '100vh',
		minHeight: '80vh',
		overflowY: 'auto',
		overflowScrolling: "touch",
		WebkitOverflowScrolling: "touch",
		zIndex: 1000,
	},
	modal: {
		width: '60%',
		backgroundColor: '#FFF',
		boxShadow: '0 2px 8px rgba(0, 0, 0, .3)',
		borderRadius: 25,
		position: 'fixed',
		top: '4vh',
		left: '20%',
		maxHeight: '100vh',
		minHeight: '80vh',
		overflowY: 'auto',
		overflowScrolling: "touch",
		WebkitOverflowScrolling: "touch",
		zIndex: 1000,
	},
	modalHeader: {
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#666',
		padding: 10
	},
	modalTitle: {
		fontSize: 28,
		fontWeight: 'bold',
		padding: 10,
		color: '#FFF'
	},
	closeButton: {
		position: 'relative',
		borderRadius: '50%',
		backgroundColor: '#FFF',
		color: '#000',
		fontSize: 24,
		right: 30,
		border: '2px solid #666',
		boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
	},
	selectChartName: {
		paddingLeft: 10,
		paddingRight: 10,
		fontSize: 20
	},
	chartTypeSelectButton: {
		backgroundColor: '#FFF',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	chartTypeSelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	selectDateRange: {
		paddingLeft: 5,
		paddingRight: 5,
		fontSize: 20
	},
}