import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

import SettingsModal from './SettingsModal';
import ModalBackdrop from './ModalBackdrop';
import SettingsModalIcon from './SettingsModalIcon';

import { getSFData, getNYCCaseData, getHongKongData, clearComparisonData } from '../actions/cities';

class CityComparison extends Component {
	constructor() {
		super();
		this.state = {
			cities: [
				{
	        id: "SF",
	        name: "San Francisco",
	        chartColor: "#D6E9C6",
	        population: 883305,
	        active: true
		    },
		    {
	        id: "NYC",
	        name: "New York City",
	        chartColor: "#FAEBCC",
	        population: 8399000,
	        active: true
		    },
		    {
		    	id: "HK",
		    	name: "Hong Kong",
	        chartColor: "#87142b",
	        population: 7451000,
	        active: true
		    }
			],
			citiesChartData: [],
			showChart: false,
			sfData: [],
			sfChartReady: false,
			nycData: [],
			nycChartReady: false,
			hkData: [],
			hkChartReady: false,
			totalCasesChart: {},
			percentTotalCasesChart: {},
			dailyTotalCasesChart: {},
			datasetsTotalCases: [],
			datasetsPercentTotalCases: [],
			datasetsDailyTotalCases: [],
			showTotalCases: true,
			showPercentTotalCases: true,
			showDailyTotalCases: true,
			sfPopulation: 883305,
			sfColor: '#D6E9C6',
			nycPopulation: 8399000,
			nycColor: '#FAEBCC',
			hkPopulation: 7451000,
			hkColor: '#87142b',
			runDataPrep: false,
			datesArray: [],
			todaysDate: '',
			dataSFReverse: [],
			dataNYCReverse: [],
			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: false,
			displayDailyAverageButtons: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom'
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	getDatesList = () => {
		var listDate = [];
		var startDate ='2020-01-23';
		var endDate = this.state.todaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		this.setState({
			datesArray: listDate
		});
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();
		// var dd = String(date.getDate()).padStart(2, '0');
		// var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
		// var yyyy = date.getFullYear();
		// var today = yyyy + '-' + mm + '-' + dd;
		var newDate = date.setDate(date.getDate() -1);
		// console.log('New Date: ', newDate);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		// console.log('Yesterday: ', today);
		this.setState({
			todaysDate: today
		});
	}

	prepData = () => {
		for (let city of this.selectedCities) {
			if (city === "HK") {
				this.prepChartDataHK();
			}
			if (city === "NYC") {
				this.prepChartDataNYC();
			}
			if (city === "SF") {
				this.prepChartDataSF();
			}
		}
	}

	handleChangeCityComparison = (id) => {
		const { cities } = this.state;
		if (this.selectedCities.has(id)) {
			let { citiesChartData } = this.state;
			this.selectedCities.delete(id);
			let newCitiesChartData = citiesChartData.filter(e => e.id !== id);
			for (var i = 0; i < cities.length; i++) {
				if (cities[i].id == id) {
					cities[i].active = false;
				}
			}
			this.setState({citiesChartData: [], cities: cities});
		} else {
			this.selectedCities.add(id);
			for (var i = 0; i < cities.length; i++) {
				if (cities[i].id == id) {
					cities[i].active = true;
				}
			}
			this.setState({runDataPrep: true, cities: cities});
		}
	}

	formatDateLabel = (s) => {
		var fields = s.split('-');
		return fields[1]+'/'+fields[2];
	}

	setChartData = () => {
		this.setState({
			totalCasesChart: {},
			percentTotalCasesChart: {},
			dailyTotalCasesChart: {},
			datasetsTotalCases: [],
			datasetsPercentTotalCases: [],
			datasetsDailyTotalCases: [],
		});
		const { sfChartReady, nycChartReady, hkChartReady } = this.state;
		const datasetsTotalCases = [];
		const datasetsPercentTotalCases = [];
		const datasetsDailyTotalCases = [];
		if (!sfChartReady || !nycChartReady || !hkChartReady) {
			return;
		}
		const { citiesChartData, datesArray } = this.state;
		const labelDatesSet = new Set();
		for (var i = 0; i < datesArray.length; i++) {
			labelDatesSet.add(datesArray[i]);
		}
		const labelDatesArray = Array.from(labelDatesSet);
		var dailyTotal = [];
		var total = [];
		var percentTotal = [];
		var dayFoundCounter = 0;
		var lastDailyTotal = 0;
		var lastTotal = 0;
		var lastPercentTotal = 0;
		var lastCityName = '';
		for (var i = 0; i < citiesChartData.length; i++) {
			for (var j = 0; j < labelDatesArray.length; j++) {
				for (var k = 0; k < citiesChartData[i].data.length; k++) {
					if (citiesChartData[i].data[k].date == labelDatesArray[j]) {
						dailyTotal.push(citiesChartData[i].data[k].daily_total);
						total.push(citiesChartData[i].data[k].cumulative_total);
						percentTotal.push(parseFloat(citiesChartData[i].data[k].percent_total));
						lastDailyTotal = citiesChartData[i].data[k].daily_total;
						lastTotal = citiesChartData[i].data[k].cumulative_total;
						lastPercentTotal = parseFloat(citiesChartData[i].data[k].percent_total);
						dayFoundCounter += 1;
					}
				}
				if (dayFoundCounter == 0) {
					dailyTotal.push(lastDailyTotal);
					total.push(lastTotal);
					percentTotal.push(lastPercentTotal);
					dayFoundCounter = 0;
				}
				lastDailyTotal = 0;
				lastTotal = 0;
				lastPercentTotal = 0;
				lastCityName = citiesChartData[i].name;
			}
			datasetsTotalCases.push({
				data: total,
				label: citiesChartData[i].name,
				fill: false,
				borderColor: citiesChartData[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentTotalCases.push({
				data: percentTotal,
				label: citiesChartData[i].name,
				fill: false,
				borderColor: citiesChartData[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyTotalCases.push({
				data: dailyTotal,
				label: citiesChartData[i].name,
				backgroundColor: citiesChartData[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			dailyTotal = [];
			total = [];
			percentTotal = [];
			dayFoundCounter = 0;
		}
		const labelDates = [];
		for (var i = 0; i < datesArray.length; i++) {
			labelDates.push(this.formatDateLabel(datesArray[i]));
		}
		this.setState({
			totalCasesChart: {
				labels: labelDates,
				datasets: datasetsTotalCases
			},
			percentTotalCasesChart: {
				labels: labelDates,
				datasets: datasetsPercentTotalCases
			},
			dailyTotalCasesChart: {
				labels: labelDates,
				datasets: datasetsDailyTotalCases
			},
			showChart: true
		})
	}

	formatDateSF = (s) => {
		var fields = s.split('T');
		return fields[0];
	}

	prepChartDataSF = () => {
		if (this.selectedCities.has('SF')) {
			if (this.props.loadingDataSF === false) {
				const { citiesChartData, dataSFReverse, datesArray } = this.state;
				let cityCounter = 0;
				for (var i = 0; i < citiesChartData.length; i++) {
					if (citiesChartData[i].id == "SF") {
						cityCounter += 1;
					}
				}
				if (cityCounter == 0) {
					const { dataSF } = this.props;
					const { sfPopulation, sfColor } = this.state;
					const sfData = [];
					const labelDatesSet = new Set();
					if (dataSFReverse.length == 0) {
						dataSF.reverse().map(datum => {
							dataSFReverse.push(datum);
						})
					}
					dataSFReverse.map((datum, index) => {
						labelDatesSet.add(datum.specimen_collection_date.split('T')[0]);
					});
					const labelDatesArray = Array.from(labelDatesSet);
					var sfDailyTotal = 0;
					var sfTotal = 0;
					var sfPercentTotal = 0;
					for (var i = 0; i < datesArray.length; i++) {
						dataSFReverse.map((datum, index) => {
							if (datum.specimen_collection_date.split('T')[0] == datesArray[i]) {
								if (datum.case_disposition == "Confirmed") {
									sfDailyTotal += parseInt(datum.case_count);
									sfTotal += parseInt(datum.case_count);
								}
							}
						});
						sfPercentTotal = (sfTotal/sfPopulation*100).toFixed(3);
						sfData.push({date: this.formatDateSF(datesArray[i]), daily_total: sfDailyTotal, cumulative_total: sfTotal, percent_total: sfPercentTotal});
						sfDailyTotal = 0;
					}
					citiesChartData.push({
						id: 'SF',
						name: 'San Francisco', 
						data: sfData,
						color: sfColor
					})
					this.setState({
						citiesChartData: citiesChartData,
						sfData: sfData,
						sfChartReady: true,
						dataSFReverse: dataSFReverse
					});
				}
			}
		}
		this.setChartData();
	}

	formatDateNYC = (s) => {
		var fields = s.split('T');
		return fields[0];
	}

	prepChartDataNYC = () => {
		if (this.selectedCities.has('NYC')) {
			if (this.props.loadingDataNYCCases === false) {
				const { citiesChartData, dataNYCReverse, datesArray } = this.state;
				let cityCounter = 0;
				for (var i = 0; i < citiesChartData.length; i++) {
					if (citiesChartData[i].id == "NYC") {
						cityCounter += 1;
					}
				}
				if (cityCounter == 0) {
					const { dataNYCCases } = this.props;
					const { nycPopulation, citiesChartData, nycColor } = this.state;
					const nycData = [];
					const labelDatesSet = new Set();
					if (dataNYCReverse.length == 0) {
						dataNYCCases.reverse().map(datum => {
							dataNYCReverse.push(datum);
						})
					}
					dataNYCReverse.map((datum, index) => {
						labelDatesSet.add(datum.test_date);
					});
					const labelDatesArray = Array.from(labelDatesSet);
					var nycDailyTotal = 0;
					var nycTotal = 0;
					var nycPercentTotal = 0;
					for (var i = 0; i < datesArray.length; i++) {
						dataNYCReverse.map((datum, index) => {
							if (this.formatDateNYC(datum.test_date) == datesArray[i]) {
								if (datum.county == "New York" || datum.county == "Queens" || datum.county == "Kings" || datum.county == "Bronx" || datum.county == "Richmond") {
									nycDailyTotal += parseInt(datum.new_positives);
									nycTotal += parseInt(datum.new_positives);
								}
							}
						});
						nycPercentTotal = (nycTotal/nycPopulation*100).toFixed(3);
						nycData.push({date: datesArray[i], daily_total: nycDailyTotal,  cumulative_total: nycTotal, percent_total: nycPercentTotal});
						nycDailyTotal = 0;
					}
					citiesChartData.push({
						id: 'NYC',
						name: 'New York City', 
						data: nycData,
						color: nycColor
					});
					this.setState({
						citiesChartData: citiesChartData,
						nycData: nycData,
						nycChartReady: true,
						dataNYCReverse: dataNYCReverse
					});
				}
			}
		}
		this.setChartData();
	}

	formatDateHK = (s) => {
		var date = s.split('/');
		return date[2]+'-'+date[1]+'-'+date[0]
	}

	prepChartDataHK = () => {
		if (this.selectedCities.has('HK')) {
			if (this.props.loadingDataHK === false) {
				const { citiesChartData } = this.state;
				let cityCounter = 0;
				for (var i = 0; i < citiesChartData.length; i++) {
					if (citiesChartData[i].id == "HK") {
						cityCounter += 1;
					}
				}
				if (cityCounter == 0) {
					const { dataHK } = this.props;
					const { hkPopulation, citiesChartData, hkColor, datesArray } = this.state;
					const hkData = [];
					const labelDatesSet = new Set();
					dataHK.map((datum, index) => {
						labelDatesSet.add(datum["Report date"])
					});
					const labelDatesArray = Array.from(labelDatesSet);
					var hkDailyTotal = 0;
					var hkTotal = 0;
					var hkTotalPercent = 0;
					var date = ''
					for (var i = 0; i < datesArray.length; i++) {
						dataHK.map((datum, index) => {
							if (this.formatDateHK(datum["Report date"]) == datesArray[i]) {
								date = datesArray[i];
								hkDailyTotal += 1;
								hkTotal += 1;
							}
						});
						hkTotalPercent = (hkTotal / hkPopulation * 100).toFixed(3);
						hkData.push({date: datesArray[i], daily_total: hkDailyTotal, cumulative_total: hkTotal, percent_total: hkTotalPercent});
						hkDailyTotal = 0;
					}
					citiesChartData.push({
						id: 'HK',
						name: 'Hong Kong',
						data: hkData,
						color: hkColor
					})
					this.setState({
						citiesChartData: citiesChartData,
						hkData: hkData,
						hkChartReady: true
					});
				}
			}
		}
		this.setChartData();
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.selectedCities = new Set();
		this.props.getSFData();
		this.props.getNYCCaseData();
		this.props.getHongKongData();
		this.selectedCities.add('SF');
		this.selectedCities.add('NYC');
		this.selectedCities.add('HK');
		this.getTodaysDate();
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.loadingDataSF === false && (lastProps.loadingDataSF === true || lastProps.loadingDataSF === undefined)) {
			this.prepData();
		}
		if (this.props.loadingDataNYCCases === false && (lastProps.loadingDataNYCCases === true || lastProps.loadingDataNYCCases === undefined)) {
			this.prepData();
		}
		if (this.props.loadingDataHK === false && (lastProps.loadingDataHK === true || lastProps.loadingDataHK === undefined)) {
			this.prepData();
		}
		if (this.state.sfChartReady === true && lastState.sfChartReady === false) {
			this.setChartData();
		}
		if (this.state.nycChartReady === true && lastState.nycChartReady === false) {
			this.setChartData();
		}
		if (this.state.hkChartReady === true && lastState.hkChartReady === false) {
			this.setChartData();
		}
		if (this.state.runDataPrep === true && lastState.runDataPrep === false) {
			this.prepData();
			this.setState({runDataPrep: false});
		}
		if (this.state.todaysDate !== '' && lastState.todaysDate === '') {
			this.getDatesList();
		}
		if (this.state.citiesChartData.length == 0 && lastState.citiesChartData.length > 0) {
			this.prepData();
		}
	}

	componentWillUnmount() {
		this.props.clearComparisonData();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { loadingDataSF, loadingDataNYCCases, loadingDataHK } = this.props;
		const { cities, showChart, showTotalCases, showPercentTotalCases, showDailyTotalCases, showAxisX, chartHeight, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons, displayDailyAverageButtons } = this.state;

		return (
			<div>
				 {this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} />
        }
        {this.state.showSettingsModal && <ModalBackdrop />}
				{showChart ? 
					<Grid container>
						{showPercentTotalCases && 
							<Grid item xs={12}>
								<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
									<Line
										data={this.state.percentTotalCasesChart}
										options={{
			                title: {
			                  display: this.props.displayTitle,
			                  text: 'Cases by Percent of Population',
			                  fontSize: 25
			                },
			                legend: {
			                  display: this.props.displayLegend,
			                  position: this.props.legendPosition
			                },
			                scales: {
			                	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [{ ticks: {mirror: true}, gridLines: {display:false, drawBorder: false} }]
			                },
			                maintainAspectRatio: false
				            }}
				        	/>
								</div>
							</Grid>
						}
						{showTotalCases &&
							<Grid item xs={12}>
								<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
									<Line
										data={this.state.totalCasesChart}
										options={{
			                title: {
			                  display: this.props.displayTitle,
			                  text: 'Total Cases',
			                  fontSize: 25
			                },
			                legend: {
			                  display: this.props.displayLegend,
			                  position: this.props.legendPosition
			                },
			                tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            }
					                }
							          } // end callbacks:
							        }, //end tooltips 
			                scales: {
			                	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
			                },
			                maintainAspectRatio: false
				            }}
				        	/>
								</div>
							</Grid>
						}
						{showDailyTotalCases &&
							<Grid item xs={12}>
								<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
									<Bar
										data={this.state.dailyTotalCasesChart}
										options={{
			                title: {
			                  display: this.props.displayTitle,
			                  text: 'Daily Confirmed Cases',
			                  fontSize: 25
			                },
			                legend: {
			                  display: this.props.displayLegend,
			                  position: this.props.legendPosition
			                },
			                tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            }
					                }
							          } // end callbacks:
							        }, //end tooltips 
			                scales: {
										    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
										  },
										  maintainAspectRatio: false
				            }}
				        	/>
								</div>
							</Grid>
						}
					</Grid> :
					<div style={styles.loadingData}>
						<span style={styles.loadingDataText}>Loading Data</span> <Dots />
					</div>

				}
				<Grid container>
					{cities.map((city, index) => (
						<Grid item sm={4} xs={6} key={index} style={{padding: 10, textAlign: 'center'}}>
							<Button key={index} style={city.active ? styles.cityButtonSelected : styles.cityButton} onClick={this.handleChangeCityComparison.bind(this, city.id)}>{city.name}</Button>
						</Grid>
					))}
				</Grid>
				<div style={styles.sourceURLContainer}>
					<a style={styles.sourceURL} target="_blank" href="https://data.sfgov.org/COVID-19/COVID-19-Cases-Summarized-by-Date-Transmission-and/tvq9-ec9w"><span style={styles.sourceURLText}>Source: Data.SFGov.org</span></a>
				</div>
				<div style={styles.sourceURLContainer}>
					<a style={styles.sourceURL} target="_blank" href="https://health.data.ny.gov/Health/New-York-State-Statewide-COVID-19-Testing/xdss-u53e"><span style={styles.sourceURLText}>Source: Health.Data.NY.gov</span></a>
				</div>
				<div style={styles.sourceURLContainer}>
					<a style={styles.sourceURL} target="_blank" href="https://api.data.gov.hk/v2/filter?q=%7B%22resource%22%3A%22http%3A%2F%2Fwww.chp.gov.hk%2Ffiles%2Fmisc%2Fenhanced_sur_covid_19_eng.csv%22%2C%22section%22%3A1%2C%22format%22%3A%22json%22%7D"><span style={styles.sourceURLText}>Source: Api.data.gov.hk</span></a>
				</div>
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		dataSF: state.cities.dataSF,
		loadingDataSF: state.cities.loadingDataSF,

		dataNYCCases: state.cities.dataNYCCases,
		loadingDataNYCCases: state.cities.loadingDataNYCCases,

		dataHK: state.cities.dataHK,
		loadingDataHK: state.cities.loadingDataHK
	}
}

export default connect(mapStateToProps, { getSFData, getNYCCaseData, getHongKongData, clearComparisonData})(CityComparison);

const styles = {
	chart: {
		margin: 20,
		flex: 1,
		minHeight: 300
	}, 
	chartWide: {
		margin: 20,
		flex: 1,
		minHeight: 400
	}, 
	cityButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px'
	},
	cityButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px'
	},
	loadingData: {
		padding: 20,
		textAlign: 'center'
	},
	loadingDataText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
	settingsModalPlaceholder: {
		position: 'relative'
	},
	settingsModalPosition: {
		position: 'fixed',
		bottom: 50,
		right: 100
	},
	settingsModalPositionMobile: {
		position: 'fixed',
		bottom: 50,
		right: 20
	},
	settingsModalCircle: {
		height: 100,
    width: 100,
    // -moz-border-radius: 50%;
    borderRadius: 50,
    backgroundColor: '#666',
    color: 'black',
    display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	settingsModalCirclePlus: {
		alignItems: 'center',
		fontSize: 48,
		fontWeight: 'bold',
		paddingBottom: 6,
		color: '#FFF'
	}
}










