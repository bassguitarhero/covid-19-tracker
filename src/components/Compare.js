import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';

import SettingsModalIcon from './SettingsModalIcon';
import SettingsModal from './SettingsModal';
import ModalBackdrop from './ModalBackdrop';

import { getSFDataCompare, getCityDataSFTesting, clearCityDataSFTesting, getCityDataSFHospitalization, clearCityDataSFHospitalization, getNYCCaseDataCompare, getHongKongDataCompare, getCHICaseDataCompare, clearComparisonDataCompare } from '../actions/cities';
import { getCaseDataCaCountyCompare, clearCaseDataCaCountyCompare, getHospitalDataCaCountyCompare, clearHospitalDataCaCountyCompare, getCaseDataCOCountyCompare, clearCaseDataCOCountyCompare, getCaseDataCTCountyCompare, clearCaseDataCTCountyCompare, getCaseDataGACountyCompare, clearCaseDataGACountyCompare, getCaseDataINCountyCompare, clearCaseDataINCountyCompare, getCaseDataMDCountyCompare, clearCaseDataMDCountyCompare, getCaseDataNyCountyCompare, clearCaseDataNyCountyCompare, getCaseDataVTCountyCompare, clearCaseDataVTCountyCompare, getCaseDataWICountyCompare, clearCaseDataWICountyCompare } from '../actions/counties';
import { getCovidTrackerSingleStateDailyCompare, resetSingleStateDailyCompare } from '../actions/covidtracker';
import { getWorldWideEuropaDataCompare, clearWorldWideEuropaDataCompare } from '../actions/europa';
import { getColorsList, getCountiesCAList, getCountiesCOList, getCountiesCTList, getCountiesGAList, getCountiesINList, getCountiesMDList, getCountiesNYList, getCountiesVTList, getCountiesWIList, getStatesList, clearImport } from '../actions/importData';

class Compare extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// Location Display 
			displayCityControls: false,
			displayCountyControls: false,
			displayStateControls: false,
			displayCountryControls: false,

			// City Controls
			displaySanFrancisco: false,
			displayNewYorkCity: false,
			displayHongKong: false,
			displayChicago: false, 

			// County Controls
			displayCalifornia: false,
			displayColorado: false,
			displayConnecticut: false, 
			displayGeorgia: false, 
			displayIndiana: false, 
			displayMaryland: false, 
			displayNewYork: false,
			displayVermont: false, 
			displayWisconsin: false, 

			// State Data
			stateName: '',
			stateAbbreviation: '',
			statePopulation: 0,
			triggerSetChartData: false,

			// Data sets
			selectedCities: [],
			selectedCounties: [],
			selectedStates: [],
			selectedCountries: [],

			// Selected Locations Data
			selectedLocationsData: [],

			// Charts
			// Line
			chartTotalCasesLine: {},
			chartPercentCasesLine: {},
			chartAltPercentCasesLine: {},
			chartDailyCasesLine: {},
			chartSevenDayAverageCasesLine: {},
			chartTotalDeathsLine: {},
			chartPercentDeathsLine: {},
			chartAltPercentDeathsLine: {},
			chartDailyDeathsLine: {},
			chartSevenDayAverageDeathsLine: {},
			chartTotalTestsLine: {},
			chartPercentTestsLine: {},
			chartAltPercentTestsLine: {},
			chartDailyPositiveTestsLine: {},
			chartSevenDayAveragePositiveTestsLine: {},
			chartHospitalizationsLine: {},
			chartSevenDayAverageHospitalizationsLine: {},
			chartICULine:  {},
			chartSevenDayAverageICULine: {},

			// Bar
			chartTotalCasesBar: {},
			chartPercentCasesBar: {},
			chartAltPercentCasesBar: {},
			chartDailyCasesBar: {},
			chartSevenDayAverageCasesBar: {},
			chartTotalDeathsBar: {},
			chartPercentDeathsBar: {},
			chartAltPercentDeathsBar: {},
			chartDailyDeathsBar: {},
			chartSevenDayAverageDeathsBar: {},
			chartTotalTestsBar: {},
			chartPercentTestsBar: {},
			chartAltPercentTestsBar: {},
			chartDailyPositiveTestsBar: {},
			chartSevenDayAveragePositiveTestsBar: {},
			chartHospitalizationsBar: {},
			chartSevenDayAverageHospitalizationsBar: {},
			chartICUBar:  {},
			chartSevenDayAverageICUBar: {},

			// Chart Controls
			showChart: false, 

			showTotal: true,
			showPercent: false,
			showDaily: false,
			showCases: true,
			showDeaths: false,
			showTests: false, 
			showHospitalizations: false,
			showICU: false,
			showControls: true,
			
			loadingData: false,
			loadingCountriesData: true,
			processingCountriesData: false, 
			
			datesArray: [],
			newDatesArray: null,
			todaysDate: '',
			startDate: '2020-03-01',
			endDate: '',
			newStartDate: '',
			newEndDate: '',
			showAxisX: false,
			showSkinnyButtons: false, 
			windowWidth: 0,
			windowHeight: 0,
			showSettingsModal: false,
			chartHeight: 300,
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: true,
			displayDailyAverageButtons: true,
			showSevenDayAverage: true,
			showDailyCount: false,
			displayCountriesList: false,

			citySanFrancisco: {
				name: "San Francisco",
				population: 883305,
				active: false
			},
			cityNewYorkCity: {
				name: "New York City",
				population: 8399000,
				active: false
			},
			cityHongKong: {
				name: "Hong Kong",
				population: 7451000,
				active: false
			},
			cityChicago: {
				name: "Chicago",
				population: 2706000,
				active: false
			},

			colorsList: [],
			citiesList: [],
			countiesCaliforniaList: [],
			countiesColoradoList:  [],
			countiesConnecticutList: [],
			countiesGeorgiaList: [],
			countiesIndianaList: [],
			countiesMarylandList: [],
			countiesNewYorkList: [],
			countiesVermontList: [],
			countiesWisconsinList: [],
			statesList: [],
			countriesList: [],
			marylandData: [],
			displayEU: false,
			populationEU: 446000000,
			countriesEuropeanUnion: [
				'Austria', 'Belgium', 'Bulgaria', 'Croatia', 'Cyprus', 'Czechia', 'Denmark', 'Estonia', 'Findland',
				'France', 'Germany', 'Greece', 'Hungary', 'Ireland', 'Italy', 'Latvia', 'Lithuania',
				'Luxembourg', 'Malta', 'Netherlands', 'Poland', 'Portugal', 'Romania', 'Slovakia', 'Slovenia', 
				'Spain', 'Sweden'
			],
			displayDateChangeControls: true,
			displayPercentAltPercentButtons: true, 
			showAltPercentGraph: true, 
			showPercentGraph: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom'
	}

	handleShowAltPercent = () => {
		this.setState({showAltPercentGraph: true, showPercentGraph: false})
	}

	handleShowPercent = () => {
		this.setState({showAltPercentGraph: false, showPercentGraph: true})
	}

	handleShowSevenDayAverage = () => {
		this.setState({showSevenDayAverage: true, showDailyCount: false});
	}

	handleShowDailyCount = () => {
		this.setState({showSevenDayAverage: false, showDailyCount: true});
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	createNewDatesArray = (newStartDate, newEndDate) => {
		// const { newStartDate, newEndDate } = this.state;
		// console.log('Final new Start date: ', newStartDate);
		// console.log('Final new End Date: ', newEndDate);
		var listDate = [];
		var listDateFormatted = [];
		// var startDate = '2020-03-01';
		// var endDate = this.state.todaysDate;
		var dateMove = new Date(newStartDate);
		var strDate = newStartDate;
		while (strDate < newEndDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		for (var i = 0; i < listDate.length; i++) {
			listDateFormatted.push(listDate[i]);
		}
		// console.log('New Dates Array: ', listDateFormatted);
		this.setState({
			newDatesArray: listDateFormatted,
			triggerSetChartData: true
		});
		// this.triggerSetChartData();
		// if (selectedLocationsData.length > 0) {
		// 	this.triggerSetChartData();
		// }
	}

	handleEditChartDate = (newStartDate, newEndDate) => {
		// console.log('Start Date, End Date: ', newStartDate, newEndDate)
		this.setState({
			newStartDate: newStartDate,
			newEndDate: newEndDate,
		});
		this.createNewDatesArray(newStartDate, newEndDate);
	}

	handleDisplayTotal = () => {
		this.setState({
			showTotal: true,
			showPercent:  false,
			showDaily: false
		});
	}

	handleDisplayPercent = () => {
		this.setState({
			showTotal: false,
			showPercent:  true,
			showDaily: false
		});
	}

	handleDisplayDaily = () => {
		this.setState({
			showTotal: false,
			showPercent:  false,
			showDaily: true
		});
	}

	handleDisplayCases = () => {
		this.setState({
			showCases: true,
			showDeaths: false,
			showTests: false,
			showHospitalizations: false,
			showICU: false,
			showControls: true
		});
	}

	handleDisplayDeaths = () => {
		this.setState({
			showCases: false,
			showDeaths: true,
			showTests: false,
			showHospitalizations: false,
			showICU: false,
			showControls: true
		});
	}

	handleDisplayTests = () => {
		this.setState({
			showCases: false, 
			showDeaths: false,
			showTests: true,
			showHospitalizations: false,
			showICU: false,
			showControls: true
		})
	}

	handleDisplayHospitalizations = () => {
		this.setState({
			showCases: false,
			showDeaths: false,
			showTests: false,
			showHospitalizations: true,
			showICU: false,
			showControls: false
		});
	}

	handleDisplayICU = () => {
		this.setState({
			showCases: false,
			showDeaths: false,
			showTests: false,
			showHospitalizations: false,
			showICU: true,
			showControls: false
		});
	}

	changeChartSizeSettings = (chartHeight) => {
		this.setState({
			chartHeight: chartHeight
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	setChartData = () => {
		const { citiesList, selectedCounties, selectedStates, countriesList, colorsList, newDatesArray } = this.state;
		var { datesArray } = this.state;
		if (newDatesArray !== null) {
			datesArray = [...newDatesArray];
		}
		// console.log('Dates Array: ', datesArray);
		// console.log('Cities List: ', citiesList);
		// console.log('Counties List: ', selectedCounties);

		//Line
		var datasetsTotalCasesLine = [];
		var datasetsPercentCasesLine = [];
		var datasetsAltPercentCasesLine = [];
		var	datasetsDailyCasesLine = [];
		var datasetsSevenDayAverageLine = [];
		var	datasetsTotalDeathsLine = [];
		var datasetsPercentDeathsLine = [];
		var datasetsAltPercentDeathsLine = [];
		var datasetsDailyDeathsLine = [];
		var datasetsSevenDayAverageDeathsLine = [];
		var datasetsTotalTestsLine = [];
		var datasetsPercentTestsLine = [];
		var datasetsAltPercentTestsLine = [];
		var datasetsDailyTestsLine = [];
		var datasetsDailyPositivePercentLine = [];
		var datasetsSevenDayAveragePositiveTestsLine = [];
		var datasetsHospitalizationsLine = [];
		var datasetsSevenDayAverageHospitalizationsLine = [];
		var datasetsICUBedsLine = [];
		var datasetsSevenDayAverageICUBedsLine = [];

		// Bar
		var datasetsTotalCasesBar = [];
		var datasetsPercentCasesBar = [];
		var datasetsAltPercentCasesBar = [];
		var	datasetsDailyCasesBar = [];
		var datasetsSevenDayAverageBar = [];
		var	datasetsTotalDeathsBar = [];
		var datasetsPercentDeathsBar = [];
		var datasetsAltPercentDeathsBar = [];
		var datasetsDailyDeathsBar = [];
		var datasetsSevenDayAverageDeathsBar = [];
		var datasetsTotalTestsBar = [];
		var datasetsPercentTestsBar = [];
		var datasetsAltPercentTestsBar = [];
		var datasetsDailyTestsBar = [];
		var datasetsDailyPositivePercentBar = [];
		var datasetsSevenDayAveragePositiveTestsBar = [];
		var datasetsHospitalizationsBar = [];
		var datasetsSevenDayAverageHospitalizationsBar = [];
		var datasetsICUBedsBar = [];
		var datasetsSevenDayAverageICUBedsBar = [];

		var dataTotalCases = [];
		var dataPercentCases = [];
		var dataAltPercentCases = [];
		var dataDailyCases = [];
		var dataSevenDayAverage = [];
		var dataTotalDeaths = [];
		var dataPercentDeaths = [];
		var dataAltPercentDeaths = [];
		var dataDailyDeaths = [];
		var dataSevenDayAverageDeaths = [];
		var dataTotalTests = [];
		var dataPercentTests = [];
		var dataAltPercentTests = [];
		var dataDailyTests = [];
		var dataDailyPositivePercent = [];
		var dataSevenDayAveragePositiveTests = [];
		var dataHospitalizations = [];
		var dataSevenDayAverageHospitalizations = [];
		var dataICUBeds = [];
		var dataSevenDayAverageICUBeds = [];

		const labelDates = [];
		for (var i = 0; i < datesArray.length; i++) {
			var fields = datesArray[i].split('-');
			var date = fields[1]+'/'+fields[2];
			labelDates.push(date);
		}

		const chartDates = [];
		for (var i = 0; i < datesArray.length; i++) {
			chartDates.push(datesArray[i]);
		}

		// CITIES
		for (var i = 0; i < citiesList.length; i++) {
			for (var k = 0; k < chartDates.length; k++) {
				for (var j = 0; j < citiesList[i].data.length; j++) {
					if (chartDates[k] == citiesList[i].data[j].date) {
						dataTotalCases.push(citiesList[i].data[j].total_cases);
						dataPercentCases.push(citiesList[i].data[j].percent_cases);
						dataAltPercentCases.push(citiesList[i].data[j].altPercent_cases);
						dataDailyCases.push(citiesList[i].data[j].daily_cases);
						dataSevenDayAverage.push(citiesList[i].data[j].sevenDayAverage);
						dataTotalDeaths.push(citiesList[i].data[j].total_deaths);
						dataPercentDeaths.push(citiesList[i].data[j].percent_deaths);
						dataAltPercentDeaths.push(citiesList[i].data[j].altPercent_deaths);
						dataDailyDeaths.push(citiesList[i].data[j].daily_deaths);
						dataSevenDayAverageDeaths.push(citiesList[i].data[j].sevenDayAverageDeaths);
						dataTotalTests.push(citiesList[i].data[j].total_tests);
						dataPercentTests.push(citiesList[i].data[j].percent_tests);
						dataAltPercentTests.push(citiesList[i].data[j].altPercent_tests);
						dataDailyTests.push(citiesList[i].data[j].daily_tests);
						dataDailyPositivePercent.push(citiesList[i].data[j].daily_positive_percent);
						dataSevenDayAveragePositiveTests.push(citiesList[i].data[j].sevenDayAveragePositiveTests);
						dataHospitalizations.push(citiesList[i].data[j].hospitalizations);
						dataSevenDayAverageHospitalizations.push(citiesList[i].data[j].sevenDayAverageHospitalizations);
						dataICUBeds.push(citiesList[i].data[j].icuBeds);
						dataSevenDayAverageICUBeds.push(citiesList[i].data[j].sevenDayAverageICUBeds);
					}
				}
			}
					
			// Line
			datasetsTotalCasesLine.push({
				label: citiesList[i].name,
				data: dataTotalCases,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentCasesLine.push({
				label: citiesList[i].name,
				data: dataPercentCases,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentCasesLine.push({
				label: citiesList[i].name,
				data: dataAltPercentCases,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyCasesLine.push({
				label: citiesList[i].name,
				data: dataDailyCases,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageLine.push({
				label: citiesList[i].name,
				data: dataSevenDayAverage,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsTotalDeathsLine.push({
				label: citiesList[i].name,
				data: dataTotalDeaths,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentDeathsLine.push({
				label: citiesList[i].name,
				data: dataPercentDeaths,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentDeathsLine.push({
				label: citiesList[i].name,
				data: dataAltPercentDeaths,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyDeathsLine.push({
				label: citiesList[i].name,
				data: dataDailyDeaths,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageDeathsLine.push({
				label: citiesList[i].name,
				data: dataSevenDayAverageDeaths,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsTotalTestsLine.push({
				label: citiesList[i].name,
				data: dataTotalTests,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentTestsLine.push({
				label: citiesList[i].name,
				data: dataPercentTests,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentTestsLine.push({
				label: citiesList[i].name,
				data: dataAltPercentTests,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyTestsLine.push({
				label: citiesList[i].name,
				data: dataDailyTests,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyPositivePercentLine.push({
				label: citiesList[i].name,
				data: dataDailyPositivePercent,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAveragePositiveTestsLine.push({
				label: citiesList[i].name,
				data: dataSevenDayAveragePositiveTests,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsHospitalizationsLine.push({
				label: citiesList[i].name,
				data: dataHospitalizations,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageHospitalizationsLine.push({
				label: citiesList[i].name,
				data: dataSevenDayAverageHospitalizations,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsICUBedsLine.push({
				label: citiesList[i].name,
				data: dataICUBeds,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageICUBedsLine.push({
				label: citiesList[i].name,
				data: dataSevenDayAverageICUBeds,
				fill: false,
				borderColor: citiesList[i].color,
				pointRadius: 1,
				lineWidth: 3
			});

			// Bar
			datasetsTotalCasesBar.push({
				label: citiesList[i].name,
				data: dataTotalCases,
				backgroundColor: citiesList[i].color
			});
			datasetsPercentCasesBar.push({
				label: citiesList[i].name,
				data: dataPercentCases,
				backgroundColor: citiesList[i].color
			});
			datasetsAltPercentCasesBar.push({
				label: citiesList[i].name,
				data: dataAltPercentCases,
				backgroundColor: citiesList[i].color
			});
			datasetsDailyCasesBar.push({
				label: citiesList[i].name,
				data: dataDailyCases,
				backgroundColor: citiesList[i].color
			});
			datasetsSevenDayAverageBar.push({
				label: citiesList[i].name,
				data: dataSevenDayAverage,
				backgroundColor: citiesList[i].color
			});
			datasetsTotalDeathsBar.push({
				label: citiesList[i].name,
				data: dataTotalDeaths,
				backgroundColor: citiesList[i].color
			});
			datasetsPercentDeathsBar.push({
				label: citiesList[i].name,
				data: dataPercentDeaths,
				backgroundColor: citiesList[i].color
			});
			datasetsAltPercentDeathsBar.push({
				label: citiesList[i].name,
				data: dataAltPercentDeaths,
				backgroundColor: citiesList[i].color
			});
			datasetsDailyDeathsBar.push({
				label: citiesList[i].name,
				data: dataDailyDeaths,
				backgroundColor: citiesList[i].color
			});
			datasetsSevenDayAverageDeathsBar.push({
				label: citiesList[i].name,
				data: dataSevenDayAverageDeaths,
				backgroundColor: citiesList[i].color
			});
			datasetsTotalTestsBar.push({
				label: citiesList[i].name,
				data: dataTotalTests,
				backgroundColor: citiesList[i].color
			});
			datasetsPercentTestsBar.push({
				label: citiesList[i].name,
				data: dataPercentTests,
				backgroundColor: citiesList[i].color
			});
			datasetsAltPercentTestsBar.push({
				label: citiesList[i].name,
				data: dataAltPercentTests,
				backgroundColor: citiesList[i].color
			});
			datasetsDailyTestsBar.push({
				label: citiesList[i].name,
				data: dataDailyTests,
				backgroundColor: citiesList[i].color
			});
			datasetsDailyPositivePercentBar.push({
				label: citiesList[i].name,
				data: dataDailyPositivePercent,
				backgroundColor: citiesList[i].color
			});
			datasetsSevenDayAveragePositiveTestsBar.push({
				label: citiesList[i].name,
				data: dataSevenDayAveragePositiveTests,
				backgroundColor: citiesList[i].color
			});
			datasetsHospitalizationsBar.push({
				label: citiesList[i].name,
				data: dataHospitalizations,
				backgroundColor: citiesList[i].color
			});
			datasetsSevenDayAverageHospitalizationsBar.push({
				label: citiesList[i].name,
				data: dataSevenDayAverageHospitalizations,
				backgroundColor: citiesList[i].color
			});
			datasetsICUBedsBar.push({
				label: citiesList[i].name,
				data: dataICUBeds,
				backgroundColor: citiesList[i].color
			});
			datasetsSevenDayAverageICUBedsBar.push({
				label: citiesList[i].name,
				data: dataSevenDayAverageICUBeds,
				backgroundColor: citiesList[i].color
			});

			dataTotalCases = [];
			dataPercentCases = [];
			dataAltPercentCases = [];
			dataDailyCases = [];
			dataSevenDayAverage = [];
			dataTotalDeaths = [];
			dataPercentDeaths = [];
			dataAltPercentDeaths = [];
			dataDailyDeaths = [];
			dataSevenDayAverageDeaths = [];
			dataTotalTests = [];
			dataPercentTests = [];
			dataAltPercentTests = [];
			dataDailyTests = [];
			dataDailyPositivePercent = [];
			dataSevenDayAveragePositiveTests = [];
			dataHospitalizations = [];
			dataSevenDayAverageHospitalizations = [];
			dataICUBeds = [];
			dataSevenDayAverageICUBeds = [];
		}

		// COUNTIES
		for (let county of this.setSelectedCountiesCA) {
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == county && selectedCounties[i].state == "CA") {
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < selectedCounties[i].data.length; j++) {
							if (selectedCounties[i].data[j].date == chartDates[k]) {
								dataTotalCases.push(selectedCounties[i].data[j].total_cases);
								dataPercentCases.push(selectedCounties[i].data[j].percent_cases);
								dataAltPercentCases.push(selectedCounties[i].data[j].altPercent_cases);
								dataDailyCases.push(selectedCounties[i].data[j].daily_cases);
								dataSevenDayAverage.push(selectedCounties[i].data[j].sevenDayAverage);
								dataTotalDeaths.push(selectedCounties[i].data[j].total_deaths);
								dataPercentDeaths.push(selectedCounties[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(selectedCounties[i].data[j].altPercent_deaths);
								dataDailyDeaths.push(selectedCounties[i].data[j].daily_deaths);
								dataSevenDayAverageDeaths.push(selectedCounties[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(selectedCounties[i].data[j].total_tests);
								dataPercentTests.push(selectedCounties[i].data[j].percent_tests);
								dataAltPercentTests.push(selectedCounties[i].data[j].altPercent_tests);
								dataDailyTests.push(selectedCounties[i].data[j].daily_tests);
								dataDailyPositivePercent.push(selectedCounties[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(selectedCounties[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(selectedCounties[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(selectedCounties[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(selectedCounties[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(selectedCounties[i].data[j].sevenDayAverageICUBeds);
							}	
						}
					}
						
					// Line
					datasetsTotalCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyPositivePercentLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAveragePositiveTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});

					// Bar
					datasetsTotalCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyPositivePercentBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAveragePositiveTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						backgroundColor: selectedCounties[i].color
					});

					dataTotalCases = [];
					dataPercentCases = [];
					dataAltPercentCases = [];
					dataDailyCases = [];
					dataSevenDayAverage = [];
					dataTotalDeaths = [];
					dataPercentDeaths = [];
					dataAltPercentDeaths = [];
					dataDailyDeaths = [];
					dataSevenDayAverageDeaths = [];
					dataTotalTests = [];
					dataPercentTests = [];
					dataAltPercentTests = [];
					dataDailyTests = [];
					dataDailyPositivePercent = [];
					dataSevenDayAveragePositiveTests = [];
					dataHospitalizations = [];
					dataSevenDayAverageHospitalizations = [];
					dataICUBeds = [];
					dataSevenDayAverageICUBeds = [];
				}
			}
		}

		for (let county of this.setSelectedCountiesCO) {
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == county && selectedCounties[i].state == "CO") {
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < selectedCounties[i].data.length; j++) {
							if (selectedCounties[i].data[j].date == chartDates[k]) {
								dataTotalCases.push(selectedCounties[i].data[j].total_cases);
								dataPercentCases.push(selectedCounties[i].data[j].percent_cases);
								dataAltPercentCases.push(selectedCounties[i].data[j].altPercent_cases);
								dataDailyCases.push(selectedCounties[i].data[j].daily_cases);
								dataSevenDayAverage.push(selectedCounties[i].data[j].sevenDayAverage);
								dataTotalDeaths.push(selectedCounties[i].data[j].total_deaths);
								dataPercentDeaths.push(selectedCounties[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(selectedCounties[i].data[j].altPercent_deaths);
								dataDailyDeaths.push(selectedCounties[i].data[j].daily_deaths);
								dataSevenDayAverageDeaths.push(selectedCounties[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(selectedCounties[i].data[j].total_tests);
								dataPercentTests.push(selectedCounties[i].data[j].percent_tests);
								dataAltPercentTests.push(selectedCounties[i].data[j].altPercent_tests);
								dataDailyTests.push(selectedCounties[i].data[j].daily_tests);
								dataDailyPositivePercent.push(selectedCounties[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(selectedCounties[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(selectedCounties[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(selectedCounties[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(selectedCounties[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(selectedCounties[i].data[j].sevenDayAverageICUBeds);
							}	
						}
					}
						
					// Line
					datasetsTotalCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyPositivePercentLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAveragePositiveTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});

					// Bar
					datasetsTotalCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyPositivePercentBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAveragePositiveTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						backgroundColor: selectedCounties[i].color
					});

					dataTotalCases = [];
					dataPercentCases = [];
					dataAltPercentCases = [];
					dataDailyCases = [];
					dataSevenDayAverage = [];
					dataTotalDeaths = [];
					dataPercentDeaths = [];
					dataAltPercentDeaths = [];
					dataDailyDeaths = [];
					dataSevenDayAverageDeaths = [];
					dataTotalTests = [];
					dataPercentTests = [];
					dataAltPercentTests = [];
					dataDailyTests = [];
					dataDailyPositivePercent = [];
					dataSevenDayAveragePositiveTests = [];
					dataHospitalizations = [];
					dataSevenDayAverageHospitalizations = [];
					dataICUBeds = [];
					dataSevenDayAverageICUBeds = [];
				}
			}
		}

		for (let county of this.setSelectedCountiesCT) {
			// console.log('NY County: ', county);
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == county && selectedCounties[i].state == "CT") {
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < selectedCounties[i].data.length; j++) {
							if (selectedCounties[i].data[j].date == chartDates[k]) {
								dataTotalCases.push(selectedCounties[i].data[j].total_cases);
								dataPercentCases.push(selectedCounties[i].data[j].percent_cases);
								dataAltPercentCases.push(selectedCounties[i].data[j].altPercent_cases);
								dataDailyCases.push(selectedCounties[i].data[j].daily_cases);
								dataSevenDayAverage.push(selectedCounties[i].data[j].sevenDayAverage);
								dataTotalDeaths.push(selectedCounties[i].data[j].total_deaths);
								dataPercentDeaths.push(selectedCounties[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(selectedCounties[i].data[j].altPercent_deaths);
								dataDailyDeaths.push(selectedCounties[i].data[j].daily_deaths);
								dataSevenDayAverageDeaths.push(selectedCounties[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(selectedCounties[i].data[j].total_tests);
								dataPercentTests.push(selectedCounties[i].data[j].percent_tests);
								dataAltPercentTests.push(selectedCounties[i].data[j].altPercent_tests);
								dataDailyTests.push(selectedCounties[i].data[j].daily_tests);
								dataDailyPositivePercent.push(selectedCounties[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(selectedCounties[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(selectedCounties[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(selectedCounties[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(selectedCounties[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(selectedCounties[i].data[j].sevenDayAverageICUBeds);
							}	
						}
					}
						
					// Line
					datasetsTotalCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyPositivePercentLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAveragePositiveTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});

					// Bar
					datasetsTotalCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyPositivePercentBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAveragePositiveTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						backgroundColor: selectedCounties[i].color
					});

					dataTotalCases = [];
					dataPercentCases = [];
					dataAltPercentCases = [];
					dataDailyCases = [];
					dataSevenDayAverage = [];
					dataTotalDeaths = [];
					dataPercentDeaths = [];
					dataAltPercentDeaths = [];
					dataDailyDeaths = [];
					dataSevenDayAverageDeaths = [];
					dataTotalTests = [];
					dataPercentTests = [];
					dataAltPercentTests = [];
					dataDailyTests = [];
					dataDailyPositivePercent = [];
					dataSevenDayAveragePositiveTests = [];
					dataHospitalizations = [];
					dataSevenDayAverageHospitalizations = [];
					dataICUBeds = [];
					dataSevenDayAverageICUBeds = [];
				}
			}
		}

		for (let county of this.setSelectedCountiesGA) {
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == county && selectedCounties[i].state == "GA") {
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < selectedCounties[i].data.length; j++) {
							if (selectedCounties[i].data[j].date == chartDates[k]) {
								dataTotalCases.push(selectedCounties[i].data[j].total_cases);
								dataPercentCases.push(selectedCounties[i].data[j].percent_cases);
								dataAltPercentCases.push(selectedCounties[i].data[j].altPercent_cases);
								dataDailyCases.push(selectedCounties[i].data[j].daily_cases);
								dataSevenDayAverage.push(selectedCounties[i].data[j].sevenDayAverage);
								dataTotalDeaths.push(selectedCounties[i].data[j].total_deaths);
								dataPercentDeaths.push(selectedCounties[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(selectedCounties[i].data[j].altPercent_deaths);
								dataDailyDeaths.push(selectedCounties[i].data[j].daily_deaths);
								dataSevenDayAverageDeaths.push(selectedCounties[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(selectedCounties[i].data[j].total_tests);
								dataPercentTests.push(selectedCounties[i].data[j].percent_tests);
								dataAltPercentTests.push(selectedCounties[i].data[j].altPercent_tests);
								dataDailyTests.push(selectedCounties[i].data[j].daily_tests);
								dataDailyPositivePercent.push(selectedCounties[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(selectedCounties[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(selectedCounties[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(selectedCounties[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(selectedCounties[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(selectedCounties[i].data[j].sevenDayAverageICUBeds);
							}	
						}
					}
						
					// Line
					datasetsTotalCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyPositivePercentLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAveragePositiveTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});

					// Bar
					datasetsTotalCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyPositivePercentBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAveragePositiveTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						backgroundColor: selectedCounties[i].color
					});

					dataTotalCases = [];
					dataPercentCases = [];
					dataAltPercentCases = [];
					dataDailyCases = [];
					dataSevenDayAverage = [];
					dataTotalDeaths = [];
					dataPercentDeaths = [];
					dataAltPercentDeaths = [];
					dataDailyDeaths = [];
					dataSevenDayAverageDeaths = [];
					dataTotalTests = [];
					dataPercentTests = [];
					dataAltPercentTests = [];
					dataDailyTests = [];
					dataDailyPositivePercent = [];
					dataSevenDayAveragePositiveTests = [];
					dataHospitalizations = [];
					dataSevenDayAverageHospitalizations = [];
					dataICUBeds = [];
					dataSevenDayAverageICUBeds = [];
				}
			}
		}

		for (let county of this.setSelectedCountiesIN) {
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == county && selectedCounties[i].state == "IN") {
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < selectedCounties[i].data.length; j++) {
							if (selectedCounties[i].data[j].date == chartDates[k]) {
								dataTotalCases.push(selectedCounties[i].data[j].total_cases);
								dataPercentCases.push(selectedCounties[i].data[j].percent_cases);
								dataAltPercentCases.push(selectedCounties[i].data[j].altPercent_cases);
								dataDailyCases.push(selectedCounties[i].data[j].daily_cases);
								dataSevenDayAverage.push(selectedCounties[i].data[j].sevenDayAverage);
								dataTotalDeaths.push(selectedCounties[i].data[j].total_deaths);
								dataPercentDeaths.push(selectedCounties[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(selectedCounties[i].data[j].altPercent_deaths);
								dataDailyDeaths.push(selectedCounties[i].data[j].daily_deaths);
								dataSevenDayAverageDeaths.push(selectedCounties[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(selectedCounties[i].data[j].total_tests);
								dataPercentTests.push(selectedCounties[i].data[j].percent_tests);
								dataAltPercentTests.push(selectedCounties[i].data[j].altPercent_tests);
								dataDailyTests.push(selectedCounties[i].data[j].daily_tests);
								dataDailyPositivePercent.push(selectedCounties[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(selectedCounties[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(selectedCounties[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(selectedCounties[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(selectedCounties[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(selectedCounties[i].data[j].sevenDayAverageICUBeds);
							}	
						}
					}
						
					// Line
					datasetsTotalCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyPositivePercentLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAveragePositiveTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});

					// Bar
					datasetsTotalCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyPositivePercentBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAveragePositiveTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						backgroundColor: selectedCounties[i].color
					});

					dataTotalCases = [];
					dataPercentCases = [];
					dataAltPercentCases = [];
					dataDailyCases = [];
					dataSevenDayAverage = [];
					dataTotalDeaths = [];
					dataPercentDeaths = [];
					dataAltPercentDeaths = [];
					dataDailyDeaths = [];
					dataSevenDayAverageDeaths = [];
					dataTotalTests = [];
					dataPercentTests = [];
					dataAltPercentTests = [];
					dataDailyTests = [];
					dataDailyPositivePercent = [];
					dataSevenDayAveragePositiveTests = [];
					dataHospitalizations = [];
					dataSevenDayAverageHospitalizations = [];
					dataICUBeds = [];
					dataSevenDayAverageICUBeds = [];
				}
			}
		}

		for (let county of this.setSelectedCountiesMD) {
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == county && selectedCounties[i].state == "MD") {
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < selectedCounties[i].data.length; j++) {
							if (selectedCounties[i].data[j].date == chartDates[k]) {
								dataTotalCases.push(selectedCounties[i].data[j].total_cases);
								dataPercentCases.push(selectedCounties[i].data[j].percent_cases);
								dataAltPercentCases.push(selectedCounties[i].data[j].altPercent_cases);
								dataDailyCases.push(selectedCounties[i].data[j].daily_cases);
								dataSevenDayAverage.push(selectedCounties[i].data[j].sevenDayAverage);
								dataTotalDeaths.push(selectedCounties[i].data[j].total_deaths);
								dataPercentDeaths.push(selectedCounties[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(selectedCounties[i].data[j].altPercent_deaths);
								dataDailyDeaths.push(selectedCounties[i].data[j].daily_deaths);
								dataSevenDayAverageDeaths.push(selectedCounties[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(selectedCounties[i].data[j].total_tests);
								dataPercentTests.push(selectedCounties[i].data[j].percent_tests);
								dataAltPercentTests.push(selectedCounties[i].data[j].altPercent_tests);
								dataDailyTests.push(selectedCounties[i].data[j].daily_tests);
								dataDailyPositivePercent.push(selectedCounties[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(selectedCounties[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(selectedCounties[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(selectedCounties[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(selectedCounties[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(selectedCounties[i].data[j].sevenDayAverageICUBeds);
							}	
						}
					}
						
					// Line
					datasetsTotalCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyPositivePercentLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAveragePositiveTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});

					// Bar
					datasetsTotalCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyPositivePercentBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAveragePositiveTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						backgroundColor: selectedCounties[i].color
					});

					dataTotalCases = [];
					dataPercentCases = [];
					dataAltPercentCases = [];
					dataDailyCases = [];
					dataSevenDayAverage = [];
					dataTotalDeaths = [];
					dataPercentDeaths = [];
					dataAltPercentDeaths = [];
					dataDailyDeaths = [];
					dataSevenDayAverageDeaths = [];
					dataTotalTests = [];
					dataPercentTests = [];
					dataAltPercentTests = [];
					dataDailyTests = [];
					dataDailyPositivePercent = [];
					dataSevenDayAveragePositiveTests = [];
					dataHospitalizations = [];
					dataSevenDayAverageHospitalizations = [];
					dataICUBeds = [];
					dataSevenDayAverageICUBeds = [];
				}
			}
		}

		for (let county of this.setSelectedCountiesNY) {
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == county && selectedCounties[i].state == "NY") {
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < selectedCounties[i].data.length; j++) {
							if (selectedCounties[i].data[j].date == chartDates[k]) {
								dataTotalCases.push(selectedCounties[i].data[j].total_cases);
								dataPercentCases.push(selectedCounties[i].data[j].percent_cases);
								dataAltPercentCases.push(selectedCounties[i].data[j].altPercent_cases);
								dataDailyCases.push(selectedCounties[i].data[j].daily_cases);
								dataSevenDayAverage.push(selectedCounties[i].data[j].sevenDayAverage);
								dataTotalDeaths.push(selectedCounties[i].data[j].total_deaths);
								dataPercentDeaths.push(selectedCounties[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(selectedCounties[i].data[j].altPercent_deaths);
								dataDailyDeaths.push(selectedCounties[i].data[j].daily_deaths);
								dataSevenDayAverageDeaths.push(selectedCounties[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(selectedCounties[i].data[j].total_tests);
								dataPercentTests.push(selectedCounties[i].data[j].percent_tests);
								dataAltPercentTests.push(selectedCounties[i].data[j].altPercent_tests);
								dataDailyTests.push(selectedCounties[i].data[j].daily_tests);
								dataDailyPositivePercent.push(selectedCounties[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(selectedCounties[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(selectedCounties[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(selectedCounties[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(selectedCounties[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(selectedCounties[i].data[j].sevenDayAverageICUBeds);
							}	
						}
					}
						
					// Line
					datasetsTotalCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyPositivePercentLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAveragePositiveTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});

					// Bar
					datasetsTotalCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyPositivePercentBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAveragePositiveTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						backgroundColor: selectedCounties[i].color
					});

					dataTotalCases = [];
					dataPercentCases = [];
					dataAltPercentCases = [];
					dataDailyCases = [];
					dataSevenDayAverage = [];
					dataTotalDeaths = [];
					dataPercentDeaths = [];
					dataAltPercentDeaths = [];
					dataDailyDeaths = [];
					dataSevenDayAverageDeaths = [];
					dataTotalTests = [];
					dataPercentTests = [];
					dataAltPercentTests = [];
					dataDailyTests = [];
					dataDailyPositivePercent = [];
					dataSevenDayAveragePositiveTests = [];
					dataHospitalizations = [];
					dataSevenDayAverageHospitalizations = [];
					dataICUBeds = [];
					dataSevenDayAverageICUBeds = [];
				}
			}
		}

		for (let county of this.setSelectedCountiesVT) {
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == county && selectedCounties[i].state == "VT") {
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < selectedCounties[i].data.length; j++) {
							if (selectedCounties[i].data[j].date == chartDates[k]) {
								dataTotalCases.push(selectedCounties[i].data[j].total_cases);
								dataPercentCases.push(selectedCounties[i].data[j].altPercent_cases);
								dataAltPercentCases.push(selectedCounties[i].data[j].percent_cases);
								dataDailyCases.push(selectedCounties[i].data[j].daily_cases);
								dataSevenDayAverage.push(selectedCounties[i].data[j].sevenDayAverage);
								dataTotalDeaths.push(selectedCounties[i].data[j].total_deaths);
								dataPercentDeaths.push(selectedCounties[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(selectedCounties[i].data[j].altPercent_deaths);
								dataDailyDeaths.push(selectedCounties[i].data[j].daily_deaths);
								dataSevenDayAverageDeaths.push(selectedCounties[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(selectedCounties[i].data[j].total_tests);
								dataPercentTests.push(selectedCounties[i].data[j].percent_tests);
								dataAltPercentTests.push(selectedCounties[i].data[j].altPercent_tests);
								dataDailyTests.push(selectedCounties[i].data[j].daily_tests);
								dataDailyPositivePercent.push(selectedCounties[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(selectedCounties[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(selectedCounties[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(selectedCounties[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(selectedCounties[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(selectedCounties[i].data[j].sevenDayAverageICUBeds);
							}	
						}
					}

					// Line
					datasetsTotalCasesLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataTotalCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentCasesLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentCasesLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyCasesLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataDailyCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalDeathsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentDeathsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentDeathsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyDeathsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageDeathsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalTestsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataTotalTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentTestsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentTestsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyTestsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataDailyTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyPositivePercentLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAveragePositiveTestsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsHospitalizationsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageHospitalizationsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsICUBedsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageICUBedsLine.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});

					// Bar
					datasetsTotalCasesBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataTotalCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentCasesBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentCasesBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyCasesBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataDailyCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalDeathsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentDeathsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentDeathsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyDeathsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageDeathsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalTestsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataTotalTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentTestsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentTestsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyTestsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataDailyTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyPositivePercentBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAveragePositiveTestsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsHospitalizationsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageHospitalizationsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsICUBedsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataICUBeds,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageICUBedsBar.push({
						label: selectedCounties[i].name + ', ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						backgroundColor: selectedCounties[i].color
					});

					dataTotalCases = [];
					dataPercentCases = [];
					dataAltPercentCases = [];
					dataDailyCases = [];
					dataSevenDayAverage = [];
					dataTotalDeaths = [];
					dataPercentDeaths = [];
					dataAltPercentDeaths = [];
					dataDailyDeaths = [];
					dataSevenDayAverageDeaths = [];
					dataTotalTests = [];
					dataPercentTests = [];
					dataAltPercentTests = [];
					dataDailyTests = [];
					dataDailyPositivePercent = [];
					dataSevenDayAveragePositiveTests = [];
					dataHospitalizations = [];
					dataSevenDayAverageHospitalizations = [];
					dataICUBeds = [];
					dataSevenDayAverageICUBeds = [];
				}
			}
		}

		for (let county of this.setSelectedCountiesWI) {
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == county && selectedCounties[i].state == "WI") {
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < selectedCounties[i].data.length; j++) {
							if (selectedCounties[i].data[j].date == chartDates[k]) {
								dataTotalCases.push(selectedCounties[i].data[j].total_cases);
								dataPercentCases.push(selectedCounties[i].data[j].percent_cases);
								dataAltPercentCases.push(selectedCounties[i].data[j].altPercent_cases);
								dataDailyCases.push(selectedCounties[i].data[j].daily_cases);
								dataSevenDayAverage.push(selectedCounties[i].data[j].sevenDayAverage);
								dataTotalDeaths.push(selectedCounties[i].data[j].total_deaths);
								dataPercentDeaths.push(selectedCounties[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(selectedCounties[i].data[j].altPercent_deaths);
								dataDailyDeaths.push(selectedCounties[i].data[j].daily_deaths);
								dataSevenDayAverageDeaths.push(selectedCounties[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(selectedCounties[i].data[j].total_tests);
								dataPercentTests.push(selectedCounties[i].data[j].percent_tests);
								dataAltPercentTests.push(selectedCounties[i].data[j].altPercent_tests);
								dataDailyTests.push(selectedCounties[i].data[j].daily_tests);
								dataDailyPositivePercent.push(selectedCounties[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(selectedCounties[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(selectedCounties[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(selectedCounties[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(selectedCounties[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(selectedCounties[i].data[j].sevenDayAverageICUBeds);
							}	
						}
					}
						
					// Line
					datasetsTotalCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyCasesLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageDeathsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsTotalTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsAltPercentTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsDailyPositivePercentLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAveragePositiveTestsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageHospitalizationsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});
					datasetsSevenDayAverageICUBedsLine.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						fill: false,
						borderColor: selectedCounties[i].color,
						pointRadius: 1,
						lineWidth: 3
					});

					// Bar
					datasetsTotalCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyCasesBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyCases,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverage,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageDeathsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageDeaths,
						backgroundColor: selectedCounties[i].color
					});
					datasetsTotalTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataTotalTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsAltPercentTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataAltPercentTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsDailyPositivePercentBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataDailyPositivePercent,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAveragePositiveTestsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAveragePositiveTests,
						backgroundColor: selectedCounties[i].color
					});
					datasetsHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageHospitalizationsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageHospitalizations,
						backgroundColor: selectedCounties[i].color
					});
					datasetsICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataICUBeds,
						backgroundColor: selectedCounties[i].color
					});
					datasetsSevenDayAverageICUBedsBar.push({
						label: selectedCounties[i].name + ' County, ' + selectedCounties[i].state,
						data: dataSevenDayAverageICUBeds,
						backgroundColor: selectedCounties[i].color
					});

					dataTotalCases = [];
					dataPercentCases = [];
					dataAltPercentCases = [];
					dataDailyCases = [];
					dataSevenDayAverage = [];
					dataTotalDeaths = [];
					dataPercentDeaths = [];
					dataAltPercentDeaths = [];
					dataDailyDeaths = [];
					dataSevenDayAverageDeaths = [];
					dataTotalTests = [];
					dataPercentTests = [];
					dataAltPercentTests = [];
					dataDailyTests = [];
					dataDailyPositivePercent = [];
					dataSevenDayAveragePositiveTests = [];
					dataHospitalizations = [];
					dataSevenDayAverageHospitalizations = [];
					dataICUBeds = [];
					dataSevenDayAverageICUBeds = [];
				}
			}
		}

		// STATES
		for (var i = 0; i < selectedStates.length; i++) {
			for (var k = 0; k < chartDates.length; k++) {
				for (var j = 0; j < selectedStates[i].data.length; j++) {
					if (chartDates[k] == selectedStates[i].data[j].date) {
						dataTotalCases.push(selectedStates[i].data[j].total_cases);
						dataPercentCases.push(selectedStates[i].data[j].percent_cases);
						dataAltPercentCases.push(selectedStates[i].data[j].altPercent_cases);
						dataDailyCases.push(selectedStates[i].data[j].daily_cases);
						dataSevenDayAverage.push(selectedStates[i].data[j].sevenDayAverage);
						dataTotalDeaths.push(selectedStates[i].data[j].total_deaths);
						dataPercentDeaths.push(selectedStates[i].data[j].percent_deaths);
						dataAltPercentDeaths.push(selectedStates[i].data[j].altPercent_deaths);
						dataDailyDeaths.push(selectedStates[i].data[j].daily_deaths);
						dataSevenDayAverageDeaths.push(selectedStates[i].data[j].sevenDayAverageDeaths);
						dataTotalTests.push(selectedStates[i].data[j].total_tests);
						dataPercentTests.push(selectedStates[i].data[j].percent_tests);
						dataAltPercentTests.push(selectedStates[i].data[j].altPercent_tests);
						dataDailyTests.push(selectedStates[i].data[j].daily_tests);
						dataDailyPositivePercent.push(selectedStates[i].data[j].daily_positive_percent);
						dataSevenDayAveragePositiveTests.push(selectedStates[i].data[j].sevenDayAveragePositiveTests);
						dataHospitalizations.push(selectedStates[i].data[j].hospitalizations);
						dataSevenDayAverageHospitalizations.push(selectedStates[i].data[j].sevenDayAverageHospitalizations);
						dataICUBeds.push(selectedStates[i].data[j].icuBeds);
						dataSevenDayAverageICUBeds.push(selectedStates[i].data[j].sevenDayAverageICUBeds);
					}
				}
			}
						
			// Line
			datasetsTotalCasesLine.push({
				label: selectedStates[i].name,
				data: dataTotalCases,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentCasesLine.push({
				label: selectedStates[i].name,
				data: dataPercentCases,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentCasesLine.push({
				label: selectedStates[i].name,
				data: dataAltPercentCases,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyCasesLine.push({
				label: selectedStates[i].name,
				data: dataDailyCases,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageLine.push({
				label: selectedStates[i].name,
				data: dataSevenDayAverage,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsTotalDeathsLine.push({
				label: selectedStates[i].name,
				data: dataTotalDeaths,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentDeathsLine.push({
				label: selectedStates[i].name,
				data: dataPercentDeaths,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentDeathsLine.push({
				label: selectedStates[i].name,
				data: dataAltPercentDeaths,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyDeathsLine.push({
				label: selectedStates[i].name,
				data: dataDailyDeaths,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageDeathsLine.push({
				label: selectedStates[i].name,
				data: dataSevenDayAverageDeaths,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsTotalTestsLine.push({
				label: selectedStates[i].name,
				data: dataTotalTests,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentTestsLine.push({
				label: selectedStates[i].name,
				data: dataPercentTests,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentTestsLine.push({
				label: selectedStates[i].name,
				data: dataAltPercentTests,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyTestsLine.push({
				label: selectedStates[i].name,
				data: dataDailyTests,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyPositivePercentLine.push({
				label: selectedStates[i].name,
				data: dataDailyPositivePercent,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAveragePositiveTestsLine.push({
				label: selectedStates[i].name,
				data: dataSevenDayAveragePositiveTests,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsHospitalizationsLine.push({
				label: selectedStates[i].name,
				data: dataHospitalizations,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageHospitalizationsLine.push({
				label: selectedStates[i].name,
				data: dataSevenDayAverageHospitalizations,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsICUBedsLine.push({
				label: selectedStates[i].name,
				data: dataICUBeds,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageICUBedsLine.push({
				label: selectedStates[i].name,
				data: dataSevenDayAverageICUBeds,
				fill: false,
				borderColor: selectedStates[i].color,
				pointRadius: 1,
				lineWidth: 3
			});

			// Bar
			datasetsTotalCasesBar.push({
				label: selectedStates[i].name,
				data: dataTotalCases,
				backgroundColor: selectedStates[i].color
			});
			datasetsPercentCasesBar.push({
				label: selectedStates[i].name,
				data: dataPercentCases,
				backgroundColor: selectedStates[i].color
			});
			datasetsAltPercentCasesBar.push({
				label: selectedStates[i].name,
				data: dataAltPercentCases,
				backgroundColor: selectedStates[i].color
			});
			datasetsDailyCasesBar.push({
				label: selectedStates[i].name,
				data: dataDailyCases,
				backgroundColor: selectedStates[i].color
			});
			datasetsSevenDayAverageBar.push({
				label: selectedStates[i].name,
				data: dataSevenDayAverage,
				backgroundColor: selectedStates[i].color
			});
			datasetsTotalDeathsBar.push({
				label: selectedStates[i].name,
				data: dataTotalDeaths,
				backgroundColor: selectedStates[i].color
			});
			datasetsPercentDeathsBar.push({
				label: selectedStates[i].name,
				data: dataPercentDeaths,
				backgroundColor: selectedStates[i].color
			});
			datasetsAltPercentDeathsBar.push({
				label: selectedStates[i].name,
				data: dataAltPercentDeaths,
				backgroundColor: selectedStates[i].color
			});
			datasetsDailyDeathsBar.push({
				label: selectedStates[i].name,
				data: dataDailyDeaths,
				backgroundColor: selectedStates[i].color
			});
			datasetsSevenDayAverageDeathsBar.push({
				label: selectedStates[i].name,
				data: dataSevenDayAverageDeaths,
				backgroundColor: selectedStates[i].color
			});
			datasetsTotalTestsBar.push({
				label: selectedStates[i].name,
				data: dataTotalTests,
				backgroundColor: selectedStates[i].color
			});
			datasetsPercentTestsBar.push({
				label: selectedStates[i].name,
				data: dataPercentTests,
				backgroundColor: selectedStates[i].color
			});
			datasetsAltPercentTestsBar.push({
				label: selectedStates[i].name,
				data: dataAltPercentTests,
				backgroundColor: selectedStates[i].color
			});
			datasetsDailyTestsBar.push({
				label: selectedStates[i].name,
				data: dataDailyTests,
				backgroundColor: selectedStates[i].color
			});
			datasetsDailyPositivePercentBar.push({
				label: selectedStates[i].name,
				data: dataDailyPositivePercent,
				backgroundColor: selectedStates[i].color
			});
			datasetsSevenDayAveragePositiveTestsBar.push({
				label: selectedStates[i].name,
				data: dataSevenDayAveragePositiveTests,
				backgroundColor: selectedStates[i].color
			});
			datasetsHospitalizationsBar.push({
				label: selectedStates[i].name,
				data: dataHospitalizations,
				backgroundColor: selectedStates[i].color
			});
			datasetsSevenDayAverageHospitalizationsBar.push({
				label: selectedStates[i].name,
				data: dataSevenDayAverageHospitalizations,
				backgroundColor: selectedStates[i].color
			});
			datasetsICUBedsBar.push({
				label: selectedStates[i].name,
				data: dataICUBeds,
				backgroundColor: selectedStates[i].color
			});
			datasetsSevenDayAverageICUBedsBar.push({
				label: selectedStates[i].name,
				data: dataSevenDayAverageICUBeds,
				backgroundColor: selectedStates[i].color
			});

			dataTotalCases = [];
			dataDailyCases = [];
			dataPercentCases = [];
			dataAltPercentCases = [];
			dataSevenDayAverage = [];
			dataTotalDeaths = [];
			dataPercentDeaths = [];
			dataAltPercentDeaths = [];
			dataDailyDeaths = [];
			dataSevenDayAverageDeaths = [];
			dataTotalTests = [];
			dataPercentTests = [];
			dataAltPercentTests = [];
			dataDailyTests = [];
			dataDailyPositivePercent = [];
			dataSevenDayAveragePositiveTests = [];
			dataHospitalizations = [];
			dataSevenDayAverageHospitalizations = [];
			dataICUBeds = [];
			dataSevenDayAverageICUBeds = [];
		}
		
		// COUNTRIES
		var countryName = '';
		var countryColor = '';

		for (let countryID of this.setSelectedCountries) {
			for (var i = 0; i < countriesList.length; i++) {
				if (countryID === countriesList[i].id) {
					countryName = countriesList[i].name;
					countryColor = countriesList[i].color;
					for (var k = 0; k < chartDates.length; k++) {
						for (var j = 0; j < countriesList[i].data.length; j++) {
							if (chartDates[k] == countriesList[i].data[j].date) {
								dataTotalCases.push(countriesList[i].data[j].total_cases);
								dataTotalDeaths.push(countriesList[i].data[j].total_deaths);
								dataPercentCases.push(countriesList[i].data[j].percent_cases);
								dataAltPercentCases.push(countriesList[i].data[j].altPercent_cases);
								dataPercentDeaths.push(countriesList[i].data[j].percent_deaths);
								dataAltPercentDeaths.push(countriesList[i].data[j].altPercent_deaths);
								dataDailyCases.push(countriesList[i].data[j].daily_cases);
								dataDailyDeaths.push(countriesList[i].data[j].daily_deaths);
								dataSevenDayAverage.push(countriesList[i].data[j].sevenDayAverage);
								dataSevenDayAverageDeaths.push(countriesList[i].data[j].sevenDayAverageDeaths);
								dataTotalTests.push(countriesList[i].data[j].total_tests);
								dataPercentTests.push(countriesList[i].data[j].percent_tests);
								dataAltPercentTests.push(countriesList[i].data[j].altPercent_tests);
								dataDailyTests.push(countriesList[i].data[j].daily_tests);
								dataDailyPositivePercent.push(countriesList[i].data[j].daily_positive_percent);
								dataSevenDayAveragePositiveTests.push(countriesList[i].data[j].sevenDayAveragePositiveTests);
								dataHospitalizations.push(countriesList[i].data[j].hospitalizations);
								dataSevenDayAverageHospitalizations.push(countriesList[i].data[j].sevenDayAverageHospitalizations);
								dataICUBeds.push(countriesList[i].data[j].icuBeds);
								dataSevenDayAverageICUBeds.push(countriesList[i].data[j].sevenDayAverageICUBeds);
							}
						}
					}
				}
			}

			// Line
			datasetsTotalCasesLine.push({
				label: countryName,
				data: dataTotalCases,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentCasesLine.push({
				label: countryName,
				data: dataPercentCases,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentCasesLine.push({
				label: countryName,
				data: dataAltPercentCases,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyCasesLine.push({
				label: countryName,
				data: dataDailyCases,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageLine.push({
				label: countryName,
				data: dataSevenDayAverage,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsTotalDeathsLine.push({
				label: countryName,
				data: dataTotalDeaths,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentDeathsLine.push({
				label: countryName,
				data: dataPercentDeaths,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentDeathsLine.push({
				label: countryName,
				data: dataAltPercentDeaths,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyDeathsLine.push({
				label: countryName,
				data: dataDailyDeaths,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageDeathsLine.push({
				label: countryName,
				data: dataSevenDayAverageDeaths,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsTotalTestsLine.push({
				label: countryName,
				data: dataTotalTests,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentTestsLine.push({
				label: countryName,
				data: dataPercentTests,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentTestsLine.push({
				label: countryName,
				data: dataAltPercentTests,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyTestsLine.push({
				label: countryName,
				data: dataDailyTests,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyPositivePercentLine.push({
				label: countryName,
				data: dataDailyPositivePercent,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAveragePositiveTestsLine.push({
				label: countryName,
				data: dataSevenDayAveragePositiveTests,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsHospitalizationsLine.push({
				label: countryName,
				data: dataHospitalizations,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageHospitalizationsLine.push({
				label: countryName,
				data: dataSevenDayAverageHospitalizations,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsICUBedsLine.push({
				label: countryName,
				data: dataICUBeds,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageICUBedsLine.push({
				label: countryName,
				data: dataSevenDayAverageICUBeds,
				fill: false,
				borderColor: countryColor,
				pointRadius: 1,
				lineWidth: 3
			});

			// Bar
			datasetsTotalCasesBar.push({
				label: countryName,
				data: dataTotalCases,
				backgroundColor: countryColor
			});
			datasetsPercentCasesBar.push({
				label: countryName,
				data: dataPercentCases,
				backgroundColor: countryColor
			});
			datasetsAltPercentCasesBar.push({
				label: countryName,
				data: dataAltPercentCases,
				backgroundColor: countryColor
			});
			datasetsDailyCasesBar.push({
				label: countryName,
				data: dataDailyCases,
				backgroundColor: countryColor
			});
			datasetsSevenDayAverageBar.push({
				label: countryName,
				data: dataSevenDayAverage,
				backgroundColor: countryColor
			});
			datasetsTotalDeathsBar.push({
				label: countryName,
				data: dataTotalDeaths,
				backgroundColor: countryColor
			});
			datasetsPercentDeathsBar.push({
				label: countryName,
				data: dataPercentDeaths,
				backgroundColor: countryColor
			});
			datasetsAltPercentDeathsBar.push({
				label: countryName,
				data: dataAltPercentDeaths,
				backgroundColor: countryColor
			});
			datasetsDailyDeathsBar.push({
				label: countryName,
				data: dataDailyDeaths,
				backgroundColor: countryColor
			});
			datasetsSevenDayAverageDeathsBar.push({
				label: countryName,
				data: dataSevenDayAverageDeaths,
				backgroundColor: countryColor
			});
			datasetsTotalTestsBar.push({
				label: countryName,
				data: dataTotalTests,
				backgroundColor: countryColor
			});
			datasetsPercentTestsBar.push({
				label: countryName,
				data: dataPercentTests,
				backgroundColor: countryColor
			});
			datasetsAltPercentTestsBar.push({
				label: countryName,
				data: dataAltPercentTests,
				backgroundColor: countryColor
			});
			datasetsDailyTestsBar.push({
				label: countryName,
				data: dataDailyTests,
				backgroundColor: countryColor
			});
			datasetsDailyPositivePercentBar.push({
				label: countryName,
				data: dataDailyPositivePercent,
				backgroundColor: countryColor
			});
			datasetsSevenDayAveragePositiveTestsBar.push({
				label: countryName,
				data: dataSevenDayAveragePositiveTests,
				backgroundColor: countryColor
			});
			datasetsHospitalizationsBar.push({
				label: countryName,
				data: dataHospitalizations,
				backgroundColor: countryColor
			});
			datasetsSevenDayAverageHospitalizationsBar.push({
				label: countryName,
				data: dataSevenDayAverageHospitalizations,
				backgroundColor: countryColor
			});
			datasetsICUBedsBar.push({
				label: countryName,
				data: dataICUBeds,
				backgroundColor: countryColor
			});
			datasetsSevenDayAverageICUBedsBar.push({
				label: countryName,
				data: dataSevenDayAverageICUBeds,
				backgroundColor: countryColor
			});

			dataTotalCases = [];
			dataPercentCases = [];
			dataAltPercentCases = [];
			dataDailyCases = [];
			dataSevenDayAverage = [];
			dataTotalDeaths = [];
			dataPercentDeaths = [];
			dataAltPercentDeaths = [];
			dataDailyDeaths = [];
			dataSevenDayAverageDeaths = [];
			dataTotalTests = [];
			dataPercentTests = [];
			dataAltPercentTests = [];
			dataDailyTests = [];
			dataDailyPositivePercent = [];
			dataSevenDayAveragePositiveTests = [];
			dataHospitalizations = [];
			dataSevenDayAverageHospitalizations = [];
			dataICUBeds = [];
			dataSevenDayAverageICUBeds = [];
		}

		if (this.state.displayEU) {
			const { countriesEuropeanUnion, populationEU } = this.state;
			var color = colorsList[(Math.random() * 56).toFixed()]
			const data = [];

			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0})
			}

			for (var i = 0; i < countriesEuropeanUnion.length; i++) {
				for (var j = 0; j < countriesList.length; j++) {
					if (countriesEuropeanUnion[i] == countriesList[j].name) {
						for (var k = 0; k < data.length; k++) {
							for (var l = 0; l < countriesList[j].data.length; l++) {
								if (data[k].date == countriesList[j].data[l].date) {
									data[k].total_cases += countriesList[j].data[l].total_cases;
									data[k].daily_cases += countriesList[j].data[l].daily_cases;
									data[k].total_deaths += countriesList[j].data[l].total_deaths;
									data[k].daily_deaths += countriesList[j].data[l].daily_deaths;
								}
							}
						}
					}
				}
			}

			var seven_day_count = 0;
			var seven_day_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += data[i].daily_cases;
				seven_day_deaths += data[i].daily_deaths;
				data[i].percent_cases = (data[i].total_cases / populationEU * 100).toFixed(3);
				data[i].altPercent_cases = (data[i].total_cases / populationEU * 1000000).toFixed(3);
				data[i].percent_deaths = (data[i].total_deaths / populationEU * 100).toFixed(3);
				data[i].altPercent_deaths = (data[i].total_deaths / populationEU * 100000).toFixed(3);
				if (i < 7) {
					data[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / (i + 1)).toFixed(3);
				} else {
					seven_day_count -= data[i - 7].daily_cases;
					seven_day_deaths -= data[i - 7].daily_deaths;
					data[i].sevenDayAverage = (seven_day_count / 7).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / 7).toFixed(3);
				}
			}

			for (var i = 0; i < data.length; i++) {
				for (var j = 0; j < chartDates.length; j++) {
					if (data[i].date == chartDates[j]) {
						dataTotalCases.push(data[i].total_cases);
						dataTotalDeaths.push(data[i].total_deaths);
						dataPercentCases.push(data[i].percent_cases);
						dataAltPercentCases.push(data[i].altPercent_cases);
						dataPercentDeaths.push(data[i].percent_deaths);
						dataAltPercentDeaths.push(data[i].altPercent_deaths);
						dataDailyCases.push(data[i].daily_cases);
						dataDailyDeaths.push(data[i].daily_deaths);
						dataSevenDayAverage.push(data[i].sevenDayAverage);
						dataSevenDayAverageDeaths.push(data[i].sevenDayAverageDeaths);
					}
				}		
			}

			// Line
			datasetsTotalCasesLine.push({
				label: 'European Union',
				data: dataTotalCases,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentCasesLine.push({
				label: 'European Union',
				data: dataPercentCases,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentCasesLine.push({
				label: 'European Union',
				data: dataAltPercentCases,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyCasesLine.push({
				label: 'European Union',
				data: dataDailyCases,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageLine.push({
				label: 'European Union',
				data: dataSevenDayAverage,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsTotalDeathsLine.push({
				label: 'European Union',
				data: dataTotalDeaths,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsPercentDeathsLine.push({
				label: 'European Union',
				data: dataPercentDeaths,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsAltPercentDeathsLine.push({
				label: 'European Union',
				data: dataAltPercentDeaths,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsDailyDeathsLine.push({
				label: 'European Union',
				data: dataDailyDeaths,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			datasetsSevenDayAverageDeathsLine.push({
				label: 'European Union',
				data: dataSevenDayAverageDeaths,
				fill: false,
				borderColor: color,
				pointRadius: 1,
				lineWidth: 3
			});
			// datasetsTotalTestsLine.push({
			// 	label: countryName,
			// 	data: dataTotalTests,
			// 	fill: false,
			// 	borderColor: countryColor,
			// 	pointRadius: 1,
			// 	lineWidth: 3
			// });
			// datasetsPercentTestsLine.push({
			// 	label: countryName,
			// 	data: dataPercentTests,
			// 	fill: false,
			// 	borderColor: countryColor,
			// 	pointRadius: 1,
			// 	lineWidth: 3
			// });
			// datasetsDailyTestsLine.push({
			// 	label: countryName,
			// 	data: dataDailyTests,
			// 	fill: false,
			// 	borderColor: countryColor,
			// 	pointRadius: 1,
			// 	lineWidth: 3
			// });
			// datasetsDailyPositivePercentLine.push({
			// 	label: countryName,
			// 	data: dataDailyPositivePercent,
			// 	fill: false,
			// 	borderColor: countryColor,
			// 	pointRadius: 1,
			// 	lineWidth: 3
			// });
			// datasetsSevenDayAveragePositiveTestsLine.push({
			// 	label: countryName,
			// 	data: dataSevenDayAveragePositiveTests,
			// 	fill: false,
			// 	borderColor: countryColor,
			// 	pointRadius: 1,
			// 	lineWidth: 3
			// });
			// datasetsHospitalizationsLine.push({
			// 	label: countryName,
			// 	data: dataHospitalizations,
			// 	fill: false,
			// 	borderColor: countryColor,
			// 	pointRadius: 1,
			// 	lineWidth: 3
			// });
			// datasetsSevenDayAverageHospitalizationsLine.push({
			// 	label: countryName,
			// 	data: dataSevenDayAverageHospitalizations,
			// 	fill: false,
			// 	borderColor: countryColor,
			// 	pointRadius: 1,
			// 	lineWidth: 3
			// });
			// datasetsICUBedsLine.push({
			// 	label: countryName,
			// 	data: dataICUBeds,
			// 	fill: false,
			// 	borderColor: countryColor,
			// 	pointRadius: 1,
			// 	lineWidth: 3
			// });
			// datasetsSevenDayAverageICUBedsLine.push({
			// 	label: countryName,
			// 	data: dataSevenDayAverageICUBeds,
			// 	fill: false,
			// 	borderColor: countryColor,
			// 	pointRadius: 1,
			// 	lineWidth: 3
			// });

			// Bar
			datasetsTotalCasesBar.push({
				label: 'European Union',
				data: dataTotalCases,
				backgroundColor: color
			});
			datasetsPercentCasesBar.push({
				label: 'European Union',
				data: dataPercentCases,
				backgroundColor: color
			});
			datasetsAltPercentCasesBar.push({
				label: 'European Union',
				data: dataAltPercentCases,
				backgroundColor: color
			});
			datasetsDailyCasesBar.push({
				label: 'European Union',
				data: dataDailyCases,
				backgroundColor: color
			});
			datasetsSevenDayAverageBar.push({
				label: 'European Union',
				data: dataSevenDayAverage,
				backgroundColor: color
			});
			datasetsTotalDeathsBar.push({
				label: 'European Union',
				data: dataTotalDeaths,
				backgroundColor: color
			});
			datasetsPercentDeathsBar.push({
				label: 'European Union',
				data: dataPercentDeaths,
				backgroundColor: color
			});
			datasetsAltPercentDeathsBar.push({
				label: 'European Union',
				data: dataAltPercentDeaths,
				backgroundColor: color
			});
			datasetsDailyDeathsBar.push({
				label: 'European Union',
				data: dataDailyDeaths,
				backgroundColor: color
			});
			datasetsSevenDayAverageDeathsBar.push({
				label: 'European Union',
				data: dataSevenDayAverageDeaths,
				backgroundColor: color
			});
			// datasetsTotalTestsBar.push({
			// 	label: countryName,
			// 	data: dataTotalTests,
			// 	backgroundColor: countryColor
			// });
			// datasetsPercentTestsBar.push({
			// 	label: countryName,
			// 	data: dataPercentTests,
			// 	backgroundColor: countryColor
			// });
			// datasetsDailyTestsBar.push({
			// 	label: countryName,
			// 	data: dataDailyTests,
			// 	backgroundColor: countryColor
			// });
			// datasetsDailyPositivePercentBar.push({
			// 	label: countryName,
			// 	data: dataDailyPositivePercent,
			// 	backgroundColor: countryColor
			// });
			// datasetsSevenDayAveragePositiveTestsBar.push({
			// 	label: countryName,
			// 	data: dataSevenDayAveragePositiveTests,
			// 	backgroundColor: countryColor
			// });
			// datasetsHospitalizationsBar.push({
			// 	label: countryName,
			// 	data: dataHospitalizations,
			// 	backgroundColor: countryColor
			// });
			// datasetsSevenDayAverageHospitalizationsBar.push({
			// 	label: countryName,
			// 	data: dataSevenDayAverageHospitalizations,
			// 	backgroundColor: countryColor
			// });
			// datasetsICUBedsBar.push({
			// 	label: countryName,
			// 	data: dataICUBeds,
			// 	backgroundColor: countryColor
			// });
			// datasetsSevenDayAverageICUBedsBar.push({
			// 	label: countryName,
			// 	data: dataSevenDayAverageICUBeds,
			// 	backgroundColor: countryColor
			// });
		}

			

		this.setState({
			// Line
			chartTotalCasesLine: {
				labels: labelDates,
				datasets: datasetsTotalCasesLine
			},
			chartPercentCasesLine: {
				labels: labelDates,
				datasets: datasetsPercentCasesLine
			},
			chartAltPercentCasesLine: {
				labels: labelDates,
				datasets: datasetsAltPercentCasesLine
			},
			chartDailyCasesLine: {
				labels: labelDates,
				datasets: datasetsDailyCasesLine
			},
			chartSevenDayAverageCasesLine: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageLine
			},
			chartTotalDeathsLine: {
				labels: labelDates,
				datasets: datasetsTotalDeathsLine
			},
			chartPercentDeathsLine: {
				labels: labelDates,
				datasets: datasetsPercentDeathsLine
			},
			chartAltPercentDeathsLine: {
				labels: labelDates,
				datasets: datasetsAltPercentDeathsLine
			},
			chartDailyDeathsLine: {
				labels: labelDates,
				datasets: datasetsDailyDeathsLine
			},
			chartSevenDayAverageDeathsLine: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageDeathsLine
			},
			chartTotalTestsLine: {
				labels: labelDates,
				datasets: datasetsTotalTestsLine
			},
			chartPercentTestsLine: {
				labels: labelDates,
				datasets: datasetsPercentTestsLine
			},
			chartAltPercentTestsLine: {
				labels: labelDates,
				datasets: datasetsAltPercentTestsLine
			},
			chartDailyPositiveTestsLine: {
				labels: labelDates,
				datasets: datasetsDailyPositivePercentLine
			},
			chartSevenDayAveragePositiveTestsLine: {
				labels: labelDates,
				datasets: datasetsSevenDayAveragePositiveTestsLine
			},
			chartHospitalizationsLine: {
				labels: labelDates,
				datasets: datasetsHospitalizationsLine
			},
			chartSevenDayAverageHospitalizationsLine: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageHospitalizationsLine
			},
			chartICULine:  {
				labels: labelDates,
				datasets: datasetsICUBedsLine
			},
			chartSevenDayAverageICULine: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageICUBedsLine
			},

			// Bar
			chartTotalCasesBar: {
				labels: labelDates,
				datasets: datasetsTotalCasesBar
			},
			chartPercentCasesBar: {
				labels: labelDates,
				datasets: datasetsPercentCasesBar
			},
			chartAltPercentCasesBar: {
				labels: labelDates,
				datasets: datasetsAltPercentCasesBar
			},
			chartDailyCasesBar: {
				labels: labelDates,
				datasets: datasetsDailyCasesBar
			},
			chartSevenDayAverageCasesBar: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageBar
			},
			chartTotalDeathsBar: {
				labels: labelDates,
				datasets: datasetsTotalDeathsBar
			},
			chartPercentDeathsBar: {
				labels: labelDates,
				datasets: datasetsPercentDeathsBar
			},
			chartAltPercentDeathsBar: {
				labels: labelDates,
				datasets: datasetsAltPercentDeathsBar
			},
			chartDailyDeathsBar: {
				labels: labelDates,
				datasets: datasetsDailyDeathsBar
			},
			chartSevenDayAverageDeathsBar: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageDeathsBar
			},
			chartTotalTestsBar: {
				labels: labelDates,
				datasets: datasetsTotalTestsBar
			},
			chartPercentTestsBar: {
				labels: labelDates,
				datasets: datasetsPercentTestsBar
			},
			chartAltPercentTestsBar: {
				labels: labelDates,
				datasets: datasetsAltPercentTestsBar
			},
			chartDailyPositiveTestsBar: {
				labels: labelDates,
				datasets: datasetsDailyPositivePercentBar
			},
			chartSevenDayAveragePositiveTestsBar: {
				labels: labelDates,
				datasets: datasetsSevenDayAveragePositiveTestsBar
			},
			chartHospitalizationsBar: {
				labels: labelDates,
				datasets: datasetsHospitalizationsBar
			},
			chartSevenDayAverageHospitalizationsBar: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageHospitalizationsBar
			},
			chartICUBar:  {
				labels: labelDates,
				datasets: datasetsICUBedsBar
			},
			chartSevenDayAverageICUBar: {
				labels: labelDates,
				datasets: datasetsSevenDayAverageICUBedsBar
			},

			showChart: true
		})
	}

	handleDisplayCityControls = () => {
		this.setState({
			displayCityControls: true,
			displayCountyControls: false,
			displayStateControls: false,
			displayCountryControls: false
		});
	}

	handleDisplayCountyControls = () => {
		this.setState({
			displayCityControls: false,
			displayCountyControls: true,
			displayStateControls: false,
			displayCountryControls: false
		});
	}

	handleDisplayStateControls = () => {
		this.setState({
			displayCityControls: false,
			displayCountyControls: false,
			displayStateControls: true,
			displayCountryControls: false
		});
	}

	handleDisplayCountryControls = () => {
		if (this.props.dataWorldWideEuropaCompare == null) {
			this.props.getWorldWideEuropaDataCompare();
		}
		this.setState({
			displayCityControls: false,
			displayCountyControls: false,
			displayStateControls: false,
			displayCountryControls: true
		});
	}

	handleSelectSanFrancisco = () => {
		const { displaySanFrancisco, citiesList } = this.state;
		if (displaySanFrancisco) {
			let newCitiesList = citiesList.filter(e => (e.name !== "San Francisco"));
			this.setState({
				citiesList: newCitiesList,
				displaySanFrancisco: false,
				triggerSetChartData: true
			});
		} else {
			this.setState({
				displaySanFrancisco: true
			});
			this.props.getSFDataCompare();
		}
	}

	handleSelectNewYorkCity = () => {
		const { displayNewYorkCity, citiesList } = this.state;
		if (displayNewYorkCity) {
			let newCitiesList = citiesList.filter(e => (e.name !== "New York City"));
			this.setState({
				citiesList: newCitiesList,
				displayNewYorkCity: false,
				triggerSetChartData: true
			});
		} else {
			this.setState({
				displayNewYorkCity: true
			});
			this.props.getNYCCaseDataCompare();
		}
	}

	handleSelectHongKong = () => {
		const { displayHongKong, citiesList } = this.state;
		if (displayHongKong) {
			let newCitiesList = citiesList.filter(e => (e.name !== "Hong Kong"));
			this.setState({
				citiesList: newCitiesList,
				displayHongKong: false,
				triggerSetChartData: true
			});
		} else {
			this.setState({
				displayHongKong: true
			});
			this.props.getHongKongDataCompare();
		}
	}

	handleSelectChicago = () => {
		const { displayChicago, citiesList } = this.state;
		if (displayChicago) {
			let newCitiesList = citiesList.filter(e => (e.name !== "Chicago"));
			this.setState({
				citiesList: newCitiesList,
				displayChicago: false,
				triggerSetChartData: true
			});
		} else {
			this.setState({
				displayChicago: true
			});
			this.props.getCHICaseDataCompare();
		}
	}

	handleDisplayCalifornia = () => {
		const { displayCalifornia } = this.state;
		if (displayCalifornia) {
			this.setState({
				displayCalifornia: false
			});
		} else {
			this.setState({
				displayCalifornia: true,
				displayColorado: false,
				displayConnecticut: false,
				displayGeorgia: false, 
				displayIndiana: false, 
				displayMaryland: false, 
				displayNewYork: false,
				displayVermont: false,
				displayWisconsin: false,
			});
		}
	}

	handleDisplayColorado = () => {
		const { displayColorado } = this.state;
		if (displayColorado) {
			this.setState({
				displayColorado: false
			});
		} else {
			this.setState({
				displayCalifornia: false,
				displayColorado: true,
				displayConnecticut: false,
				displayGeorgia: false, 
				displayIndiana: false, 
				displayMaryland: false, 
				displayNewYork: false,
				displayVermont: false,
				displayWisconsin: false,
			});
		}
	}

	handleDisplayConnecticut = () => {
		const { displayConnecticut } = this.state;
		if (displayConnecticut) {
			this.setState({
				displayConnecticut: false
			});
		} else {
			this.setState({
				displayCalifornia: false,
				displayColorado: false,
				displayConnecticut: true,
				displayGeorgia: false, 
				displayIndiana: false, 
				displayMaryland: false, 
				displayNewYork: false,
				displayVermont: false,
				displayWisconsin: false,
			});
		}
	}

	handleDisplayGeorgia = () => {
		const { displayGeorgia } = this.state;
		if (displayGeorgia) {
			this.setState({
				displayGeorgia: false
			});
		} else {
			this.setState({
				displayCalifornia: false,
				displayColorado: false,
				displayConnecticut: false,
				displayGeorgia: true, 
				displayIndiana: false, 
				displayMaryland: false, 
				displayNewYork: false,
				displayVermont: false,
				displayWisconsin: false,
			});
		}
	}

	handleDisplayIndiana = () => {
		const { displayIndiana } = this.state;
		if (displayIndiana) {
			this.setState({
				displayNewYork: false
			});
		} else {
			this.setState({
				displayCalifornia: false,
				displayColorado: false,
				displayConnecticut: false,
				displayGeorgia: false, 
				displayIndiana: true, 
				displayMaryland: false, 
				displayNewYork: false,
				displayVermont: false,
				displayWisconsin: false,
			});
		}
	}

	handleDisplayMaryland = () => {
		const { displayMaryland } = this.state;
		if (displayMaryland) {
			this.setState({
				displayMaryland: false
			});
		} else {
			this.setState({
				displayCalifornia: false,
				displayColorado: false,
				displayConnecticut: false,
				displayGeorgia: false, 
				displayIndiana: false, 
				displayMaryland: true, 
				displayNewYork: false,
				displayVermont: false,
				displayWisconsin: false,
			});
		}
	}

	handleDisplayNewYork = () => {
		const { displayNewYork } = this.state;
		if (displayNewYork) {
			this.setState({
				displayNewYork: false
			});
		} else {
			this.setState({
				displayCalifornia: false,
				displayColorado: false,
				displayConnecticut: false,
				displayGeorgia: false, 
				displayIndiana: false, 
				displayMaryland: false, 
				displayNewYork: true,
				displayVermont: false,
				displayWisconsin: false,
			});
		}
	}

	handleDisplayVermont = () => {
		const { displayVermont } = this.state;
		if (displayVermont) {
			this.setState({
				displayVermont: false
			});
		} else {
			this.setState({
				displayCalifornia: false,
				displayColorado: false,
				displayConnecticut: false,
				displayGeorgia: false, 
				displayIndiana: false, 
				displayMaryland: false, 
				displayNewYork: false,
				displayVermont: true,
				displayWisconsin: false,
			});
		}
	}

	handleDisplayWisconsin = () => {
		const { displayWisconsin } = this.state;
		if (displayWisconsin) {
			this.setState({
				displayWisconsin: false
			});
		} else {
			this.setState({
				displayCalifornia: false,
				displayColorado: false,
				displayConnecticut: false,
				displayGeorgia: false, 
				displayIndiana: false, 
				displayMaryland: false, 
				displayNewYork: false,
				displayVermont: false,
				displayWisconsin: true,
			});
		}
	}

	handleSelectCountyCA =  (county, population) => {
		const { countiesCaliforniaList } = this.state;
		const { selectedCounties } = this.state;
		let { showChart } = this.state;
		if (this.setSelectedCountiesCA.has(county)) {
			this.setSelectedCountiesCA.delete(county);
			let newSelectedCountiesList = selectedCounties.filter(e => (e.name !== county && e.state == e.state));
			for (var i = 0; i < countiesCaliforniaList.length; i++) {
				if (countiesCaliforniaList[i].name == county) {
					countiesCaliforniaList[i].active = false;
				}
			}
			this.setState({
				countiesCaliforniaList: countiesCaliforniaList,
				selectedCounties: newSelectedCountiesList,
				triggerSetChartData: true
			});
		} else {
			this.setSelectedCountiesCA.add(county);
			for (var i = 0; i < countiesCaliforniaList.length; i++) {
				if (countiesCaliforniaList[i].name == county) {
					countiesCaliforniaList[i].active = true;
				}
			}
			this.props.getCaseDataCaCountyCompare(county);
			this.setState({countiesCaliforniaList: countiesCaliforniaList, population: population});
		}
	}

	handleSelectCountyCO = (county, population) => {
		const { countiesColoradoList, selectedCounties } = this.state;
		if (this.setSelectedCountiesCO.has(county)) {
			this.setSelectedCountiesCO.delete(county);
			let newSelectedCountiesList = selectedCounties.filter(e => (e.name !== county && e.state == e.state));
			for (var i = 0; i < countiesColoradoList.length; i++) {
				if (countiesColoradoList[i].name == county) {
					countiesColoradoList[i].active = false;
				}
			}
			this.setState({
				selectedCounties: newSelectedCountiesList,
				countiesColoradoList: countiesColoradoList
			});
		} else {
			this.setSelectedCountiesCO.add(county);
			for (var i = 0; i < countiesColoradoList.length; i++) {
				if (countiesColoradoList[i].name == county) {
					countiesColoradoList[i].active = true;
				}
			}
			this.props.getCaseDataCOCountyCompare(county);
			this.setState({
				countiesColoradoList: countiesColoradoList,
				population: population
			});
		}
	}

	handleSelectCountyCT =  (county, population) => {
		const { countiesConnecticutList } = this.state;
		const { selectedCounties } = this.state;
		let { showChart } = this.state;
		if (this.setSelectedCountiesCT.has(county)) {
			this.setSelectedCountiesCT.delete(county);
			let newSelectedCountiesList = selectedCounties.filter(e => (e.name !== county && e.state == e.state));
			for (var i = 0; i < countiesConnecticutList.length; i++) {
				if (countiesConnecticutList[i].name == county) {
					countiesConnecticutList[i].active = false;
				}
			}
			this.setState({
				countiesConnecticutList: countiesConnecticutList,
				selectedCounties: newSelectedCountiesList,
				triggerSetChartData: true
			});
		} else {
			this.setSelectedCountiesCT.add(county);
			for (var i = 0; i < countiesConnecticutList.length; i++) {
				if (countiesConnecticutList[i].name == county) {
					countiesConnecticutList[i].active = true;
				}
			}
			this.props.getCaseDataCTCountyCompare(county);
			this.setState({countiesConnecticutList: countiesConnecticutList, population: population});
		}
	}

	handleSelectCountyGA = (county, population) => {
		// console.log('Georgia County: ', county);
		const { colorsList, countiesGeorgiaList, datesArray, selectedCounties } = this.state;
		if (this.setSelectedCountiesGA.has(county)) {
			this.setSelectedCountiesGA.delete(county);
			let newSelectedCountiesList = selectedCounties.filter(e => (e.name !== county && e.state == e.state));
			for (var i = 0; i < countiesGeorgiaList.length; i++) {
				if (countiesGeorgiaList[i].name == county) {
					countiesGeorgiaList[i].active = false;
				}
			}
			this.setState({
				selectedCounties: newSelectedCountiesList, 
				countiesGeorgiaList: countiesGeorgiaList,
				triggerSetChartData: true
			});
		} else {
			this.setSelectedCountiesGA.add(county);
			for (var i = 0; i < countiesGeorgiaList.length; i++) {
				if (countiesGeorgiaList[i].name == county) {
					countiesGeorgiaList[i].active = true;
				}
			}
			this.setState({
				population: population,
				countiesGeorgiaList: countiesGeorgiaList
			});
			this.props.getCaseDataGACountyCompare(county);
		}
	}

	handleSelectCountyIN = (county, population) => {
		// console.log('County: ', county);
		// console.log('Population: ', population);
		const { countiesIndianaList, selectedCounties } = this.state;
		if (this.setSelectedCountiesIN.has(county)) {
			this.setSelectedCountiesIN.delete(county);
			for (var i = 0; i < countiesIndianaList.length; i++) {
				if (countiesIndianaList[i].name == county) {
					countiesIndianaList[i].active = false;
				}
			}
			this.setState({
				countiesIndianaList: countiesIndianaList
			});
		} else {
			this.setSelectedCountiesIN.add(county);
			for (var i = 0; i < countiesIndianaList.length; i++) {
				if (countiesIndianaList[i].name == county) {
					countiesIndianaList[i].active = true;
				}
			}
			this.setState({
				countiesIndianaList: countiesIndianaList,
				population: population
			});
			this.props.getCaseDataINCountyCompare(county);
		}
	}

	handleSelectCountyMD = (county, population) => {
		const { datesArray, marylandData, colorsList, selectedCounties, countiesMarylandList } = this.state;
		if (this.setSelectedCountiesMD.has(county)) {
			this.setSelectedCountiesMD.delete(county);
			let newSelectedCounties = selectedCounties.filter(e => (e.name !== county && e.state == "MD"));
			for (var i = 0; i < countiesMarylandList.length; i++) {
				if (countiesMarylandList[i].name == county) {
					countiesMarylandList[i].active = false;
				}
			}
			this.setState({
				countiesMarylandList: countiesMarylandList,
				selectedCounties: newSelectedCounties,
				triggerSetChartData: true
			})
		} else {
			this.setSelectedCountiesMD.add(county);
			for (var i = 0; i < countiesMarylandList.length; i++) {
				if (countiesMarylandList[i].name == county) {
					countiesMarylandList[i].active = true;
				}
			}
			const data = [];
			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0})
			}
			for (var i = 0; i < marylandData.length; i++) {
				if (marylandData[i].name == county) {
					for (var j = 0; j < data.length; j++) {
						for (var k = 0; k < marylandData[i].data.length; k++) {
							if (data[j].date == marylandData[i].data[k].date) {
								data[j].total_cases = marylandData[i].data[k].total_cases;
							}
						}
					}
				}
			}
			var previous_total_cases = 0; 
			var daily_cases = 0;
			for (var i = 0; i < data.length; i++) {
				daily_cases = data[i].total_cases - previous_total_cases;
				previous_total_cases = data[i].total_cases;
				data[i].percent_cases = (parseInt(data[i].total_cases)/parseInt(population)*100).toFixed(3);
				data[i].altPercent_cases = (parseInt(data[i].total_cases)/parseInt(population)*1000000).toFixed(3);
				data[i].daily_cases = daily_cases;
			}

			var seven_day_count = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += parseInt(data[i].daily_cases);
				if (i < 7) {
					data[i].sevenDayAverage = (parseInt(seven_day_count) / parseInt(i + 1)).toFixed(3);
				}
				if (i >= 7) {
					seven_day_count -= data[i - 7].daily_cases;
					data[i].sevenDayAverage = (parseInt(seven_day_count) / 7).toFixed(3);
				}
			}

			selectedCounties.push({
				name: county,
				data: data,
				color: colorsList[(Math.random() * 56).toFixed()],
				state: "MD"
			});
			this.setState({
				selectedCounties: selectedCounties,
				triggerSetChartData: true
			});
		}
	}

	handleSelectCountyNY = (county, population) => {
		const { countiesNewYorkList } = this.state;
		const { selectedCounties } = this.state;
		if (this.setSelectedCountiesNY.has(county)) {
			this.setSelectedCountiesNY.delete(county);
			let newSelectedCountiesList = selectedCounties.filter(e => (e.name !== county && e.state == e.state));
			for (var i = 0; i < countiesNewYorkList.length; i++) {
				if (countiesNewYorkList[i].name == county) {
					countiesNewYorkList[i].active = false;
				}
			}
			this.setState({
				countiesNewYorkList: countiesNewYorkList,
				selectedCounties: newSelectedCountiesList,
				triggerSetChartData: true
			});
		} else {
			this.setSelectedCountiesNY.add(county);
			for (var i = 0; i < countiesNewYorkList.length; i++) {
				if (countiesNewYorkList[i].name == county) {
					countiesNewYorkList[i].active = true;
				}
			}
			this.props.getCaseDataNyCountyCompare(county);
			this.setState({countiesNewYorkList: countiesNewYorkList, population: population});
		}
	}

	handleSelectCountyVT = (county, population) => {
		const { countiesVermontList, selectedCounties } = this.state;
		// console.log('County: ', county);
		if (this.setSelectedCountiesVT.has(county)) {
			this.setSelectedCountiesVT.delete(county);
			let newSelectedCounties = selectedCounties.filter(e => (e.name != county && e.state == "VT"));
			// console.log('newSelectedCounties: ', newSelectedCounties);
			for (var i = 0; i < countiesVermontList.length; i++) {
				if (countiesVermontList[i].name == county) {
					countiesVermontList[i].active = false;
				}
			}
			this.setState({
				selectedCounties: newSelectedCounties,
				countiesVermontList: countiesVermontList,
				triggerSetChartData: true
			});
		} else {
			this.setSelectedCountiesVT.add(county);
			for (var i = 0; i < countiesVermontList.length; i++) {
				if (countiesVermontList[i].name == county) {
					countiesVermontList[i].active = true;
				}
			}
		}
		this.props.getCaseDataVTCountyCompare(county);
		this.setState({
			countiesVermontList:  countiesVermontList,
			population: population
		});
	}

	handleSelectCountyWI = (county, population) => {
		// console.log('County: ', county);
		const { selectedCounties, countiesWisconsinList } = this.state;
		if (this.setSelectedCountiesWI.has(county)) {
			this.setSelectedCountiesWI.delete(county);
			for (var i = 0; i < countiesWisconsinList.length; i++) {
				if (countiesWisconsinList[i].name == county) {
					countiesWisconsinList[i].active = false;
				}
			}
			let newSelectedCounties = selectedCounties.filter(e => (e.name != county && e.state == "WI"));
			this.setState({
				countiesWisconsinList: countiesWisconsinList,
				selectedCounties: newSelectedCounties,
				triggerSetChartData: true
			})
		} else {
			this.setSelectedCountiesWI.add(county);
			for (var i = 0; i < countiesWisconsinList.length; i++) {
				if (countiesWisconsinList[i].name == county) {
					countiesWisconsinList[i].active = true;
				}
			}
			this.props.getCaseDataWICountyCompare(county);
			this.setState({
				countiesWisconsinList: countiesWisconsinList,
				population: population
			});
		}
	}

	handleSelectState = (state, abbreviation, population) => {
		const { statesList, selectedStates } = this.state;
		if (this.setSelectedStates.has(abbreviation)) {
			this.setSelectedStates.delete(abbreviation);
			let newSelectedStates = selectedStates.filter(e => e.name !== state);
			for (var i = 0; i < statesList.length; i++) {
				if (statesList[i].name == state) {
					statesList[i].active = false;
				}
			}
			this.setState({
				selectedStates: newSelectedStates,
				statesList: statesList,
				triggerSetChartData: true
			});
		} else {
			this.setSelectedStates.add(abbreviation);
			this.props.getCovidTrackerSingleStateDailyCompare(abbreviation.toLowerCase());
			for (var i = 0; i < statesList.length; i++) {
				if (statesList[i].abbreviation == abbreviation) {
					statesList[i].active = true;
				}
			}
			this.setState({
				stateName: state,
				stateAbbreviation: abbreviation,
				statePopulation: population,
				statesList: statesList
			});
		}
	}

	handleSelectCountry = (countryID) => {
		const { countriesList } = this.state;
		if (this.setSelectedCountries.has(countryID)) {
			this.setSelectedCountries.delete(countryID);
			for (var i = 0; i < countriesList.length; i++) {
				if (countryID == countriesList[i].id) {
					countriesList[i].active = false;
				}
			}
		} else {
			this.setSelectedCountries.add(countryID);
			for (var i = 0; i < countriesList.length; i++) {
				if (countryID == countriesList[i].id) {
					countriesList[i].active = true;
				}
			}
		}
		this.setState({
			countriesList: countriesList
		});
		this.setChartData();
	}

	minTwoDigits = (n) => {
	  return (n < 10 ? '0' : '') + n;
	}

	formatDate = (s) => {
		var fields = s.split('-');
		return fields[0]+fields[1]+fields[2];
	}

	formatDateState = (date) => {
		var dateFields = date.split('-');
		return dateFields[0]+dateFields[1]+dateFields[2];
	}

	formatDateCountry = (s) => {
		var fields = s.split('-');
		return fields[2]+'/'+fields[1]+'/'+fields[0];
	}

	formatDateCO = (s) => {
		var fields = s.split('/');
		return fields[2]+'-'+fields[0]+'-'+fields[1];
	}

	// formatDateHK = (s) => {
	// 	var date = s.split('/');
	// 	return date[2]+'-'+this.minTwoDigits(date[1])+'-'+this.minTwoDigits(date[0])
	// }

	formatDateHK = (s) => {
		var date = s.split('/');
		return date[2]+'-'+date[1]+'-'+date[0];
	}

	handleDisplayEuropeanUnion = () => {
		const { displayEU } = this.state;
		if (displayEU) {
			this.setState({
				displayEU: false,
			});
		} else {
			this.setState({
				displayEU: true
			});
		}
		this.setState({
			triggerSetChartData: true
		});
	}

	setDisplayCountries = () => {
		const { dataWorldWideEuropaCompare } = this.props;
		const { countriesList, datesArray, colorsList } = this.state;
		// console.log('Countries List: ', countriesList);
		// console.log('Dates Array: ', datesArray);
		dataWorldWideEuropaCompare.map(datum => {
			this.worldCountries.add(datum.countriesAndTerritories);
		});
		// console.log('Data: ', dataWorldWideEuropaCompare);
		for (let country of this.worldCountries) {
			countriesList.push({id: '', name: country, population: 0, continent: '', color: colorsList[(Math.random() * 56).toFixed()], active: false, data: []});
		}
		for (var i = 0; i < countriesList.length; i++) {
			dataWorldWideEuropaCompare.map(datum => {
				if (countriesList[i].name == datum.countriesAndTerritories) {
					countriesList[i].name = String(datum.countriesAndTerritories).replace(/_/g,' ');
					countriesList[i].id = datum.countryterritoryCode;
					countriesList[i].population = parseInt(datum.popData2019);
					countriesList[i].continent = datum.continentExp;
				}
			});
			for (var j = 0; j < datesArray.length; j++) {
				countriesList[i].data.push({date: datesArray[j], daily_cases: 0, daily_deaths: 0, total_cases: 0, total_deaths: 0, percent_cases: 0, percent_deaths: 0, altPercent_cases: 0, altPercent_deaths: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, altPercent_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}
		}
		var totalCases = 0;
		var totalDeaths = 0;
		var sevenDayCount = 0;
		var sevenDayDeaths = 0;
		for (var i = 0; i < countriesList.length; i++) {
			for (var j = 0; j < countriesList[i].data.length; j++) {
				dataWorldWideEuropaCompare.map(datum => {
					if (countriesList[i].id == datum.countryterritoryCode && this.formatDateCountry(countriesList[i].data[j].date) == datum.dateRep) {
						totalCases += Math.abs(datum.cases);
						totalDeaths += Math.abs(datum.deaths);
						countriesList[i].data[j].daily_cases = Math.abs(datum.cases);
						countriesList[i].data[j].daily_deaths = Math.abs(datum.deaths);
						countriesList[i].data[j].total_cases = Math.abs(totalCases);
						countriesList[i].data[j].total_deaths = Math.abs(totalDeaths);
						countriesList[i].data[j].percent_cases = ((Math.abs(totalCases)/parseInt(countriesList[i].population))*100).toFixed(10);
						countriesList[i].data[j].altPercent_cases = ((Math.abs(totalCases)/parseInt(countriesList[i].population))*10000000).toFixed(10);
						countriesList[i].data[j].percent_deaths = ((Math.abs(totalDeaths)/parseInt(countriesList[i].population))*100).toFixed(10);
						countriesList[i].data[j].altPercent_deaths = ((Math.abs(totalDeaths)/parseInt(countriesList[i].population))*100000).toFixed(10);
					}
				})
			}
			for (var j = 0; j < countriesList[i].data.length; j++) {
				sevenDayCount += countriesList[i].data[j].daily_cases;
				sevenDayDeaths += countriesList[i].data[j].daily_deaths;
				if (j < 7) {
					countriesList[i].data[j].sevenDayAverage = (sevenDayCount / (j + 1)).toFixed(2);
					countriesList[i].data[j].sevenDayAverageDeaths = (sevenDayDeaths / (j + 1)).toFixed(2);
				}
				if (j >= 7) {
					sevenDayCount -= countriesList[i].data[j - 7].daily_cases;
					sevenDayDeaths -= countriesList[i].data[j - 7].daily_deaths;
					countriesList[i].data[j].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
					countriesList[i].data[j].sevenDayAverageDeaths = (sevenDayDeaths / 7).toFixed(2);
				}
			}
			totalCases = 0;
			totalDeaths = 0;
			sevenDayCount = 0;
			sevenDayDeaths = 0;
		}

		for (var i = 0; i < countriesList.length; i++) {
			// Check 2nd to Last
			if (countriesList[i].data[countriesList[i].data.length -2].total_cases == 0) {
				countriesList[i].data[countriesList[i].data.length -2].total_cases = countriesList[i].data[countriesList[i].data.length -3].total_cases;
			}
			if (countriesList[i].data[countriesList[i].data.length -2].total_deaths == 0) {
				countriesList[i].data[countriesList[i].data.length -2].total_deaths = countriesList[i].data[countriesList[i].data.length -3].total_deaths;
			}
			if (countriesList[i].data[countriesList[i].data.length -2].percent_cases == 0) {
				countriesList[i].data[countriesList[i].data.length -2].percent_cases = countriesList[i].data[countriesList[i].data.length -3].percent_cases;
			}
			if (countriesList[i].data[countriesList[i].data.length -2].percent_deaths == 0) {
				countriesList[i].data[countriesList[i].data.length -2].percent_deaths = countriesList[i].data[countriesList[i].data.length -3].percent_deaths;
			}
			// Check Last
			if (countriesList[i].data[countriesList[i].data.length -1].total_cases == 0) {
				countriesList[i].data[countriesList[i].data.length -1].total_cases = countriesList[i].data[countriesList[i].data.length -2].total_cases;
			}
			if (countriesList[i].data[countriesList[i].data.length -1].total_deaths == 0) {
				countriesList[i].data[countriesList[i].data.length -1].total_deaths = countriesList[i].data[countriesList[i].data.length -2].total_deaths;
			}
			if (countriesList[i].data[countriesList[i].data.length -1].percent_cases == 0) {
				countriesList[i].data[countriesList[i].data.length -1].percent_cases = countriesList[i].data[countriesList[i].data.length -2].percent_cases;
			}
			if (countriesList[i].data[countriesList[i].data.length -1].percent_deaths == 0) {
				countriesList[i].data[countriesList[i].data.length -1].percent_deaths = countriesList[i].data[countriesList[i].data.length -2].percent_deaths;
			}
		}
		this.setState({
			processingCountriesData: false, 
			displayCountriesList: true,
			countriesList: countriesList
		});
	}

	getDatesList = () => {
		const { startDate, endDate, selectedLocationsData } = this.state;
		var listDate = [];
		var listDateFormatted = [];
		// var startDate = '2020-03-01';
		// var endDate = this.state.todaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		for (var i = 0; i < listDate.length; i++) {
			listDateFormatted.push(listDate[i]);
		}
		this.setState({
			datesArray: listDateFormatted
		});
		if (selectedLocationsData.length > 0) {
			this.setChartData();
		}
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();

		// today
		// var dd = String(date.getDate()).padStart(2, '0');
		// var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
		// var yyyy = date.getFullYear();
		// var today = yyyy + '-' + mm + '-' + dd;

		// yesterday
		var newDate = date.setDate(date.getDate() -1);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		this.setState({
			newEndDate: today, 
			todaysDate: today,
			endDate: today
		});
	}

	convertDate = (date) => {
		var dd = String(date.getDate()).padStart(2, '0');
		var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = date.getFullYear();
		return yyyy + '-' + mm + '-' + dd;
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
		if (window.innerWidth/window.innerHeight <= .75) {
			this.setState({showSkinnyButtons: true});
		} else {
			this.setState({showSkinnyButtons: false});
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.setSelectedCities = new Set();
		this.setSelectedCountiesCA = new Set();
		this.setSelectedCountiesCO = new Set();
		this.setSelectedCountiesCT = new Set();
		this.setSelectedCountiesGA = new Set();
		this.setSelectedCountiesIN = new Set();
		this.setSelectedCountiesMD = new Set();
		this.setSelectedCountiesNY = new Set();
		this.setSelectedCountiesVT = new Set();
		this.setSelectedCountiesWI = new Set();
		this.setSelectedStates = new Set();
		this.setSelectedCountries = new Set();
		this.worldCountries = new Set();
		this.props.getColorsList();
		this.props.getCountiesCAList();
		this.props.getCountiesCOList();
		this.props.getCountiesCTList();
		this.props.getCountiesGAList();
		this.props.getCountiesINList();
		this.props.getCountiesMDList();
		this.props.getCountiesNYList();
		this.props.getCountiesVTList();
		this.props.getCountiesWIList();
		this.props.getStatesList();
		this.props.getCaseDataMDCountyCompare();
		this.getTodaysDate();
		this.updateWindowDimensions();
		this.setState({newStartDate: '2020-03-01'});
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.state.todaysDate != '' && lastState.todaysDate == '') {
			this.getDatesList();
		}

		if (this.state.triggerSetChartData === true && lastState.triggerSetChartData === false) {
			this.setChartData();
			this.setState({triggerSetChartData: false});
		}

		if (this.props.importColorsList !== null && lastProps.importColorsList === null) {
			const colorsList = [];
			this.props.importColorsList.map(datum  => {
				colorsList.push(datum);
			});
			this.setState({colorsList: colorsList});
		}

		if (this.props.importCountiesCAList !== null && lastProps.importCountiesCAList === null) {
			const countiesCaliforniaList = [];
			this.props.importCountiesCAList.map(datum => {
				countiesCaliforniaList.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesCaliforniaList: countiesCaliforniaList
			});
		}

		if (this.props.importCountiesCOList !== null && lastProps.importCountiesCOList === null) {
			const countiesColoradoList = [];
			this.props.importCountiesCOList.map(datum => {
				countiesColoradoList.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesColoradoList: countiesColoradoList
			});
		}

		if (this.props.importCountiesCTList !== null && lastProps.importCountiesCTList === null) {
			const countiesConnecticutList = [];
			this.props.importCountiesCTList.map(datum => {
				countiesConnecticutList.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesConnecticutList: countiesConnecticutList
			});
		}

		if (this.props.importCountiesGAList !== null && lastProps.importCountiesGAList === null) {
			const countiesGeorgiaList = [];
			this.props.importCountiesGAList.map(datum => {
				countiesGeorgiaList.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesGeorgiaList: countiesGeorgiaList
			});
		}

		if (this.props.importCountiesINList !== null && lastProps.importCountiesINList === null) {
			const countiesIndianaList = [];
			this.props.importCountiesINList.map(datum => {
				countiesIndianaList.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesIndianaList: countiesIndianaList
			});
		}

		if (this.props.importCountiesMDList !==  null && lastProps.importCountiesMDList === null) {
			const countiesMarylandList = [];
			this.props.importCountiesMDList.map(datum => {
				countiesMarylandList.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesMarylandList: countiesMarylandList
			});
		}

		if (this.props.importCountiesNYList !== null && lastProps.importCountiesNYList === null) {
			const countiesNewYorkList = [];
			this.props.importCountiesNYList.map(datum => {
				countiesNewYorkList.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesNewYorkList: countiesNewYorkList
			});
		}

		if (this.props.importCountiesVTList !== null && lastProps.importCountiesVTList === null) {
			const countiesVermontList = [];
			this.props.importCountiesVTList.map(datum => {
				countiesVermontList.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesVermontList: countiesVermontList
			});
		}

		if (this.props.importCountiesWIList !== null && lastProps.importCountiesWIList === null) {
			const countiesWisconsinList = [];
			this.props.importCountiesWIList.map(datum => {
				countiesWisconsinList.push({
					name: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesWisconsinList: countiesWisconsinList
			});
		}

		if (this.props.importStatesList !== null && lastProps.importStatesList === null) {
			const statesList = [];
			this.props.importStatesList.map(datum => {
				statesList.push({
					name: datum.name,
					population: datum.population, 
					abbreviation: datum.abbreviation,
					active: false
				});
			});
			this.setState({
				statesList: statesList
			});
		}

		if (this.props.loadingDataSFCompare === false && lastProps.loadingDataSFCompare === true) {
			const { dataSFCompare } = this.props;
			const { citiesList, datesArray, colorsList, citySanFrancisco } = this.state;
			const cityData = [];
			for (var i = 0; i < datesArray.length; i++) {
				cityData.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, daily_deaths: 0, percent_deaths: 0, altPercent_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}

			var total = 0;
			var total_deaths = 0;
			for (var i = 0; i < cityData.length; i++) {
				dataSFCompare.map(datum => {
					if (datum.specimen_collection_date.split('T')[0] == cityData[i].date) {
						if (datum.case_disposition == "Confirmed") {
							total += parseInt(datum.case_count)
							cityData[i].daily_cases += parseInt(datum.case_count)
						}
						if (datum.case_disposition == 'Death') {
							total_deaths += parseInt(datum.case_count);
							cityData[i].daily_deaths += parseInt(datum.case_count);
						}
					}
				});
				cityData[i].total_cases = total;
				cityData[i].percent_cases = (total / citySanFrancisco.population * 100).toFixed(3);
				cityData[i].altPercent_cases = (total / citySanFrancisco.population * 1000000).toFixed(3);
				cityData[i].total_deaths = total_deaths;
				cityData[i].percent_deaths = (total_deaths / citySanFrancisco.population * 100).toFixed(3);
				cityData[i].altPercent_deaths = (total_deaths / citySanFrancisco.population * 100000).toFixed(3);
			}

			var sevenDayCount = 0;
			var sevenDayDeaths  = 0;
			for (var i = 0; i < cityData.length; i++) {
				sevenDayCount += parseFloat(cityData[i].daily_cases);
				sevenDayDeaths += parseFloat(cityData[i].daily_deaths);
				if (i < 7) {
					cityData[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
					cityData[i].sevenDayAverageDeaths = (sevenDayDeaths / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(cityData[i - 7].daily_cases);
					sevenDayDeaths -= parseFloat(cityData[i - 7].daily_deaths);
					cityData[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
					cityData[i].sevenDayAverageDeaths = (sevenDayDeaths / 7).toFixed(2);
				}
			}

			citiesList.push({name: "San Francisco", color: colorsList[(Math.random() * 56).toFixed()], data: cityData})
			// console.log('Cities List: ', citiesList);
			this.setState({citiesList: citiesList, triggerSetChartData: true});
			this.props.clearComparisonDataCompare();
			this.props.getCityDataSFTesting();
		}

		if (this.props.loadingCityDataSFTesting === false && (lastProps.loadingCityDataSFTesting === true || lastProps.loadingCityDataSFTesting === undefined)) {
			const { cityDataSFTesting } = this.props;
			const { citiesList, citySanFrancisco } = this.state;
			var total_tests = 0;
			for (var i = 0; i < citiesList.length; i++) {
				if (citiesList[i].name == "San Francisco") {
					for (var j = 0; j < citiesList[i].data.length; j++) {
						cityDataSFTesting.map(datum => {
							if (citiesList[i].data[j].date == datum.specimen_collection_date.split('T')[0]) {
								total_tests += parseInt(datum.tests);
								citiesList[i].data[j].total_tests += total_tests;
								citiesList[i].data[j].percent_tests = ((total_tests / citySanFrancisco.population) * 100).toFixed(3);
								citiesList[i].data[j].altPercent_tests = ((total_tests / citySanFrancisco.population) * 1000000).toFixed(3);
								citiesList[i].data[j].daily_tests = parseInt(datum.tests);
								citiesList[i].data[j].daily_positive_percent = parseFloat(datum.pct);
							}
						});
					}
				}
			}

			var seven_day_count = 0;
			for (var i = 0; i < citiesList.length; i++) {
				if (citiesList[i].name == "San Francisco") {
					for (var j = 0; j < citiesList[i].data.length; j++) {
						seven_day_count += citiesList[i].data[j].daily_positive_percent;
						if (j < 7) {
							citiesList[i].data[j].sevenDayAveragePositiveTests = (seven_day_count / (j + 1)).toFixed(3);
						} else {
							seven_day_count -= citiesList[i].data[j - 7].daily_positive_percent;
							citiesList[i].data[j].sevenDayAveragePositiveTests = (seven_day_count / 7).toFixed(3);
						}
					}
				}
			}
			this.setState({citiesList: citiesList, triggerSetChartData: true});
			this.props.clearCityDataSFTesting();
			this.props.getCityDataSFHospitalization();
		}

		if (this.props.loadingCityDataSFHospitalization === false && (lastProps.loadingCityDataSFHospitalization === true || lastProps.loadingCityDataSFHospitalization ==  undefined)) {
			const { cityDataSFHospitalization } = this.props;
			const { citiesList, citySanFrancisco } = this.state;
			for (var i = 0; i < citiesList.length; i++) {
				if (citiesList[i].name == "San Francisco") {
					for (var j = 0; j < citiesList[i].data.length; j++) {
						cityDataSFHospitalization.map(datum => {
							if (citiesList[i].data[j].date == datum.reportdate.split('T')[0]) {
								if (datum.dphcategory == "Med/Surg" && datum.covidstatus != "PUI") {
									citiesList[i].data[j].hospitalizations = parseInt(datum.patientcount);
								}
								if (datum.dphcategory == "ICU" && datum.covidstatus != "PUI") {
									citiesList[i].data[j].icuBeds = parseInt(datum.patientcount);
								}
							}
						});
					}
				}
			}
			var seven_day_hospital = 0;
			var seven_day_icu = 0;
			for (var i = 0; i < citiesList.length; i++) {
				if (citiesList[i].name == "San Francisco") {
					for (var j = 0; j < citiesList[i].data.length; j++) {
						seven_day_hospital += citiesList[i].data[j].hospitalizations;
						seven_day_icu += citiesList[i].data[j].icuBeds;
						if (j < 7) {
							citiesList[i].data[j].sevenDayAverageHospitalizations = (seven_day_hospital / (i + 1)).toFixed(3);
							citiesList[i].data[j].sevenDayAverageICUBeds = (seven_day_icu / (i + 1)).toFixed(3);
						} else {
							seven_day_hospital -= citiesList[i].data[j - 7].hospitalizations;
							seven_day_icu -= citiesList[i].data[j - 7].icuBeds;
							citiesList[i].data[j].sevenDayAverageHospitalizations = (seven_day_hospital / 7).toFixed(3);
							citiesList[i].data[j].sevenDayAverageICUBeds = (seven_day_icu / 7).toFixed(3);
						}
					}
				}
			}
			
			this.setState({citiesList: citiesList, triggerSetChartData: true});
			this.props.clearCityDataSFHospitalization();
		}

		if (this.props.loadingDataNYCCasesCompare === false && lastProps.loadingDataNYCCasesCompare === true) {
			const { dataNYCCasesCompare } = this.props;
			// console.log('Data NYC Cases: ', dataNYCCasesCompare);
			const { citiesList, datesArray, colorsList, cityNewYorkCity } = this.state;
			const cityData = [];
			for (var i = 0; i < datesArray.length; i++) {
				cityData.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}

			var total = 0;
			for (var i = 0; i < cityData.length; i++) {
				dataNYCCasesCompare.map(datum => {
					if (datum.test_date.split('T')[0] == cityData[i].date) {
						cityData[i].total_cases += parseInt(datum.cumulative_number_of_positives);
						cityData[i].daily_cases += parseInt(datum.new_positives);
						cityData[i].total_tests = datum.cumulative_number_of_tests;
						cityData[i].percent_tests = (datum.cumulative_number_of_tests / cityNewYorkCity.population * 100).toFixed(3);
						cityData[i].altPercent_tests = (datum.cumulative_number_of_tests / cityNewYorkCity.population * 1000000).toFixed(3);
						cityData[i].daily_tests = datum.total_number_of_tests;
						if (datum.total_number_of_tests > 0 && datum.new_positives > 0) {
							cityData[i].daily_positive_percent = (datum.new_positives / datum.total_number_of_tests * 100).toFixed(3);
						}
					}
				});
			}

			var sevenDayCount = 0;
			var sevenDayPositivity = 0;
			for (var i = 0; i < cityData.length; i++) {
				cityData[i].percent_cases = (cityData[i].total_cases / cityNewYorkCity.population * 100).toFixed(3);
				cityData[i].altPercent_cases = (cityData[i].total_cases / cityNewYorkCity.population * 1000000).toFixed(3);
				sevenDayCount += parseFloat(cityData[i].daily_cases);
				sevenDayPositivity += parseFloat(cityData[i].daily_positive_percent);
				if (i < 7) {
					cityData[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
					cityData[i].sevenDayAveragePositiveTests = (sevenDayPositivity / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(cityData[i - 7].daily_cases);
					sevenDayPositivity -= parseFloat(cityData[i - 7].daily_positive_percent);
					cityData[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
					cityData[i].sevenDayAveragePositiveTests = (sevenDayPositivity / 7).toFixed(2);
				}
			}

			if (cityData[cityData.length - 2].total_cases == 0) {
				cityData[cityData.length - 2].total_cases = cityData[cityData.length - 3].total_cases;
			}
			if (cityData[cityData.length - 2].percent_cases == 0) {
				cityData[cityData.length - 2].percent_cases = cityData[cityData.length - 3].percent_cases;
			}

			if (cityData[cityData.length - 1].total_cases == 0) {
				cityData[cityData.length - 1].total_cases = cityData[cityData.length - 2].total_cases;
			}
			if (cityData[cityData.length - 1].percent_cases == 0) {
				cityData[cityData.length - 1].percent_cases = cityData[cityData.length - 2].percent_cases;
			}

			citiesList.push({name: "New York City", color: colorsList[(Math.random() * 56).toFixed()], data: cityData})

			this.setState({citiesList: citiesList, triggerSetChartData: true});
			this.props.clearComparisonDataCompare();
		}

		if (this.props.loadingDataHKCompare === false && lastProps.loadingDataHKCompare === true) {
			const { dataHKCompare } = this.props;
			// console.log('HK Data: ', dataHKCompare);
			const { citiesList, datesArray, colorsList, cityHongKong } = this.state;
			const cityData = [];
			for (var i = 0; i < datesArray.length; i++) {
				cityData.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, altPercent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}

			for (var i = 0; i < cityData.length; i++) {
				dataHKCompare.map(datum => {
					if (this.formatDateHK(datum["Report date"]) == cityData[i].date) {
						cityData[i].daily_cases += 1;
						if (datum["Hospitalised/Discharged/Deceased"] == "Deceased") {
							cityData[i].daily_deaths += 1;
						}
					}
				});
			}

			var total = 0;
			var total_deaths = 0;
			for (var i = 0; i < cityData.length; i++) {
				total += cityData[i].daily_cases;
				total_deaths +=  cityData[i].daily_deaths;
				cityData[i].total_cases = total;
				cityData[i].total_deaths = total_deaths;
				cityData[i].percent_cases = (total / cityHongKong.population * 100).toFixed(3);
				cityData[i].altPercent_cases = (total / cityHongKong.population * 1000000).toFixed(3);
				cityData[i].percent_deaths = (total_deaths / cityHongKong.population * 100).toFixed(3);
				cityData[i].altPercent_deaths = (total_deaths / cityHongKong.population * 100000).toFixed(3);
			}

			var sevenDayCount = 0;
			var sevenDayDeaths = 0;
			for (var i = 0; i < cityData.length; i++) {
				sevenDayCount += parseFloat(cityData[i].daily_cases);
				sevenDayDeaths += parseFloat(cityData[i].daily_deaths);
				if (i < 7) {
					cityData[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
					cityData[i].sevenDayAverageDeaths = (sevenDayDeaths / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(cityData[i - 7].daily_cases);
					sevenDayDeaths -= parseFloat(cityData[i - 7].daily_deaths);
					cityData[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
					cityData[i].sevenDayAverageDeaths = (sevenDayDeaths / 7).toFixed(2);
				}
			}

			citiesList.push({name: "Hong Kong", color: colorsList[(Math.random() * 56).toFixed()], data: cityData})
			this.setState({citiesList: citiesList, triggerSetChartData: true});
			this.props.clearComparisonDataCompare();
		}

		if (this.props.loadingDataCHICaseCompare === false && (lastProps.loadingDataCHICaseCompare === true || lastProps.loadingDataCHICaseCompare === undefined)) {
			const { dataCHICaseCompare } = this.props;
			const { colorsList, datesArray, citiesList, cityChicago } = this.state;
			console.log('dataCHICaseCompare: ', dataCHICaseCompare);
			const data = [];
			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, total_deaths: 0, percent_deaths: 0, altPercent_deaths: 0, daily_deaths: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}
			var total_cases = 0;
			var total_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				dataCHICaseCompare.map(datum => {
					if (datum.lab_report_date !== undefined) {
						if (data[i].date == datum.lab_report_date.split('T')[0]) {
							data[i].daily_cases = parseInt(datum.cases_total);
							data[i].daily_deaths = parseInt(datum.deaths_total);
							total_cases += parseInt(datum.cases_total);
							total_deaths += parseInt(datum.deaths_total);
							data[i].total_cases = total_cases;
							data[i].total_deaths = total_deaths;
							data[i].percent_cases = (total_cases / cityChicago.population * 100).toFixed(3);
							data[i].altPercent_cases = (total_cases / cityChicago.population * 1000000).toFixed(3);
							data[i].percent_deaths = (total_deaths / cityChicago.population * 100).toFixed(3);
							data[i].altPercent_deaths = (total_deaths / cityChicago.population * 100000).toFixed(3);
						}
					}
				});
			}
			if (data[data.length - 1].total_cases == 0) {
				data[data.length - 1].total_cases = data[data.length -2].total_cases;
			}
			if (data[data.length - 1].percent_cases == 0) {
				data[data.length - 1].percent_cases = data[data.length -2].percent_cases;
			}
			if (data[data.length -1].total_deaths == 0) {
				data[data.length -1].total_deaths = data[data.length -2].total_deaths;
			}
			if (data[data.length -1].percent_deaths == 0) {
				data[data.length -1].percent_deaths = data[data.length -2].percent_deaths;
			}
			var seven_day_count = 0;
			var seven_day_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += data[i].daily_cases;
				seven_day_deaths += data[i].daily_deaths;
				if (i < 7) {
					data[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / (i + 1)).toFixed(3);
				}
				else {
					seven_day_count -= data[i - 7].daily_cases;
					seven_day_deaths -= data[i - 7].daily_deaths;
					data[i].sevenDayAverage = (seven_day_count / 7).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / 7).toFixed(3);
				}
			}
			citiesList.push({
				name: "Chicago",
				color: colorsList[(Math.random() * 56).toFixed()],
				data: data
			});
			this.setState({
				citiesList: citiesList,
				triggerSetChartData: true
			});
		}

		if (this.props.loadingCaseDataCountyCaCompare === false && (lastProps.loadingCaseDataCountyCaCompare === true || lastProps.loadingCaseDataCountyCaCompare === undefined)) {
			const { caseDataCountyCaCompare } = this.props;
			// console.log('Case Data County CA: ', caseDataCountyCaCompare);
			const { selectedCounties, datesArray, colorsList, population } = this.state;
			var randomNumber = (Math.random() * 56).toFixed();
			const countyData = {name: caseDataCountyCaCompare[0].county, color: colorsList[randomNumber], state: "CA", data: []};
			for (var i = 0; i < datesArray.length; i++) {
				countyData.data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, altPercent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}
			for (var i = 0; i < countyData.data.length; i++) {
				caseDataCountyCaCompare.map(datum => {
					if (countyData.data[i].date == datum.date.split('T')[0]) {
						countyData.data[i].total_cases = datum.totalcountconfirmed;
						countyData.data[i].percent_cases = (datum.totalcountconfirmed / population * 100).toFixed(3);
						countyData.data[i].altPercent_cases = (datum.totalcountconfirmed / population * 1000000).toFixed(3);
						countyData.data[i].daily_cases = datum.newcountconfirmed;
						countyData.data[i].total_deaths = datum.totalcountdeaths;
						countyData.data[i].percent_deaths = (datum.totalcountdeaths / population *  100).toFixed(3);
						countyData.data[i].altPercent_deaths = (datum.totalcountdeaths / population *  100000).toFixed(3);
						countyData.data[i].daily_deaths = datum.newcountdeaths;
					}
				});
			}

			var sevenDayCount = 0;
			var sevenDayDeaths = 0;
			for (var i = 0; i < countyData.data.length; i++) {
				sevenDayCount += parseFloat(countyData.data[i].daily_cases);
				sevenDayDeaths += parseFloat(countyData.data[i].daily_deaths);
				if (i < 7) {
					countyData.data[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
					countyData.data[i].sevenDayAverageDeaths = (sevenDayDeaths / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(countyData.data[i - 7].daily_cases);
					sevenDayDeaths -= parseFloat(countyData.data[i - 7].daily_deaths);
					countyData.data[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
					countyData.data[i].sevenDayAverageDeaths = (sevenDayDeaths / 7).toFixed(2);
				}
			}

			if (countyData.data[countyData.data.length - 1].total_cases == 0) {
				countyData.data[countyData.data.length - 1].total_cases = countyData.data[countyData.data.length - 2].total_cases;
			}
			if (countyData.data[countyData.data.length - 1].percent_cases == 0) {
				countyData.data[countyData.data.length - 1].percent_cases = countyData.data[countyData.data.length - 2].percent_cases;
			}

			if (countyData.data[countyData.data.length - 1].total_deaths == 0) {
				countyData.data[countyData.data.length - 1].total_deaths = countyData.data[countyData.data.length - 2].total_deaths;
			}
			if (countyData.data[countyData.data.length - 1].percent_deaths == 0) {
				countyData.data[countyData.data.length - 1].percent_deaths = countyData.data[countyData.data.length - 2].percent_deaths;
			}

			selectedCounties.push(countyData);
			this.setState({selectedCounties: selectedCounties, population: 0, triggerSetChartData: true});
			this.props.clearCaseDataCaCountyCompare();
			this.props.getHospitalDataCaCountyCompare(caseDataCountyCaCompare[0].county);
		}

		if (this.props.loadingHospitalDataCountyCACompare === false && (lastProps.loadingHospitalDataCountyCACompare === true || lastProps.loadingHospitalDataCountyCaCompare === undefined)) {
			const { hospitalDataCountyCACompare } = this.props;
			const { selectedCounties } = this.state;
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == hospitalDataCountyCACompare[0].county && selectedCounties[i].state == "CA") {
					for (var j = 0; j < selectedCounties[i].data.length; j++) {
						hospitalDataCountyCACompare.map(datum => {
							if (selectedCounties[i].data[j].date == datum.todays_date.split('T')[0]) {
								selectedCounties[i].data[j].hospitalizations = parseFloat(datum.hospitalized_covid_confirmed_patients) + parseFloat(datum.hospitalized_suspected_covid_patients);
								selectedCounties[i].data[j].icuBeds = parseFloat(datum.icu_covid_confirmed_patients) + parseFloat(datum.icu_suspected_covid_patients);
							}
						})
					}
				}
			}

			var seven_day_hospitalizations = 0;
			var seven_day_icu = 0;
			for (var i = 0; i < selectedCounties.length; i++) {
				if (selectedCounties[i].name == hospitalDataCountyCACompare[0].county && selectedCounties[i].state == "CA") {
					for (var j = 0; j < selectedCounties[i].data.length; j++) {
						seven_day_hospitalizations += selectedCounties[i].data[j].hospitalizations;
						seven_day_icu += selectedCounties[i].data[j].icuBeds;
						if (j < 7) {
							selectedCounties[i].data[j].sevenDayAverageHospitalizations = (seven_day_hospitalizations / (j + 1)).toFixed(3);
							selectedCounties[i].data[j].sevenDayAverageICUBeds = (seven_day_icu / (j + 1)).toFixed(3);
						} else {
							seven_day_hospitalizations -= selectedCounties[i].data[j - 7].hospitalizations;
							seven_day_icu -= selectedCounties[i].data[j - 7].icuBeds;
							selectedCounties[i].data[j].sevenDayAverageHospitalizations = (seven_day_hospitalizations / 7).toFixed(3);
							selectedCounties[i].data[j].sevenDayAverageICUBeds = (seven_day_icu / 7).toFixed(3);
						}
					}
				}
			}
			this.props.clearHospitalDataCaCountyCompare();
			this.setState({
				selectedCounties: selectedCounties,
				triggerSetChartData: true
			})
		}

		// if (this.props.something === false && lastProps.somethingElse === true) {

		// }

		if (this.props.loadingCaseDataCountyCOCompare === false && (lastProps.loadingCaseDataCountyCOCompare === true || lastProps.loadingCaseDataCountyCTCompare === undefined)) {
			const { caseDataCountyCOCompare } = this.props;
			// console.log('caseDataCountyCOCompare: ', caseDataCountyCOCompare);
			const { colorsList, datesArray, population, selectedCounties } = this.state;
			const data = [];
			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, total_deaths: 0, percent_deaths: 0, altPercent_deaths: 0, daily_deaths: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}
			var previous_total_cases = 0;
			for (var i = 0; i < data.length; i++) {
				caseDataCountyCOCompare.map(datum => {
					if (data[i].date == this.formatDateCO(datum.attributes.Date) && datum.attributes.Desc_ == "Colorado Case Counts by County") {
						data[i].total_cases = (parseInt(datum.attributes.Value) > 0) ? parseInt(datum.attributes.Value) : 0;
						data[i].percent_cases = (datum.attributes.Value / population * 100).toFixed(3);
						data[i].altPercent_cases = (datum.attributes.Value / population * 1000000).toFixed(3);
						data[i].daily_cases = ((parseInt(datum.attributes.Value) > 0) ? parseInt(datum.attributes.Value) : 0) - previous_total_cases;
						previous_total_cases = datum.attributes.Value
					}
				});
			}

			var seven_day_count = 0;
			var seven_day_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += data[i].daily_cases;
				seven_day_deaths += data[i].daily_deaths;
				if (i < 7) {
					data[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / (i + 1)).toFixed(3);
				}
				else {
					seven_day_count -= data[i - 7].daily_cases;
					seven_day_deaths -= data[i - 7].daily_deaths;
					data[i].sevenDayAverage = (seven_day_count / 7).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / 7).toFixed(3);
				}
			}

			selectedCounties.push({
				name:  caseDataCountyCOCompare[0].attributes.LABEL,
				state: "CO",
				color: colorsList[(Math.random() * 56).toFixed()],
				data: data
			});
			this.setState({
				selectedCounties: selectedCounties,
				triggerSetChartData: true,
				population: 0
			})
			this.props.clearCaseDataCOCountyCompare();
		}

		if (this.props.loadingCaseDataCountyCTCompare === false && (lastProps.loadingCaseDataCountyCTCompare === true || lastProps.loadingCaseDataCountyCTCompare === undefined)) {
			const { caseDataCountyCTCompare } = this.props;
			const { colorsList, datesArray, population, selectedCounties } = this.state;
			const countiesConnecticutList = [];
			// console.log('caseDataCountyCTCompare: ', caseDataCountyCTCompare);
			var randomNumber = (Math.random() * 56).toFixed();
			const countyData = {name: caseDataCountyCTCompare[0].county, color: colorsList[randomNumber], state: "CT", data: []};
			for (var i = 0; i < datesArray.length; i++) {
				countyData.data.push({date: datesArray[i], daily_cases: 0, daily_deaths: 0, total_cases: 0, total_deaths: 0, percent_cases: 0, altPercent_cases: 0, percent_deaths: 0, altPercent_deaths: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}
			var previous_total_cases = 0;
			var previous_percent_cases = 0;
			var previous_total_deaths = 0;
			var previous_percent_deaths = 0;
			for (var i = 0; i < countyData.data.length; i++) {
				caseDataCountyCTCompare.map(datum => {
					if (countyData.data[i].date == datum.dateupdated.split('T')[0]) {
						countyData.data[i].total_cases = datum.totalcases;
						countyData.data[i].percent_cases = (datum.totalcases / population * 100).toFixed(3);
						countyData.data[i].altPercent_cases = (datum.totalcases / population * 1000000).toFixed(3);
						countyData.data[i].daily_cases = datum.totalcases - previous_total_cases;
						previous_total_cases = datum.totalcases;
						previous_percent_cases = (datum.totalcases / population * 100).toFixed(3);
						countyData.data[i].total_deaths = datum.totaldeaths;
						countyData.data[i].percent_deaths = (datum.totaldeaths / population * 100).toFixed(3);
						countyData.data[i].altPercent_deaths = (datum.totaldeaths / population * 100000).toFixed(3);
						countyData.data[i].daily_deaths = datum.totaldeaths - previous_total_deaths;
						previous_total_deaths = datum.totaldeaths;
						previous_percent_deaths = (datum.totaldeaths / population * 100).toFixed(3);
					} else {
						countyData.data[i].total_cases = previous_total_cases;
						countyData.data[i].percent_cases = previous_percent_cases;
						countyData.data[i].total_deaths = previous_total_deaths;
						countyData.data[i].percent_deaths = previous_percent_deaths;
					}
				});
			}

			var seven_day_count = 0;
			var seven_day_deaths = 0;
			for (var i = 0; i < countyData.data.length; i++) {
				seven_day_count += countyData.data[i].daily_cases;
				seven_day_deaths += countyData.data[i].daily_deaths;
				if (i < 7) {
					countyData.data[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(3);
					countyData.data[i].sevenDayAverageDeaths = (seven_day_deaths / (i + 1)).toFixed(3);
				}
				else {
					seven_day_count -= countyData.data[i - 7].daily_cases;
					seven_day_deaths -= countyData.data[i - 7].daily_deaths;
					countyData.data[i].sevenDayAverage = (seven_day_count / 7).toFixed(3);
					countyData.data[i].sevenDayAverageDeaths = (seven_day_deaths / 7).toFixed(3);
				}
			}

			if (countyData.data[countyData.data.length - 1].total_cases == 0) {
				countyData.data[countyData.data.length - 1].total_cases = countyData.data[countyData.data.length - 2].total_cases;
			}
			if (countyData.data[countyData.data.length - 1].percent_cases == 0) {
				countyData.data[countyData.data.length - 1].percent_cases = countyData.data[countyData.data.length - 2].percent_cases;
			}
			// console.log('County Data: ', countyData);
			selectedCounties.push(countyData);
			this.setState({selectedCounties: selectedCounties, population: 0, triggerSetChartData: true});
			this.props.clearCaseDataCTCountyCompare();
		}

		if (this.props.loadingCaseDataCountyGACompare === false && (lastProps.loadingCaseDataCountyGACompare === true || lastProps.loadingCaseDataCountyGACompare === undefined)) {
			const { caseDataCountyGACompare } = this.props;
			// console.log('caseDataCountyGACompare: ', caseDataCountyGACompare);
			const { selectedCounties, population, colorsList, datesArray } = this.state;
			const data = [];
			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_deaths: 0, daily_cases: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}
			var previous_total_cases = 0;
			var previous_total_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				caseDataCountyGACompare.map(datum => {
					if (data[i].date == this.convertDate(new Date(datum.attributes.SnapshotDate))) {
						data[i].total_cases = datum.attributes.COVID_Cases;
						data[i].daily_cases = datum.attributes.COVID_Cases - previous_total_cases;
						previous_total_cases = datum.attributes.COVID_Cases;
						data[i].percent_cases = ((parseInt(datum.attributes.COVID_Cases) / population) * 100).toFixed(3);
						data[i].altPercent_cases = ((parseInt(datum.attributes.COVID_Cases) / population) * 1000000).toFixed(3);
						data[i].total_deaths = datum.attributes.COVID_Deaths;
						data[i].daily_deaths = datum.attributes.COVID_Deaths - previous_total_deaths;
						previous_total_deaths = datum.attributes.COVID_Deaths;
						data[i].percent_deaths = ((parseInt(datum.attributes.COVID_Deaths) / population) * 100).toFixed(3);
						data[i].altPercent_deaths = ((parseInt(datum.attributes.COVID_Deaths) / population) * 100000).toFixed(3);
					}
				})
			}

			var seven_day_count = 0;
			var seven_day_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += data[i].daily_cases;
				seven_day_deaths += data[i].daily_deaths;
				if (i < 7) {
					data[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / (i + 1)).toFixed(3);
				}
				else {
					seven_day_count -= data[i - 7].daily_cases;
					seven_day_deaths -= data[i - 7].daily_deaths;
					data[i].sevenDayAverage = (seven_day_count / 7).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / 7).toFixed(3);
				}
			}

			selectedCounties.push({
				name: caseDataCountyGACompare[0].attributes.NAME,
				color: colorsList[(Math.random() * 56).toFixed()],
				state: "GA",
				data: data
			});
			this.setState({
				selectedCounties: selectedCounties,
				triggerSetChartData: true
			});
			this.props.clearCaseDataGACountyCompare();
		}

		if (this.props.loadingCaseDataCountyINCompare === false && (lastProps.loadingCaseDataCountyINCompare === true || lastProps.loadingCaseDataCountyINCompare === undefined)) {
			const { caseDataCountyINCompare } = this.props;
			const { datesArray, colorsList, selectedCounties, population } = this.state;
			// console.log('caseDataCountyINCompare: ', caseDataCountyINCompare);
			const data = [];
			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, total_deaths: 0, percent_deaths: 0, altPercent_deaths: 0, daily_deaths: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}
			var total_cases = 0;
			var daily_cases = 0;
			for (var i = 0; i < data.length; i++) {
				caseDataCountyINCompare.map(datum => {
					if (data[i].date == datum.DATE.split('T')[0]) {
						total_cases += 1;
						daily_cases += 1;
					}
				})
				data[i].total_cases = total_cases;
				data[i].daily_cases = daily_cases;
				data[i].percent_cases = (total_cases / population * 100).toFixed(3);
				data[i].altPercent_cases = (total_cases / population * 1000000).toFixed(3);
				daily_cases = 0;
			}
			var seven_day_count = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += data[i].daily_cases;
				if (i < 7) {
					data[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(3);
				}
				if (i >= 7) {
					seven_day_count -= data[i - 7].daily_cases;
					data[i].sevenDayAverage = (seven_day_count / 7).toFixed(3);
				}
			}
			selectedCounties.push({
				name: caseDataCountyINCompare[0].COUNTY_NAME,
				state: "IN",
				color: colorsList[(Math.random() * 56).toFixed()],
				data: data
			});
			this.setState({
				selectedCounties: selectedCounties,
				triggerSetChartData: true
			});
			this.props.clearCaseDataINCountyCompare();
		}

		if (this.props.loadingCaseDataCountyMDCompare === false && (lastProps.loadingCaseDataCountyMDCompare === true || lastProps.loadingCaseDataCountyMDCompare === undefined)) {
			const { caseDataCountyMDCompare } = this.props;
			// console.log('caseDataCountyMDCompare: ', caseDataCountyMDCompare);
			const marylandData = [];
			const allegany = [];
			const anne_Arundel = [];
			const baltimore = [];
			const baltimore_City = [];
			const calvert = [];
			const caroline = [];
			const carroll = [];
			const cecil = [];
			const charles = [];
			const dorchester = [];
			const frederick = [];
			const garrett = [];
			const harford = [];
			const howard = [];
			const kent = [];
			const montgomery = [];
			const prince_Georges = [];
			const queen_Annes = [];
			const somerset = [];
			const st_Marys = [];
			const talbot = [];
			const washington = [];
			const wicomico = [];
			const worcester = [];
			caseDataCountyMDCompare.map(datum => {
				allegany.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Allegany, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				anne_Arundel.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Anne_Arundel, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				baltimore.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Baltimore, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				baltimore_City.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Baltimore_City, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				calvert.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Calvert, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				caroline.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Caroline, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				carroll.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Carroll, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				cecil.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Cecil, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				charles.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Charles, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				dorchester.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Dorchester, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				frederick.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Frederick, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				garrett.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Garrett, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				harford.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Harford, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				howard.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Howard, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				kent.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Kent, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				montgomery.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Montgomery, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				prince_Georges.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Prince_Georges, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				queen_Annes.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Queen_Annes, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				somerset.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Somerset, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				st_Marys.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.St_Marys, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				talbot.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Talbot, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				washington.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Washington, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				wicomico.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Wicomico, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
				worcester.push({date: this.convertDate(new Date(datum.attributes.DATE)), total_cases: datum.attributes.Worcester, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0, total_deaths: 0, percent_deaths: 0, daily_deaths: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			});

			marylandData.push({
				name: "Allegany",
				data: allegany
			});
			marylandData.push({
				name: "Anne_Arundel",
				data: anne_Arundel
			});
			marylandData.push({
				name: "Baltimore",
				data: baltimore
			});
			marylandData.push({
				name: "Baltimore_City",
				data: baltimore_City
			});
			marylandData.push({
				name: "Calvert",
				data: calvert
			});
			marylandData.push({
				name: "Caroline",
				data: caroline
			});
			marylandData.push({
				name: "Carroll",
				data: carroll
			});
			marylandData.push({
				name: "Cecil",
				data: cecil
			});
			marylandData.push({
				name: "Charles",
				data: charles
			});
			marylandData.push({
				name: "Dorchester",
				data: dorchester
			});
			marylandData.push({
				name: "Frederick",
				data: frederick
			});
			marylandData.push({
				name: "Garrett",
				data: garrett
			});
			marylandData.push({
				name: "Harford",
				data: harford
			});
			marylandData.push({
				name: "Howard",
				data: howard
			});
			marylandData.push({
				name: "Kent",
				data: kent
			});
			marylandData.push({
				name: "Montgomery",
				data: montgomery
			});
			marylandData.push({
				name: "Prince_Georges",
				data: prince_Georges
			});
			marylandData.push({
				name: "Queen_Annes",
				data: queen_Annes
			});
			marylandData.push({
				name: "Somerset",
				data: somerset
			});
			marylandData.push({
				name: "St_Marys",
				data: st_Marys
			});
			marylandData.push({
				name: "Talbot",
				data: talbot
			});
			marylandData.push({
				name: "Washington",
				data: washington
			});
			marylandData.push({
				name: "Wicomico",
				data: wicomico
			});
			marylandData.push({
				name: "Worcester",
				data: worcester
			});
			
			this.setState({
				marylandData: marylandData
			});
			// console.log('Maryland Data: ', marylandData);
		}

		if (this.props.loadingCaseDataCountyNyCompare === false && (lastProps.loadingCaseDataCountyNyCompare === true || lastProps.loadingCaseDataCountyNyCompare === undefined)) {
			const { caseDataCountyNyCompare } = this.props;
			// console.log('caseDataCountyNyCompare: ', caseDataCountyNyCompare);
			const { selectedCounties, datesArray, colorsList, population } = this.state;
			var randomNumber = (Math.random() * 56).toFixed();
			const countyData = {name: caseDataCountyNyCompare[0].county, color: colorsList[randomNumber], state: "NY", data: []};
			for (var i = 0; i < datesArray.length; i++) {
				countyData.data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}
			for (var i = 0; i < countyData.data.length; i++) {
				caseDataCountyNyCompare.map(datum => {
					if (countyData.data[i].date == datum.test_date.split('T')[0]) {
						countyData.data[i].total_cases = datum.cumulative_number_of_positives;
						countyData.data[i].percent_cases = (datum.cumulative_number_of_positives / population * 100).toFixed(3);
						countyData.data[i].altPercent_cases = (datum.cumulative_number_of_positives / population * 1000000).toFixed(3);
						countyData.data[i].daily_cases = datum.new_positives;
						countyData.data[i].total_tests = datum.cumulative_number_of_tests;
						countyData.data[i].percent_tests = (datum.cumulative_number_of_tests / population * 100).toFixed(3);
						countyData.data[i].altPercent_tests = (datum.cumulative_number_of_tests / population * 1000000).toFixed(3);
						countyData.data[i].daily_tests = datum.total_number_of_tests;
						if (datum.total_number_of_tests > 0 && datum.new_positives > 0) {
							countyData.data[i].daily_positive_percent = (datum.new_positives / datum.total_number_of_tests * 100).toFixed(3);
						}
					}
				});
			}

			var sevenDayCount = 0;
			var sevenDayPositivity = 0;
			for (var i = 0; i < countyData.data.length; i++) {
				sevenDayCount += parseFloat(countyData.data[i].daily_cases);
				sevenDayPositivity += parseFloat(countyData.data[i].daily_positive_percent);
				if (i < 7) {
					countyData.data[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
					countyData.data[i].sevenDayAveragePositiveTests = (sevenDayPositivity / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(countyData.data[i - 7].daily_cases);
					sevenDayPositivity -= parseFloat(countyData.data[i - 7].daily_positive_percent);
					countyData.data[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
					countyData.data[i].sevenDayAveragePositiveTests = (sevenDayPositivity / 7).toFixed(2);
				}
			}
			if (countyData.data[countyData.data.length - 1].total_cases == 0) {
				countyData.data[countyData.data.length - 1].total_cases = countyData.data[countyData.data.length - 2].total_cases;
			}
			if (countyData.data[countyData.data.length - 1].percent_cases == 0) {
				countyData.data[countyData.data.length - 1].percent_cases = countyData.data[countyData.data.length - 2].percent_cases;
			}
			selectedCounties.push(countyData);
			this.setState({selectedCounties: selectedCounties, population: 0, triggerSetChartData: true});
			this.props.clearCaseDataNyCountyCompare();
		}

		if (this.state.selectedStates !== lastState.selectedStates) {
			this.setChartData();
		}

		if (this.props.loadingCaseDataCountyVTCompare === false && lastProps.loadingCaseDataCountyVTCompare === true) {
			const { caseDataCountyVTCompare } = this.props;
			// console.log('caseDataCountyVTCompare: ', caseDataCountyVTCompare);
			const { countiesVermontList, datesArray, colorsList, selectedCounties, population } = this.state;
			const data = [];
			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, total_deaths: 0, percent_deaths: 0, altPercent_deaths: 0, daily_deaths: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}

			for (var i = 0; i < data.length; i++) {
				caseDataCountyVTCompare.map(datum => {
					if (data[i].date == this.convertDate(new Date(datum.attributes.date))) {
						data[i].total_cases = datum.attributes.C_Total;
						data[i].daily_cases = datum.attributes.C_New;
						data[i].total_deaths = datum.attributes.D_Total;
						data[i].daily_deaths = datum.attributes.D_New;
						data[i].percent_cases = (parseInt(datum.attributes.C_Total) / population * 100).toFixed(3);
						data[i].altPercent_cases = (parseInt(datum.attributes.C_Total) / population * 1000000).toFixed(3);
						data[i].percent_deaths = (parseInt(datum.attributes.D_Total) / population * 100).toFixed(3);
						data[i].altPercent_deaths = (parseInt(datum.attributes.D_Total) / population * 100000).toFixed(3);
					};
				});
			}

			if (data[data.length - 1].total_cases == 0) {
				data[data.length - 1].total_cases = data[data.length -2].total_cases;
			}
			if (data[data.length - 1].percent_cases == 0) {
				data[data.length - 1].percent_cases = data[data.length -2].percent_cases;
			}
			if (data[data.length - 1].total_deaths == 0) {
				data[data.length - 1].total_deaths = data[data.length -2].total_deaths;
			}
			if (data[data.length - 1].percent_deaths == 0) {
				data[data.length - 1].percent_deaths = data[data.length -2].percent_deaths;
			}

			var seven_day_count = 0;
			var seven_day_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += data[i].daily_cases;
				seven_day_deaths += data[i].daily_deaths;
				if (i < 7) {
					data[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / (i + 1)).toFixed(3);
				}
				else {
					seven_day_count -= data[i - 7].daily_cases;
					seven_day_deaths -= data[i - 7].daily_deaths;
					data[i].sevenDayAverage = (seven_day_count / 7).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / 7).toFixed(3);
				}
			}

			selectedCounties.push({
				name: caseDataCountyVTCompare[0].attributes.map_county,
				state: "VT",
				color: colorsList[(Math.random() * 56).toFixed()],
				data: data
			});
			this.setState({
				selectedCounties: selectedCounties,
				triggerSetChartData: true
			});
			this.props.clearCaseDataVTCountyCompare();
		}

		if (this.props.loadingCaseDataCountyWICompare === false && lastProps.loadingCaseDataCountyWICompare === true) {
			const { caseDataCountyWICompare } = this.props;
			// console.log('caseDataCountyWICompare: ', caseDataCountyWICompare);
			const { datesArray, colorsList, population, selectedCounties } = this.state;
			const data = [];
			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, altPercent_cases: 0, daily_cases: 0, total_deaths: 0, percent_deaths: 0, altPercent_deaths: 0, daily_deaths: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0});
			}
			for (var i = 0; i < data.length; i++) {
				caseDataCountyWICompare.map(datum => {
					if (data[i].date == this.convertDate(new Date(datum.attributes.DATE))) {
						data[i].total_cases = (parseInt(datum.attributes.POSITIVE) > 0) ? parseInt(datum.attributes.POSITIVE) : 0;
						data[i].daily_cases = (parseInt(datum.attributes.POS_NEW) > 0) ? parseInt(datum.attributes.POS_NEW) : 0;
						data[i].percent_cases = ((parseInt(datum.attributes.POSITIVE) / population) * 100).toFixed(3);
						data[i].altPercent_cases = ((parseInt(datum.attributes.POSITIVE) / population) * 1000000).toFixed(3);
						data[i].total_deaths = parseInt(datum.attributes.DEATHS);
						data[i].daily_deaths = parseInt(datum.attributes.DTH_NEW);
						data[i].percent_deaths = ((parseInt(datum.attributes.DEATHS) / population) * 100).toFixed(3);
						data[i].altPercent_deaths = ((parseInt(datum.attributes.DEATHS) / population) * 100000).toFixed(3);
					}
				});
			}
			
			var seven_day_count = 0;
			var seven_day_deaths = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += data[i].daily_cases;
				seven_day_deaths += data[i].daily_deaths;
				if (i < 7) {
					data[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / (i + 1)).toFixed(3);
				}
				else {
					seven_day_count -= data[i - 7].daily_cases;
					seven_day_deaths -= data[i - 7].daily_deaths;
					data[i].sevenDayAverage = (seven_day_count / 7).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / 7).toFixed(3);
				}
			}

			selectedCounties.push({
				name: caseDataCountyWICompare[0].attributes.NAME,
				state: "WI",
				color: colorsList[(Math.random() * 56).toFixed()],
				data: data
			});
			this.setState({
				selectedCounties: selectedCounties,
				triggerSetChartData: true
			});
			this.props.clearCaseDataWICountyCompare();
		}

		if (this.props.loadingSingleStateCompare == false && lastProps.loadingSingleStateCompare == true) {
			const { dataSingleStateCompare } = this.props;
			// console.log('State Data: ', dataSingleStateCompare);
			const { selectedStates, colorsList, datesArray, stateName, stateAbbreviation, statePopulation } = this.state;
			const data = [];

			for (var i = 0; i < datesArray.length; i++) {
				data.push({date: datesArray[i], daily_cases: 0, daily_deaths: 0, total_cases: 0, total_deaths: 0, percent_cases: 0, altPercent_cases: 0, percent_deaths: 0, altPercent_deaths: 0, sevenDayAverage: 0, sevenDayAverageDeaths: 0, total_tests: 0, percent_tests: 0, altPercent_tests: 0, daily_tests: 0, daily_positive_percent: 0, sevenDayAveragePositiveTests: 0, hospitalizations: 0, sevenDayAverageHospitalizations: 0, icuBeds: 0, sevenDayAverageICUBeds: 0})
			}

			for (var i = 0; i < data.length; i++) {
				dataSingleStateCompare.map(datum => {
					if (datum.date == this.formatDateState(data[i].date)) {
						data[i].daily_cases += datum.positiveIncrease;
						data[i].total_cases = datum.positive;
						data[i].percent_cases = (datum.positive / statePopulation * 100).toFixed(3);
						data[i].altPercent_cases = (datum.positive / statePopulation * 1000000).toFixed(3);
						data[i].total_deaths = datum.death;
						data[i].percent_deaths = (datum.death / statePopulation * 100).toFixed(3);
						data[i].altPercent_deaths = (datum.death / statePopulation * 100000).toFixed(3);
						data[i].daily_deaths = datum.deathIncrease;
						data[i].total_tests = datum.posNeg;
						data[i].percent_tests = (datum.posNeg / statePopulation * 100).toFixed(3);
						data[i].altPercent_tests = (datum.posNeg / statePopulation * 1000000).toFixed(3);
						data[i].daily_tests = datum.totalTestResultsIncrease;
						if (parseInt(datum.positiveIncrease) > 0 && parseInt(datum.totalTestResultsIncrease) > 0) {
							data[i].daily_positive_percent = (parseInt(datum.positiveIncrease) / parseInt(datum.totalTestResultsIncrease) * 100).toFixed(3);
						} else {
							data[i].daily_positive_percent = 0;
						}
						data[i].hospitalizations = datum.hospitalizedCurrently;
						data[i].icuBeds = datum.inIcuCurrently;
					}
				});
			}

			var seven_day_count = 0;
			var seven_day_deaths = 0;
			var seven_day_positivity = 0;
			var seven_day_hospitalizations = 0;
			var seven_day_icu = 0;
			for (var i = 0; i < data.length; i++) {
				seven_day_count += data[i].daily_cases;
				seven_day_deaths += data[i].daily_deaths;
				seven_day_positivity += parseFloat(data[i].daily_positive_percent);
				seven_day_hospitalizations += data[i].hospitalizations;
				seven_day_icu += data[i].icuBeds;
				if (i < 7) {
					data[i].sevenDayAverage = (seven_day_count / (i + 1)).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / (i + 1)).toFixed(3);
					data[i].sevenDayAveragePositiveTests = (parseInt(seven_day_positivity) / (i + 1)).toFixed(3);
					data[i].sevenDayAverageHospitalizations = (seven_day_hospitalizations / (i + 1)).toFixed(3);
					data[i].sevenDayAverageICUBeds = (seven_day_icu / (i + 1)).toFixed(3);
				}
				if (i >= 7) {
					seven_day_count -= data[i - 7].daily_cases;
					seven_day_deaths -= data[i - 7].daily_deaths;
					seven_day_positivity -= parseFloat(data[i - 7].daily_positive_percent);
					seven_day_hospitalizations -= data[i - 7].hospitalizations;
					seven_day_icu -= data[i - 7].icuBeds;
					data[i].sevenDayAverage = (seven_day_count / 7).toFixed(3);
					data[i].sevenDayAverageDeaths = (seven_day_deaths / 7).toFixed(3);
					data[i].sevenDayAveragePositiveTests = (parseInt(seven_day_positivity) / 7).toFixed(3);
					data[i].sevenDayAverageHospitalizations = (seven_day_hospitalizations / 7).toFixed(3);
					data[i].sevenDayAverageICUBeds = (seven_day_icu / 7).toFixed(3);
				}
			}

			selectedStates.push({name: stateName, abbreviation: stateAbbreviation, population: statePopulation, color: colorsList[(Math.random() * 56).toFixed()], data: data});

			this.setState({
				stateName: '',
				stateAbbreviation: '',
				statePopulation: 0,
				selectedStates: selectedStates,
				triggerSetChartData: true
			});
			this.props.resetSingleStateDailyCompare();
		}

		if (this.props.loadingDataWorldWideEuropaCompare == false && lastProps.loadingDataWorldWideEuropaCompare == true) {
			this.setDisplayCountries();
			this.setState({
				loadingCountriesData: false,
				processingCountriesData: true
			});
		}
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
		this.props.clearComparisonDataCompare();
		this.props.clearWorldWideEuropaDataCompare();
		this.props.clearImport();
	}

	render() {
		function dropCounty(s) {
			return s.split(' County')[0];
		}

		function replaceUnderscore(s) {
			return s.replace(/_/g, ' ');
		}

		const { displayCityControls, displayCountyControls, displayStateControls, displayCountryControls } = this.state;
		const { displaySanFrancisco, displayNewYorkCity, displayHongKong, displayChicago } = this.state;
		const { displayCalifornia, displayColorado, displayConnecticut, displayGeorgia, displayIndiana, displayMaryland, displayNewYork, displayVermont, displayWisconsin } = this.state;
		const { citiesList, stateCountiesList, countiesCaliforniaList, countiesColoradoList, countiesConnecticutList, countiesGeorgiaList, countiesIndianaList, countiesMarylandList, countiesNewYorkList, countiesVermontList, countiesWisconsinList, statesList,  countriesList, displayCountriesList } = this.state;
		const { loadingData, showChart, showTotalCasesChart, showPercentCasesChart, showDailyCasesChart, showTotalDeathsChart, showPercentDeathsChart, showDailyDeathsChart } = this.state;
		const { chartHeight, showAxisX, showSkinnyButtons, startDate, endDate, showLineGraph, showBarGraph, displayLineBarButtons, displayDailyAverageButtons, showSevenDayAverage, showDailyCount } = this.state;
		const { showCases, showDeaths, showTests, showHospitalizations, showICU } = this.state;
		const { loadingCountriesData, processingCountriesData, displayEU } = this.state;
		const { showTotal, showPercent, showDaily, showControls } = this.state;
		const { displayDateChangeControls, newStartDate, newEndDate } = this.state;
		const { displayPercentAltPercentButtons, showAltPercentGraph, showPercentGraph } = this.state;

		return (
			<div style={{flex: 1}}>
				{this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} editChartDate={this.handleEditChartDate} startDate={newStartDate} endDate={newEndDate} changeChartSizeSettings={this.changeChartSizeSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} showSevenDayAverage={showSevenDayAverage} showDailyCount={showDailyCount} displaySevenDayAverage={this.handleShowSevenDayAverage} displayDailyCount={this.handleShowDailyCount} displayDateChangeControls={displayDateChangeControls} displayPercentAltPercentButtons={displayPercentAltPercentButtons} showAltPercentGraph={showAltPercentGraph} showPercentGraph={showPercentGraph} showPercent={this.handleShowPercent} showAltPercent={this.handleShowAltPercent} />
        }
        {this.state.showSettingsModal && <ModalBackdrop />}
        <div style={{paddingTop: 20}} />
        {showChart ? 
        	<div>
        		{showControls && 
        			<div>
        				{showSkinnyButtons ? 
									<Grid container>
										<Grid item xs={4} style={styles.chartTypeButtonContainer}>
											<Button style={showTotal ? styles.chartTotalPercentDailyButtonSelectedSkinny : styles.chartTotalPercentDailyButtonSkinny} onClick={this.handleDisplayTotal}>Total</Button>
										</Grid>
										{showAltPercentGraph && 
											<Grid item xs={4} style={styles.chartTypeButtonContainer}>
												{(showCases || showTests) && 
													<Button style={showPercent ? styles.chartTotalPercentDailyButtonSelectedSkinny : styles.chartTotalPercentDailyButtonSkinny} onClick={this.handleDisplayPercent}>X/Million</Button>
												}
												{showDeaths && 
													<Button style={showPercent ? styles.chartTotalPercentDailyButtonSelectedSkinny : styles.chartTotalPercentDailyButtonSkinny} onClick={this.handleDisplayPercent}>X/100K</Button>
												}
											</Grid>
										}
										{showPercentGraph && 
											<Grid item xs={4} style={styles.chartTypeButtonContainer}>
												<Button style={showPercent ? styles.chartTotalPercentDailyButtonSelectedSkinny : styles.chartTotalPercentDailyButtonSkinny} onClick={this.handleDisplayPercent}>Percent</Button>
											</Grid>
										}
										<Grid item xs={4} style={styles.chartTypeButtonContainer}>
											<Button style={showDaily ? styles.chartTotalPercentDailyButtonSelectedSkinny : styles.chartTotalPercentDailyButtonSkinny} onClick={this.handleDisplayDaily}>Daily</Button>
										</Grid>
									</Grid> : 
									<Grid container>
										<Grid item xs={4} style={styles.chartTypeButtonContainer}>
											<Button style={showTotal ? styles.chartTotalPercentDailyButtonSelected : styles.chartTotalPercentDailyButton} onClick={this.handleDisplayTotal}>Total</Button>
										</Grid>
										{showAltPercentGraph && 
											<Grid item xs={4} style={styles.chartTypeButtonContainer}>
												{(showCases || showTests) && 
													<Button style={showPercent ? styles.chartTotalPercentDailyButtonSelected : styles.chartTotalPercentDailyButton} onClick={this.handleDisplayPercent}>X/Million</Button>
												}
												{showDeaths  && 
													<Button style={showPercent ? styles.chartTotalPercentDailyButtonSelected : styles.chartTotalPercentDailyButton} onClick={this.handleDisplayPercent}>X/100K</Button>
												}
											</Grid>
										}
										{showPercentGraph && 
											<Grid item xs={4} style={styles.chartTypeButtonContainer}>
												<Button style={showPercent ? styles.chartTotalPercentDailyButtonSelected : styles.chartTotalPercentDailyButton} onClick={this.handleDisplayPercent}>Percent</Button>
											</Grid>
										}
										<Grid item xs={4} style={styles.chartTypeButtonContainer}>
											<Button style={showDaily ? styles.chartTotalPercentDailyButtonSelected : styles.chartTotalPercentDailyButton} onClick={this.handleDisplayDaily}>Daily</Button>
										</Grid>
									</Grid>
        				}
							</div>
						}
        		{showCases && 
        			<div>
		        		{showTotal && 
		        			<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        				{showLineGraph && 
		        					<Line 
					          		data={this.state.chartTotalCasesLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Cases',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         // position: 'right',
			                         gridLines: {
			                         	display: true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
		        				}
		        				{showBarGraph && 
		        					<Bar
												data={this.state.chartTotalCasesBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: 'Total Cases',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
		        				}
		        			</div>
		        		}
		        		{showPercent && 
		        			<div>
		        				{showAltPercentGraph && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
				        					<Line 
							          		data={this.state.chartAltPercentCasesLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Cases Per Million / Population',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         // position: 'right',
					                         gridLines: {
					                         	display: true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartAltPercentCasesBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Cases Per Million / Population',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
				        				}
		        					</div>
		        				}
		        				{showPercentGraph && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
				        					<Line 
							          		data={this.state.chartPercentCasesLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Percent Cases of Population',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartPercentCasesBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Percent Cases of Population',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
				        				}
		        					</div>
		        				}
		        			</div>
		        		}
		        		{showDaily && 
		        			<div>
		        				{showDailyCount && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
		        							<Line 
							          		data={this.state.chartDailyCasesLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Daily Confirmed Cases',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
		        						}
		        						{showBarGraph && 
		        							<Bar
														data={this.state.chartDailyCasesBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Daily Confirmed Cases',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
		        						}
		        					</div>
		        				}
		        				{showSevenDayAverage && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
		        							<Line 
							          		data={this.state.chartSevenDayAverageCasesLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Seven Day Average Confirmed Cases',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
		        						}
		        						{showBarGraph && 
		        							<Bar
														data={this.state.chartSevenDayAverageCasesBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Seven Day Average Confirmed Cases',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
		        						}
		        					</div>
		        				}
		        			</div>
		        		}
		        	</div>
		        }
		        {showDeaths && 
		        	<div>
		        		{showTotal && 
		        			<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        				{showLineGraph && 
		        					<Line 
					          		data={this.state.chartTotalDeathsLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Deaths',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
		        				}
		        				{showBarGraph && 
		        					<Bar
												data={this.state.chartTotalDeathsBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: 'Total Deaths',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
		        				}
		        			</div>
		        		}
		        		{showPercent && 
		        			<div>
		        				{showAltPercentGraph && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
				        					<Line 
							          		data={this.state.chartAltPercentDeathsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Deaths Per 100,000 / Population',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         // position: 'right',
					                         gridLines: {
					                         	display: true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartAltPercentDeathsBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Deaths Per 100,000 / Population',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
				        				}
		        					</div>
		        				}
		        				{showPercentGraph && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
				        					<Line 
							          		data={this.state.chartPercentDeathsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Percent Deaths of Population',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartPercentDeathsBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Percent Deaths of Population',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
				        				}
		        					</div>
		        				}
		        			</div>
		        		}
		        		{showDaily && 
		        			<div>
		        				{showDailyCount && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
				        				{showLineGraph && 
				        					<Line 
							          		data={this.state.chartDailyDeathsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Daily Confirmed Deaths',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartDailyDeathsBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Daily Confirmed Deaths',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
				        				}
				        			</div>
				        		}
				        		{showSevenDayAverage && 
				        			<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
				        				{showLineGraph && 
				        					<Line 
							          		data={this.state.chartSevenDayAverageDeathsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: '7 Day Avg Confirmed Deaths',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartSevenDayAverageDeathsBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: '7 Day Average Confirmed Deaths',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
				        				}
				        			</div>
				        		}
		        			</div>
		        		}
		        	</div>
		        }
		        {showTests && 
		        	<div>
		        		{showTotal && 
		        			<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        				{showLineGraph && 
		        					<Line 
					          		data={this.state.chartTotalTestsLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Total Tests',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
		        				}
		        				{showBarGraph && 
		        					<Bar
												data={this.state.chartTotalTestsBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: 'Total Tests',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
		        				}
		        			</div>
		        		}
		        		{showPercent && 
		        			<div>
		        				{showAltPercentGraph && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
		        							<Line 
							          		data={this.state.chartAltPercentTestsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Tests Per Million / Population',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartAltPercentTestsBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Tests Per Million / Population',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
								        }
		        					</div>
		        				}
		        				{showPercentGraph && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
		        							<Line 
							          		data={this.state.chartPercentTestsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Percent of Population Tested',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartAltPercentTestsBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Tests Per Million / Population',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
								        }
								      </div>
		        				}
		        			</div>
		        		}
		        		{showDaily && 
		        			<div>
		        				{showDailyCount && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
		        							<Line 
							          		data={this.state.chartDailyPositiveTestsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: 'Daily Positivity Percent',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartDailyPositiveTestsBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: 'Daily Positivity Percent',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
				        				}
		        					</div>
		        				}
		        				{showSevenDayAverage && 
		        					<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        						{showLineGraph && 
		        							<Line 
							          		data={this.state.chartSevenDayAveragePositiveTestsLine}
							          		options={{
						                  title: {
						                    display: this.props.displayTitle,
						                    text: '7 Day Average Positivity Percent',
						                    fontSize: 25
						                  },
						                  legend: {
						                    display: this.props.displayLegend,
						                    position: this.props.legendPosition
						                  },
						                  tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
						                  scales: {
						                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
						                  },
						                  maintainAspectRatio: false
							              }}
							          	/>
				        				}
				        				{showBarGraph && 
				        					<Bar
														data={this.state.chartSevenDayAveragePositiveTestsBar}
														options={{
							                title: {
							                  display: this.props.displayTitle,
							                  text: '7 Day Average Positivity Percent',
							                  fontSize: 25
							                },
							                legend: {
							                  display: this.props.displayLegend,
							                  position: this.props.legendPosition
							                },
							                tooltips: {
											          callbacks: {
									                label: function(tooltipItem, data) {
								                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
								                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				                            } else {
				                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                            }
									                }
											          } // end callbacks:
											        }, //end tooltips 
							                scales: {
														    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
														    yAxes: [
														    	{ ticks: {
														    		mirror: true, 
														    		beginAtZero: true,
					                          callback: function(value, index, values) {
					                            if(parseInt(value) >= 1000){
					                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
					                            } else {
					                              return value;
					                            }
					                          }
					                         }, 
					                         stacked: true, 
					                         gridLines: {
					                         	display:true, 
					                         	drawBorder: false
					                         } 
					                       }
					                     ]
														  },
														  maintainAspectRatio: false
								            }}
								        	/>
				        				}
		        					</div>
		        				}
		        			</div>
		        		}
		        	</div>
		        }
		        {showHospitalizations && 
		        	<div>
		        		{showDailyCount && 
		        			<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        				{showLineGraph && 
		        					<Line 
					          		data={this.state.chartHospitalizationsLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Daily Hospitalizations',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
		        				}
		        				{showBarGraph && 
		        					<Bar
												data={this.state.chartHospitalizationsBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: 'Daily Hospitalizations',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
		        				}
		        			</div>
		        		}
		        		{showSevenDayAverage && 
		        			<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        				{showLineGraph && 
		        					<Line 
					          		data={this.state.chartSevenDayAverageHospitalizationsLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: '7 Day Average Hospitalizations',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
		        				}
		        				{showBarGraph && 
		        					<Bar
												data={this.state.chartSevenDayAverageHospitalizationsBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: '7 Day Average Hospitalizations',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
		        				}
		        			</div>
		        		}
		        	</div>
		        }
		        {showICU && 
		        	<div>
		        		{showDailyCount && 
		        			<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        				{showLineGraph && 
		        					<Line 
					          		data={this.state.chartICULine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Daily ICU Beds',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
		        				}
		        				{showBarGraph && 
		        					<Bar
												data={this.state.chartICUBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: 'Daily ICU Beds',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
		        				}
		        			</div>
		        		}
		        		{showSevenDayAverage && 
		        			<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
		        				{showLineGraph && 
		        					<Line 
					          		data={this.state.chartSevenDayAverageICULine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: '7 Day Average ICU Beds',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
		        				}
		        				{showBarGraph && 
		        					<Bar
												data={this.state.chartSevenDayAverageICUBar}
												options={{
					                title: {
					                  display: this.props.displayTitle,
					                  text: '7 Day Average ICU Beds',
					                  fontSize: 25
					                },
					                legend: {
					                  display: this.props.displayLegend,
					                  position: this.props.legendPosition
					                },
					                tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                            } else {
		                              return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
		                            }
							                }
									          } // end callbacks:
									        }, //end tooltips 
					                scales: {
												    xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:true, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
												  },
												  maintainAspectRatio: false
						            }}
						        	/>
		        				}
		        			</div>
		        		}
		        	</div>
		        }
		        {showSkinnyButtons ? 
							<Grid container style={{paddingTop: 10}}>
	        			<Grid item xs={4} style={styles.chartTypeButtonContainer}>
	        				<Button style={showCases ? styles.chartCasesDeathsTestsButtonSelectedSkinny : styles.chartCasesDeathsTestsButtonSkinny} onClick={this.handleDisplayCases}>Cases</Button>
	        			</Grid>
	        			<Grid item xs={4} style={styles.chartTypeButtonContainer}>
	        				<Button style={showDeaths ? styles.chartCasesDeathsTestsButtonSelectedSkinny : styles.chartCasesDeathsTestsButtonSkinny} onClick={this.handleDisplayDeaths}>Deaths</Button>
	        			</Grid>
	        			<Grid item xs={4} style={styles.chartTypeButtonContainer}>
	        				<Button style={showTests ? styles.chartCasesDeathsTestsButtonSelectedSkinny : styles.chartCasesDeathsTestsButtonSkinny} onClick={this.handleDisplayTests}>Tests</Button>
	        			</Grid>
	        			<Grid item xs={6} style={styles.chartTypeButtonContainer}>
	        				<Button style={showHospitalizations ? styles.chartCasesDeathsTestsButtonSelectedSkinny : styles.chartCasesDeathsTestsButtonSkinny} onClick={this.handleDisplayHospitalizations}>Hospital Beds</Button>
	        			</Grid>
	        			<Grid item xs={6} style={styles.chartTypeButtonContainer}>
	        				<Button style={showICU ? styles.chartCasesDeathsTestsButtonSelectedSkinny : styles.chartCasesDeathsTestsButtonSkinny} onClick={this.handleDisplayICU}>ICU Beds</Button>
	        			</Grid>
	        		</Grid> : 
							<Grid container style={{paddingTop: 10}}>
	        			<Grid item xs={4} style={styles.chartTypeButtonContainer}>
	        				<Button style={showCases ? styles.chartCasesDeathsTestsButtonSelected : styles.chartCasesDeathsTestsButton} onClick={this.handleDisplayCases}>Cases</Button>
	        			</Grid>
	        			<Grid item xs={4} style={styles.chartTypeButtonContainer}>
	        				<Button style={showDeaths ? styles.chartCasesDeathsTestsButtonSelected : styles.chartCasesDeathsTestsButton} onClick={this.handleDisplayDeaths}>Deaths</Button>
	        			</Grid>
	        			<Grid item xs={4} style={styles.chartTypeButtonContainer}>
	        				<Button style={showTests ? styles.chartCasesDeathsTestsButtonSelected : styles.chartCasesDeathsTestsButton} onClick={this.handleDisplayTests}>Tests</Button>
	        			</Grid>
	        			<Grid item xs={6} style={styles.chartTypeButtonContainer}>
	        				<Button style={showHospitalizations ? styles.chartCasesDeathsTestsButtonSelected : styles.chartCasesDeathsTestsButton} onClick={this.handleDisplayHospitalizations}>Hospital Beds</Button>
	        			</Grid>
	        			<Grid item xs={6} style={styles.chartTypeButtonContainer}>
	        				<Button style={showICU ? styles.chartCasesDeathsTestsButtonSelected : styles.chartCasesDeathsTestsButton} onClick={this.handleDisplayICU}>ICU Beds</Button>
	        			</Grid>
	        		</Grid>
		        }
			        
        	</div> :
        	<div style={{textAlign: 'center', paddingTop: 10, paddingBottom: 10}}>
        		<span style={{fontSize: 20, fontWeight: 'bold', alignSelf: 'center'}}>Select a City, County, State or Country</span> <Dots />
        	</div>
        }
        {loadingData ? 
        	<div>
        		<span>Loading Data</span> <Dots />
        	</div> :
	        <div>
	        	{showSkinnyButtons ? 
							<Grid container style={{textAlign: 'center', paddingTop: 10, paddingBottom: 10}}>
								<Grid item sm={3} xs={6} style={{paddingTop: 10}}>
									<Button style={displayCityControls ? styles.controlTypeButtonSelectedSkinny : styles.controlTypeButtonSkinny} onClick={this.handleDisplayCityControls}>Cities</Button>
								</Grid>
								<Grid item sm={3} xs={6} style={{paddingTop: 10}}>
									<Button style={displayCountyControls ? styles.controlTypeButtonSelectedSkinny : styles.controlTypeButtonSkinny} onClick={this.handleDisplayCountyControls}>Counties</Button>
								</Grid>
								<Grid item sm={3} xs={6} style={{paddingTop: 10}}>
									<Button style={displayStateControls ? styles.controlTypeButtonSelectedSkinny : styles.controlTypeButtonSkinny} onClick={this.handleDisplayStateControls}>States</Button>
								</Grid>
								<Grid item sm={3} xs={6} style={{paddingTop: 10}}>
									<Button style={displayCountryControls ? styles.controlTypeButtonSelectedSkinny : styles.controlTypeButtonSkinny} onClick={this.handleDisplayCountryControls}>Countries</Button>
								</Grid>
							</Grid> : 
							<Grid container style={{textAlign: 'center', paddingTop: 10, paddingBottom: 10}}>
								<Grid item sm={3} xs={6} style={{paddingTop: 10}}>
									<Button style={displayCityControls ? styles.controlTypeButtonSelected : styles.controlTypeButton} onClick={this.handleDisplayCityControls}>Cities</Button>
								</Grid>
								<Grid item sm={3} xs={6} style={{paddingTop: 10}}>
									<Button style={displayCountyControls ? styles.controlTypeButtonSelected : styles.controlTypeButton} onClick={this.handleDisplayCountyControls}>Counties</Button>
								</Grid>
								<Grid item sm={3} xs={6} style={{paddingTop: 10}}>
									<Button style={displayStateControls ? styles.controlTypeButtonSelected : styles.controlTypeButton} onClick={this.handleDisplayStateControls}>States</Button>
								</Grid>
								<Grid item sm={3} xs={6} style={{paddingTop: 10}}>
									<Button style={displayCountryControls ? styles.controlTypeButtonSelected : styles.controlTypeButton} onClick={this.handleDisplayCountryControls}>Countries</Button>
								</Grid>
							</Grid>
	        	}				
						{displayCityControls && 
							<div>
								<div style={styles.locationSelectButtonsContainer}>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displaySanFrancisco ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleSelectSanFrancisco}>San Francisco</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayNewYorkCity ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleSelectNewYorkCity}>New York City</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayHongKong ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleSelectHongKong}>Hong Kong</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayChicago ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleSelectChicago}>Chicago</Button>
									</div>
								</div>
								<div style={styles.sourceURLContainer}>
									<a style={styles.sourceURL} target="_blank" href="https://data.sfgov.org/COVID-19/COVID-19-Cases-Summarized-by-Date-Transmission-and/tvq9-ec9w"><span style={styles.sourceURLText}>Source: Data.SFGov.org</span></a>
								</div>
								<div style={styles.sourceURLContainer}>
									<a style={styles.sourceURL} target="_blank" href="https://health.data.ny.gov/Health/New-York-State-Statewide-COVID-19-Testing/xdss-u53e"><span style={styles.sourceURLText}>Source: Health.Data.NY.gov</span></a>
								</div>
								<div style={styles.sourceURLContainer}>
									<a style={styles.sourceURL} target="_blank" href="https://api.data.gov.hk/v2/filter?q=%7B%22resource%22%3A%22http%3A%2F%2Fwww.chp.gov.hk%2Ffiles%2Fmisc%2Fenhanced_sur_covid_19_eng.csv%22%2C%22section%22%3A1%2C%22format%22%3A%22json%22%7D"><span style={styles.sourceURLText}>Source: Api.data.gov.hk</span></a>
								</div>
								<div style={styles.sourceURLContainer}>
									<a style={styles.sourceURL} target="_blank" href="https://data.cityofchicago.org/resource/naz8-j4nc.json"><span style={styles.sourceURLText}>Source: data.cityofchicago.org</span></a>
								</div>
							</div>
						}
						{displayCountyControls && 
							<div>
								<div style={styles.locationSelectButtonsContainer}>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayCalifornia ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayCalifornia}>California</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayColorado ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayColorado}>Colorado</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayConnecticut ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayConnecticut}>Connecticut</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayGeorgia ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayGeorgia}>Georgia</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayIndiana ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayIndiana}>Indiana</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayMaryland ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayMaryland}>Maryland</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayNewYork ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayNewYork}>New York</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayVermont ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayVermont}>Vermont</Button>
									</div>
									<div style={styles.locationSelectButtonContainer}>
										<Button style={displayWisconsin ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayWisconsin}>Wisconsin</Button>
									</div>
								</div>
								{displayCalifornia && 
									<div>
										<div style={styles.locationSelectButtonsContainer}>
											{countiesCaliforniaList.map((county, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyCA.bind(this, county.name, county.population)}>{county.name}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://data.ca.gov/dataset/covid-19-cases"><span style={styles.sourceURLText}>Source: data.ca.gov</span></a>
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://data.ca.gov/dataset/covid-19-hospital-data"><span style={styles.sourceURLText}>Source: data.ca.gov</span></a>
										</div>
									</div>
								}
								{displayColorado && 
									<div>
										<div style={styles.locationSelectButtonsContainer}>
											{countiesColoradoList.map((county, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyCO.bind(this, county.name, county.population)}>{county.name}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://services3.arcgis.com/66aUo8zsujfVXRIT/arcgis/rest/services/colorado_covid19_county_statistics_cumulative/FeatureServer"><span style={styles.sourceURLText}>Source: Colorado ARCGis</span></a>
										</div>
									</div>
								}
								{displayConnecticut && 
									<div>
										<div style={styles.locationSelectButtonsContainer}>
											{countiesConnecticutList.map((county, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyCT.bind(this, county.name, county.population)}>{county.name}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://data.ct.gov/Health-and-Human-Services/COVID-19-Cases-Hospitalizations-and-Deaths-By-Coun/bfnu-rgqt"><span style={styles.sourceURLText}>Source: data.ct.gov</span></a>
										</div>
									</div>
								}
								{displayGeorgia && 
									<div>
										<div style={styles.locationSelectButtonsContainer}>
											{countiesGeorgiaList.map((county, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyGA.bind(this, county.name, county.population)}>{county.name}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://services1.arcgis.com/2iUE8l8JKrP2tygQ/arcgis/rest/services/COVID19_County_Archive/FeatureServer"><span style={styles.sourceURLText}>Source: Georgia ArcGIS</span></a>
										</div>
									</div>
								}
								{displayIndiana && 
									<div>
										<div style={styles.locationSelectButtonsContainer}>
											{countiesIndianaList.map((county, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyIN.bind(this, county.name, county.population)}>{county.name}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://hub.mph.in.gov/dataset/covid-19-county-statistics"><span style={styles.sourceURLText}>Source: hub.mph.in.gov</span></a>
										</div>
									</div>
								}
								{displayMaryland && 
									<div>
										<div style={styles.locationSelectButtonsContainer}>
											{countiesMarylandList.map((county, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyMD.bind(this, county.name, county.population)}>{replaceUnderscore(county.name)}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://services.arcgis.com/njFNhDsUCentVYJW/arcgis/rest/services/MDCOVID19_CasesByCounty/FeatureServer/"><span style={styles.sourceURLText}>Source: Maryland ArcGIS</span></a>
										</div>
									</div>
								}
								{displayNewYork && 
									<div>
										<div style={styles.locationSelectButtonsContainer}>
											{countiesNewYorkList.map((county, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyNY.bind(this, county.name, county.population)}>{county.name}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://health.data.ny.gov/Health/New-York-State-Statewide-COVID-19-Testing/xdss-u53e"><span style={styles.sourceURLText}>Source: Health.Data.NY.gov</span></a>
										</div>
									</div>
								}
								{displayVermont && 
									<div>
										<div style={styles.locationSelectButtonsContainer}>
											{countiesVermontList.map((county, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyVT.bind(this, county.name, county.population)}>{dropCounty(county.name)}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://services1.arcgis.com/BkFxaEFNwHqX3tAw/arcgis/rest/services/VIEW_EPI_CountyDailyCountTS_PUBLIC/FeatureServer/"><span style={styles.sourceURLText}>Source: Vermont ArcGIS</span></a>
										</div>
									</div>
								}
								{displayWisconsin && 
									<div>
										<div style={styles.locationSelectButtonsContainer}>
											{countiesWisconsinList.map((county, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyWI.bind(this, county.name, county.population)}>{county.name}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://services1.arcgis.com/ISZ89Z51ft1G16OK/ArcGIS/rest/services/COVID19_WI/FeatureServer/"><span style={styles.sourceURLText}>Source: Wisconsin ArcGIS</span></a>
										</div>
									</div>
								}
							</div>
						}
						{displayStateControls && 
							<div>
								<div style={styles.locationSelectButtonsContainer}>
									{statesList.map((state, index) => (
										<div style={styles.locationSelectButtonContainer} key={index}>
											<Button style={state.active ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleSelectState.bind(this, state.name, state.abbreviation, state.population)}>{state.abbreviation}</Button>
										</div>
									))}
								</div>
								<div style={styles.sourceURLContainer}>
									<a style={styles.sourceURL} target="_blank" href="https://covidtracking.com/"><span style={styles.sourceURLText}>Source: CovidTracking.com</span></a>
								</div>
							</div>
						}
						{displayCountryControls && 
							<div>
								{(displayCountriesList == false) ? 
									<div style={{textAlign: 'center', padding: 20}}>
										{loadingCountriesData && 
											<div>
					        			<div><span style={{fontSize: 20, fontWeight: 'bold', alignSelf: 'center'}}>Loading Data</span> <Dots /></div>
					        			<div style={{paddingTop: 20}}><span style={{fontSize: 20, fontWeight: 'bold', alignSelf: 'center'}}>(Large Dataset, may take a while)</span> <Dots /></div>
					        		</div>
										}
										{processingCountriesData && 
											<div>
												<span style={{fontSize: 20, fontWeight: 'bold', alignSelf: 'center'}}>Processing Data</span> <Dots />
											</div>
										}
				        	</div> : 
				        	<div>
										<div style={styles.locationSelectButtonsContainer}>
											<div style={styles.locationSelectButtonContainer}>
												<Button style={displayEU ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleDisplayEuropeanUnion}>European Union</Button>
											</div>
											{countriesList.map((country, index) => (
												<div style={styles.locationSelectButtonContainer} key={index}>
													<Button style={country.active ? styles.locationSelectButtonSelected : styles.locationSelectButton} onClick={this.handleSelectCountry.bind(this, country.id)}>{country.name}</Button>
												</div>
											))}
										</div>
										<div style={styles.sourceURLContainer}>
											<a style={styles.sourceURL} target="_blank" href="https://opendata.ecdc.europa.eu/covid19/casedistribution/json/"><span style={styles.sourceURLText}>Source: OpenData.ECDC.Europa.EU</span></a>
										</div>
									</div>
								}
							</div>
						}
					</div>
				}
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		importColorsList: state.importData.importColorsList,

		importCountiesCAList: state.importData.importCountiesCAList,
		importCountiesCOList: state.importData.importCountiesCOList, 
		importCountiesCTList: state.importData.importCountiesCTList,
		importCountiesGAList: state.importData.importCountiesGAList,
		importCountiesINList: state.importData.importCountiesINList, 
		importCountiesMDList: state.importData.importCountiesMDList,  
		importCountiesNYList: state.importData.importCountiesNYList,
		importCountiesVTList: state.importData.importCountiesVTList, 
		importCountiesWIList: state.importData.importCountiesWIList, 

		importStatesList: state.importData.importStatesList,

		dataSFCompare: state.cities.dataSFCompare,
		loadingDataSFCompare: state.cities.loadingDataSFCompare, 

		cityDataSFTesting: state.cities.cityDataSFTesting,
		loadingCityDataSFTesting: state.cities.loadingCityDataSFTesting,

		cityDataSFHospitalization: state.cities.cityDataSFHospitalization,
		loadingCityDataSFHospitalization: state.cities.loadingCityDataSFHospitalization,

		dataNYCCasesCompare: state.cities.dataNYCCasesCompare,
		loadingDataNYCCasesCompare: state.cities.loadingDataNYCCasesCompare,

		dataHKCompare: state.cities.dataHKCompare,
		loadingDataHKCompare: state.cities.loadingDataHKCompare,

		dataCHICaseCompare: state.cities.dataCHICaseCompare,
		loadingDataCHICaseCompare: state.cities.loadingDataCHICaseCompare,

		caseDataCountyCaCompare: state.counties.caseDataCountyCaCompare,
		loadingCaseDataCountyCaCompare: state.counties.loadingCaseDataCountyCaCompare,

		hospitalDataCountyCACompare: state.counties.hospitalDataCountyCACompare,
		loadingHospitalDataCountyCACompare: state.counties.loadingHospitalDataCountyCACompare,

		caseDataCountyCOCompare: state.counties.caseDataCountyCOCompare,
		loadingCaseDataCountyCOCompare: state.counties.loadingCaseDataCountyCOCompare,

		caseDataCountyCTCompare: state.counties.caseDataCountyCTCompare,
		loadingCaseDataCountyCTCompare: state.counties.loadingCaseDataCountyCTCompare,

		caseDataCountyGACompare: state.counties.caseDataCountyGACompare,
		loadingCaseDataCountyGACompare: state.counties.loadingCaseDataCountyGACompare,

		caseDataCountyINCompare: state.counties.caseDataCountyINCompare,
		loadingCaseDataCountyINCompare: state.counties.loadingCaseDataCountyINCompare,

		caseDataCountyMDCompare: state.counties.caseDataCountyMDCompare,
		loadingCaseDataCountyMDCompare: state.counties.loadingCaseDataCountyMDCompare,

		caseDataCountyNyCompare: state.counties.caseDataCountyNyCompare,
		loadingCaseDataCountyNyCompare: state.counties.loadingCaseDataCountyNyCompare,

		caseDataCountyVTCompare: state.counties.caseDataCountyVTCompare,
		loadingCaseDataCountyVTCompare: state.counties.loadingCaseDataCountyVTCompare,

		caseDataCountyWICompare: state.counties.caseDataCountyWICompare,
		loadingCaseDataCountyWICompare: state.counties.loadingCaseDataCountyWICompare,

		dataSingleStateCompare: state.covidtracker.dataSingleStateCompare,
		loadingSingleStateCompare: state.covidtracker.loadingSingleStateCompare,

		dataWorldWideEuropaCompare: state.europa.dataWorldWideEuropaCompare,
		loadingDataWorldWideEuropaCompare: state.europa.loadingDataWorldWideEuropaCompare
	}
}

export default connect(mapStateToProps, { getSFDataCompare, getCityDataSFTesting, clearCityDataSFTesting, getCityDataSFHospitalization, clearCityDataSFHospitalization, getNYCCaseDataCompare, getHongKongDataCompare, getCHICaseDataCompare, clearComparisonDataCompare, getCaseDataCaCountyCompare, clearCaseDataCaCountyCompare, getHospitalDataCaCountyCompare, clearHospitalDataCaCountyCompare, getCaseDataCOCountyCompare, clearCaseDataCOCountyCompare, getCaseDataCTCountyCompare, clearCaseDataCTCountyCompare, getCaseDataGACountyCompare, clearCaseDataGACountyCompare, getCaseDataINCountyCompare, clearCaseDataINCountyCompare, getCaseDataMDCountyCompare, clearCaseDataMDCountyCompare, getCaseDataNyCountyCompare, clearCaseDataNyCountyCompare, getCaseDataVTCountyCompare, clearCaseDataVTCountyCompare, getCaseDataWICountyCompare, clearCaseDataWICountyCompare, getCovidTrackerSingleStateDailyCompare, resetSingleStateDailyCompare, getWorldWideEuropaDataCompare, clearWorldWideEuropaDataCompare, getColorsList, getCountiesCAList, getCountiesCOList, getCountiesCTList, getCountiesGAList, getCountiesINList, getCountiesMDList, getCountiesNYList, getCountiesVTList, getCountiesWIList, getStatesList, clearImport })(Compare);

const styles = {
	chartTotalPercentDailyButtonSkinny: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 10px',
	},
	chartTotalPercentDailyButtonSelectedSkinny: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 10px',
	},
	chartTotalPercentDailyButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartTotalPercentDailyButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartTypeButtonsContainer: {
		display: 'flex',
		flexDirection: 'row',
		flex: 1,
		justifyContent: 'center'
	},
	chartTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		padding: 10
	},
	chartTypeButton: {
		backgroundColor: '#32a852',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartCasesDeathsTestsButtonSkinny: {
		backgroundColor: '#5cba57',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 10px',
	},
	chartCasesDeathsTestsButtonSelectedSkinny:  {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 10px',
	},
	chartCasesDeathsTestsButton: {
		backgroundColor: '#5cba57',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	chartCasesDeathsTestsButtonSelected:  {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	controlTypeButtonSkinny: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 10px',
	},
	controlTypeButtonSelectedSkinny: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 10px',
	},
	controlTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	controlTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	locationSelectButton: {
		//b9cded
		backgroundColor: '#b9cded',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	locationSelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	countySelectButton: {
		//b9cded
		backgroundColor: '#edf0fc',
    border: 0, 
    borderRadius: 3,
    // boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	countySelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    // boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	locationSelectButtonsContainer: {
		display: 'flex',
		flex: 1, 
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		marginTop: 20
	},
	locationSelectButtonContainer: {
		margin: 5
	},
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10,
		paddingTop: 10,
		paddingBottom: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
}










