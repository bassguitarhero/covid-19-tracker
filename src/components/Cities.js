import React, { Component } from 'react';
import { Button, Grid, Select, MenuItem } from '@material-ui/core';

import CityDetail from './CityDetail';

class Cities extends Component {
	constructor() {
		super();
		this.state = {
			showCityDetail: true,
			showCityComparison: false,
			cityName: 'San Francisco',
			cities: [
				{
	        name: "San Francisco",
	        id: "SF",
		    },
		    {
	        name: "New York City",
	        id: "NYC",
		    },
		    {
		    	name: "Chicago",
		    	id: "CHI"
		    }
			],
			selectedCity: ''
		}
	}

	handleCityChange = (e) => {
		this.setState({
			cityName: e.target.value
		});
	}

	setDisplayCityDetail = () => {
		this.setState({
			showCityDetail: true,
			showCityComparison: false
		});
	}

	setDisplayCityComparison = () => {
		this.setState({
			showCityDetail: false,
			showCityComparison: true
		});
	}

	render() {
		const { showCityDetail, showCityComparison, cityName, cities, selectedCity } = this.state;

		return (
			<div style={styles.citiesData}>
				<Grid container style={styles.cityHeader}>
					{showCityDetail && 
						<Grid item xs={12} style={styles.buttonContainer}>
							<div style={styles.cityTitle}>
								<span style={styles.cityNameText}>City:  </span>
								<Select
									value={cityName} 
									onChange={this.handleCityChange.bind(this)}
									inputProps={{
	                  name: `San Francisco`,
	                  id: "SF"
	                }} 
	                style={styles.selectCityName} 
								>
									{cities.map((city, index) => (
										<MenuItem value={city.name} key={index}>{city.name}</MenuItem>
									))}
								</Select>
							</div>
						</Grid>
					}
				</Grid>
				<div style={styles.cityData}>
					{showCityDetail && 
						<CityDetail location={cityName} />
					}
				</div>
			</div>
		);
	}
}

export default Cities;

const styles = {
	selectCityName: {
		paddingLeft: 10,
		paddingRight: 10
	},
	cityNameText: {
		fontSize: 20,
		fontWeight: 'bold'
	},
	citiesData: {
		flex: 1
	},
	cityHeader: {
		// display: 'flex',
		// flexDirection: 'row',
		// flex: 1,
		padding: 10,
		textAlign: 'center',
		alignItems: 'center'
	},
	cityTitle: {
		// flex: 1
		paddingTop: 10
	},
	buttonsContainer: {
		// display: 'flex',
		// flexDirection: 'row',
		// flex: 1,
		alignItems: 'center'
	},
	buttonContainer: {
		// flex: 1,
		alignSelf: 'center',
		paddingLeft: 5,
		paddingRight: 5
	},
	cityData: {
		flex: 1
	},
	cityButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	cityButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
}









