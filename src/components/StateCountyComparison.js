import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { Button, Grid } from '@material-ui/core';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import Icon from '@mdi/react'
import { mdiCogOutline } from '@mdi/js';

import SettingsModal from './SettingsModal';
import ModalBackdrop from './ModalBackdrop';
import SettingsModalIcon from './SettingsModalIcon';

import { getCaseDataCaCounty, clearCaseDataCaCounty, getCaseDataNyCounty, clearCaseDataNyCounty } from '../actions/counties';
import { getColorsListCountiesCompare, getCountiesCAListCompare, getCountiesNYListCompare, clearImportCountiesCompare } from '../actions/importData';

class StateCountyComparison extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedCountiesList: [],
			chartColors: [],
			statesAvailable: [],
			countiesCalifornia: [],
			countiesNewYork: [],

			showAxisX: false,
			windowWidth: 0,
			windowHeight: 0,
			showCounties: false, 
			showChart: false,
			showTotalCasesChart: true,
			showPercentCasesChart: false,
			showDailyCasesChart: false, 
			showTotalTestsChart: false, 
			showDailyTestsChart: false,

			// Line
			totalCasesChartLine: {},
			percentCasesChartLine: {},
			dailyCasesChartLine: {},
			totalTestsChartLine: {},
			dailyTestsChartLine: {},
			sevenDayAverageChartLine: {},

			// Bar
			totalCasesChartBar: {},
			percentCasesChartBar: {},
			dailyCasesChartBar: {},
			totalTestsChartBar: {},
			dailyTestsChartBar: {},
			sevenDayAverageChartBar: {},

			todaysDate: '',
			datesArray: [],
			showSettingsModal: false,
			chartHeight: 300,
			startDate: '',
			endDate: '',
			showCaliforniaCounties: false,
			showNewYorkCounties: false,
			population: 0,
			showLineGraph: true,
			showBarGraph: false,
			displayLineBarButtons: true,
			displayDailyAverageButtons: true,
			showSevenDayAverage: true,
			showDailyCount: false
		}
	}

	static defaultProps = {
		displayTitle: true,
		displayLegend: true,
		legendPosition: 'bottom'
	}

	handleShowSevenDayAverage = () => {
		this.setState({showSevenDayAverage: true, showDailyCount: false});
	}

	handleShowDailyCount = () => {
		this.setState({showSevenDayAverage: false, showDailyCount: true});
	}

	handleShowLineGraph = () => {
		this.setState({showLineGraph: true, showBarGraph: false});
	}

	handleShowBarGraph = () => {
		this.setState({showLineGraph: false, showBarGraph: true});
	}

	setChartData = () => {
		const { datesArray, selectedCountiesList } = this.state;
		// console.log('Selected Counties List: ', selectedCountiesList);
		if (selectedCountiesList.length > 0) {
			const labelDates = [];
			var total_cases = [];
			var percent_cases = [];
			var daily_cases = [];
			var seven_day_average = [];

			// Line
			const dataset_total_cases_line = [];
			const dataset_percent_cases_line = [];
			const dataset_daily_cases_line = [];
			const dataset_seven_day_average_line = [];

			// Bar
			const dataset_total_cases_bar = [];
			const dataset_percent_cases_bar = [];
			const dataset_daily_cases_bar = [];
			const dataset_seven_day_average_bar = [];

			for (var i = 0; i < datesArray.length; i++) {
				var fields = datesArray[i].split('-');
				labelDates.push(fields[1]+'/'+fields[2]);
			}
			for (let county of this.selectedCaCounties) {
				for (var i = 0; i < selectedCountiesList.length; i++) {
					if (selectedCountiesList[i].name == county && selectedCountiesList[i].state == "CA") {
						for (var j = 0; j < selectedCountiesList[i].data.length; j++) {
							total_cases.push(selectedCountiesList[i].data[j].total_cases);
							percent_cases.push(selectedCountiesList[i].data[j].percent_cases);
							daily_cases.push(selectedCountiesList[i].data[j].daily_cases);
							seven_day_average.push(selectedCountiesList[i].data[j].sevenDayAverage);
						}

						// Line
						dataset_total_cases_line.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: total_cases,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_percent_cases_line.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: percent_cases,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_daily_cases_line.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: daily_cases,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_seven_day_average_line.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: seven_day_average,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});

						// Bar
						dataset_total_cases_bar.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: total_cases,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_percent_cases_bar.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: percent_cases,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_daily_cases_bar.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: daily_cases,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_seven_day_average_bar.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: seven_day_average,
							backgroundColor: selectedCountiesList[i].color
						});

						total_cases = [];
						percent_cases = [];
						daily_cases = [];
						seven_day_average = [];
					}
				}
			}
			for (let county of this.selectedNyCounties) {
				for (var i = 0; i < selectedCountiesList.length; i++) {
					if (selectedCountiesList[i].name == county && selectedCountiesList[i].state == "NY") {
						for (var j = 0; j < selectedCountiesList[i].data.length; j++) {
							total_cases.push(selectedCountiesList[i].data[j].total_cases);
							percent_cases.push(selectedCountiesList[i].data[j].percent_cases);
							daily_cases.push(selectedCountiesList[i].data[j].daily_cases);
							seven_day_average.push(selectedCountiesList[i].data[j].sevenDayAverage);
						}

						// Line
						dataset_total_cases_line.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: total_cases,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_percent_cases_line.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: percent_cases,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_daily_cases_line.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: daily_cases,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});
						dataset_seven_day_average_line.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: seven_day_average,
							fill: false,
							borderColor: selectedCountiesList[i].color,
							pointRadius: 1,
							lineWidth: 3
						});

						// Bar
						dataset_total_cases_bar.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: total_cases,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_percent_cases_bar.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: percent_cases,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_daily_cases_bar.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: daily_cases,
							backgroundColor: selectedCountiesList[i].color
						});
						dataset_seven_day_average_bar.push({
							label: selectedCountiesList[i].name + ', ' + selectedCountiesList[i].state,
							data: seven_day_average,
							backgroundColor: selectedCountiesList[i].color
						});

						total_cases = [];
						percent_cases = [];
						daily_cases = [];
						seven_day_average = [];
					}
				}
			}
			this.setState({
				// Line
				totalCasesChartLine: {
					labels: labelDates,
					datasets: dataset_total_cases_line
				},
				percentCasesChartLine: {
					labels: labelDates,
					datasets: dataset_percent_cases_line
				},
				dailyCasesChartLine: {
					labels: labelDates,
					datasets: dataset_daily_cases_line
				},
				sevenDayAverageChartLine: {
					labels: labelDates,
					datasets: dataset_seven_day_average_line
				},

				// Bar
				totalCasesChartBar: {
					labels: labelDates,
					datasets: dataset_total_cases_bar
				},
				percentCasesChartBar: {
					labels: labelDates,
					datasets: dataset_percent_cases_bar
				},
				dailyCasesChartBar: {
					labels: labelDates,
					datasets: dataset_daily_cases_bar
				},
				sevenDayAverageChartBar: {
					labels: labelDates,
					datasets: dataset_seven_day_average_bar
				},

				showChart: true
			})
		} else {
			this.setState({showChart: false});
		}
	}

	handleDisplayTotalCasesChart = () => {
		this.setState({
			showTotalCasesChart: true,
			showPercentCasesChart: false,
			showDailyCasesChart: false
		})
	}

	handleDisplayPercentCasesChart = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: true,
			showDailyCasesChart: false
		})
	}

	handleDisplayDailyCasesChart = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: true
		})
	}

	handleSelectCountyCa = (county, population, state) => {
		const { countiesCalifornia } = this.state;
		const { selectedCountiesList } = this.state;
		let { showChart } = this.state;
		if (this.selectedCaCounties.has(county)) {
			this.selectedCaCounties.delete(county);
			let newSelectedCountiesList = selectedCountiesList.filter(e => (e.name !== county && e.state == e.state));
			for (var i = 0; i < countiesCalifornia.length; i++) {
				if (countiesCalifornia[i].county == county) {
					countiesCalifornia[i].active = false;
				}
			}
			this.setState({
				countiesCalifornia: countiesCalifornia,
				selectedCountiesList: newSelectedCountiesList,
				showChart: showChart
			});
		} else {
			this.selectedCaCounties.add(county);
			for (var i = 0; i < countiesCalifornia.length; i++) {
				if (countiesCalifornia[i].county == county) {
					countiesCalifornia[i].active = true;
				}
			}
			this.props.getCaseDataCaCounty(county);
			this.setState({countiesCalifornia: countiesCalifornia, population: population});
		}
	}

	handleSelectCountyNy = (county, population, state) => {
		const { countiesNewYork } = this.state;
		const { selectedCountiesList } = this.state;
		let { showChart } = this.state;
		if (this.selectedNyCounties.has(county)) {
			this.selectedNyCounties.delete(county);
			let newSelectedCountiesList = selectedCountiesList.filter(e => (e.name !== county && e.state == e.state));
			for (var i = 0; i < countiesNewYork.length; i++) {
				if (countiesNewYork[i].county == county) {
					countiesNewYork[i].active = false;
				}
			}
			this.setState({
				countiesNewYork: countiesNewYork,
				selectedCountiesList: newSelectedCountiesList,
				showChart: showChart
			});
		} else {
			this.selectedNyCounties.add(county);
			for (var i = 0; i < countiesNewYork.length; i++) {
				if (countiesNewYork[i].county == county) {
					countiesNewYork[i].active = true;
				}
			}
			this.props.getCaseDataNyCounty(county);
			this.setState({countiesNewYork: countiesNewYork, population: population});
		}
	}

	getDatesList = () => {
		var listDate = [];
		var listDateFormatted = [];
		var startDate = '2020-03-01';
		var endDate = this.state.todaysDate;
		var dateMove = new Date(startDate);
		var strDate = startDate;
		while (strDate < endDate){
		  var strDate = dateMove.toISOString().slice(0,10);
		  listDate.push(strDate);
		  dateMove.setDate(dateMove.getDate()+1);
		};
		this.setState({
			datesArray: listDate
		});
	}

	// fix later
	getTodaysDate = () => {
		var date = new Date();
		var newDate = date.setDate(date.getDate() -1);
		var yesterday = new Date(newDate);
		var dd = String(yesterday.getDate()).padStart(2, '0');
		var mm = String(yesterday.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = yesterday.getFullYear();
		var today = yyyy + '-' + mm + '-' + dd;
		this.setState({
			todaysDate: today
		});
	}

	setDisplayTotalCases = () => {
		this.setState({
			showTotalCasesChart: true,
			showPercentCasesChart: false,
			showDailyCasesChart: false
		})
	}

	setDisplayPercentCases = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: true,
			showDailyCasesChart: false
		})
	}

	setDisplayDailyCases = () => {
		this.setState({
			showTotalCasesChart: false,
			showPercentCasesChart: false,
			showDailyCasesChart: true
		})
	}

	handleSelectCalifornia = () => {
		this.setState({
			showCaliforniaCounties: true,
			showNewYorkCounties: false
		})
	}

	handleSelectNewYork = () => {
		this.setState({
			showCaliforniaCounties: false,
			showNewYorkCounties: true
		})
	}

	changeChartSettings = (chartHeight, startDate, endDate) => {
		this.setState({
			chartHeight: chartHeight,
			startDate: startDate,
			endDate: endDate
		});
	}

	closeSettingsModal = () => {
		this.setState({
			showSettingsModal: false
		})
	}

	openSettingsModal = () => {
		this.setState({
			showSettingsModal: true
		});
	}

	updateWindowDimensions = () => {
		if (window.innerWidth/window.innerHeight < 1) {
			this.setState({showAxisX: false})
		} else {
			this.setState({showAxisX: true})
		}
	  this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight });
	}

	componentDidMount() {
		this.selectedCaCounties = new Set();
		this.selectedNyCounties = new Set();
		this.getTodaysDate();
		this.updateWindowDimensions();
		this.props.getColorsListCountiesCompare();
		this.props.getCountiesCAListCompare();
		this.props.getCountiesNYListCompare();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentDidUpdate(lastProps, lastState) {
		if (this.props.importColorsListCountiesCompare !== null && lastProps.importColorsListCountiesCompare === null) {
			const chartColors = [];
			this.props.importColorsListCountiesCompare.map(datum => {
				chartColors.push(datum);
			});
			this.setState({
				chartColors: chartColors
			});
		}

		if (this.props.importCountiesCAListCompare !== null && lastProps.importCountiesCAListCompare === null) {
			const countiesCalifornia = [];
			this.props.importCountiesCAListCompare.map(datum => {
				countiesCalifornia.push({
					county: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesCalifornia: countiesCalifornia,
				showCaliforniaCounties: true
			});
		}

		if (this.props.importCountiesNYListCompare !== null && lastProps.importCountiesNYListCompare === null) {
			const countiesNewYork = [];
			this.props.importCountiesNYListCompare.map(datum => {
				countiesNewYork.push({
					county: datum.name,
					population: datum.population,
					active: false
				});
			});
			this.setState({
				countiesNewYork: countiesNewYork
			});
		}

		if (this.props.loadingCaseDataCountyCa === false && lastProps.loadingCaseDataCountyCa === true) {
			const { caseDataCountyCa } = this.props;
			// console.log('Case Data County CA: ', caseDataCountyCa);
			const { selectedCountiesList, datesArray, chartColors, population } = this.state;
			var randomNumber = (Math.random() * 56).toFixed();
			const countyData = {name: caseDataCountyCa[0].county, color: chartColors[randomNumber], state: "CA", data: []};
			for (var i = 0; i < datesArray.length; i++) {
				countyData.data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0});
			}
			for (var i = 0; i < countyData.data.length; i++) {
				caseDataCountyCa.map(datum => {
					if (countyData.data[i].date == datum.date.split('T')[0]) {
						countyData.data[i].total_cases = datum.totalcountconfirmed;
						countyData.data[i].percent_cases = (datum.totalcountconfirmed / population * 100).toFixed(2);
						countyData.data[i].daily_cases = datum.newcountconfirmed;
					}
				});
			}
			var sevenDayCount = 0;
			for (var i = 0; i < countyData.data.length; i++) {
				sevenDayCount += parseFloat(countyData.data[i].daily_cases);
				if (i < 7) {
					countyData.data[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(countyData.data[i - 7].daily_cases);
					countyData.data[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
				}
			}
			if (countyData.data[countyData.data.length - 1].total_cases == 0) {
				countyData.data[countyData.data.length - 1].total_cases = countyData.data[countyData.data.length - 2].total_cases;
			}
			if (countyData.data[countyData.data.length - 1].percent_cases == 0) {
				countyData.data[countyData.data.length - 1].percent_cases = countyData.data[countyData.data.length - 2].percent_cases;
			}
			selectedCountiesList.push(countyData);
			this.setState({selectedCountiesList: selectedCountiesList, population: 0});
			this.props.clearCaseDataCaCounty();
			this.setChartData();
		}
		if (this.props.loadingCaseDataCountyNy === false && lastProps.loadingCaseDataCountyNy === true) {
			const { caseDataCountyNy } = this.props;
			// console.log('Case Data County NY: ', caseDataCountyNy);
			const { selectedCountiesList, datesArray, chartColors, population } = this.state;
			var randomNumber = (Math.random() * 56).toFixed();
			const countyData = {name: caseDataCountyNy[0].county, color: chartColors[randomNumber], state: "NY", data: []};
			for (var i = 0; i < datesArray.length; i++) {
				countyData.data.push({date: datesArray[i], total_cases: 0, percent_cases: 0, daily_cases: 0, sevenDayAverage: 0});
			}
			for (var i = 0; i < countyData.data.length; i++) {
				caseDataCountyNy.map(datum => {
					if (countyData.data[i].date == datum.test_date.split('T')[0]) {
						countyData.data[i].total_cases = datum.cumulative_number_of_positives;
						countyData.data[i].percent_cases = (datum.cumulative_number_of_positives / population * 100).toFixed(2);
						countyData.data[i].daily_cases = datum.new_positives;
					}
				});
			}
			var sevenDayCount = 0;
			for (var i = 0; i < countyData.data.length; i++) {
				sevenDayCount += parseFloat(countyData.data[i].daily_cases);
				if (i < 7) {
					countyData.data[i].sevenDayAverage = (sevenDayCount / (i + 1)).toFixed(2);
				}
				if (i >= 7) {
					sevenDayCount -= parseFloat(countyData.data[i - 7].daily_cases);
					countyData.data[i].sevenDayAverage = (sevenDayCount / 7).toFixed(2);
				}
			}
			if (countyData.data[countyData.data.length - 1].total_cases == 0) {
				countyData.data[countyData.data.length - 1].total_cases = countyData.data[countyData.data.length - 2].total_cases;
			}
			if (countyData.data[countyData.data.length - 1].percent_cases == 0) {
				countyData.data[countyData.data.length - 1].percent_cases = countyData.data[countyData.data.length - 2].percent_cases;
			}
			selectedCountiesList.push(countyData);
			this.setState({selectedCountiesList: selectedCountiesList, population: 0});
			this.props.clearCaseDataNyCounty();
			this.setChartData();
		}
		if (this.state.todaysDate !== '' && lastState.todaysDate === '') {
			this.getDatesList();
		}
		if (this.state.selectedCountiesList !== lastState.selectedCountiesList) {
			this.setChartData();
		}
	}

	componentWillUnmount() {
		this.props.clearImportCountiesCompare();
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	render() {
		const { countiesCalifornia, countiesNewYork, showCaliforniaCounties, showNewYorkCounties } = this.state;
		const { showChart, showTotalCasesChart, showPercentCasesChart, showDailyCasesChart } = this.state;
		const { chartHeight, startDate, endDate, showAxisX, showLineGraph, showBarGraph, displayLineBarButtons } = this.state;
		const { displayDailyAverageButtons, showSevenDayAverage, showDailyCount } = this.state;

		return (
			<div>
				{this.state.showSettingsModal && 
          <SettingsModal chartHeight={chartHeight} startDate={startDate} endDate={endDate} changeChartSettings={this.changeChartSettings} closeModal={this.closeSettingsModal} displayLineGraph={this.handleShowLineGraph} displayBarGraph={this.handleShowBarGraph} showLineGraph={showLineGraph} showBarGraph={showBarGraph} displayLineBarButtons={displayLineBarButtons} displayDailyAverageButtons={displayDailyAverageButtons} showSevenDayAverage={showSevenDayAverage} showDailyCount={showDailyCount} displaySevenDayAverage={this.handleShowSevenDayAverage} displayDailyCount={this.handleShowDailyCount} />
        }
				{showChart ? 
					<div>
						<div>
							<Grid container>
								<Grid item xs={4} style={styles.chartTypeButtonContainer}>
									<Button style={showTotalCasesChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayTotalCases}>Total Cases</Button>
								</Grid>
								<Grid item xs={4} style={styles.chartTypeButtonContainer}>
									<Button style={showPercentCasesChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayPercentCases}>Percent Cases</Button>
								</Grid>
								<Grid item xs={4} style={styles.chartTypeButtonContainer}>
									<Button style={showDailyCasesChart ? styles.chartTypeButtonSelected : styles.chartTypeButton} onClick={this.setDisplayDailyCases}>Daily Cases</Button>
								</Grid>
							</Grid>
						</div>
						{showTotalCasesChart && 
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								{showLineGraph && 
									<Line 
			          		data={this.state.totalCasesChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Total Cases by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
			          }
			          {showBarGraph && 
			          	<Bar 
			          		data={this.state.totalCasesChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Total Cases by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
			          }
		          </div>
						}
						{showPercentCasesChart && 
							<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
								{showLineGraph && 
									<Line 
			          		data={this.state.percentCasesChartLine}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Percent Cases by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
		                  maintainAspectRatio: false
			              }}
			          	/>
			          }
			          {showBarGraph && 
			          	<Bar 
			          		data={this.state.percentCasesChartBar}
			          		options={{
		                  title: {
		                    display: this.props.displayTitle,
		                    text: 'Percent Cases by County',
		                    fontSize: 25
		                  },
		                  legend: {
		                    display: this.props.displayLegend,
		                    position: this.props.legendPosition
		                  },
		                  tooltips: {
							          callbacks: {
					                label: function(tooltipItem, data) {
				                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
				                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                          } else {
	                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	                          }
					                }
							          } // end callbacks:
							        }, //end tooltips 
		                  scales: {
		                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
										    yAxes: [
										    	{ ticks: {
										    		mirror: true, 
										    		beginAtZero: true,
	                          callback: function(value, index, values) {
	                            if(parseInt(value) >= 1000){
	                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	                            } else {
	                              return value;
	                            }
	                          }
	                         }, 
	                         stacked: true, 
	                         gridLines: {
	                         	display:false, 
	                         	drawBorder: false
	                         } 
	                       }
	                     ]
		                  },
	                		maintainAspectRatio: false
			              }}
			          	/>
			          }
		          </div>
						}
						{showDailyCasesChart  && 
							<div>
								{showSevenDayAverage && 
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										{showLineGraph && 
											<Line 
					          		data={this.state.sevenDayAverageChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Seven Day Average per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
										}
										{showBarGraph && 
											<Bar 
					          		data={this.state.sevenDayAverageChartBar}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Seven Day Average per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
			                		maintainAspectRatio: false
					              }}
					          	/>
					          }
									</div>
								}
								{showDailyCount && 
									<div style={{position: 'relative', marginLeft: 20, marginRight: 20, marginTop: 10, marginBottom: 10, flex: 1, minHeight: this.state.chartHeight, maxHeight: this.state.chartHeight}}>
										{showLineGraph && 
											<Line 
					          		data={this.state.dailyCasesChartLine}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Daily Cases per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
				                  maintainAspectRatio: false
					              }}
					          	/>
										}
										{showBarGraph && 
											<Bar 
					          		data={this.state.dailyCasesChartBar}
					          		options={{
				                  title: {
				                    display: this.props.displayTitle,
				                    text: 'Daily Cases per County',
				                    fontSize: 25
				                  },
				                  legend: {
				                    display: this.props.displayLegend,
				                    position: this.props.legendPosition
				                  },
				                  tooltips: {
									          callbacks: {
							                label: function(tooltipItem, data) {
						                    var value = data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						                    if(parseInt(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]) >= 1000){
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                          } else {
			                             return data.datasets[tooltipItem.datasetIndex].label + ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			                          }
							                }
									          } // end callbacks:
									        }, //end tooltips 
				                  scales: {
				                  	xAxes: [{ display: showAxisX, stacked: true, gridLines: {display:false} }],
												    yAxes: [
												    	{ ticks: {
												    		mirror: true, 
												    		beginAtZero: true,
			                          callback: function(value, index, values) {
			                            if(parseInt(value) >= 1000){
			                              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			                            } else {
			                              return value;
			                            }
			                          }
			                         }, 
			                         stacked: true, 
			                         gridLines: {
			                         	display:false, 
			                         	drawBorder: false
			                         } 
			                       }
			                     ]
				                  },
			                		maintainAspectRatio: false
					              }}
					          	/>
					          }
									</div>
								}
							</div>
						}
					</div> :
					<div style={styles.selectCounty}>
						<span style={styles.selectCountyText}>Select one or more counties...</span>
					</div>
				}
				<div style={styles.countySelectButtonsContainer}>
					<div style={styles.countySelectButtonContainer}>
						<Button style={showCaliforniaCounties ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCalifornia}>California</Button>
					</div>
					<div style={styles.countySelectButtonContainer}>
						<Button style={showNewYorkCounties ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectNewYork}>New York</Button>
					</div>
				</div>
				{(showCaliforniaCounties && (countiesCalifornia.length > 0)) && 
					<div style={styles.countySelectButtonsContainer}>
						{countiesCalifornia.map((county, index) => (
							<div style={styles.countySelectButtonContainer} key={index}>
								<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyCa.bind(this, county.county, county.population, county.state)}>{county.county}</Button>
							</div>
						))}
					</div>
				}
				{(showNewYorkCounties && (countiesNewYork.length > 0)) && 
					<div style={styles.countySelectButtonsContainer}>
						{countiesNewYork.map((county, index) => (
							<div style={styles.countySelectButtonContainer} key={index}>
								<Button style={county.active ? styles.countySelectButtonSelected : styles.countySelectButton} onClick={this.handleSelectCountyNy.bind(this, county.county, county.population, county.state)}>{county.county}</Button>
							</div>
						))}
					</div>
				}
				<div style={styles.sourceURLContainer}>
					<a style={styles.sourceURL} target="_blank" href="https://data.ca.gov/dataset/covid-19-cases"><span style={styles.sourceURLText}>Source: data.ca.gov</span></a>
				</div>
				<div style={styles.sourceURLContainer}>
					<a style={styles.sourceURL} target="_blank" href="https://health.data.ny.gov/resource/xdss-u53e"><span style={styles.sourceURLText}>Source: health.data.ny.gov</span></a>
				</div>
				<SettingsModalIcon openSettingsModal={this.openSettingsModal} />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		importColorsListCountiesCompare: state.importData.importColorsListCountiesCompare,
		importCountiesCAListCompare: state.importData.importCountiesCAListCompare,
		importCountiesNYListCompare: state.importData.importCountiesNYListCompare,

		caseDataCountyCa: state.counties.caseDataCountyCa,
		loadingCaseDataCountyCa: state.counties.loadingCaseDataCountyCa,

		caseDataCountyNy: state.counties.caseDataCountyNy,
		loadingCaseDataCountyNy: state.counties.loadingCaseDataCountyNy
	}
}

export default connect(mapStateToProps, { getCaseDataCaCounty, clearCaseDataCaCounty, getCaseDataNyCounty, clearCaseDataNyCounty, getColorsListCountiesCompare, getCountiesCAListCompare, getCountiesNYListCompare, clearImportCountiesCompare })(StateCountyComparison);

const styles = {
	chartTypeButtonContainer: {
		justifyContent: 'center',
		textAlign: 'center',
		padding: 10
	},
	chartTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	chartTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	selecCounty: {
		padding: 20,
		textAlign: 'center'
	},
	selectCountyText: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	countySelectButtonsContainer: {
		display: 'flex',
		flex: 1, 
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		marginTop: 20
	},
	countySelectButtonContainer: {
		margin: 5
	},
	countySelectButton: {
		backgroundColor: '#FFF',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'black',
    height: 24,
    padding: '0 30px',
	},
	countySelectButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	sourceURL: {
		textDecoration: 'none'
	},
	sourceURLContainer: {
		display: 'flex',
		flex: 1, 
		justifyContent: 'center',
		margin: 10
	},
	sourceURLText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: 'black'
	},
}










