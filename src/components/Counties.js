import React, { Component } from 'react';
import { Button, Grid, Select, MenuItem } from '@material-ui/core';

import StateCountyDetail from './StateCountyDetail';

class Counties extends Component {
	constructor() {
		super();
		this.state = {
			showStateDetail: true,
			showStateComparison: false,
			stateName: 'California',
			states: [
				{
	        name: "California",
	        id: "CA",
		    },
		    {
		    	name: "Connecticut",
		    	id: "CT"
		    },
		    // {
		    // 	name: "Florida",
		    // 	id: "FL"
		    // },
		    {
	        name: "New York",
	        id: "NY",
		    }
			],
			selectedState: ''
		}
	}

	handleStateChange = (e) => {
		this.setState({
			stateName: e.target.value
		});
	}

	setDisplayStateDetail = () => {
		this.setState({
			showStateDetail: true,
			showStateComparison: false
		});
	}

	setDisplayStateComparison = () => {
		this.setState({
			showStateDetail: false,
			showStateComparison: true
		});
	}

	render() {
		const { showStateDetail, showStateComparison, stateName, states, selectedState } = this.state;

		return (
			<div style={styles.statesData}>
				<Grid container style={styles.stateHeader}>
					<Grid item xs={12} style={styles.buttonContainer}>
						<div style={styles.stateTitle}>
							<span style={styles.stateNameText}>State:  </span>
							<Select
								value={stateName} 
								onChange={this.handleStateChange.bind(this)}
								inputProps={{
                  name: `California`,
                  id: "CA"
                }} 
                style={styles.selectStateName} 
							>
								{states.map((state, index) => (
									<MenuItem value={state.name} key={index}>{state.name}</MenuItem>
								))}
							</Select>
						</div>
					</Grid>
				</Grid>
				<div style={styles.stateData}>
					{showStateDetail && 
						<StateCountyDetail location={stateName} />
					}
				</div>
			</div>
		);
	}
}

export default Counties;

const styles = {
	selectStateName: {
		paddingLeft: 10,
		paddingRight: 10
	},
	stateNameText: {
		fontSize: 20,
		fontWeight: 'bold'
	},
	statesData: {
		flex: 1
	},
	stateHeader: {
		// display: 'flex',
		// flexDirection: 'row',
		// flex: 1,
		padding: 10,
		textAlign: 'center',
		alignItems: 'center'
	},
	stateTitle: {
		// flex: 1
		paddingTop: 10
	},
	buttonsContainer: {
		// display: 'flex',
		// flexDirection: 'row',
		// flex: 1,
		alignItems: 'center'
	},
	buttonContainer: {
		// flex: 1,
		alignSelf: 'center',
		paddingLeft: 5,
		paddingRight: 5
	},
	stateData: {
		flex: 1
	},
	stateButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
	stateButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 30px',
	},
}










