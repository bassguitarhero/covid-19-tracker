import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';

import Cities from './Cities';
// import Counties from './Counties';
// import States from './States';
// import WorldWide from './WorldWide';

class Detail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			displayCities: true,
			displayCounties: false,
			displayStates: false,
			displayCountries: false
		}
	}

	handleDisplayCities = () => {
		this.setState({
			displayCities: true,
			displayCounties: false,
			displayStates: false,
			displayCountries: false
		});
	}

	handleDisplayCounties = () => {
		this.setState({
			displayCities: false,
			displayCounties: true,
			displayStates: false,
			displayCountries: false
		});
	}

	handleDisplayStates = () => {
		this.setState({
			displayCities: false,
			displayCounties: false,
			displayStates: true,
			displayCountries: false
		});
	}

	handleDisplayCountries = () => {
		this.setState({
			displayCities: false,
			displayCounties: false,
			displayStates: false,
			displayCountries: true
		});
	}

	render() {
		const { displayCities, displayCounties, displayStates, displayCountries } = this.state;
		return (
			<div>
				{displayCities && 
					<Cities />
				}
			</div>
		);
	}
}

export default Detail;

const styles = {
	controlTypeButton: {
		backgroundColor: '#4287f5',
    border: 0, 
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
	controlTypeButtonSelected: {
		backgroundColor: '#666',
    border: 0, 
    borderRadius: 3,
    boxShadow: 'inset 0 3px 5px 2px rgba(0, 0, 0, .3)',
    color: 'white',
    height: 24,
    padding: '0 25px',
	},
}











