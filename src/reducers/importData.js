import { 
	GET_COLORS_LIST,
	GET_COUNTIES_CA_LIST,
	GET_COUNTIES_CO_LIST, 
	GET_COUNTIES_CT_LIST, 
	GET_COUNTIES_GA_LIST,
	GET_COUNTIES_IN_LIST, 
	GET_COUNTIES_NY_LIST,
	GET_COUNTIES_FL_LIST, 
	GET_COUNTIES_VT_LIST, 
	GET_COUNTIES_WI_LIST, 
	GET_STATES_LIST,
	CLEAR_IMPORT,

	GET_COLORS_LIST_CITIES_CHI,
	GET_COLORS_LIST_COUNTIES_CA,
	GET_COLORS_LIST_COUNTIES_CT,
	GET_COLORS_LIST_COUNTIES_MD,
	GET_COLORS_LIST_COUNTIES_NY,
	GET_COLORS_LIST_COUNTIES_FL,
	GET_COLORS_LIST_COUNTIES_COMPARE,
	GET_COLORS_LIST_STATES,
	GET_COLORS_LIST_COUNTRIES,

	GET_COUNTIES_CA_LIST_CA,
	GET_COUNTIES_CA_LIST_COMPARE,
	GET_COUNTIES_CT_LIST_CT,
	GET_COUNTIES_CT_LIST_COMPARE,
	GET_COUNTIES_MD_LIST_MD,
	GET_COUNTIES_MD_LIST_COMPARE,
	GET_COUNTIES_NY_LIST_NY,
	GET_COUNTIES_NY_LIST_COMPARE,
	GET_COUNTIES_FL_LIST_FL,
	GET_COUNTIES_FL_LIST_COMPARE,

	GET_STATES_LIST_STATES,

	CLEAR_IMPORT_COUNTIES_CA, 
	CLEAR_IMPORT_COUNTIES_CT, 
	CLEAR_IMPORT_COUNTIES_MD, 
	CLEAR_IMPORT_COUNTIES_NY, 
	CLEAR_IMPORT_COUNTIES_FL, 
	CLEAR_IMPORT_COUNTIES_COMPARE, 
	CLEAR_IMPORT_STATES, 
	CLEAR_IMPORT_COUNTRIES
} from '../actions/types';

const initialState = {
	importColorsList: null,
	importCountiesCAList: null,
	importCountiesCOList: null,
	importCountiesCTList: null, 
	importCountiesGAList: null,
	importCountiesINList: null,
	importCountiesMDList: null, 
	importCountiesNYList: null,
	importCountiesFLList: null, 
	importCountiesVTList: null, 
	importCountiesWIList: null, 
	importStatesList: null,

	importColorsListCitiesCHI: null,
	importColorsListCountiesCA: null,
	importColorsListCountiesCT: null,
	importColorsListCountiesNY: null,
	importColorsListCountiesFL: null,
	importColorsListCountiesCompare: null,
	importColorsListStates: null,
	importColorsListCountries: null,

	importCountiesCAListCA: null,
	importCountiesCAListCompare: null,

	importCountiesCTListCT: null,
	importCountiesCTListCompare: null,

	importCountiesMDListMD: null,
	importCountiesMDListCompare: null,

	importCountiesNYListNY: null,
	importCountiesNYListCompare: null,

	importCountiesFLListFL: null,
	importCountiesFLListCompare: null,

	importStatesListStates: null
}

export default function(state=initialState, action) {
	switch(action.type) {
		case 'GET_COLORS_LIST':
			return {
				...state,
				importColorsList: action.payload
			};

		case 'GET_COUNTIES_CA_LIST':
			return {
				...state,
				importCountiesCAList: action.payload
			};

		case 'GET_COUNTIES_CO_LIST':
			return {
				...state,
				importCountiesCOList: action.payload
			};

		case 'GET_COUNTIES_CT_LIST':
			return {
				...state,
				importCountiesCTList: action.payload
			}

		case 'GET_COUNTIES_GA_LIST':
			return {
				...state,
				importCountiesGAList: action.payload
			}

		case 'GET_COUNTIES_IN_LIST':
			return {
				...state,
				importCountiesINList: action.payload
			}

		case 'GET_COUNTIES_MD_LIST':
			return {
				...state,
				importCountiesMDList: action.payload
			}

		case 'GET_COUNTIES_NY_LIST':
			return {
				...state,
				importCountiesNYList: action.payload
			};

		case 'GET_COUNTIES_VT_LIST':
			return {
				...state,
				importCountiesVTList: action.payload
			}

		case 'GET_COUNTIES_WI_LIST':
			return {
				...state,
				importCountiesWIList: action.payload
			}

		case 'GET_STATES_LIST':
			return {
				...state,
				importStatesList: action.payload
			};

		case 'GET_COLORS_LIST_CITIES_CHI':
			return {
				...state,
				importColorsListCitiesCHI: action.payload
			}

		case 'GET_COLORS_LIST_COUNTIES_CA':
			return {
				...state,
				importColorsListCountiesCA: action.payload
			};

		case 'GET_COLORS_LIST_COUNTIES_CT':
			return {
				...state,
				importColorsListCountiesCT: action.payload
			};

		case 'GET_COLORS_LIST_COUNTIES_NY':
			return {
				...state,
				importColorsListCountiesNY: action.payload
			};

		case 'GET_COLORS_LIST_COUNTIES_FL':
			return {
				...state,
				importColorsListCountiesFL: action.payload
			};

		case 'GET_COLORS_LIST_COUNTIES_COMPARE':
			return {
				...state,
				importColorsListCountiesCompare: action.payload
			};

		case 'GET_COLORS_LIST_STATES':
			return {
				...state,
				importColorsListStates: action.payload
			};

		case 'GET_COLORS_LIST_COUNTRIES':
			return {
				...state,
				importColorsListCountries: action.payload
			};

		case 'GET_COUNTIES_CA_LIST_CA':
			return {
				...state,
				importCountiesCAListCA: action.payload
			};

		case 'GET_COUNTIES_CA_LIST_COMPARE':
			return {
				...state,
				importCountiesCAListCompare: action.payload
			};

		case 'GET_COUNTIES_CT_LIST_CT':
			return {
				...state,
				importCountiesCTListCT: action.payload
			};

		case 'GET_COUNTIES_CT_LIST_COMPARE':
			return {
				...state,
				importCountiesCTListCompare: action.payload
			};

		case 'GET_COUNTIES_NY_LIST_NY':
			return {
				...state,
				importCountiesNYListNY: action.payload
			};

		case 'GET_COUNTIES_NY_LIST_COMPARE':
			return {
				...state,
				importCountiesNYListCompare: action.payload
			};

		case 'GET_COUNTIES_FL_LIST_FL':
			return {
				...state,
				importCountiesFLListFL: action.payload
			};

		case 'GET_COUNTIES_FL_LIST_COMPARE':
			return {
				...state,
				importCountiesFLListCompare: action.payload
			};

		case 'GET_STATES_LIST_STATES':
			return {
				...state,
				importStatesListStates: action.payload
			};

		case 'CLEAR_IMPORT':
			return {
				...state,
				importColorsList: null,
				importCountiesCAList: null,
				importCountiesNYList: null,
				importStatesList: null
			};

		case 'CLEAR_IMPORT_COUNTIES_CA':
			return {
				...state,
				importColorsListCountiesCA: null,
				importCountiesCAListCA: null
			};

		case 'CLEAR_IMPORT_COUNTIES_CT':
			return {
				...state,
				importColorsListCountiesCT: null,
				importCountiesCAListCT: null
			};

		case 'CLEAR_IMPORT_COUNTIES_NY':
			return {
				...state,
				importColorsListCountiesNY: null,
				importCountiesNYListNY: null
			};

		case 'CLEAR_IMPORT_COUNTIES_FL':
			return {
				...state,
				importColorsListCountiesFL: null,
				importCountiesFLListFL: null
			};

		case 'CLEAR_IMPORT_COUNTIES_COMPARE':
			return {
				...state,
				importColorsListCountiesCompare: null,
				importCountiesCAListCompare: null,
				importCountiesNYListCompare: null,
				importCountiesFLListCompare: null
			};

		case 'CLEAR_IMPORT_STATES':
			return {
				...state,
				importColorsListStates: null,
				importStatesListStates: null
			};

		case 'CLEAR_IMPORT_COUNTRIES':
			return {
				...state,
				importColorsListCountries: null,
			};

	default:
			return state;
	}
}