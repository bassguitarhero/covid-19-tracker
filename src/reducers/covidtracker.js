import { 
	DATA_CT_SINGLE_STATE_DAILY,
	LOADING_DATA_CS_SINGLE_STATE_DAILY, 
	CLEAR_COVID_TRACKER_DATA,

	GET_SINGLE_STATE_COMPARE,
	LOADING_SINGLE_STATE_COMPARE
} from '../actions/types';

const initialState = {
	dataSingleStateDaily: null,
	loadingDataSingleStateDaily: true,

	dataSingleStateCompare: null,
	loadingSingleStateCompare: true
}

export default function(state=initialState, action) {
	switch(action.type) {
		case 'DATA_CT_SINGLE_STATE_DAILY': 
			return {
				...state,
				dataSingleStateDaily: action.payload
			};

		case 'LOADING_DATA_CS_SINGLE_STATE_DAILY': 
			return {
				...state,
				loadingDataSingleStateDaily: action.payload
			};

		case 'CLEAR_COVID_TRACKER_DATA':
			return {
				...state,
				dataSingleStateDaily: null,
				loadingDataSingleStateDaily: true
			};

		case 'GET_SINGLE_STATE_COMPARE': 
			return {
				...state,
				dataSingleStateCompare: action.payload
			};

		case 'LOADING_SINGLE_STATE_COMPARE': 
			return {
				...state,
				loadingSingleStateCompare: action.payload
			};

		default:
			return state;
	}
}