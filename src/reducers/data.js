import { 
	GET_DATA_DATE_AND_TRANSMISSION,
	LOADING_DATA_DATE_AND_TRANSMISSION,

	GET_DATA_AGE_GROUP_AND_GENDER,
	LOADING_DATA_AGE_GROUP_AND_GENDER,

	GET_DATA_HOSPITALIZATIONS,
	LOADING_DATA_HOSPITALIZATIONS,

	GET_DATA_TESTING,
	LOADING_DATA_TESTING,

	GET_DATA_RACE_AND_ETHNICITY,
	LOADING_DATA_RACE_AND_ETHNICITY,

	CLEAR_CHART_DATA,

	GET_NYC_CONFIRMED_CASE_DATA,
	LOADING_NYC_CONFIRMED_CASE_DATA,

	GET_SF_CONFIRMED_CASE_DATA,
	LOADING_SF_CONFIRMED_CASE_DATA,

	CLEAR_COMPARISON_DATA,

	GET_ZIP_CODE_DATA,
	LOADING_ZIP_CODE_DATA
} from '../actions/types';

const initialState = {
	dataDateAndTransmission: null,
	loadingDataDateAndTransmission: true,

	dataAgeGroupAndGender: null,
	loadingDataAgeGroupAndGender: true,

	dataHospitalizations: null,
	loadingDataHospitalizations: true,

	dataTesting: null,
	loadingDataTesting: true,

	dataRaceAndEthnicity: null,
	loadingDataRaceAndEthnicity: true,

	dataNYCConfirmed: null,
	loadingDataNYCConfirmed: true,

	dataSFConfirmed: null,
	loadingDataSFConfirmed: true,

	dataZipCode: null,
	loadingDataZipCode: true
};

export default function(state=initialState, action) {
	switch(action.type) {
		case 'GET_DATA_DATE_AND_TRANSMISSION': 
			// console.log('Get Data');
			return {
				...state,
				dataDateAndTransmission: action.payload
			};
		case 'LOADING_DATA_DATE_AND_TRANSMISSION':
			// console.log('Loading Data');
			return {
				...state,
				loadingDataDateAndTransmission: action.payload
			};

		case 'GET_DATA_AGE_GROUP_AND_GENDER': 
			// console.log('Get Data');
			return {
				...state,
				dataAgeGroupAndGender: action.payload
			};
		case 'LOADING_DATA_AGE_GROUP_AND_GENDER':
			// console.log('Loading Data');
			return {
				...state,
				loadingDataAgeGroupAndGender: action.payload
			};

		case 'GET_DATA_HOSPITALIZATIONS': 
			// console.log('Get Data');
			return {
				...state,
				dataHospitalizations: action.payload
			};
		case 'LOADING_DATA_HOSPITALIZATIONS':
			// console.log('Loading Data');
			return {
				...state,
				loadingDataHospitalizations: action.payload
			};

		case 'GET_DATA_TESTING': 
			// console.log('Get Data');
			return {
				...state,
				dataTesting: action.payload
			};
		case 'LOADING_DATA_TESTING':
			// console.log('Loading Data');
			return {
				...state,
				loadingDataTesting: action.payload
			};

		case 'GET_DATA_RACE_AND_ETHNICITY': 
			// console.log('Get Data');
			return {
				...state,
				dataRaceAndEthnicity: action.payload
			};
		case 'LOADING_DATA_RACE_AND_ETHNICITY':
			// console.log('Loading Data');
			return {
				...state,
				loadingDataRaceAndEthnicity: action.payload
			};

		case 'CLEAR_CHART_DATA':
			return {
				...state,
				dataDateAndTransmission: null,
				loadingDataDateAndTransmission: true,

				dataAgeGroupAndGender: null,
				loadingDataAgeGroupAndGender: true,

				dataHospitalizations: null,
				loadingDataHospitalizations: true,

				dataTesting: null,
				loadingDataTesting: true,

				dataRaceAndEthnicity: null,
				loadingDataRaceAndEthnicity: true,

				dataZipCode: null,
				loadingDataZipCode: true
			}

		case 'GET_NYC_CONFIRMED_CASE_DATA': 
			// console.log('Get Data');
			return {
				...state,
				dataNYCConfirmed: action.payload
			};
		case 'LOADING_NYC_CONFIRMED_CASE_DATA':
			// console.log('Loading Data');
			return {
				...state,
				loadingDataNYCConfirmed: action.payload
			};

		case 'GET_SF_CONFIRMED_CASE_DATA': 
			// console.log('Get Data');
			return {
				...state,
				dataSFConfirmed: action.payload
			};
		case 'LOADING_SF_CONFIRMED_CASE_DATA':
			// console.log('Loading Data');
			return {
				...state,
				loadingDataSFConfirmed: action.payload
			};

		case 'GET_ZIP_CODE_DATA':
			// console.log('Loading Data');
			return {
				...state,
				dataZipCode: action.payload
			};

		case 'LOADING_ZIP_CODE_DATA':
			// console.log('Loading Data');
			return {
				...state,
				loadingDataZipCode: action.payload
			};

		case 'CLEAR_COMPARISON_DATA':
			return {
				...state,
				dataNYCConfirmed: null,
				loadingDataNYCConfirmed: true,

				dataSFConfirmed: null,
				loadingDataSFConfirmed: true,
			}

		default:
			return state;
	}
}