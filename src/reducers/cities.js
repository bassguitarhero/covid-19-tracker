import { 
	GET_SF_DATA,
	LOADING_SF_DATA,
	CLEAR_SF_DATA, 

	GET_NYC_CASE_DATA,
	LOADING_NYC_CASE_DATA,
	CLEAR_NYC_DATA,

	GET_HK_DATA,
	LOADING_HK_DATA,
	CLEAR_HK_DATA,

	GET_CHI_CASE_DATA,
	LOADING_CHI_CASE_DATA,
	CLEAR_CHI_CASE_DATA,

	GET_PLACEHOLDER_DATA,
	LOADING_PLACEHOLDER_DATA,

	GET_SF_DATA_COMPARE,
	LOADING_SF_DATA_COMPARE,
	CLEAR_SF_DATA_COMPARE, 

	GET_CITY_DATA_SF_TESTING,
	LOADING_CITY_DATA_SF_TESTING,
	CLEAR_CITY_DATA_SF_TESTING,

	GET_CITY_DATA_SF_HOSPITALIZATION,
	LOADING_CITY_DATA_SF_HOSPITALIZATION,
	CLEAR_CITY_DATA_SF_HOSPITALIZATION,

	GET_NYC_CASE_DATA_COMPARE,
	LOADING_NYC_CASE_DATA_COMPARE,
	CLEAR_NYC_DATA_COMPARE,

	GET_HK_DATA_COMPARE,
	LOADING_HK_DATA_COMPARE,
	CLEAR_HK_DATA_COMPARE,

	GET_CHI_CASE_DATA_COMPARE,
	LOADING_CHI_CASE_DATA_COMPARE,
	CLEAR_CHI_CASE_DATA_COMPARE
} from '../actions/types';

const initialState = {
	dataSF: null,
	loadingDataSF: true, 

	dataNYCCases: null,
	loadingDataNYCCases: true,

	dataHK: null,
	loadingDataHK: true,

	dataChicago: null,
	loadingDataChicago: true,

	dataPlaceHolder: null,
	loadingDataPlaceHolder: true,

	dataSFCompare: null,
	loadingDataSFCompare: true, 

	cityDataSFTesting: null,
	loadingCityDataSFTesting: true,

	cityDataSFHospitalization: null,
	loadingCityDataSFHospitalization: true, 

	dataNYCCasesCompare: null,
	loadingDataNYCCasesCompare: true,

	dataHKCompare: null,
	loadingDataHKCompare: true,

	dataCHICaseCompare: null,
	loadingDataCHICaseCompare: true
}

export default function(state=initialState, action) {
	switch(action.type) {
		case 'GET_SF_DATA':
			return {
				...state,
				dataSF: action.payload
			};

		case 'LOADING_SF_DATA':
			return {
				...state,
				loadingDataSF: action.payload
			};

		case 'CLEAR_SF_DATA':
			return {
				dataSF: null,
				loadingDataSF: true, 
			};

		case 'GET_NYC_CASE_DATA':
			return {
				...state,
				dataNYCCases: action.payload
			};

		case 'LOADING_NYC_CASE_DATA':
			return {
				...state,
				loadingDataNYCCases: action.payload
			};

		case 'CLEAR_NYC_DATA':
			return {
				...state,
				dataNYCCases: null,
				loadingDataNYCCases: true
			};

		case 'GET_HK_DATA':
			return {
				...state,
				dataHK: action.payload
			};

		case 'LOADING_HK_DATA':
			return {
				...state,
				loadingDataHK: action.payload
			};

		case 'CLEAR_HK_DATA':
			return {
				...state,
				dataHK: null,
				loadingDataHK: true
			};

		case 'GET_CHI_CASE_DATA':
			return {
				...state,
				dataChicago: action.payload
			};

		case 'LOADING_CHI_CASE_DATA':
			return {
				...state,
				loadingDataChicago: action.payload
			};

		case 'CLEAR_CHI_CASE_DATA':
			return {
				...state,
				dataChicago: null,
				loadingDataChicago: true
			};

		case 'GET_PLACEHOLDER_DATA':
			return {
				...state,
				dataPlaceHolder: action.payload
			};

		case 'LOADING_PLACEHOLDER_DATA':
			return {
				...state,
				loadingDataPlaceHolder: action.payload
			};

		case 'GET_SF_DATA_COMPARE':
			return {
				...state,
				dataSFCompare: action.payload
			};

		case 'LOADING_SF_DATA_COMPARE':
			return {
				...state,
				loadingDataSFCompare: action.payload
			};

		case 'CLEAR_SF_DATA_COMPARE':
			return {
				dataSFCompare: null,
				loadingDataSFCompare: true, 
			};

		case 'GET_CITY_DATA_SF_TESTING':
			return {
				...state,
				cityDataSFTesting: action.payload
			};

		case 'LOADING_CITY_DATA_SF_TESTING':
			return {
				...state,
				loadingCityDataSFTesting: action.payload
			}

		case 'CLEAR_SF_DATA_TESTING':
			return {
				cityDataSFTesting: null,
				loadingCityDataSFTesting: true, 
			};

		case 'GET_CITY_DATA_SF_HOSPITALIZATION':
			return {
				...state,
				cityDataSFHospitalization: action.payload
			};

		case 'LOADING_CITY_DATA_SF_HOSPITALIZATION':
			return {
				...state,
				loadingCityDataSFHospitalization: action.payload
			}

		case 'CLEAR_SF_DATA_HOSPITALIZATION':
			return {
				cityDataSFHospitalization: null,
				loadingCityDataSFHospitalization: true, 
			};

		case 'GET_NYC_CASE_DATA_COMPARE':
			return {
				...state,
				dataNYCCasesCompare: action.payload
			};

		case 'LOADING_NYC_CASE_DATA_COMPARE':
			return {
				...state,
				loadingDataNYCCasesCompare: action.payload
			};

		case 'CLEAR_NYC_DATA_COMPARE':
			return {
				...state,
				dataNYCCasesCompare: null,
				loadingDataNYCCasesCompare: true
			};

		case 'GET_HK_DATA_COMPARE':
			return {
				...state,
				dataHKCompare: action.payload
			};

		case 'LOADING_HK_DATA_COMPARE':
			return {
				...state,
				loadingDataHKCompare: action.payload
			};

		case 'CLEAR_HK_DATA_COMPARE':
			return {
				...state,
				dataHKCompare: null,
				loadingDataHKCompare: true
			};



		case 'GET_CHI_CASE_DATA_COMPARE':
			return {
				...state,
				dataCHICaseCompare: action.payload
			};

		case 'LOADING_CHI_CASE_DATA_COMPARE':
			return {
				...state,
				loadingDataCHICaseCompare: action.payload
			};

		case 'CLEAR_CHI_CASE_DATA_COMPARE':
			return {
				...state,
				dataCHICaseCompare: null,
				loadingDataCHICaseCompare: true
			};

		default:
			return state;
	}
}










