import { 
	CHANGE_HEADER_CITIES,
	CHANGE_HEADER_COUNTIES,
	CHANGE_HEADER_STATES,
	CHANGE_HEADER_COUNTRIES,
	CHANGE_HEADER_COMPARE,
	CHANGE_HEADER_DETAIL,
	CHANGE_HEADER_ABOUT
} from '../actions/types';

const initialState = {
	updateHeaderCities: false,
	updateHeaderCounties: false, 
	updateHeaderStates: false,
	updateHeaderCountries: false,
	updateHeaderCompare: false,
	updateHeaderDetail: false,
	updateHeaderAbout: false
}

export default function(state=initialState, action) {
	switch(action.type) {
		case 'CHANGE_HEADER_CITIES':
			return {
				...state,
				updateHeaderCities: action.payload
			};

		case 'CHANGE_HEADER_COUNTIES':
			return {
				...state,
				updateHeaderCounties: action.payload
			};

		case 'CHANGE_HEADER_STATES':
			return {
				...state,
				updateHeaderStates: action.payload
			};
			
		case 'CHANGE_HEADER_COUNTRIES':
			return {
				...state,
				updateHeaderCountries: action.payload
			};
			
		case 'CHANGE_HEADER_COMPARE':
			return {
				...state,
				updateHeaderCompare: action.payload
			};
			
		case 'CHANGE_HEADER_DETAIL':
			return {
				...state,
				updateHeaderDetail: action.payload
			};
			
		case 'CHANGE_HEADER_ABOUT':
			return {
				...state,
				updateHeaderAbout: action.payload
			};
			
	default:
			return state;
	}
}