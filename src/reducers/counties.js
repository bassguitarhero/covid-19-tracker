import {
	GET_CASE_DATA_CA_COUNTY,
	LOADING_CASE_DATA_CA_COUNTY,
	CLEAR_CASE_DATA_CA_COUNTY,

	GET_HOSPITAL_DATA_CA_COUNTY,
	LOADING_HOSPITAL_DATA_CA_COUNTY,
	CLEAR_HOSPITAL_DATA_CA_COUNTY,

	GET_CASE_DATA_CT_COUNTY,
	LOADING_CASE_DATA_CT_COUNTY,
	CLEAR_CASE_DATA_CT_COUNTY,

	GET_CASE_DATA_NY_COUNTY,
	LOADING_CASE_DATA_NY_COUNTY,
	CLEAR_CASE_DATA_NY_COUNTY,

	GET_FLORIDA_DATA,
	LOADING_FLORIDA_DATA,
	CLEAR_FLORIDA_DATA,

	GET_CASE_DATA_CA_COUNTY_COMPARE,
	LOADING_CASE_DATA_CA_COUNTY_COMPARE,
	CLEAR_CASE_DATA_CA_COUNTY_COMPARE,

	GET_HOSPITAL_DATA_CA_COUNTY_COMPARE,
	LOADING_HOSPITAL_DATA_CA_COUNTY_COMPARE,
	CLEAR_HOSPITAL_DATA_CA_COUNTY_COMPARE,

	GET_TESTING_DATA_CA_COUNTY_COMPARE,
	LOADING_TESTING_DATA_CA_COUNTY_COMPARE,
	CLEAR_TESTING_DATA_CA_COUNTY_COMPARE,

	GET_CASE_DATA_CO_COUNTY_COMPARE,
	LOADING_CASE_DATA_CO_COUNTY_COMPARE,
	CLEAR_CASE_DATA_CO_COUNTY_COMPARE,

	GET_CASE_DATA_CT_COUNTY_COMPARE,
	LOADING_CASE_DATA_CT_COUNTY_COMPARE,
	CLEAR_CASE_DATA_CT_COUNTY_COMPARE,

	GET_CASE_DATA_GA_COUNTY_COMPARE,
	LOADING_CASE_DATA_GA_COUNTY_COMPARE,
	CLEAR_CASE_DATA_GA_COUNTY_COMPARE,

	GET_CASE_DATA_IN_COUNTY_COMPARE,
	LOADING_CASE_DATA_IN_COUNTY_COMPARE,
	CLEAR_CASE_DATA_IN_COUNTY_COMPARE,

	GET_CASE_DATA_MD_COUNTY_COMPARE,
	LOADING_CASE_DATA_MD_COUNTY_COMPARE,
	CLEAR_CASE_DATA_MD_COUNTY_COMPARE,

	GET_CASE_DATA_NY_COUNTY_COMPARE,
	LOADING_CASE_DATA_NY_COUNTY_COMPARE,
	CLEAR_CASE_DATA_NY_COUNTY_COMPARE,

	GET_CASE_DATA_VT_COUNTY_COMPARE,
	LOADING_CASE_DATA_VT_COUNTY_COMPARE,
	CLEAR_CASE_DATA_VT_COUNTY_COMPARE,

	GET_CASE_DATA_WI_COUNTY_COMPARE,
	LOADING_CASE_DATA_WI_COUNTY_COMPARE,
	CLEAR_CASE_DATA_WI_COUNTY_COMPARE
} from '../actions/types';

const initialState = {
	caseDataCountyCa: null,
	loadingCaseDataCountyCa: true,

	hospitalDataCountyCa: null,
	loadingHospitalDataCountyCa: true,

	caseDataCountyCT: null,
	loadingCaseDataCountyCT: true,


	caseDataCountyNy: null,
	loadingCaseDataCountyNy: true,

	dataFlorida: null,
	loadingDataFlorida: true,

	caseDataCountyCaCompare: null,
	loadingCaseDataCountyCaCompare: true,

	hospitalDataCountyCACompare: null,
	loadingHospitalDataCountyCACompare: true,

	testingDataCountyCACompare: null,
	loadingTestingDataCountyCACompare: true,

	caseDataCountyCOCompare: null,
	loadingCaseDataCountyCOCompare: true,

	caseDataCountyCTCompare: null,
	loadingCaseDataCountyCTCompare: true,

	caseDataCountyGACompare: null,
	loadingCaseDataCountyGACompare: true,

	caseDataCountyINCompare: null,
	loadingCaseDataCountyINCompare: true,

	caseDataCountyMDCompare: null,
	loadingCaseDataCountyMDCompare: true,

	caseDataCountyNyCompare: null,
	loadingCaseDataCountyNyCompare: true,

	caseDataCountyVTCompare: null,
	loadingCaseDataCountyVTCompare: true,

	caseDataCountyWICompare: null,
	loadingCaseDataCountyWICompare: true
}

export default function(state=initialState, action) {
	switch(action.type) {
		case 'GET_CASE_DATA_CA_COUNTY':
			return {
				...state,
				caseDataCountyCa: action.payload
			};

		case 'LOADING_CASE_DATA_CA_COUNTY':
			return {
				...state,
				loadingCaseDataCountyCa: action.payload
			};
		
		case 'CLEAR_CASE_DATA_CA_COUNTY':
			return {
				...state,
				caseDataCountyCa: null,
				loadingCaseDataCountyCa: true
			};

		case 'GET_HOSPITAL_DATA_CA_COUNTY':
			return {
				...state,
				hospitalDataCountyCa: action.payload
			};

		case 'LOADING_HOSPITAL_DATA_CA_COUNTY':
			return {
				...state,
				loadingHospitalDataCountyCa: action.payload
			};
		
		case 'CLEAR_HOSPITAL_DATA_CA_COUNTY':
			return {
				...state,
				hospitalDataCountyCa: null,
				loadingHospitalDataCountyCa: true
			};



		case 'GET_CASE_DATA_CT_COUNTY':
			// console.log('Case Data: ', action.payload);
			return {
				caseDataCountyCT: action.payload
			};

		case 'LOADING_CASE_DATA_CT_COUNTY':
			// console.log('Updated Status: ', action.payload);
			return {
				...state,
				loadingCaseDataCountyCT: action.payload
			}

		case  'CLEAR_CASE_DATA_CT_COUNTY':
			return {
				...state,
				caseDataCountyCT: null,
				loadingCaseDataCountyCT: true,
			}




		case 'GET_CASE_DATA_NY_COUNTY':
			return {
				...state,
				caseDataCountyNy: action.payload
			};

		case 'LOADING_CASE_DATA_NY_COUNTY':
			return {
				...state,
				loadingCaseDataCountyNy: action.payload
			};
		
		case 'CLEAR_CASE_DATA_NY_COUNTY':
			return {
				...state,
				caseDataCountyNy: null,
				loadingCaseDataCountyNy: true
			};




		case 'GET_FLORIDA_DATA':
			return {
				...state,
				dataFlorida: action.payload
			};

		case 'LOADING_FLORIDA_DATA':
			return {
				...state,
				loadingDataFlorida: action.payload
			};

		case 'CLEAR_FLORIDA_DATA':
			return {
				...state,
				dataFlorida: null,
				loadingDataFlorida: true
			};





		case 'GET_CASE_DATA_CA_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyCaCompare: action.payload
			};

		case 'LOADING_CASE_DATA_CA_COUNTY_COMPARE':
			return {
				...state,
				loadingCaseDataCountyCaCompare: action.payload
			};
		
		case 'CLEAR_CASE_DATA_CA_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyCaCompare: null,
				loadingCaseDataCountyCaCompare: true
			};

		case 'GET_HOSPITAL_DATA_CA_COUNTY_COMPARE':
			return {
				...state,
				hospitalDataCountyCACompare: action.payload
			};

		case 'LOADING_HOSPITAL_DATA_CA_COUNTY_COMPARE':
			return {
				...state,
				loadingHospitalDataCountyCACompare: action.payload
			};
		
		case 'CLEAR_HOSPITAL_DATA_CA_COUNTY_COMPARE':
			return {
				...state,
				hospitalDataCountyCACompare: null,
				loadingHospitalDataCountyCACompare: true
			};

		case 'GET_TESTING_DATA_CA_COUNTY_COMPARE':
			return {
				...state,
				testingDataCountyCaCompare: action.payload
			};

		case 'LOADING_TESTING_DATA_CA_COUNTY_COMPARE':
			return {
				...state,
				loadingTestingDataCountyCaCompare: action.payload
			};
		
		case 'CLEAR_TESTING_DATA_CA_COUNTY_COMPARE':
			return {
				...state,
				testingDataCountyCaCompare: null,
				loadingTestingDataCountyCaCompare: true
			};



		case 'GET_CASE_DATA_CO_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyCOCompare: action.payload
			};

		case 'LOADING_CASE_DATA_CO_COUNTY_COMPARE':
			return {
				...state,
				loadingCaseDataCountyCOCompare: action.payload
			};
		
		case 'CLEAR_CASE_DATA_CO_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyCOCompare: null,
				loadingCaseDataCountyCOCompare: true
			};



		case 'GET_CASE_DATA_CT_COUNTY_COMPARE':
			// console.log('Case Data: ', action.payload);
			return {
				caseDataCountyCTCompare: action.payload
			};

		case 'LOADING_CASE_DATA_CT_COUNTY_COMPARE':
			// console.log('Updated Status: ', action.payload);
			return {
				...state,
				loadingCaseDataCountyCTCompare: action.payload
			}

		case  'CLEAR_CASE_DATA_CT_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyCTCompare: null,
				loadingCaseDataCountyCTCompare: true,
			}



		case 'GET_CASE_DATA_GA_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyGACompare: action.payload
			};

		case 'LOADING_CASE_DATA_GA_COUNTY_COMPARE':
			return {
				...state,
				loadingCaseDataCountyGACompare: action.payload
			};
		
		case 'CLEAR_CASE_DATA_GA_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyGACompare: null,
				loadingCaseDataCountyGACompare: true
			};


		case 'GET_CASE_DATA_IN_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyINCompare: action.payload
			};

		case 'LOADING_CASE_DATA_IN_COUNTY_COMPARE':
			return {
				...state,
				loadingCaseDataCountyINCompare: action.payload
			};
		
		case 'CLEAR_CASE_DATA_IN_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyINCompare: null,
				loadingCaseDataCountyINCompare: true
			};




		case 'GET_CASE_DATA_MD_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyMDCompare: action.payload
			};

		case 'LOADING_CASE_DATA_MD_COUNTY_COMPARE':
			return {
				...state,
				loadingCaseDataCountyMDCompare: action.payload
			};
		
		case 'CLEAR_CASE_DATA_MD_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyMDCompare: null,
				loadingCaseDataCountyMDCompare: true
			};





		case 'GET_CASE_DATA_NY_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyNyCompare: action.payload
			};

		case 'LOADING_CASE_DATA_NY_COUNTY_COMPARE':
			return {
				...state,
				loadingCaseDataCountyNyCompare: action.payload
			};
		
		case 'CLEAR_CASE_DATA_NY_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyNyCompare: null,
				loadingCaseDataCountyNyCompare: true
			};







		case 'GET_CASE_DATA_VT_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyVTCompare: action.payload
			};

		case 'LOADING_CASE_DATA_VT_COUNTY_COMPARE':
			return {
				...state,
				loadingCaseDataCountyVTCompare: action.payload
			};
		
		case 'CLEAR_CASE_DATA_VT_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyVTCompare: null,
				loadingCaseDataCountyVTCompare: true
			};




		case 'GET_CASE_DATA_WI_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyWICompare: action.payload
			};

		case 'LOADING_CASE_DATA_WI_COUNTY_COMPARE':
			return {
				...state,
				loadingCaseDataCountyWICompare: action.payload
			};
		
		case 'CLEAR_CASE_DATA_WI_COUNTY_COMPARE':
			return {
				...state,
				caseDataCountyWICompare: null,
				loadingCaseDataCountyWICompare: true
			};
		
		default:
			return state;
	}
}