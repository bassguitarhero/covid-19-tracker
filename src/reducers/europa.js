import {
	GET_WORLD_WIDE_EUROPA_DATA,
	LOADING_WORLD_WIDE_EUROPA_DATA,
	CLEAR_WORLD_WIDE_EUROPA_DATA,

	GET_WORLD_WIDE_EUROPA_DATA_COMPARE,
	LOADING_WORLD_WIDE_EUROPA_DATA_COMPARE,
	CLEAR_WORLD_WIDE_EUROPA_DATA_COMPARE	
} from '../actions/types';

const initialState = {
	dataWorldWideEuropa: null,
	loadingDataWorldWideEuropa: true,

	dataWorldWideEuropaCompare: null,
	loadingDataWorldWideEuropaCompare: true,
}

export default function(state=initialState, action) {
	switch(action.type) {
		case 'GET_WORLD_WIDE_EUROPA_DATA':
			// console.log('Action Payload: ', action.payload);
			return {
				...state,
				dataWorldWideEuropa: action.payload
			};

		case 'LOADING_WORLD_WIDE_EUROPA_DATA':
			// console.log('True False: ', action.payload);
			return {
				...state,
				loadingDataWorldWideEuropa: action.payload
			};
		
		case 'CLEAR_WORLD_WIDE_EUROPA_DATA':
			return {
				...state,
				dataWorldWideEuropa: null,
				loadingDataWorldWideEuropa: true
			};

		case 'GET_WORLD_WIDE_EUROPA_DATA_COMPARE':
			return {
				...state,
				dataWorldWideEuropaCompare: action.payload
			};

		case 'LOADING_WORLD_WIDE_EUROPA_DATA_COMPARE':
			return {
				...state,
				loadingDataWorldWideEuropaCompare: action.payload
			};
		
		case 'CLEAR_WORLD_WIDE_EUROPA_DATA_COMPARE':
			return {
				...state,
				dataWorldWideEuropaCompare: null,
				loadingDataWorldWideEuropaCompare: true
			};
		
		default:
			return state;
	}
}