import { combineReducers } from 'redux';
import cities from './cities';
import counties from './counties';
import covidtracker from './covidtracker';
import data from './data';
import europa from './europa';
import importData from './importData';
import layout from './layout';
import world from './world';

export default combineReducers({
	cities, 
	counties, 
	covidtracker,
	data,
	europa,
	importData,
	layout,
	world
});