import { 
	GET_WORLD_DATA,
	LOADING_WORLD_DATA,
	CLEAR_WORLD_DATA,
	GET_WORLD_COUNTRIES,
	LOADING_WORLD_COUNTRIES,
	GET_COUNTRY_DATA,
	LOADING_COUNTRY_DATA
} from '../actions/types';

const initialState = {
	worldData: null,
	loadingWorldData: true,
	worldCountries: null,
	loadingWorldCountries: true,
	countryData: null,
	loadingCountryData: true
}

export default function(state=initialState, action) {
	switch(action.type) {
		case 'GET_WORLD_DATA':
			return {
				...state,
				worldData: action.payload
			};

		case 'LOADING_WORLD_DATA':
			return {
				...state,
				loadingWorldData: action.payload
			};

		case 'GET_WORLD_COUNTRIES':
			return {
				...state,
				worldCountries: action.payload
			};

		case 'LOADING_WORLD_COUNTRIES':
			return {
				...state,
				loadingWorldCountries: action.payload
			};

		case 'GET_COUNTRY_DATA':
			return {
				...state,
				countryData: action.payload
			};

		case 'LOADING_COUNTRY_DATA':
			return {
				...state,
				loadingCountryData: action.payload
			};

		case 'CLEAR_WORLD_DATA':
			return {
				...state,
				worldData: null,
				loadingWorldData: true,
				worldCountries: null,
				loadingWorldCountries: true,
				countryData: null,
				loadingCountryData: true
			};

		default:
			return state;
	}
}