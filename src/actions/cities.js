import axios from 'axios';

import { 
	GET_SF_DATA,
	LOADING_SF_DATA,
	CLEAR_SF_DATA, 

	GET_NYC_CASE_DATA,
	LOADING_NYC_CASE_DATA,
	CLEAR_NYC_DATA,

	GET_HK_DATA,
	LOADING_HK_DATA,
	CLEAR_HK_DATA,

	GET_CHI_CASE_DATA,
	LOADING_CHI_CASE_DATA,
	CLEAR_CHI_CASE_DATA,

	GET_PLACEHOLDER_DATA,
	LOADING_PLACEHOLDER_DATA,

	GET_SF_DATA_COMPARE,
	LOADING_SF_DATA_COMPARE,
	CLEAR_SF_DATA_COMPARE, 

	GET_CITY_DATA_SF_TESTING,
	LOADING_CITY_DATA_SF_TESTING,
	CLEAR_CITY_DATA_SF_TESTING,

	GET_CITY_DATA_SF_HOSPITALIZATION,
	LOADING_CITY_DATA_SF_HOSPITALIZATION,
	CLEAR_CITY_DATA_SF_HOSPITALIZATION,

	GET_NYC_CASE_DATA_COMPARE,
	LOADING_NYC_CASE_DATA_COMPARE,
	CLEAR_NYC_DATA_COMPARE,

	GET_HK_DATA_COMPARE,
	LOADING_HK_DATA_COMPARE,
	CLEAR_HK_DATA_COMPARE,

	GET_CHI_CASE_DATA_COMPARE,
	LOADING_CHI_CASE_DATA_COMPARE,
	CLEAR_CHI_CASE_DATA_COMPARE
} from './types';

// Detail
export const getSFData = () => (dispatch, getState) => {
	axios 
		.get('https://data.sfgov.org/resource/tvq9-ec9w.json')
		.then(res => {
			dispatch({
				type: GET_SF_DATA,
				payload: res.data
			});
			dispatch({
				type: LOADING_SF_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearSFData = () => (dispatch) => {
	dispatch({
		type: CLEAR_SF_DATA
	});
}

export const getNYCCaseData = () => (dispatch) => {
	axios
		.get(`https://health.data.ny.gov/resource/xdss-u53e.json?$where=county='Queens' OR county='Kings' OR county='Bronx' OR county='New York' OR county='Richmond'`)
		.then(res => {
			dispatch({
				type: GET_NYC_CASE_DATA,
				payload: res.data
			});
			dispatch({
				type: LOADING_NYC_CASE_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearNYCCaseData = () => (dispatch) => {
	dispatch({
		type: CLEAR_NYC_DATA
	});
}


export const getHongKongData = () => (dispatch) => {
	axios
		.get(`https://api.data.gov.hk/v2/filter?q=%7B%22resource%22%3A%22http%3A%2F%2Fwww.chp.gov.hk%2Ffiles%2Fmisc%2Fenhanced_sur_covid_19_eng.csv%22%2C%22section%22%3A1%2C%22format%22%3A%22json%22%7D`)
		.then(res => {
			dispatch({
				type: GET_HK_DATA,
				payload: res.data
			});
			dispatch({
				type: LOADING_HK_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearHongKongData = () => (dispatch) => {
	dispatch({
		type: CLEAR_HK_DATA
	});
}









export const clearComparisonData = () => (dispatch) => {
	dispatch({
		type: CLEAR_SF_DATA
	});
	dispatch({
		type: CLEAR_NYC_DATA
	});
	dispatch({
		type: CLEAR_HK_DATA
	});
}



export const getPlaceHolderDataSet = () => (dispatch) => {
	const config = {
		headers: {
			'Access-Control-Allow-Origin': '*',
			'Content-Type': 'multipart/form-data'
		}
	}

	axios
		.get(`https://cors-anywhere.herokuapp.com/https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_daily_reports/04-28-2020.csv`, config)
		.then(res => {
			dispatch({
				type: GET_PLACEHOLDER_DATA,
				payload: res.data
			});
			dispatch({
				type: LOADING_PLACEHOLDER_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

// Compare
export const getSFDataCompare = () => (dispatch, getState) => {
	axios 
		.get('https://data.sfgov.org/resource/tvq9-ec9w.json')
		.then(res => {
			dispatch({
				type: GET_SF_DATA_COMPARE,
				payload: res.data
			});
			dispatch({
				type: LOADING_SF_DATA_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearSFDataCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_SF_DATA_COMPARE
	});
}

export const getCityDataSFTesting = () => (dispatch, getState) => {
	axios 
		.get('https://data.sfgov.org/resource/nfpa-mg4g.json')
		.then(res => {
			dispatch({
				type: GET_CITY_DATA_SF_TESTING,
				payload: res.data
			});
			dispatch({
				type: LOADING_CITY_DATA_SF_TESTING,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCityDataSFTesting = () => (dispatch) => {
	dispatch({
		type: CLEAR_CITY_DATA_SF_TESTING
	});
}

export const getCityDataSFHospitalization = () => (dispatch, getState) => {
	axios 
		.get('https://data.sfgov.org/resource/nxjg-bhem.json')
		.then(res => {
			dispatch({
				type: GET_CITY_DATA_SF_HOSPITALIZATION,
				payload: res.data
			});
			dispatch({
				type: LOADING_CITY_DATA_SF_HOSPITALIZATION,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCityDataSFHospitalization = () => (dispatch) => {
	dispatch({
		type: CLEAR_CITY_DATA_SF_HOSPITALIZATION
	});
}

export const getNYCCaseDataCompare = () => (dispatch) => {
	axios
		.get(`https://health.data.ny.gov/resource/xdss-u53e.json?$where=county='Queens' OR county='Kings' OR county='Bronx' OR county='New York' OR county='Richmond'`)
		.then(res => {
			dispatch({
				type: GET_NYC_CASE_DATA_COMPARE,
				payload: res.data
			});
			dispatch({
				type: LOADING_NYC_CASE_DATA_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearNYCCaseDataCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_NYC_DATA_COMPARE
	});
}


export const getHongKongDataCompare = () => (dispatch) => {
	axios
		.get(`https://api.data.gov.hk/v2/filter?q=%7B%22resource%22%3A%22http%3A%2F%2Fwww.chp.gov.hk%2Ffiles%2Fmisc%2Fenhanced_sur_covid_19_eng.csv%22%2C%22section%22%3A1%2C%22format%22%3A%22json%22%7D`)
		.then(res => {
			dispatch({
				type: GET_HK_DATA_COMPARE,
				payload: res.data
			});
			dispatch({
				type: LOADING_HK_DATA_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearHongKongDataCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_HK_DATA_COMPARE
	});
}

export const clearComparisonDataCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_SF_DATA_COMPARE
	});
	dispatch({
		type: CLEAR_NYC_DATA_COMPARE
	});
	dispatch({
		type: CLEAR_HK_DATA_COMPARE
	});
}




export const getChicagoData = () => (dispatch) => {
	axios
		.get(`https://data.cityofchicago.org/resource/naz8-j4nc.json`)
		.then(res => {
			dispatch({
				type: GET_CHI_CASE_DATA,
				payload: res.data
			});
			dispatch({
				type: LOADING_CHI_CASE_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearChicagoData = () => (dispatch) => {
	dispatch({
		type: CLEAR_CHI_CASE_DATA
	})
}





export const getCHICaseDataCompare = () => (dispatch) => {
	axios
		.get(`https://data.cityofchicago.org/resource/naz8-j4nc.json`)
		.then(res => {
			dispatch({
				type: GET_CHI_CASE_DATA_COMPARE,
				payload: res.data
			});
			dispatch({
				type: LOADING_CHI_CASE_DATA_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCHICaseDataCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CHI_CASE_DATA_COMPARE
	})
}

















