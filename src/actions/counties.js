import axios from 'axios';

import { 
	GET_CASE_DATA_CA_COUNTY,
	LOADING_CASE_DATA_CA_COUNTY,
	CLEAR_CASE_DATA_CA_COUNTY,

	GET_HOSPITAL_DATA_CA_COUNTY,
	LOADING_HOSPITAL_DATA_CA_COUNTY,
	CLEAR_HOSPITAL_DATA_CA_COUNTY,

	GET_CASE_DATA_NY_COUNTY,
	LOADING_CASE_DATA_NY_COUNTY,
	CLEAR_CASE_DATA_NY_COUNTY,

	GET_FLORIDA_DATA,
	LOADING_FLORIDA_DATA,
	CLEAR_FLORIDA_DATA,

	GET_CASE_DATA_CT_COUNTY,
	LOADING_CASE_DATA_CT_COUNTY,
	CLEAR_CASE_DATA_CT_COUNTY,

	GET_CASE_DATA_CA_COUNTY_COMPARE,
	LOADING_CASE_DATA_CA_COUNTY_COMPARE,
	CLEAR_CASE_DATA_CA_COUNTY_COMPARE,

	GET_HOSPITAL_DATA_CA_COUNTY_COMPARE,
	LOADING_HOSPITAL_DATA_CA_COUNTY_COMPARE,
	CLEAR_HOSPITAL_DATA_CA_COUNTY_COMPARE,

	GET_TESTING_DATA_CA_COUNTY_COMPARE,
	LOADING_TESTING_DATA_CA_COUNTY_COMPARE,
	CLEAR_TESTING_DATA_CA_COUNTY_COMPARE,

	GET_CASE_DATA_CO_COUNTY_COMPARE,
	LOADING_CASE_DATA_CO_COUNTY_COMPARE,
	CLEAR_CASE_DATA_CO_COUNTY_COMPARE,

	GET_CASE_DATA_CT_COUNTY_COMPARE,
	LOADING_CASE_DATA_CT_COUNTY_COMPARE,
	CLEAR_CASE_DATA_CT_COUNTY_COMPARE,

	GET_CASE_DATA_GA_COUNTY_COMPARE,
	LOADING_CASE_DATA_GA_COUNTY_COMPARE,
	CLEAR_CASE_DATA_GA_COUNTY_COMPARE,

	GET_CASE_DATA_IN_COUNTY_COMPARE,
	LOADING_CASE_DATA_IN_COUNTY_COMPARE,
	CLEAR_CASE_DATA_IN_COUNTY_COMPARE,

	GET_CASE_DATA_MD_COUNTY_COMPARE,
	LOADING_CASE_DATA_MD_COUNTY_COMPARE,
	CLEAR_CASE_DATA_MD_COUNTY_COMPARE,

	GET_CASE_DATA_NY_COUNTY_COMPARE,
	LOADING_CASE_DATA_NY_COUNTY_COMPARE,
	CLEAR_CASE_DATA_NY_COUNTY_COMPARE,

	GET_CASE_DATA_VT_COUNTY_COMPARE,
	LOADING_CASE_DATA_VT_COUNTY_COMPARE,
	CLEAR_CASE_DATA_VT_COUNTY_COMPARE,

	GET_CASE_DATA_WI_COUNTY_COMPARE,
	LOADING_CASE_DATA_WI_COUNTY_COMPARE,
	CLEAR_CASE_DATA_WI_COUNTY_COMPARE
} from './types';

// Detail
export const getCaseDataCaCounty = (county) => (dispatch) => {
	axios 
		.get(`https://cors-anywhere.herokuapp.com/https://data.ca.gov/api/3/action/datastore_search_sql?sql=SELECT%20*%20from%20%22926fd08f-cc91-4828-af38-bd45de97f8c3%22%20WHERE%20%22county%22%20LIKE%20%27${county}%27`)
		.then(res => {
			dispatch({
				type: GET_CASE_DATA_CA_COUNTY,
				payload: res.data.result.records
			});
			dispatch({
				type: LOADING_CASE_DATA_CA_COUNTY,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataCaCounty = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_CA_COUNTY
	});
}

export const getHospitalDataCaCounty = (county) => (dispatch) => {
	axios 
		.get(`https://cors-anywhere.herokuapp.com/https://data.ca.gov/api/3/action/datastore_search_sql?sql=SELECT%20*%20from%20%2242d33765-20fd-44b8-a978-b083b7542225%22%20WHERE%20%22county%22%20LIKE%20%27${county}%27`)
		.then(res => {
			dispatch({
				type: GET_HOSPITAL_DATA_CA_COUNTY,
				payload: res.data.result.records
			});
			dispatch({
				type: LOADING_HOSPITAL_DATA_CA_COUNTY,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearHospitalDataCaCounty = () => (dispatch) => {
	dispatch({
		type: CLEAR_HOSPITAL_DATA_CA_COUNTY
	});
}

export const getCaseDataNyCounty = (county) => (dispatch) => {
	axios 
		.get(`https://health.data.ny.gov/resource/xdss-u53e.json?$where=county='${county}'`)
		.then(res => {
			dispatch({
				type: GET_CASE_DATA_NY_COUNTY,
				payload: res.data
			});
			dispatch({
				type: LOADING_CASE_DATA_NY_COUNTY,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataNyCounty = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_NY_COUNTY
	});
}

export const getFloridaData = () => (dispatch) => {
	axios
		// .get(`https://services1.arcgis.com/CY1LXxl9zlJeBuRZ/arcgis/rest/services/Florida_COVID19_Case_Line_Data_NEW/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json`)
		// .get(`https://services1.arcgis.com/CY1LXxl9zlJeBuRZ/arcgis/rest/services/Florida_COVID19_Case_Line_Data_NEW/FeatureServer/0/query?where=County%20%3D%20%27ORANGE%27&outFields=*&outSR=4326&f=json`)
		// .get(`https://services9.arcgis.com/pptP3j8cINwF8DAR/ArcGIS/rest/services/FCA_Daily_and_Cumulative_Cases_and_Deaths/FeatureServer/0/query?where=County='Orange'&outFields=*&outSR=4326&f=json`)
		// .get(`https://services9.arcgis.com/pptP3j8cINwF8DAR/ArcGIS/rest/services/Florida_COVID_19_Cases_by_County/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json`)
		.get(`https://services9.arcgis.com/pptP3j8cINwF8DAR/ArcGIS/rest/services/FCA_Cases_and_Deaths_by_County_Over_Time/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json`)
		.then(res => {
			// console.log('FLorida Results: ', res);
			dispatch({
				type: GET_FLORIDA_DATA,
				payload: res
			});
			dispatch({
				type: LOADING_FLORIDA_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearFloridaData = () => (dispatch) => {
	dispatch({
		type: CLEAR_FLORIDA_DATA
	});
}

export const getCaseDataCTCounty = (county) => (dispatch) => {
	axios
		.get(`https://data.ct.gov/resource/bfnu-rgqt.json?$where=county='${county}'`)
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_CASE_DATA_CT_COUNTY,
				payload: res.data
			});
			dispatch({
				type: LOADING_CASE_DATA_CT_COUNTY,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataCTCounty = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_CT_COUNTY
	})
}













// Compare
export const getCaseDataCaCountyCompare = (county) => (dispatch) => {
	axios 
		.get(`https://cors-anywhere.herokuapp.com/https://data.ca.gov/api/3/action/datastore_search_sql?sql=SELECT%20*%20from%20%22926fd08f-cc91-4828-af38-bd45de97f8c3%22%20WHERE%20%22county%22%20LIKE%20%27${county}%27`)
		.then(res => {
			dispatch({
				type: GET_CASE_DATA_CA_COUNTY_COMPARE,
				payload: res.data.result.records
			});
			dispatch({
				type: LOADING_CASE_DATA_CA_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataCaCountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_CA_COUNTY_COMPARE
	});
}

export const getHospitalDataCaCountyCompare = (county) => (dispatch) => {
	axios 
		.get(`https://cors-anywhere.herokuapp.com/https://data.ca.gov/api/3/action/datastore_search_sql?sql=SELECT%20*%20from%20%2242d33765-20fd-44b8-a978-b083b7542225%22%20WHERE%20%22county%22%20LIKE%20%27${county}%27`)
		.then(res => {
			dispatch({
				type: GET_HOSPITAL_DATA_CA_COUNTY_COMPARE,
				payload: res.data.result.records
			});
			dispatch({
				type: LOADING_HOSPITAL_DATA_CA_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearHospitalDataCaCountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_HOSPITAL_DATA_CA_COUNTY_COMPARE
	});
}

export const getTestingDataCaCountyCompare = (county) => (dispatch) => {
	axios 
		.get(`https://cors-anywhere.herokuapp.com/https://data.ca.gov/api/3/action/datastore_search_sql?sql=SELECT%20*%20from%20%2242d33765-20fd-44b8-a978-b083b7542225%22%20WHERE%20%22county%22%20LIKE%20%27${county}%27`)
		.then(res => {
			dispatch({
				type: GET_TESTING_DATA_CA_COUNTY_COMPARE,
				payload: res.data.result.records
			});
			dispatch({
				type: LOADING_TESTING_DATA_CA_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearTestingDataCaCountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_TESTING_DATA_CA_COUNTY_COMPARE
	});
}

export const getCaseDataNyCountyCompare = (county) => (dispatch) => {
	axios 
		.get(`https://health.data.ny.gov/resource/xdss-u53e.json?$where=county='${county}'`)
		.then(res => {
			dispatch({
				type: GET_CASE_DATA_NY_COUNTY_COMPARE,
				payload: res.data
			});
			dispatch({
				type: LOADING_CASE_DATA_NY_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataNyCountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_NY_COUNTY_COMPARE
	});
}

export const getCaseDataCOCountyCompare = (county) => (dispatch) => {
	axios
		.get(`https://services3.arcgis.com/66aUo8zsujfVXRIT/arcgis/rest/services/colorado_covid19_county_statistics_cumulative/FeatureServer/0/query?where=COUNTY%20%3D%20'${county}'&outFields=*&outSR=4326&f=json`)
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_CASE_DATA_CO_COUNTY_COMPARE,
				payload: res.data.features
			});
			dispatch({
				type: LOADING_CASE_DATA_CO_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataCOCountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_CO_COUNTY_COMPARE
	})
}

export const getCaseDataCTCountyCompare = (county) => (dispatch) => {
	axios
		.get(`https://data.ct.gov/resource/bfnu-rgqt.json?$where=county='${county}'`)
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_CASE_DATA_CT_COUNTY_COMPARE,
				payload: res.data
			});
			dispatch({
				type: LOADING_CASE_DATA_CT_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataCTCountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_CT_COUNTY_COMPARE
	})
}

export const getCaseDataGACountyCompare = (county) => (dispatch) => {
	axios
		.get(`https://services1.arcgis.com/2iUE8l8JKrP2tygQ/arcgis/rest/services/COVID19_County_Archive/FeatureServer/0/query?where=NAME%20%3D%20'${county}'&outFields=*&outSR=4326&f=json`)
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_CASE_DATA_GA_COUNTY_COMPARE,
				payload: res.data.features
			});
			dispatch({
				type: LOADING_CASE_DATA_GA_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataGACountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_GA_COUNTY_COMPARE
	})
}

export const getCaseDataINCountyCompare = (county) => (dispatch) => {
	axios
		.get(`https://cors-anywhere.herokuapp.com/https://hub.mph.in.gov/api/3/action/datastore_search?q=${county}&resource_id=46b310b9-2f29-4a51-90dc-3886d9cf4ac1`)
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_CASE_DATA_IN_COUNTY_COMPARE,
				payload: res.data.result.records
			});
			dispatch({
				type: LOADING_CASE_DATA_IN_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataINCountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_IN_COUNTY_COMPARE
	})
}

export const getCaseDataMDCountyCompare = () => (dispatch) => {
	axios
		.get(`https://services.arcgis.com/njFNhDsUCentVYJW/arcgis/rest/services/MDCOVID19_CasesByCounty/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json`)
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_CASE_DATA_MD_COUNTY_COMPARE,
				payload: res.data.features
			});
			dispatch({
				type: LOADING_CASE_DATA_MD_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataMDCountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_MD_COUNTY_COMPARE
	})
}

export const getCaseDataVTCountyCompare = (county) => (dispatch) => {
	var countyName = county + ' County';
	axios
		.get(`https://services1.arcgis.com/BkFxaEFNwHqX3tAw/arcgis/rest/services/VIEW_EPI_CountyDailyCountTS_PUBLIC/FeatureServer/0/query?where=map_county%20%3D%20'${county}'&outFields=*&outSR=4326&f=json`)
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_CASE_DATA_VT_COUNTY_COMPARE,
				payload: res.data.features
			});
			dispatch({
				type: LOADING_CASE_DATA_VT_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataVTCountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_VT_COUNTY_COMPARE
	})
}

export const getCaseDataWICountyCompare = (county) => (dispatch) => {
	var countyName = county + ' County';
	axios
		.get(`https://services1.arcgis.com/ISZ89Z51ft1G16OK/ArcGIS/rest/services/COVID19_WI/FeatureServer/10/query?where=NAME%20%3D%20'${county}'&outFields=*&outSR=4326&f=json`)
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_CASE_DATA_WI_COUNTY_COMPARE,
				payload: res.data.features
			});
			dispatch({
				type: LOADING_CASE_DATA_WI_COUNTY_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearCaseDataWICountyCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_CASE_DATA_WI_COUNTY_COMPARE
	})
}











