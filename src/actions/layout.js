import { 
	CHANGE_HEADER_CITIES,
	CHANGE_HEADER_COUNTIES,
	CHANGE_HEADER_STATES,
	CHANGE_HEADER_COUNTRIES,
	CHANGE_HEADER_COMPARE,
	CHANGE_HEADER_DETAIL,
	CHANGE_HEADER_ABOUT
} from './types';

export const changeHeaderCities = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_CITIES,
		payload: true
	});
}

export const changeHeaderCounties = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_COUNTIES,
		payload: true
	});
}

export const changeHeaderStates = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_STATES,
		payload: true
	});
}

export const changeHeaderCountries = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_COUNTRIES,
		payload: true
	});
}

export const changeHeaderCompare = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_COMPARE,
		payload: true
	});
}

export const changeHeaderDetail = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_DETAIL,
		payload: true
	});
}

export const changeHeaderAbout = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_ABOUT,
		payload: true
	});
}

export const resetHeaderCities = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_CITIES,
		payload: false
	});
}

export const resetHeaderCounties = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_COUNTIES,
		payload: false
	});
}

export const resetHeaderStates = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_STATES,
		payload: false
	});
}

export const resetHeaderCountries = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_COUNTRIES,
		payload: false
	});
}

export const resetHeaderCompare = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_COMPARE,
		payload: false
	});
}

export const resetHeaderDetail = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_DETAIL,
		payload: false
	});
}

export const resetHeaderAbout = () => (dispatch) => {
	dispatch({
		type: CHANGE_HEADER_ABOUT,
		payload: false
	});
}