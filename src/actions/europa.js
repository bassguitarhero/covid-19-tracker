import axios from 'axios';

import { 
	GET_WORLD_WIDE_EUROPA_DATA,
	LOADING_WORLD_WIDE_EUROPA_DATA,
	CLEAR_WORLD_WIDE_EUROPA_DATA,

	GET_WORLD_WIDE_EUROPA_DATA_COMPARE,
	LOADING_WORLD_WIDE_EUROPA_DATA_COMPARE,
	CLEAR_WORLD_WIDE_EUROPA_DATA_COMPARE
} from './types';


// Detail
export const getWorldWideEuropaData = () => (dispatch) => {
	axios
		.get('https://cors-anywhere.herokuapp.com/https://opendata.ecdc.europa.eu/covid19/casedistribution/json/')
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_WORLD_WIDE_EUROPA_DATA,
				payload: res.data.records
			});
			dispatch({
				type: LOADING_WORLD_WIDE_EUROPA_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearWorldWideEuropaData = () => (dispatch) => {
	dispatch({
		type: CLEAR_WORLD_WIDE_EUROPA_DATA
	})
}

// Compare
export const getWorldWideEuropaDataCompare = () => (dispatch) => {
	axios
		.get('https://cors-anywhere.herokuapp.com/https://opendata.ecdc.europa.eu/covid19/casedistribution/json/')
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_WORLD_WIDE_EUROPA_DATA_COMPARE,
				payload: res.data.records
			});
			dispatch({
				type: LOADING_WORLD_WIDE_EUROPA_DATA_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearWorldWideEuropaDataCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_WORLD_WIDE_EUROPA_DATA_COMPARE
	})
}