import axios from 'axios';
// import { createMessage, returnErrors } from "./messages";

import { 
	DATA_CT_SINGLE_STATE_DAILY,
	LOADING_DATA_CS_SINGLE_STATE_DAILY, 

	CLEAR_COVID_TRACKER_DATA,

	GET_SINGLE_STATE_COMPARE,
	LOADING_SINGLE_STATE_COMPARE, 
} from './types';

// Detail
export const getCovidTrackerSingleStateDaily = (abbreviation) => (dispatch, getState) => {
	axios 
		.get(`https://api.covidtracking.com/v1/states/${abbreviation}/daily.json`)
		// .get(`https://cors-anywhere.herokuapp.com/https://covidtracking.com/api/states/daily?state=${abbreviation}`)
		.then(res => {
			// console.log('Results: ', res);
			dispatch({
				type: DATA_CT_SINGLE_STATE_DAILY,
				payload: res.data
			});
			dispatch({
				type: LOADING_DATA_CS_SINGLE_STATE_DAILY,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const resetSingleStateDaily = () => (dispatch) => {
	dispatch({
		type: DATA_CT_SINGLE_STATE_DAILY,
		payload: null
	});
	dispatch({
		type: LOADING_DATA_CS_SINGLE_STATE_DAILY,
		payload: true
	});
}

export const finishLoadingChartDataState = () => (dispatch) => {
	dispatch({
		type: LOADING_DATA_CS_SINGLE_STATE_DAILY,
		payload: true
	});
}

// Compare
export const getCovidTrackerSingleStateDailyCompare = (abbreviation) => (dispatch, getState) => {
	axios 
		.get(`https://api.covidtracking.com/v1/states/${abbreviation}/daily.json`)
		// .get(`https://cors-anywhere.herokuapp.com/https://covidtracking.com/api/states/daily?state=${abbreviation}`)
		.then(res => {
			// console.log('Results: ', res);
			dispatch({
				type: GET_SINGLE_STATE_COMPARE,
				payload: res.data
			});
			dispatch({
				type: LOADING_SINGLE_STATE_COMPARE,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const resetSingleStateDailyCompare = () => (dispatch) => {
	dispatch({
		type: GET_SINGLE_STATE_COMPARE,
		payload: null
	});
	dispatch({
		type: LOADING_SINGLE_STATE_COMPARE,
		payload: true
	});
}











