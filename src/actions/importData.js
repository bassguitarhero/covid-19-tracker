import { 
	GET_COLORS_LIST,
	GET_COUNTIES_CA_LIST,
	GET_COUNTIES_CO_LIST, 
	GET_COUNTIES_CT_LIST, 
	GET_COUNTIES_GA_LIST,
	GET_COUNTIES_IN_LIST, 
	GET_COUNTIES_MD_LIST, 
	GET_COUNTIES_NY_LIST,
	GET_COUNTIES_FL_LIST, 
	GET_COUNTIES_VT_LIST,
	GET_COUNTIES_WI_LIST, 
	GET_STATES_LIST,
	CLEAR_IMPORT,

	GET_COLORS_LIST_CITIES_CHI,
	GET_COLORS_LIST_COUNTIES_CA,
	GET_COLORS_LIST_COUNTIES_CT,
	GET_COLORS_LIST_COUNTIES_MD,
	GET_COLORS_LIST_COUNTIES_NY,
	GET_COLORS_LIST_COUNTIES_FL,
	GET_COLORS_LIST_COUNTIES_COMPARE,
	GET_COLORS_LIST_STATES,
	GET_COLORS_LIST_COUNTRIES,

	GET_COUNTIES_CA_LIST_CA,
	GET_COUNTIES_CA_LIST_COMPARE,
	GET_COUNTIES_CT_LIST_CT,
	GET_COUNTIES_CT_LIST_COMPARE,
	GET_COUNTIES_MD_LIST_MD,
	GET_COUNTIES_MD_LIST_COMPARE,
	GET_COUNTIES_NY_LIST_NY,
	GET_COUNTIES_NY_LIST_COMPARE,
	GET_COUNTIES_FL_LIST_FL,
	GET_COUNTIES_FL_LIST_COMPARE,

	GET_STATES_LIST_STATES,

	CLEAR_IMPORT_COUNTIES_CA, 
	CLEAR_IMPORT_COUNTIES_CT, 
	CLEAR_IMPORT_COUNTIES_MD, 
	CLEAR_IMPORT_COUNTIES_NY, 
	CLEAR_IMPORT_COUNTIES_FL, 
	CLEAR_IMPORT_COUNTIES_COMPARE, 
	CLEAR_IMPORT_STATES, 
	CLEAR_IMPORT_COUNTRIES
} from './types';

const colorsList = [
	"#D6E9C6", "#FAEBCC", "#EBCCD1", "#d1b986", "#9195cf", "#c969b1", "#666666", "#32a852",
	"#5ab337", "#4ea832", "#3cb1b5", "#5db0b3", "#5fb38d", "#7532a8", "#1cad6c", "#a88132",
	"#1d7578", "#467173", "#333444", "#92a63c", "#92b305", "#b007b3", "#ad111e", "#9e474e",
	"#9e654a", "#a68451", "#e3b674", "#de8c14", "#7a14e0", "#9a53e0", "#341652", "#7c7980",
	"#46414d", "#080cd1", "#0ab2cc", "#4baebd", "#0aa63e", "#137032", "#396e4b", "#648a45",
	"#386b0f", "#11694e", "#3d5704", "#727a5e", "#857a61", "#806a33", "#c29117", "#b8ab8d",
	"#bd919b", "#824d58", "#87142b", "#c2116f", "#4f59e8", "#878bc7", "#60bbdb", "#5dd487"
];

const countiesCaliforniaList = [
	{
		name: "Alameda",
		population: 1671329
	},
	{
		name: "Alpine",
		population: 1129
	},
	{
		name: "Amador",
		population: 39752
	},
	{
		name: "Butte",
		population: 219186,
		active: false
	},
	{
		name: "Calaveras",
		population: 45905
	},
	{
		name: "Colusa",
		population: 21547
	},
	{
		name: "Contra Costa",
		population: 1153526
	},
	{
		name: "Del Norte",
		population: 27812
	},
	{
		name: "El Dorado",
		population: 192843
	},
	{
		name: "Fresno",
		population: 999101
	},
	{
		name: "Glenn",
		population: 28393
	},
	{
		name: "Humboldt",
		population: 135558
	},
	{
		name: "Imperial",
		population: 181215
	},
	{
		name: "Inyo",
		population: 18039
	},
	{
		name: "Kern",
		population: 900202
	},
	{
		name: "Kings",
		population: 152940
	},
	{
		name: "Lake",
		population: 64386
	},
	{
		name: "Lassen",
		population: 30573
	},
	{
		name: "Los Angeles",
		population: 10039107
	},
	{
		name: "Madera",
		population: 157327
	},
	{
		name: "Marin",
		population: 258826
	},
	{
		name: "Mariposa",
		population: 17203
	},
	{
		name: "Mendocino",
		population: 86749
	},
	{
		name: "Merced",
		population: 277680
	},
	{
		name: "Modoc",
		population: 8841
	},
	{
		name: "Mono",
		population: 14444
	},
	{
		name: "Monterey",
		population: 434061
	},
	{
		name: "Napa",
		population: 137744
	},
	{
		name: "Nevada",
		population: 99755
	},
	{
		name: "Orange",
		population: 3175692
	},
	{
		name: "Placer",
		population: 398329
	},
	{
		name: "Plumas",
		population: 18807
	},
	{
		name: "Riverside",
		population: 2470546
	},
	{
		name: "Sacramento",
		population: 1552058
	},
	{
		name: "San Benito",
		population: 62808
	},
	{
		name: "San Bernardino",
		population: 2180085
	},
	{
		name: "San Diego",
		population: 3338330
	},
	{
		name: "San Francisco",
		population: 881549
	},
	{
		name: "San Joaquin",
		population: 762148
	},
	{
		name: "San Luis Obispo",
		population: 283111
	},
	{
		name: "San Mateo",
		population: 766573
	},
	{
		name: "Santa Barbara",
		population: 446499
	},
	{
		name: "Santa Clara",
		population: 1927852
	},
	{
		name: "Santa Cruz",
		population: 273213
	},
	{
		name: "Shasta",
		population: 180080
	},
	{
		name: "Sierra",
		population: 3005
	},
	{
		name: "Siskiyou",
		population: 43539
	},
	{
		name: "Solano",
		population: 447643
	},
	{
		name: "Sonoma",
		population: 494336
	},
	{
		name: "Stanislaus",
		population: 550660
	},
	{
		name: "Sutter",
		population: 96971
	},
	{
		name: "Tehama",
		population: 65084
	},
	{
		name: "Trinity",
		population: 12285
	},
	{
		name: "Tulare",
		population: 466195
	},
	{
		name: "Tuolomne",
		population: 54478
	},
	{
		name: "Ventura",
		population: 846006
	},
	{
		name: "Yolo",
		population: 220500
	},
	{
		name: "Yuba",
		population: 78668
	}
];

const countiesColoradoList = [
	{
		name: "Adams",
		population: 441603
	},
	{
		name: "Alamosa",
		population: 15445
	},
	{
		name: "Arapahoe",
		population: 572003
	},
	{
		name: "Archuleta",
		population: 12084
	},
	{
		name: "Baca",
		population: 3788
	},
	{
		name: "Bent",
		population: 6499
	},
	{
		name: "Boulder",
		population: 294567
	},
	{
		name: "Broomfield",
		population: 55889
	},
	{
		name: "Chaffee",
		population: 17809
	},
	{
		name: "Cheyenne",
		population: 1836
	},
	{
		name: "Clear Creek",
		population: 9088
	},
	{
		name: "Conejos",
		population: 8256
	},
	{
		name: "Costilla",
		population: 3524
	},
	{
		name: "Crowley",
		population: 5823
	},
	{
		name: "Custer",
		population: 4255
	},
	{
		name: "Delta",
		population: 30952
	},
	{
		name: "Denver",
		population: 600158
	},
	{
		name: "Dolores",
		population: 2064
	},
	{
		name: "Douglas",
		population: 285465
	},
	{
		name: "Eagle",
		population: 52197
	},
	{
		name: "El Paso",
		population: 622263
	},
	{
		name: "Elbert",
		population: 23086
	},
	{
		name: "Fremont",
		population: 46824
	},
	{
		name: "Garfield",
		population: 56389
	},
	{
		name: "Gilpin",
		population: 5441
	},
	{
		name: "Grand",
		population: 14843
	},
	{
		name: "Gunnison",
		population: 15324
	},
	{
		name: "Hinsdale",
		population: 843
	},
	{
		name: "Huerfano",
		population: 6711
	},
	{
		name: "Jackson",
		population: 1394
	},
	{
		name: "Jefferson",
		population: 534543
	},
	{
		name: "Kiowa",
		population: 1398
	},
	{
		name: "Kit Caron",
		population: 8270
	},
	{
		name: "La Plata",
		population: 51334
	},
	{
		name: "Lake",
		population: 7310
	},
	{
		name: "Larimer",
		population: 299630
	},
	{
		name: "Las Animas",
		population: 15507
	},
	{
		name: "Lincoln",
		population: 5467
	},
	{
		name: "Logan",
		population: 22709
	},
	{
		name: "Mesa",
		population: 146723
	},
	{
		name: "Mineral",
		population: 712
	},
	{
		name: "Moffat",
		population: 13795
	},
	{
		name: "Montezuma",
		population: 25535
	},
	{
		name: "Montrose",
		population: 41276
	},
	{
		name: "Morgan",
		population: 28159
	},
	{
		name: "Otero",
		population: 18831
	},
	{
		name: "Ouray",
		population: 4436
	},
	{
		name: "Park",
		population: 16206
	},
	{
		name: "Phillips",
		population: 4442
	},
	{
		name: "Pitkin",
		population: 17148
	},
	{
		name: "Prowers",
		population: 12551
	},
	{
		name: "Pueblo",
		population: 159063
	},
	{
		name: "Rio Blanco",
		population: 6666
	},
	{
		name: "Rio Grande",
		population: 11982
	},
	{
		name: "Routt",
		population: 23509
	},
	{
		name: "Saguache",
		population: 6108
	},
	{
		name: "San Juan",
		population: 699
	},
	{
		name: "San Miguel",
		population: 7359
	},
	{
		name: "Sedgwick",
		population: 2379
	},
	{
		name: "Summit",
		population: 27994
	},
	{
		name: "Teller",
		population: 23350
	},
	{
		name: "Washington",
		population: 4814
	},
	{
		name: "Weld",
		population: 252825
	},
	{
		name: "Yuma",
		population: 10043
	}
];

const countiesConnecticutList = [
	{
		name: "Fairfield",
		population: 916829
	},
	{
		name: "Hartford",
		population: 894014
	},
	{
		name: "Litchfield",
		population: 189927
	},
	{
		name: "Middlesex",
		population: 165676
	},
	{
		name: "New Haven",
		population: 862477
	},
	{
		name: "New London",
		population: 274055
	},
	{
		name: "Tolland",
		population: 152691
	},
	{
		name: "Windham",
		population: 118428
	}
];

const countiesGeorgiaList = [
	{
		name: "Appling",
		population: 18368
	},
	{
		name: "Atkinson",
		population: 8284
	},
	{
		name: "Bacon",
		population: 11198
	},
	{
		name: "Baker",
		population: 3366
	},
	{
		name: "Baldwin",
		population: 46367
	},
	{
		name: "Banks",
		population: 18316
	},
	{
		name: "Barrow",
		population: 70169
	},
	{
		name: "Bartow",
		population: 100661
	},
	{
		name: "Ben Hill",
		population: 17538
	},
	{
		name: "Berrien",
		population: 19041
	},
	{
		name: "Bibb",
		population: 156462
	},
	{
		name: "Bleckley",
		population: 12913
	},
	{
		name: "Brantley",
		population: 18587
	},
	{
		name: "Brooks",
		population: 15403
	},
	{
		name: "Bryan",
		population: 32214
	},
	{
		name: "Bulloch",
		population: 72694
	},
	{
		name: "Burke",
		population: 23125
	},
	{
		name: "Butts",
		population: 23524
	},
	{
		name: "Calhoun",
		population: 6504
	},
	{
		name: "Camden",
		population: 51402
	},
	{
		name: "Candler",
		population: 11117
	},
	{
		name: "Carroll",
		population: 111580
	},
	{
		name: "Catoosa",
		population: 65046
	},
	{
		name: "Charlton",
		population: 13295
	},
	{
		name: "Chatham",
		population: 276434
	},
	{
		name: "Chattahoochee",
		population: 13037
	},
	{
		name: "Chattooga",
		population: 25725
	},
	{
		name: "Cherokee",
		population: 221315
	},
	{
		name: "Clarke",
		population: 120266
	},
	{
		name: "Clay",
		population: 3116
	},
	{
		name: "Clayton",
		population: 265888
	},
	{
		name: "Clinch",
		population: 6718
	},
	{
		name: "Cobb",
		population: 707422
	},
	{
		name: "Coffee",
		population: 43170
	},
	{
		name: "Colquitt",
		population: 46137
	},
	{
		name: "Columbia",
		population: 131627
	},
	{
		name: "Cook",
		population: 16923
	},
	{
		name: "Coweta",
		population: 130929
	},
	{
		name: "Crawford",
		population: 12600
	},
	{
		name: "Crisp",
		population: 23606
	},
	{
		name: "Dade",
		population: 16490
	},
	{
		name: "Dawson",
		population: 22422
	},
	{
		name: "Decatur",
		population: 27509
	},
	{
		name: "DeKalb",
		population: 707089
	},
	{
		name: "Dodge",
		population: 21329
	},
	{
		name: "Dooly",
		population: 14318
	},
	{
		name: "Dougherty",
		population: 94501
	},
	{
		name: "Douglas",
		population: 133971
	},
	{
		name: "Early",
		population: 10594
	},
	{
		name: "Echols",
		population: 3988
	},
	{
		name: "Effingham",
		population: 53293
	},
	{
		name: "Elbert",
		population: 19684
	},
	{
		name: "Emanuel",
		population: 22898
	},
	{
		name: "Evans",
		population: 10689
	},
	{
		name: "Fannin",
		population: 23492
	},
	{
		name: "Fayette",
		population: 107524
	},
	{
		name: "Floyd",
		population: 96177
	},
	{
		name: "Forsyth",
		population: 187928
	},
	{
		name: "Franklin",
		population: 21894
	},
	{
		name: "Fulton",
		population: 1041423
	},
	{
		name: "Gilmer",
		population: 28190
	},
	{
		name: "Glascock",
		population: 3142
	},
	{
		name: "Glynn",
		population: 81022
	},
	{
		name: "Gordon",
		population: 55766
	},
	{
		name: "Grady",
		population: 25440
	},
	{
		name: "Greene",
		population: 16092
	},
	{
		name: "Gwinnett",
		population: 842046
	},
	{
		name: "Habersham",
		population: 43520
	},
	{
		name: "Hall",
		population: 185416
	},
	{
		name: "Hancock",
		population: 8996
	},
	{
		name: "Haralson",
		population: 28400
	},
	{
		name: "Harris",
		population: 32550
	},
	{
		name: "Hart",
		population: 25518
	},
	{
		name: "Heard",
		population: 11633
	},
	{
		name: "Henry",
		population: 209053
	},
	{
		name: "Houston",
		population: 146136
	},
	{
		name: "Irwin",
		population: 9600
	},
	{
		name: "Jackson",
		population: 60571
	},
	{
		name: "Jasper",
		population: 13630
	},
	{
		name: "Jeff Davis",
		population: 15156
	},
	{
		name: "Jefferson",
		population: 16432
	},
	{
		name: "Jenkins",
		population: 9213
	},
	{
		name: "Johnson",
		population: 9897
	},
	{
		name: "Jones",
		population: 28577
	},
	{
		name: "Lamar",
		population: 18057
	},
	{
		name: "Lanier",
		population: 10400
	},
	{
		name: "Laurens",
		population: 48041
	},
	{
		name: "Lee",
		population: 28746
	},
	{
		name: "Liberty",
		population: 65471
	},
	{
		name: "Lincoln",
		population: 7737
	},
	{
		name: "Long",
		population: 16048
	},
	{
		name: "Lowndes",
		population: 114552
	},
	{
		name: "Lumpkin",
		population: 30611
	},
	{
		name: "Macon",
		population: 14263
	},
	{
		name: "Madison",
		population: 27922
	},
	{
		name: "Marion",
		population: 8711
	},
	{
		name: "McDuffie",
		population: 21663
	},
	{
		name: "McIntosh",
		population: 13839
	},
	{
		name: "Meriwether",
		population: 21273
	},
	{
		name: "Miller",
		population: 5969
	},
	{
		name: "Mitchell",
		population: 23144
	},
	{
		name: "Monroe",
		population: 26637
	},
	{
		name: "Montgomery",
		population: 8913
	},
	{
		name: "Morgan",
		population: 17881
	},
	{
		name: "Murray",
		population: 39392
	},
	{
		name: "Muscogee",
		population: 198413
	},
	{
		name: "Newton",
		population: 101505
	},
	{
		name: "Oconee",
		population: 33619
	},
	{
		name: "Oglethorpe",
		population: 14618
	},
	{
		name: "Paulding",
		population: 144800
	},
	{
		name: "Peach",
		population: 27622
	},
	{
		name: "Pickens",
		population: 29268
	},
	{
		name: "Pierce",
		population: 18844
	},
	{
		name: "Pike",
		population: 17810
	},
	{
		name: "Polk",
		population: 41188
	},
	{
		name: "Polaski",
		population: 11729
	},
	{
		name: "Putnam",
		population: 21198
	},
	{
		name: "Quitman",
		population: 2404
	},
	{
		name: "Rabun",
		population: 16297
	},
	{
		name: "Randolph",
		population: 7327
	},
	{
		name: "Richmond",
		population: 202587
	},
	{
		name: "Rockdale",
		population: 85820
	},
	{
		name: "Schley",
		population: 4990
	},
	{
		name: "Screven",
		population: 14202
	},
	{
		name: "Seminole",
		population: 8947
	},
	{
		name: "Spalding",
		population: 63865
	},
	{
		name: "Stephens",
		population: 25891
	},
	{
		name: "Stewart",
		population: 6042
	},
	{
		name: "Sumter",
		population: 31554
	},
	{
		name: "Talbot",
		population: 6517
	},
	{
		name: "Taliaferro",
		population: 1680
	},
	{
		name: "Tattnall",
		population: 25384
	},
	{
		name: "Taylor",
		population: 8420
	},
	{
		name: "Telfair",
		population: 16349
	},
	{
		name: "Terrell",
		population: 9045
	},
	{
		name: "Thomas",
		population: 44724
	},
	{
		name: "Tift",
		population: 41064
	},
	{
		name: "Toombs",
		population: 27315
	},
	{
		name: "Towns",
		population: 10495
	},
	{
		name: "Treutlen",
		population: 6769
	},
	{
		name: "Troup",
		population: 68468
	},
	{
		name: "Turner",
		population: 8410
	},
	{
		name: "Twiggs",
		population: 8447
	},
	{
		name: "Union",
		population: 21451
	},
	{
		name: "Upson",
		population: 26630
	},
	{
		name: "Walker",
		population: 68094
	},
	{
		name: "Walton",
		population: 84575
	},
	{
		name: "Ware",
		population: 35821
	},
	{
		name: "Warren",
		population: 5578
	},
	{
		name: "Washington",
		population: 20879
	},
	{
		name: "Wayne",
		population: 30305
	},
	{
		name: "Webster",
		population: 2793
	},
	{
		name: "Wheeler",
		population: 7888
	},
	{
		name: "White",
		population: 27556
	},
	{
		name: "Whitfield",
		population: 103359
	},
	{
		name: "Wilcox",
		population: 9068
	},
	{
		name: "Wilkes",
		population: 10076
	},
	{
		name: "Wilkinson",
		population: 9577
	},
	{
		name: "Worth",
		population: 21741
	}
];

const countiesIndianaList = [
	{
		name: "Adams",
		population: 34387
	},
	{
		name: "Allen",
		population: 355329
	},
	{
		name: "Bartholomew",
		population: 76794
	},
	{
		name: "Benton",
		population: 8854
	},
	{
		name: "Blackford",
		population: 12766
	},
	{
		name: "Boone",
		population: 56640
	},
	{
		name: "Brown",
		population: 15242
	},
	{
		name: "Carroll",
		population: 20165
	},
	{
		name: "Cass",
		population: 38966
	},
	{
		name: "Clark",
		population: 110232
	},
	{
		name: "Clay",
		population: 26890
	},
	{
		name: "Clinton",
		population: 33244
	},
	{
		name: "Crawford",
		population: 10713
	},
	{
		name: "Daviess",
		population: 31648
	},
	{
		name: "Dearborn",
		population: 50047
	},
	{
		name: "Decatur",
		population: 25470
	},
	{
		name: "DeKalb",
		population: 40285
	},
	{
		name: "Delaware",
		population: 117671
	},
	{
		name: "Dubois",
		population: 41889
	},
	{
		name: "Elkhart",
		population: 197559
	},
	{
		name: "Fayette",
		population: 24277
	},
	{
		name: "Floyd",
		population: 74578
	},
	{
		name: "Fountain",
		population: 17240
	},
	{
		name: "Franklin",
		population: 23087
	},
	{
		name: "Fulton",
		population: 20836
	},
	{
		name: "Gibson",
		population: 33503
	},
	{
		name: "Grant",
		population: 70061
	},
	{
		name: "Greene",
		population: 33165
	},
	{
		name: "Hamilton",
		population: 274569
	},
	{
		name: "Hancock",
		population: 70002
	},
	{
		name: "Harrison",
		population: 39364
	},
	{
		name: "Hendricks",
		population: 145488
	},
	{
		name: "Henry",
		population: 49462
	},
	{
		name: "Howard",
		population: 82752
	},
	{
		name: "Huntington",
		population: 37124
	},
	{
		name: "Jackson",
		population: 42367
	},
	{
		name: "Jasper",
		population: 33478
	},
	{
		name: "Jay",
		population: 21253
	},
	{
		name: "Jefferson",
		population: 32428
	},
	{
		name: "Jennings",
		population: 28525
	},
	{
		name: "Johnson",
		population: 139654
	},
	{
		name: "Knox",
		population: 38440
	},
	{
		name: "Kosciusko",
		population: 77358
	},
	{
		name: "LaGrange",
		population: 37128
	},
	{
		name: "Lake",
		population: 496005
	},
	{
		name: "LaPorte",
		population: 111467
	},
	{
		name: "Lawrence",
		population: 46134
	},
	{
		name: "Madison",
		population: 131636
	},
	{
		name: "Marion",
		population: 903393
	},
	{
		name: "Marshall",
		population: 47051
	},
	{
		name: "Martin",
		population: 10344
	},
	{
		name: "Miami",
		population: 36903
	},
	{
		name: "Monroe",
		population: 137974
	},
	{
		name: "Montgomery",
		population: 38124
	},
	{
		name: "Morgan",
		population: 68894
	},
	{
		name: "Newton",
		population: 14244
	},
	{
		name: "Noble",
		population: 47536
	},
	{
		name: "Ohio",
		population: 6128
	},
	{
		name: "Orange",
		population: 19840
	},
	{
		name: "Owen",
		population: 21575
	},
	{
		name: "Parke",
		population: 17339
	},
	{
		name: "Perry",
		population: 19338
	},
	{
		name: "Pike",
		population: 12845
	},
	{
		name: "Porter",
		population: 164343
	},
	{
		name: "Posey",
		population: 25910
	},
	{
		name: "Pulaski",
		population: 13402
	},
	{
		name: "Putnam",
		population: 36019
	},
	{
		name: "Randolph",
		population: 26171
	},
	{
		name: "Ripley",
		population: 28818
	},
	{
		name: "Rush",
		population: 17392
	},
	{
		name: "St. Joseph",
		population: 266931
	},
	{
		name: "Scott",
		population: 24181
	},
	{
		name: "Shelby",
		population: 44436
	},
	{
		name: "Spencer",
		population: 20952
	},
	{
		name: "Starke",
		population: 23363
	},
	{
		name: "Steuben",
		population: 34185
	},
	{
		name: "Sullivan",
		population: 21745
	},
	{
		name: "Switzerland",
		population: 10613
	},
	{
		name: "Tippecanoe",
		population: 172780
	},
	{
		name: "Tipton",
		population: 15936
	},
	{
		name: "Union",
		population: 7516
	},
	{
		name: "Vanderburgh",
		population: 179703
	},
	{
		name: "Vermillion",
		population: 16212
	},
	{
		name: "Vigo",
		population: 107818
	},
	{
		name: "Wabash",
		population: 32888
	},
	{
		name: "Warren",
		population: 8508
	},
	{
		name: "Warrick",
		population: 59689
	},
	{
		name: "Washington",
		population: 28262
	},
	{
		name: "Wayne",
		population: 68917
	},
	{
		name: "Wells",
		population: 27636
	},
	{
		name: "White",
		population: 24643
	},
	{
		name: "Whitley",
		population: 33292
	}
];

const countiesMarylandList = [
	{
		name: "Allegany",
		population: 74012
	},
	{
		name: "Anne_Arundel",
		population: 550488
	},
	{
		name: "Baltimore",
		population: 817455
	},
	{
		name: "Baltimore_City",
		population: 621342
	},
	{
		name: "Calvert",
		population: 89628
	},
	{
		name: "Caroline",
		population: 32718
	},
	{
		name: "Carroll",
		population: 167217
	},
	{
		name: "Cecil",
		population: 101696
	},
	{
		name: "Charles",
		population: 150592
	},
	{
		name: "Dorchester",
		population: 32551
	},
	{
		name: "Frederick",
		population: 239582
	},
	{
		name: "Garrett",
		population: 29854
	},
	{
		name: "Harford",
		population: 248622
	},
	{
		name: "Howard",
		population: 299430
	},
	{
		name: "Kent",
		population: 20191
	},
	{
		name: "Montgomery",
		population: 1004709
	},
	{
		name: "Prince_Georges",
		population: 881138
	},
	{
		name: "Queen_Annes",
		population: 48595
	},
	{
		name: "Somerset",
		population: 26253
	},
	{
		name: "St_Marys",
		population: 108987
	},
	{
		name: "Talbot",
		population: 38098
	},
	{
		name: "Washington",
		population: 149180
	},
	{
		name: "Wicomico",
		population: 100647
	},
	{
		name: "Worcester",
		population: 51578
	},
]

const countiesNewYorkList = [
	{
		name: "Albany",
		population: 304204
	},
	{
		name: "Allegany",
		population: 48946
	},
	{
		name: "Bronx",
		population: 1385108
	},
	{
		name: "Broome",
		population: 200600
	},
	{
		name: "Cattaraugus",
		population: 80317
	},
	{
		name: "Cayuga",
		population: 80026
	},
	{
		name: "Chautauqa",
		population: 134905
	},
	{
		name: "Chemung",
		population: 88830
	},
	{
		name: "Chenango",
		population: 50477
	},
	{
		name: "Clinton",
		population: 82128
	},
	{
		name: "Columbia",
		population: 63096
	},
	{
		name: "Cortland",
		population: 49336
	},
	{
		name: "Delaware",
		population: 47980
	},
	{
		name: "Dutchess",
		population: 297488
	},
	{
		name: "Erie",
		population: 919040
	},
	{
		name: "Essex",
		population: 39370
	},
	{
		name: "Franklin",
		population: 51599
	},
	{
		name: "Fulton",
		population: 55531
	},
	{
		name: "Genesee",
		population: 60079
	},
	{
		name: "Greene",
		population: 49221
	},
	{
		name: "Hamilton",
		population: 4836
	},
	{
		name: "Herkimer",
		population: 64519
	},
	{
		name: "Jefferson",
		population: 116229
	},
	{
		name: "Kings",
		population: 2504700
	},
	{
		name: "Lewis",
		population: 27087
	},
	{
		name: "Livingston",
		population: 65393
	},
	{
		name: "Madison",
		population: 73442
	},
	{
		name: "Monroe",
		population: 744344
	},
	{
		name: "Montgomery",
		population: 50219
	},
	{
		name: "Nassau",
		population: 1339532
	},
	{
		name: "New York",
		population: 1585873
	},
	{
		name: "Niagara",
		population: 216469
	},
	{
		name: "Oneida",
		population: 234878
	},
	{
		name: "Onondaga",
		population: 467026
	},
	{
		name: "Ontario",
		population: 107931
	},
	{
		name: "Orange",
		population: 372813
	},
	{
		name: "Orleans",
		population: 42883
	},
	{
		name: "Oswego",
		population: 122109
	},
	{
		name: "Otsego",
		population: 62259
	},
	{
		name: "Putnam",
		population: 99710
	},
	{
		name: "Queens",
		population: 2230722
	},
	{
		name: "Rensselaer",
		population: 159429
	},
	{
		name: "Richmond",
		population: 468730
	},
	{
		name: "Rockland",
		population: 311687
	},
	{
		name: "St. Lawrence",
		population: 111944
	},
	{
		name: "Saratoga",
		population: 219607
	},
	{
		name: "Schenectady",
		population: 154727
	},
	{
		name: "Schoharie",
		population: 32749
	},
	{
		name: "Schuyler",
		population: 18343
	},
	{
		name: "Seneca",
		population: 35251
	},
	{
		name: "Steuben",
		population: 98990
	},
	{
		name: "Suffolk",
		population: 1493350
	},
	{
		name: "Sullivan",
		population: 77547
	},
	{
		name: "Tioga",
		population: 51125
	},
	{
		name: "Tompkins",
		population: 101564
	},
	{
		name: "Ulster",
		population: 182493
	},
	{
		name: "Warren",
		population: 65707
	},
	{
		name: "Washington",
		population: 63216
	},
	{
		name: "Wayne",
		population: 93772
	},
	{
		name: "Westchester",
		population: 949113
	},
	{
		name: "Wyoming",
		population: 42155
	},
	{
		name: "Yates",
		population: 25348
	}
];

const countiesFloridaList = [
	{
		name: "Orange",
		population: 1000
	}
];

const countiesVermontList = [
	{
		name: "Addison County",
		population: 37035
	},
	{
		name: "Bennington County",
		population: 36317
	},
	{
		name: "Caledonia County",
		population: 30780
	},
	{
		name: "Chittenden County",
		population: 161382
	},
	{
		name: "Essex County",
		population: 6163
	},
	{
		name: "Franklin County",
		population: 48799
	},
	{
		name: "Grand Isle County",
		population: 6861
	},
	{
		name: "Lamoille County",
		population: 25235
	},
	{
		name: "Orange County",
		population: 28899
	},
	{
		name: "Orleans County",
		population: 27100
	},
	{
		name: "Rutland County",
		population: 59736
	},
	{
		name: "Washington County",
		population: 58612
	},
	{
		name: "Windham County",
		population: 43386
	},
	{
		name: "Windsor County",
		population: 55737
	},
];

const countiesWisconsinList = [
	{
		name: "Adams",
		population: 20875
	},
	{
		name: "Ashland",
		population: 16157
	},
	{
		name: "Barron",
		population: 45870
	},
	{
		name: "Bayfield",
		population: 15014
	},
	{
		name: "Brown",
		population: 248007
	},
	{
		name: "Buffalo",
		population: 13587
	},
	{
		name: "Burnett",
		population: 15457
	},
	{
		name: "Calumet",
		population: 48971
	},
	{
		name: "Chippewa",
		population: 62415
	},
	{
		name: "Clark",
		population: 34690
	},
	{
		name: "Columbia",
		population: 56833
	},
	{
		name: "Crawford",
		population: 16644
	},
	{
		name: "Dane",
		population: 488073
	},
	{
		name: "Dodge",
		population: 88759
	},
	{
		name: "Door",
		population: 27785
	},
	{
		name: "Douglas",
		population: 44159
	},
	{
		name: "Dunn",
		population: 43857
	},
	{
		name: "Eau Claire",
		population: 98736
	},
	{
		name: "Florence",
		population: 4423
	},
	{
		name: "Fond du Lac",
		population: 101633
	},
	{
		name: "Forest",
		population: 9304
	},
	{
		name: "Grant",
		population: 51208
	},
	{
		name: "Green",
		population: 36842
	},
	{
		name: "Green Lake",
		population: 19051
	},
	{
		name: "Iowa",
		population: 23687
	},
	{
		name: "Iron",
		population: 5916
	},
	{
		name: "Jackson",
		population: 20449
	},
	{
		name: "Jefferson",
		population: 83686
	},
	{
		name: "Juneau",
		population: 26664
	},
	{
		name: "Kenosha",
		population: 166426
	},
	{
		name: "Kewaunee",
		population: 20574
	},
	{
		name: "La Crosse",
		population: 114638
	},
	{
		name: "Lafayette",
		population: 16836
	},
	{
		name: "Langlade",
		population: 19977
	},
	{
		name: "Lincoln",
		population: 28743
	},
	{
		name: "Manitowoc",
		population: 81422
	},
	{
		name: "Marathon",
		population: 134063
	},
	{
		name: "Marinette",
		population: 41749
	},
	{
		name: "Marquette",
		population: 15404
	},
	{
		name: "Menominee",
		population: 4232
	},
	{
		name: "Milwaukee",
		population: 947735
	},
	{
		name: "Monroe",
		population: 44673
	},
	{
		name: "Oconto",
		population: 37670
	},
	{
		name: "Oneida",
		population: 35998
	},
	{
		name: "Outagamie",
		population: 178695
	},
	{
		name: "Ozaukee",
		population: 86395
	},
	{
		name: "Pepin",
		population: 7469
	},
	{
		name: "Pierce",
		population: 41019
	},
	{
		name: "Polk",
		population: 44205
	},
	{
		name: "Portage",
		population: 70019
	},
	{
		name: "Price",
		population: 14159
	},
	{
		name: "Racine",
		population: 195408
	},
	{
		name: "Richland",
		population: 18021
	},
	{
		name: "Rock",
		population: 160331
	},
	{
		name: "Rusk",
		population: 14755
	},
	{
		name: "Sauk",
		population: 61976
	},
	{
		name: "Sawyer",
		population: 16557
	},
	{
		name: "Shawano",
		population: 41949
	},
	{
		name: "Sheboygan",
		population: 115507
	},
	{
		name: "St. Croix",
		population: 84345
	},
	{
		name: "Taylor",
		population: 20689
	},
	{
		name: "Trempealeau",
		population: 28816
	},
	{
		name: "Vernon",
		population: 29773
	},
	{
		name: "Vilas",
		population: 21430
	},
	{
		name: "Walworth",
		population: 102228
	},
	{
		name: "Washburn",
		population: 15911
	},
	{
		name: "Washington",
		population: 131877
	},
	{
		name: "Waukesha",
		population: 389891
	},
	{
		name: "Waupaca",
		population: 52410
	},
	{
		name: "Waushara",
		population: 24496
	},
	{
		name: "Winnebago",
		population: 166994
	},
	{
		name: "Wood",
		population: 74749
	}
];

const statesList = [
	{
		name: "Alabama",
		abbreviation: "AL",
		population: 4903185
	},
	{
		name: "Alaska",
		abbreviation: "AK",
		population: 731545
	},
	{
		name: "American Samoa",
		abbreviation: "AS",
		population: 55641
	},
	{
		name: "Arizona",
		abbreviation: "AZ",
		population: 7278717
	},
	{
		name: "Arkansas",
		abbreviation: "AR",
		population: 3017825
	},
	{
		name: "California",
		abbreviation: "CA",
		population: 39512223
	},
	{
		name: "Colorado",
		abbreviation: "CO",
		population: 5758736
	},
	{
		name: "Connecticut",
		abbreviation: "CT",
		population: 3565287
	},
	{
		name: "Delaware",
		abbreviation: "DE",
		population: 973764
	},
	{
		name: "District Of Columbia",
		abbreviation: "DC",
		population: 705749
	},
	{
		name: "Florida",
		abbreviation: "FL",
		population: 21477737
	},
	{
		name: "Georgia",
		abbreviation: "GA",
		population: 10617423
	},
	{
		name: "Guam",
		abbreviation: "GU",
		population: 165718
	},
	{
		name: "Hawaii",
		abbreviation: "HI",
		population: 1415872
	},
	{
		name: "Idaho",
		abbreviation: "ID",
		population: 1787065
	},
	{
		name: "Illinois",
		abbreviation: "IL",
		population: 12671821
	},
	{
		name: "Indiana",
		abbreviation: "IN",
		population: 6732219
	},
	{
		name: "Iowa",
		abbreviation: "IA",
		population: 3155070
	},
	{
		name: "Kansas",
		abbreviation: "KS",
		population: 2913314
	},
	{
		name: "Kentucky",
		abbreviation: "KY",
		population: 4467673
	},
	{
		name: "Louisiana",
		abbreviation: "LA",
		population: 4648794
	},
	{
		name: "Maine",
		abbreviation: "ME",
		population: 1344212
	},
	{
		name: "Maryland",
		abbreviation: "MD",
		population: 6045680
	},
	{
		name: "Massachusetts",
		abbreviation: "MA",
		population: 6949503
	},
	{
		name: "Michigan",
		abbreviation: "MI",
		population: 9986857
	},
	{
		name: "Minnesota",
		abbreviation: "MN",
		population: 5639632
	},
	{
		name: "Mississippi",
		abbreviation: "MS",
		population: 2976149
	},
	{
		name: "Missouri",
		abbreviation: "MO",
		population: 6137428
	},
	{
		name: "Montana",
		abbreviation: "MT",
		population: 1068778
	},
	{
		name: "Nebraska",
		abbreviation: "NE",
		population: 1934408
	},
	{
		name: "Nevada",
		abbreviation: "NV",
		population: 3080156
	},
	{
		name: "New Hampshire",
		abbreviation: "NH",
		population: 1359711
	},
	{
		name: "New Jersey",
		abbreviation: "NJ",
		population: 8882190
	},
	{
		name: "New Mexico",
		abbreviation: "NM",
		population: 2096829
	},
	{
		name: "New York",
		abbreviation: "NY",
		population: 19453561
	},
	{
		name: "North Carolina",
		abbreviation: "NC",
		population: 10488084
	},
	{
		name: "North Dakota",
		abbreviation: "ND",
		population: 762062
	},
	{
		name: "Northern Mariana Islands",
		abbreviation: "MP",
		population: 55194
	},
	{
		name: "Ohio",
		abbreviation: "OH",
		population: 11689100
	},
	{
		name: "Oklahoma",
		abbreviation: "OK",
		population: 3956971
	},
	{
		name: "Oregon",
		abbreviation: "OR",
		population: 4217737
	},
	{
		name: "Pennsylvania",
		abbreviation: "PA",
		population: 12801989
	},
	{
		name: "Puerto Rico",
		abbreviation: "PR",
		population: 3193694
	},
	{
		name: "Rhode Island",
		abbreviation: "RI",
		population: 1059361
	},
	{
		name: "South Carolina",
		abbreviation: "SC",
		population: 5148714
	},
	{
		name: "South Dakota",
		abbreviation: "SD",
		population: 884659
	},
	{
		name: "Tennessee",
		abbreviation: "TN",
		population: 6833174
	},
	{
		name: "Texas",
		abbreviation: "TX",
		population: 28995881
	},
	{
		name: "Utah",
		abbreviation: "UT",
		population: 3205958
	},
	{
		name: "Vermont",
		abbreviation: "VT",
		population: 623989
	},
	{
		name: "Virgin Islands",
		abbreviation: "VI",
		population: 104914
	},
	{
		name: "Virginia",
		abbreviation: "VA",
		population: 8535519
	},
	{
		name: "Washington",
		abbreviation: "WA",
		population: 7614893
	},
	{
		name: "West Virginia",
		abbreviation: "WV",
		population: 1792147
	},
	{
		name: "Wisconsin",
		abbreviation: "WI",
		population: 5822434
	},
	{
		name: "Wyoming",
		abbreviation: "WY",
		population: 578759
	}
];

export const getColorsList = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST,
		payload: colorsList
	});
}

export const getColorsListCitiesCHI = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST_CITIES_CHI,
		payload: colorsList
	})
}

export const getColorsListCountiesCA = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST_COUNTIES_CA,
		payload: colorsList
	});
}

export const getColorsListCountiesCT = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST_COUNTIES_CT,
		payload: colorsList
	});
}

export const getColorsListCountiesMD = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST_COUNTIES_MD,
		payload: colorsList
	});
}

export const getColorsListCountiesNY = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST_COUNTIES_NY,
		payload: colorsList
	});
}

export const getColorsListCountiesFL = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST_COUNTIES_FL,
		payload: colorsList
	});
}

export const getColorsListCountiesCompare = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST_COUNTIES_COMPARE,
		payload: colorsList
	});
}

export const getColorsListStates = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST_STATES,
		payload: colorsList
	});
}

export const getColorsListCountries = () => (dispatch) => {
	dispatch({
		type: GET_COLORS_LIST_COUNTRIES,
		payload: colorsList
	});
}




export const getCountiesCAList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_CA_LIST,
		payload: countiesCaliforniaList
	});
}

export const getCountiesCAListCA = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_CA_LIST_CA,
		payload: countiesCaliforniaList
	});
}

// export const getCountiesCAListCompare = () => (dispatch) => {
// 	dispatch({
// 		type: GET_COUNTIES_CA_LIST_COMPARE,
// 		payload: countiesCaliforniaList
// 	});
// }





export const getCountiesCOList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_CO_LIST,
		payload: countiesColoradoList
	});
}




export const getCountiesCTList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_CT_LIST,
		payload: countiesConnecticutList
	})
}

export const getCountiesCTListCT = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_CT_LIST_CT,
		payload: countiesConnecticutList
	});
}

// export const getCountiesCTListCompare = () => (dispatch) => {
// 	dispatch({
// 		type: GET_COUNTIES_CT_LIST_COMPARE,
// 		payload: countiesConnecticutList
// 	});
// }




export const getCountiesGAList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_GA_LIST,
		payload: countiesGeorgiaList
	})
}



export const getCountiesINList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_IN_LIST,
		payload: countiesIndianaList
	})
}



export const getCountiesMDList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_MD_LIST,
		payload: countiesMarylandList
	});
}

export const getCountiesMDListMD = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_MD_LIST_MD,
		payload: countiesMarylandList
	});
}

// export const getCountiesNYListCompare = () => (dispatch) => {
// 	dispatch({
// 		type: GET_COUNTIES_MD_LIST_COMPARE,
// 		payload: countiesMarylandList
// 	});
// }




export const getCountiesNYList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_NY_LIST,
		payload: countiesNewYorkList
	});
}

export const getCountiesNYListNY = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_NY_LIST_NY,
		payload: countiesNewYorkList
	});
}

// export const getCountiesNYListCompare = () => (dispatch) => {
// 	dispatch({
// 		type: GET_COUNTIES_NY_LIST_COMPARE,
// 		payload: countiesNewYorkList
// 	});
// }




export const getCountiesFLList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_FL_LIST,
		payload: countiesFloridaList
	});
}

export const getCountiesFLListFL = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_FL_LIST_FL,
		payload: countiesFloridaList
	});
}

// export const getCountiesFLListCompare = () => (dispatch) => {
// 	dispatch({
// 		type: GET_COUNTIES_FL_LIST_COMPARE,
// 		payload: countiesFloridaList
// 	});
// }



export const getCountiesVTList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_VT_LIST,
		payload: countiesVermontList
	});
}



export const getCountiesWIList = () => (dispatch) => {
	dispatch({
		type: GET_COUNTIES_WI_LIST,
		payload: countiesWisconsinList
	})
}




export const getStatesList = () => (dispatch) => {
	dispatch({
		type: GET_STATES_LIST,
		payload: statesList
	});
}

export const getStatesListStates = () => (dispatch) => {
	dispatch({
		type: GET_STATES_LIST_STATES,
		payload: statesList
	});
}




export const clearImport = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMPORT
	});
}

export const clearImportCountiesCA = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMPORT_COUNTIES_CA
	});
}

export const clearImportCountiesCT = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMPORT_COUNTIES_CT
	});
}

export const clearImportCountiesMD = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMPORT_COUNTIES_MD
	});
}

export const clearImportCountiesNY = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMPORT_COUNTIES_NY
	});
}

export const clearImportCountiesFL = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMPORT_COUNTIES_FL
	});
}

export const clearImportCountiesCompare = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMPORT_COUNTIES_COMPARE
	});
}

export const clearImportStates = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMPORT_STATES
	});
}

export const clearImportCountries = () => (dispatch) => {
	dispatch({
		type: CLEAR_IMPORT_COUNTRIES
	});
}











