import axios from 'axios';
// import { createMessage, returnErrors } from "./messages";

import { 
	GET_WORLD_DATA,
	LOADING_WORLD_DATA,
	CLEAR_WORLD_DATA,

	GET_WORLD_COUNTRIES,
	LOADING_WORLD_COUNTRIES,

	GET_COUNTRY_DATA,
	LOADING_COUNTRY_DATA
} from './types';

export const getWorldData = () => (dispatch, getState) => {
	axios 
		.get('https://api.apify.com/v2/datasets/jaycEQiGMlb7aOmtI/items?format=json&clean=1')
		.then(res => {
			dispatch({
				type: GET_WORLD_DATA,
				payload: res.data
			});
			dispatch({
				type: LOADING_WORLD_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const getWorldCountries = () => (dispatch) => {
	const config = {
		headers: {
			"x-rapidapi-host": "covid-193.p.rapidapi.com",
      "x-rapidapi-key": "3518e47229msha04de349b9c2745p1268bejsn942bed38d199"
		}
	}
	axios
    .get('https://covid-193.p.rapidapi.com/countries', config)
    .then(res => {
      dispatch({
				type: GET_WORLD_COUNTRIES,
				payload: res.data.response
			});
			dispatch({
				type: LOADING_WORLD_COUNTRIES,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const getCountryData = (country) => (dispatch) => {
	const config = {
		headers: {
			"x-rapidapi-host": "covid-193.p.rapidapi.com",
      "x-rapidapi-key": "3518e47229msha04de349b9c2745p1268bejsn942bed38d199"
		}
	}
	axios
    .get(`https://covid-193.p.rapidapi.com/history?country=${country}`, config)
    .then(res => {
      dispatch({
				type: GET_COUNTRY_DATA,
				payload: res.data.response
			});
			dispatch({
				type: LOADING_COUNTRY_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearWorldData = () => (dispatch) => {
	dispatch({
		type: CLEAR_WORLD_DATA
	});
}

export const finishLoadingCountryData = () => (dispatch) => {
	dispatch({
		type: GET_COUNTRY_DATA,
		payload: null
	});
	dispatch({
		type: LOADING_COUNTRY_DATA,
		payload: true
	});
}