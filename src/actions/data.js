import axios from 'axios';
// import { createMessage, returnErrors } from "./messages";

import { 
	GET_DATA_DATE_AND_TRANSMISSION,
	LOADING_DATA_DATE_AND_TRANSMISSION,

	GET_DATA_AGE_GROUP_AND_GENDER,
	LOADING_DATA_AGE_GROUP_AND_GENDER,

	GET_DATA_HOSPITALIZATIONS,
	LOADING_DATA_HOSPITALIZATIONS,

	GET_DATA_TESTING,
	LOADING_DATA_TESTING,

	GET_DATA_RACE_AND_ETHNICITY,
	LOADING_DATA_RACE_AND_ETHNICITY, 

	CLEAR_CHART_DATA,

	GET_NYC_CONFIRMED_CASE_DATA,
	LOADING_NYC_CONFIRMED_CASE_DATA,

	GET_SF_CONFIRMED_CASE_DATA,
	LOADING_SF_CONFIRMED_CASE_DATA,

	CLEAR_COMPARISON_DATA,

	GET_ZIP_CODE_DATA,
	LOADING_ZIP_CODE_DATA
} from './types';

export const getDataDateAndTransmission = () => (dispatch, getState) => {
	axios 
		.get('https://data.sfgov.org/resource/tvq9-ec9w.json')
		.then(res => {
			// console.log('Res: ', res);
			dispatch({
				type: GET_DATA_DATE_AND_TRANSMISSION,
				payload: res.data
			});
			dispatch({
				type: LOADING_DATA_DATE_AND_TRANSMISSION,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const getDataAgeGroupAndGender = () => (dispatch, getState) => {
	axios
		.get('https://data.sfgov.org/resource/sunc-2t3k.json')
		.then(res => {
			// console.log('Res: ', res.data);
			dispatch({
				type: GET_DATA_AGE_GROUP_AND_GENDER,
				payload: res.data
			});
			dispatch({
				type: LOADING_DATA_AGE_GROUP_AND_GENDER,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const getDataHospitalizations = () => (dispatch, getState) => {
	axios
		.get('https://data.sfgov.org/resource/nxjg-bhem.json')
		.then(res => {
			// console.log('Res: ', res.data);
			dispatch({
				type: GET_DATA_HOSPITALIZATIONS,
				payload: res.data
			});
			dispatch({
				type: LOADING_DATA_HOSPITALIZATIONS,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const getDataTesting = () => (dispatch, getState) => {
	axios
		.get('https://data.sfgov.org/resource/nfpa-mg4g.json')
		.then(res => {
			// console.log('Res: ', res.data);
			dispatch({
				type: GET_DATA_TESTING,
				payload: res.data
			});
			dispatch({
				type: LOADING_DATA_TESTING,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const getDataRaceAndEthnicity = () => (dispatch, getState) => {
	axios
		.get('https://data.sfgov.org/resource/vqqm-nsqg.json')
		.then(res => {
			// console.log('Res: ', res.data);
			dispatch({
				type: GET_DATA_RACE_AND_ETHNICITY,
				payload: res.data
			});
			dispatch({
				type: LOADING_DATA_RACE_AND_ETHNICITY,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearAllChartData = () => (dispatch, getState) => {
	dispatch({
		type: CLEAR_CHART_DATA
	})
}

export const getComparisonNYCConfirmedCaseData = () => (dispatch, getState) => {
	axios
		.get(`https://health.data.ny.gov/resource/xdss-u53e.json?$where=test_date>='2020-03-05'&county='Queens' OR county='Kings' OR county='Bronx' OR county='New York' OR county='Richmond'`)
		.then(res => {
			// console.log('NYC Results: ', res.data);
			dispatch({
				type: GET_NYC_CONFIRMED_CASE_DATA,
				payload: res.data
			});
			dispatch({
				type: LOADING_NYC_CONFIRMED_CASE_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const getComparisonSFConfirmedCaseData = () => (dispatch, getState) => {
	axios 
		.get('https://data.sfgov.org/resource/tvq9-ec9w.json?$where=date%3E=%272020-03-05%27&$order=date DESC')
		.then(res => {
			// console.log('SF Results: ', res.data);
			dispatch({
				type: GET_SF_CONFIRMED_CASE_DATA,
				payload: res.data
			});
			dispatch({
				type: LOADING_SF_CONFIRMED_CASE_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}

export const clearComparisonData = () => (dispatch, getState) => {
	dispatch({
		type: CLEAR_COMPARISON_DATA
	})
}

export const getSFZipCodeData = () => (dispatch) => {
	axios
		.get('https://data.sfgov.org/resource/favi-qct6.json')
		.then(res => {
			dispatch({
				type: GET_ZIP_CODE_DATA,
				payload: res.data
			});
			dispatch({
				type: LOADING_ZIP_CODE_DATA,
				payload: false
			});
		})
		.catch(err => console.log(err.response.data, err.response.status));
}







